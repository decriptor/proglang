﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MJC.IRBackend.LLVMIR;
using MJC.IRBackend.Parse;

namespace MJC.IRBackend
{
	class IRBackend
	{
		IRLexer _lexer = new IRLexer();
		IRParser _parser = new IRParser();

		private IArchBackend _archBackend;

		private LLVMConvert _llvmConvert;
		private LLVMBackend _llvmBackend;

		public IRBackend()
		{
			Platform.InitPlatform();

			if (CmdLine.UseLLVM)
			{
				_llvmConvert = new LLVMConvert();
				_llvmBackend = new LLVMBackend();
			}
			else
			{
				_archBackend = Arch.Backends[Platform.Info.ArchitectureType]();
			}
		}

		public void Process(IRModule module, string fileName)
		{
			if (CmdLine.Interpret)
				return;

			if (CmdLine.UseLLVM)
			{
				var llvmModule = _llvmConvert.Convert(module);
				_llvmBackend.Process(llvmModule, fileName);
			}
			else
			{
				IAsmUnit asmUnit = _archBackend.Translate(module);
				_archBackend.Assemble(asmUnit);

				asmUnit.OutputFile(fileName + ".s");
			}
		}

		public IRModule ParseModule(string irSource, string name)
		{
			List<IRToken> tokens = _lexer.Lex(irSource);
			return _parser.Parse(name, tokens);
		}

		public List<string> Execute(List<string> irFiles, List<IRModule> modules = null)
		{
			List<string> oFiles = new List<string>();

			if (modules == null)
			{
				modules = new List<IRModule>();

				foreach (string irFile in irFiles)
				{
					string source = File.ReadAllText(irFile);
					string fileName = Path.GetFileNameWithoutExtension(irFile);
				}
			}

			for (var i = 0; i < irFiles.Count; i++)
			{
				string irFile = irFiles[i];
				string fileName = Path.GetFileNameWithoutExtension(irFile);
				IRModule module = modules[i];

				Process(module, fileName);

				oFiles.Add(fileName + ".o");
			}

			/*if (CmdLine.Interpret)
			{
				IRInterpreter interpreter = new IRInterpreter();
				int res = interpreter.Interpret(modules);
				Console.WriteLine($"Interpreted code returned: {res}");
			}*/

			return oFiles;
		}

	}
}
