﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.IRBackend.General;

namespace MJC.IRBackend
{
	public class IROperand
	{
		public IROperand(string iden, IRType type, bool global = false, bool charArrLit = false)
		{
			Iden = iden;
			Type = type;
			Global = global;
		}

		public IROperand(long value, IRType type)
		{
			IntLiteral = value;
			Type = type;
			Constant = true;
		}

		public string Iden;
		public IRType Type;
		public bool Global;
		public bool Constant;

		public long IntLiteral;

		public override string ToString()
		{
			if (Constant)
			{
				if (!Type.Address && Type.PtrCount == 0)
				{
					if (Type.BaseType == IRBaseType.F32 || Type.BaseType == IRBaseType.F64)
					{
						return $"0x{IntLiteral:X} : {Type}";
					}
				}

				return $"{IntLiteral} : {Type}";
			}

			return $"{(Global ? "@" : "%")}{Iden} : {Type}";
		}

		public string ToStringNoType()
		{
			if (Iden == null)
			{
				if (!Type.Address && Type.PtrCount == 0)
				{
					if (Type.BaseType == IRBaseType.F32 || Type.BaseType == IRBaseType.F64)
					{
						return $"0x{IntLiteral:X}";
					}
					else
					{
						return $"{IntLiteral}";
					}
				}

				return "";
			}

			return $"%{Iden}";
		}
	}

	public class IRGlobal
	{
		public string Identifier;
		public IRType Type;
		public IRAttributes Attribs;

		public bool HasValue;
		public string StringValue;
		public long NumValue;

		public IRGlobal(string identifier, IRType type, IRAttributes attribs)
		{
			Identifier = identifier;
			Type = type;
			Attribs = attribs;
		}
	}
}
