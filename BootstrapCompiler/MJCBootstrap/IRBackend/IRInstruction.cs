﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MJC.IRBackend
{
	public enum IRInstructionType
	{
		Alloca,

		Store,
		Load,
		Gep,

		Neg,
		Inc,
		Dec,

		Add,
		Sub,
		Mul,
		Div,
		Rem,
		Shift,
		Or,
		Xor,
		And,

		Trunc,
		Ext,
		Fptoi,
		Itofp,
		Ptrtoi,
		Itoptr,
		Bitcast,

		ExtractValue,
		InsertValue,

		Call,
		Cmp,

		Return,
		Branch,
		CondBranch,
	}

	public class IRInstruction
	{
		public IRInstructionType InstructionType;
	}

	public class IRAllocaInstruction : IRInstruction
	{
		public IRAllocaInstruction(IROperand retOperand, IRType type)
		{
			InstructionType = IRInstructionType.Alloca;
			RetOperand = retOperand;
			Type = type;
		}

		public IROperand RetOperand;
		public IRType Type;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = alloca {Type}";
		}
	}

	public class IRStoreInstruction : IRInstruction
	{
		public IRStoreInstruction(IROperand src, IROperand dst)
		{
			InstructionType = IRInstructionType.Store;
			Src = src;
			Dst = dst;
		}

		public IROperand Src;
		public IROperand Dst;

		public override string ToString()
		{
			return $"store {Src} , {Dst}";
		}
	}

	public class IRLoadInstruction : IRInstruction
	{
		public IRLoadInstruction(IROperand retOperand, IROperand src)
		{
			InstructionType = IRInstructionType.Load;
			RetOperand = retOperand;
			Src = src;
		}

		public IROperand RetOperand;
		public IROperand Src;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = load {Src}";
		}
	}

	public class IRGepInstruction : IRInstruction
	{
		public IRGepInstruction(IROperand retOperand, IROperand src, params int[] indices)
		{
			InstructionType = IRInstructionType.Gep;
			RetOperand = retOperand;
			Src = src;
			Indices = indices.ToList();
		}

		public IROperand RetOperand;
		public IROperand Src;
		public List<int> Indices;

		public override string ToString()
		{
			string indexStr = null;
			foreach (int i in Indices)
			{
				if (indexStr == null)
					indexStr = $"{i}";
				else
					indexStr += $" , {i}";
			}

			return $"%{RetOperand.Iden} = gep : {RetOperand.Type} , {Src} , {indexStr}";
		}
	}

	public class IRNegInstruction : IRInstruction
	{
		public IRNegInstruction(IROperand op, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Neg;
			Operand = op;
			RetOperand = retOperand;
		}

		public IROperand Operand;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = neg {Operand}";
		}
	}

	public class IRComplInstruction : IRInstruction
	{
		public IRComplInstruction(IROperand op, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Neg;
			Operand = op;
			RetOperand = retOperand;
		}

		public IROperand Operand;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = compl {Operand}";
		}
	}

	public class IRIncInstruction : IRInstruction
	{
		public IRIncInstruction(IROperand op, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Inc;
			Operand = op;
			Type = type;
			RetOperand = retOperand;
		}

		public IROperand Operand;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = inc {Operand}";
		}
	}

	public class IRDecInstruction : IRInstruction
	{
		public IRDecInstruction(IROperand op, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Dec;
			Operand = op;
			Type = type;
			RetOperand = retOperand;
		}

		public IROperand Operand;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = sub {Operand}";
		}
	}

	public class IRAddInstruction : IRInstruction
	{
		public IRAddInstruction(IROperand op0, IROperand op1, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Add;
			Operand0 = op0;
			Operand1 = op1;
			Type = type;
			RetOperand = retOperand;
		}

		public IROperand Operand0;
		public IROperand Operand1;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = add %{Operand0.Iden} , %{Operand1.Iden} : {Type}";
		}
	}

	public class IRSubInstruction : IRInstruction
	{
		public IRSubInstruction(IROperand op0, IROperand op1, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Sub;
			Operand0 = op0;
			Operand1 = op1;
			Type = type;
			RetOperand = retOperand;
		}

		public IROperand Operand0;
		public IROperand Operand1;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = sub %{Operand0.Iden} , %{Operand1.Iden} : {Type}";
		}
	}

	public class IRMulInstruction : IRInstruction
	{
		public IRMulInstruction(IROperand op0, IROperand op1, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Mul;
			Operand0 = op0;
			Operand1 = op1;
			Type = type;
			RetOperand = retOperand;
		}

		public IROperand Operand0;
		public IROperand Operand1;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = mul %{Operand0.Iden} , %{Operand1.Iden} : {Type}";
		}
	}

	public class IRDivInstruction : IRInstruction
	{
		public IRDivInstruction(IROperand op0, IROperand op1, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Div;
			Operand0 = op0;
			Operand1 = op1;
			Type = type;
			RetOperand = retOperand;
		}

		public IROperand Operand0;
		public IROperand Operand1;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = div %{Operand0.Iden} , %{Operand1.Iden} : {Type}";
		}
	}

	public class IRRemInstruction : IRInstruction
	{
		public IRRemInstruction(IROperand op0, IROperand op1, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Rem;
			Operand0 = op0;
			Operand1 = op1;
			Type = type;
			RetOperand = retOperand;
		}

		public IROperand Operand0;
		public IROperand Operand1;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = rem %{Operand0.Iden} , %{Operand1.Iden} : {Type}";
		}
	}

	public enum IRShiftDir : byte
	{
		Left,
		LogicRight,
		ArithRight,
	}

	public class IRShiftInstruction : IRInstruction
	{
		public IRShiftInstruction(IRShiftDir dir, IROperand op0, IROperand op1, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Shift;
			Dir = dir;
			Operand0 = op0;
			Operand1 = op1;
			Type = type;
			RetOperand = retOperand;
		}

		public IRShiftDir Dir;
		public IROperand Operand0;
		public IROperand Operand1;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			string shiftDir = "";
			switch (Dir)
			{
			case IRShiftDir.Left:		shiftDir = "l";		break;
			case IRShiftDir.LogicRight:	shiftDir = "lr";	break;
			case IRShiftDir.ArithRight:	shiftDir = "ar";	break;
			}

			return $"%{RetOperand.Iden} = shift {shiftDir} %{Operand0.Iden} , %{Operand1.Iden} : {Type}";
		}
	}

	public class IROrInstruction : IRInstruction
	{
		public IROrInstruction(IROperand op0, IROperand op1, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Or;
			Operand0 = op0;
			Operand1 = op1;
			Type = type;
			RetOperand = retOperand;
		}

		public IROperand Operand0;
		public IROperand Operand1;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = or %{Operand0.Iden} , %{Operand1.Iden} : {Type}";
		}
	}

	public class IRXorInstruction : IRInstruction
	{
		public IRXorInstruction(IROperand op0, IROperand op1, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Xor;
			Operand0 = op0;
			Operand1 = op1;
			Type = type;
			RetOperand = retOperand;
		}

		public IROperand Operand0;
		public IROperand Operand1;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = xor %{Operand0.Iden} , %{Operand1.Iden} : {Type}";
		}
	}

	public class IRAndInstruction : IRInstruction
	{
		public IRAndInstruction(IROperand op0, IROperand op1, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.And;
			Operand0 = op0;
			Operand1 = op1;
			Type = type;
			RetOperand = retOperand;
		}

		public IROperand Operand0;
		public IROperand Operand1;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = and %{Operand0.Iden} , %{Operand1.Iden} : {Type}";
		}
	}

	public class IRTruncInstruction : IRInstruction
	{
		public IRTruncInstruction(IROperand op, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Trunc;
			Operand = op;
			Type = type;
			RetOperand = retOperand;
		}

		public IROperand Operand;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = trunc {Operand} to {Type}";
		}
	}

	public enum IRExtType : byte
	{
		Default,
		Zero,
		Signed
	}

	public class IRExtInstruction : IRInstruction
	{
		public IRExtInstruction(IRExtType ext, IROperand op, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Ext;
			Ext = ext;
			Operand = op;
			Type = type;
			RetOperand = retOperand;
		}

		public IRExtType Ext;
		public IROperand Operand;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			string extTypeStr = "";
			switch (Ext)
			{
			case IRExtType.Zero:	extTypeStr = " z"; break;
			case IRExtType.Signed:	extTypeStr = " s"; break;
			}

			return $"%{RetOperand.Iden} = shift{extTypeStr} {Operand} to {Type}";
		}
	}

	public class IRFptoiInstruction : IRInstruction
	{
		public IRFptoiInstruction(IROperand op, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Fptoi;
			Operand = op;
			Type = type;
			RetOperand = retOperand;
		}
		
		public IROperand Operand;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = fptoi {Operand} to {Type}";
		}
	}

	public class IRItofpInstruction : IRInstruction
	{
		public IRItofpInstruction(IROperand op, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Itofp;
			Operand = op;
			Type = type;
			RetOperand = retOperand;
		}

		public IROperand Operand;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = itofp {Operand} to {Type}";
		}
	}

	public class IRPtrtoiInstruction : IRInstruction
	{
		public IRPtrtoiInstruction(IROperand op, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Ptrtoi;
			Operand = op;
			Type = type;
			RetOperand = retOperand;
		}

		public IROperand Operand;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = ptrtoi {Operand} to {Type}";
		}
	}

	public class IRItoptrInstruction : IRInstruction
	{
		public IRItoptrInstruction(IROperand op, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Itoptr;
			Operand = op;
			Type = type;
			RetOperand = retOperand;
		}

		public IROperand Operand;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = itoptr {Operand} to {Type}";
		}
	}

	public class IRBitcastInstruction : IRInstruction
	{
		public IRBitcastInstruction(IROperand op, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Bitcast;
			Operand = op;
			Type = type;
			RetOperand = retOperand;
		}

		public IROperand Operand;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = bitcast {Operand} to {Type}";
		}
	}

	public class IRExtractValueInstruction : IRInstruction
	{
		public IRExtractValueInstruction(IROperand retOperand, IROperand aggregate, params int[] indices)
		{
			InstructionType = IRInstructionType.ExtractValue;
			RetOperand = retOperand;
			Aggregate = aggregate;
			Indices = indices.ToList();
		}

		public IROperand RetOperand;
		public IROperand Aggregate;
		public List<int> Indices;

		public override string ToString()
		{
			string indexStr = null;
			foreach (int i in Indices)
			{
				if (indexStr == null)
					indexStr = $"{i}";
				else
					indexStr += $" , {i}";
			}

			return $"%{RetOperand.Iden} = extract_value : {RetOperand.Type} , {Aggregate} , {indexStr}";
		}
	}

	public class IRInsertValueInstruction : IRInstruction
	{
		public IRInsertValueInstruction(IROperand aggregate, IROperand src, params int[] indices)
		{
			InstructionType = IRInstructionType.ExtractValue;
			Aggregate = aggregate;
			Src = src;
			Indices = indices.ToList();
		}

		public IROperand Aggregate;
		public IROperand Src;
		public List<int> Indices;

		public override string ToString()
		{
			string indexStr = null;
			foreach (int i in Indices)
			{
				if (indexStr == null)
					indexStr = $"{i}";
				else
					indexStr += $" , {i}";
			}

			return $"insert_value {Aggregate} , {Src} , {indexStr}";
		}
	}

	public class IRCallInstruction : IRInstruction
	{
		public IRCallInstruction(string identifier, List<IROperand> operands, IRType retType, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Call;
			Identifier = identifier;
			Operands = operands;
			RetType = retType;
			RetOperand = retOperand;
		}

		public string Identifier;
		public List<IROperand> Operands;
		public IRType RetType;
		public IROperand RetOperand;

		public override string ToString()
		{
			string opsStr = null;
			if (Operands != null)
			{
				foreach (IROperand operand in Operands)
				{
					if (opsStr == null)
						opsStr = $"{operand}";
					else
						opsStr += $" , {operand}";
				}
			}

			if (RetOperand != null)
				return $"%{RetOperand.Iden} = call @{Identifier} ( {opsStr} ) : {RetType}";
			return $"call @{Identifier} ( {opsStr} )";
		}
	}

	public enum IRCmpType
	{
		Eq,
		Ne,
		Gt,
		Ge,
		Lt,
		Le,
		UEq,
		UNe,
		UGt,
		UGe,
		ULt,
		ULe,
		UNo
	}

	public class IRCmpInstruction : IRInstruction
	{
		public IRCmpInstruction(IRCmpType cmp, IROperand op0, IROperand op1, IRType type, IROperand retOperand)
		{
			InstructionType = IRInstructionType.Cmp;
			Cmp = cmp;
			Operand0 = op0;
			Operand1 = op1;
			Type = type;
			RetOperand = retOperand;
		}

		public IRCmpType Cmp;
		public IROperand Operand0;
		public IROperand Operand1;
		public IRType Type;
		public IROperand RetOperand;

		public override string ToString()
		{
			string cmpTypeStr = Cmp.ToString().ToLower();
			
			return $"%{RetOperand.Iden} = cmp {cmpTypeStr} {Operand0} , {Operand1}";
		}
	}

	public class IRReturnInstruction : IRInstruction
	{
		public IRReturnInstruction(IROperand operand = null)
		{
			InstructionType = IRInstructionType.Return;
			Operand = operand;
		}

		public IROperand Operand;

		public override string ToString()
		{
			if (Operand != null)
				return $"ret {Operand}";
			return "ret";
		}
	}

	public class IRBranchInstruction : IRInstruction
	{
		public IRBranchInstruction(string label)
		{
			InstructionType = IRInstructionType.Branch;
			Label = label;
		}

		public string Label;

		public override string ToString()
		{
			return $"br {Label}";
		}
	}

	public class IRCondBranchInstruction : IRInstruction
	{
		public IRCondBranchInstruction(IROperand condition, string trueLabel, string falseLabel)
		{
			InstructionType = IRInstructionType.CondBranch;
			Condition = condition;
			TrueLabel = trueLabel;
			FalseLabel = falseLabel;
		}

		public IROperand Condition;
		public string TrueLabel;
		public string FalseLabel;

		public override string ToString()
		{
			return $"cond_br {Condition} , {TrueLabel} , {FalseLabel}";
		}
	}
}
