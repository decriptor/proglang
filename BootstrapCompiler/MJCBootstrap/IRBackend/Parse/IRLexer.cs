﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.IRBackend.Parse
{
	class IRLexer
	{

		private Dictionary<string, IRTokenType> _instructions = new Dictionary<string, IRTokenType>
		{
			{"def", IRTokenType.Define},

			{"i8", IRTokenType.I8},
			{"u8", IRTokenType.U8},
			{"i16", IRTokenType.I16},
			{"u16", IRTokenType.U16},
			{"i32", IRTokenType.I32},
			{"u32", IRTokenType.U32},
			{"i64", IRTokenType.I64},
			{"u64", IRTokenType.U64},
			{"f32", IRTokenType.F32},
			{"f64", IRTokenType.F64},


			{"alloca", IRTokenType.Alloca},

			{"store", IRTokenType.Store},
			{"load", IRTokenType.Load},
			{"gep", IRTokenType.Gep},
			{"getelementptr", IRTokenType.Gep},

			{"neg", IRTokenType.Neg},
			{"inc", IRTokenType.Inc},
			{"dec", IRTokenType.Dec},

			{"add", IRTokenType.Add},
			{"sub", IRTokenType.Sub},
			{"mul", IRTokenType.Mul},
			{"div", IRTokenType.Div},
			{"rem", IRTokenType.Rem},
			{"shift", IRTokenType.Shift},
			{"or", IRTokenType.Or},
			{"xor", IRTokenType.Xor},
			{"and", IRTokenType.And},

			{"trunc", IRTokenType.Trunc},
			{"ext", IRTokenType.Ext},
			{"fptoi", IRTokenType.Fptoi},
			{"itofp", IRTokenType.Itofp},
			{"bitcast", IRTokenType.Bitcast},

			{"call", IRTokenType.Call},
			{"cmp", IRTokenType.Cmp},

			{"ret", IRTokenType.Return},


			{"to", IRTokenType.To},
		};


		public List<IRToken> Lex(string source)
		{
			int offset = 0;
			List<IRToken> tokens = new List<IRToken>();

			while (offset < source.Length)
			{
				switch (source[offset])
				{
				case '\r':
				case '\n':
				case ' ':
				{
					if (source[offset] == '\n')
						tokens.Add(new IRToken(IRTokenType.EoL));
					++offset;
					break;
				}
				case '\t':
				{
					tokens.Add(new IRToken(IRTokenType.Indent));
					++offset;
					break;
				}
				case '(':
				{
					tokens.Add(new IRToken(IRTokenType.LParen));

					++offset;
					break;
				}
				case ')':
				{
					tokens.Add(new IRToken(IRTokenType.RParen));

					++offset;
					break;
				}
				case '[':
				{
					tokens.Add(new IRToken(IRTokenType.LBracket));

					++offset;
					break;
				}
				case ']':
				{
					tokens.Add(new IRToken(IRTokenType.RBracket));

					++offset;
					break;
				}
				case '{':
				{
					tokens.Add(new IRToken(IRTokenType.LBrace));

					++offset;
					break;
				}
				case '}':
				{
					tokens.Add(new IRToken(IRTokenType.RBrace));

					++offset;
					break;
				}
				case '=':
				{
					tokens.Add(new IRToken(IRTokenType.Equals));

					++offset;
					break;
				}
				case ',':
				{
					tokens.Add(new IRToken(IRTokenType.Comma));

					++offset;
					break;
				}
				case '^':
				{
					tokens.Add(new IRToken(IRTokenType.Caret));

					++offset;
					break;
				}
				case ':':
				{
					tokens.Add(new IRToken(IRTokenType.Colon));

					++offset;
					break;
				}
				case '*':
				{
					tokens.Add(new IRToken(IRTokenType.Asterisk));

					++offset;
					break;
				}
				case '%':
				{
					++offset;
					int end = offset;
					while (!Char.IsWhiteSpace(source[end]))
					{
						++end;
					}

					string tmp = source.Substring(offset, end - offset);
					tokens.Add(new IRToken(IRTokenType.LocalValue, tmp));

					offset = end;

					break;
				}
				case '@':
				{
					++offset;

					IRTokenType type = IRTokenType.GlobalValue;
					if (source[offset] == '.')
					{
						type = IRTokenType.FuncValue;
						++offset;
					}
					
					int end = offset;
					while (!Char.IsWhiteSpace(source[end]))
					{
						++end;
					}

					string tmp = source.Substring(offset, end - offset);
					tokens.Add(new IRToken(type, tmp));

					offset = end;
					break;
				}
				case '?':
				{
					++offset;
					int end = offset;
					while (!Char.IsWhiteSpace(source[end]))
					{
						++end;
					}

					string tmp = source.Substring(offset, end - offset);
					tokens.Add(new IRToken(IRTokenType.TypeValue, tmp));

					offset = end;

					break;
				}
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				{
					IRTokenType type = IRTokenType.LiteralVal;
					if (source[offset] == '0' && source[offset + 1] == 'x')
					{
						type = IRTokenType.HexLiteralVal;
						offset += 2;
					}

					int end = offset;
					while (Char.IsDigit(source[end]) || (type == IRTokenType.HexLiteralVal && "abcdef".IndexOf(Char.ToLower(source[end])) != -1))
					{
						++end;
					}

					string tmp = source.Substring(offset, end - offset);
					tokens.Add(new IRToken(type, tmp));

					offset = end;
					break;
				}
				default:
				{
					int end = offset;
					while (Char.IsLetterOrDigit(source[end]) || source[end] == '_' || source[end] == '.')
					{
						++end;
					}

					string tmp = source.Substring(offset, end - offset);

					if (_instructions.ContainsKey(tmp))
						tokens.Add(new IRToken(_instructions[tmp], tmp));
					else
						tokens.Add(new IRToken(IRTokenType.Unknown, tmp));

					offset = end;
					break;
				}
				}
			}

			return tokens;
		}
	}
}
