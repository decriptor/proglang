﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.IRBackend.Parse
{
	public enum IRTokenType
	{
		Unknown,

		Define,

		Indent,
		EoL,

		LocalValue,
		FuncValue,
		GlobalValue,
		TypeValue,

		LiteralVal,
		HexLiteralVal,
		
		LParen,
		RParen,
		LBrace,
		RBrace,
		LBracket,
		RBracket,
		Comma,
		Equals,
		Caret,
		Colon,
		Asterisk,

		I8,
		U8,
		I16,
		U16,
		I32,
		U32,
		I64,
		U64,
		F32,
		F64,


		Alloca,

		Store,
		Load,
		Gep,

		Neg,
		Inc,
		Dec,

		Add,
		Sub,
		Mul,
		Div,
		Rem,
		Shift,
		Or,
		Xor,
		And,

		Trunc,
		Ext,
		Fptoi,
		Itofp,
		Bitcast,

		Call,

		Cmp,

		Return,

		To,
	}

	class IRToken
	{
		public IRTokenType Type;
		public string Value;

		public IRToken(IRTokenType type, string value = null)
		{
			Type = type;
			Value = value;
		}

		public override string ToString()
		{
			string str = Type.ToString();
			if (Value != null)
				str += " | " + Value;
			return str;
		}
	}
}
