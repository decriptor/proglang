﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using MJC.IRBackend.General;

// TODO: Not all instructions are implemented

namespace MJC.IRBackend.Parse
{
	class IRParser
	{

		private List<IRToken> _tokens;
		private int _index;

		private Dictionary<string, IROperand> _localMapping = new Dictionary<string, IROperand>();
		private Dictionary<string, IROperand> _globalMapping = new Dictionary<string, IROperand>();

		public IRModule Parse(string name, List<IRToken> tokens)
		{
			_tokens = tokens;
			_index = 0;

			List<IRFunction> blocks = new List<IRFunction>();

			while (_index < _tokens.Count)
			{
				switch (PeekToken().Type)
				{
				case IRTokenType.Define:
				{
					IRFunction func = ParseFunction();
					blocks.Add(func);
					break;
				}
				default:
				{
					NextToken();
					break;
				}
				}
			}

			IRModule module = new IRModule(name);
			module.Functions = blocks;
			return module;
		}

		IRFunction ParseFunction()
		{
			EatToken(IRTokenType.Define);

			string iden = PeekToken().Value;
			EatToken(IRTokenType.FuncValue);

			EatToken(IRTokenType.LParen);
			// TODO
			EatToken(IRTokenType.RParen);

			EatToken(IRTokenType.Colon);
			IRType retType = ParseType();

			EatToken(IRTokenType.LBrace);
			EatToken(IRTokenType.EoL);
			
			List<IRBasicBlock> blocks = new List<IRBasicBlock>();
			while (PeekToken().Type != IRTokenType.RBrace)
			{
				while (PeekToken().Type == IRTokenType.EoL)
				{
					NextToken();
				}

				IRBasicBlock basicBlock = ParseBasicBlock();
				blocks.Add(basicBlock);
			}

			_localMapping.Clear();

			return new IRFunction { Identifier = iden, Params = null, RetType = retType, Blocks = blocks };
		}

		List<IRTypeDef> ParseTypeDefs()
		{
			List<IRTypeDef> typedefs = new List<IRTypeDef>();

			while (PeekToken().Type == IRTokenType.TypeValue)
			{
				string typeName = PeekToken().Value;
				EatToken(IRTokenType.TypeValue);
				EatToken(IRTokenType.Equals);
				IRTypeDef type = ParseTypeDef(typeName);
				typedefs.Add(type);
			}

			return typedefs;
		}

		IRBasicBlock ParseBasicBlock()
		{
			string label = PeekToken().Value;
			NextToken();
			EatToken(IRTokenType.Colon);

			List<IRInstruction> instructions = new List<IRInstruction>();
			while (PeekToken().Type == IRTokenType.Indent || PeekToken().Type == IRTokenType.EoL)
			{
				if (PeekToken().Type == IRTokenType.EoL)
				{
					NextToken();
				}
				else
				{
					EatToken(IRTokenType.Indent);
					IRInstruction instruction = ParseInstruction();
					instructions.Add(instruction);
				}

			}

			return new IRBasicBlock { Label = label, Instructions = instructions };
		}

		IRInstruction ParseInstruction()
		{
			string localValue = null;

			while (_index < _tokens.Count)
			{
				switch (PeekToken().Type)
				{
				case IRTokenType.LocalValue:
				{
					localValue = PeekToken().Value;
					NextToken();
					EatToken(IRTokenType.Equals);
					break;
				}
				case IRTokenType.EoL:
				{
					NextToken();
					break;
				}

				case IRTokenType.Alloca:
					return ParseAlloca(localValue);
				case IRTokenType.Store:
					return ParseStore();
				case IRTokenType.Load:
					return ParseLoad(localValue);
				case IRTokenType.Gep:
					return ParseGep(localValue);

				case IRTokenType.Neg:
					return ParseNeg(localValue);
				case IRTokenType.Inc:
					return ParseInc(localValue);
				case IRTokenType.Dec:
					return ParseDec(localValue);

				case IRTokenType.Add:
					return ParseAdd(localValue);
				case IRTokenType.Sub:
					return ParseSub(localValue);
				case IRTokenType.Mul:
					return ParseMul(localValue);
				case IRTokenType.Div:
					return ParseDiv(localValue);
				case IRTokenType.Rem:
					return ParseRem(localValue);
				case IRTokenType.Or:
					return ParseOr(localValue);
				case IRTokenType.Xor:
					return ParseXor(localValue);
				case IRTokenType.And:
					return ParseAnd(localValue);

				case IRTokenType.Trunc:
					return ParseTrunc(localValue);
				case IRTokenType.Ext:
					return ParseExt(localValue);
				case IRTokenType.Fptoi:
					return ParseFptoi(localValue);
				case IRTokenType.Itofp:
					return ParseItofp(localValue);
				case IRTokenType.Bitcast:
					return ParseBitcast(localValue);

				case IRTokenType.Call:
					return ParseCall(localValue);

				case IRTokenType.Cmp:
					return ParseCmp(localValue);

				case IRTokenType.Return:
					return ParseReturn();

				}
			}

			return null;
		}

		private IRTypeDef ParseTypeDef(string localValue)
		{
			EatToken(IRTokenType.LBrace);

			List<IRType> subTypes = new List<IRType>();
			while (PeekToken().Type != IRTokenType.RBrace)
			{
				IRType type = ParseType();
				subTypes.Add(type);

				if (PeekToken().Type == IRTokenType.Comma)
					EatToken(IRTokenType.Comma);
			}

			EatToken(IRTokenType.RBrace);
			EatToken(IRTokenType.EoL);

			// TODO: Incorrect
			return new IRTypeDef(localValue, subTypes, IRTypeDefKind.Struct);
		}

		IRAllocaInstruction ParseAlloca(string retval)
		{
			EatToken(IRTokenType.Alloca);

			IRType type = ParseType();

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retval, new IRType(type, forceAddress: true));
			_localMapping.Add(retval, retOperand);

			return new IRAllocaInstruction(retOperand, type);
		}

		IRStoreInstruction ParseStore()
		{
			EatToken(IRTokenType.Store);
			IROperand src = ParseOperand();
			EatToken(IRTokenType.Colon);
			IROperand dst = ParseOperand();

			return new IRStoreInstruction(src, dst);
		}

		IRLoadInstruction ParseLoad(string retVal)
		{
			EatToken(IRTokenType.Load);
			IROperand src = ParseOperand();
			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, new IRType(src.Type, false));
			_localMapping.Add(retVal, retOperand);

			return new IRLoadInstruction(retOperand, src);
		}

		IRGepInstruction ParseGep(string retVal)
		{
			EatToken(IRTokenType.Gep);
			EatToken(IRTokenType.Colon);

			IRType retType = ParseType();

			EatToken(IRTokenType.Comma);

			IROperand src = ParseOperand();

			EatToken(IRTokenType.Comma);

			List<int> indices = new List<int>();
			while (PeekToken().Type != IRTokenType.EoL)
			{
				indices.Add(Convert.ToInt32(PeekToken().Value));

				EatToken(IRTokenType.LiteralVal);

				if (PeekToken().Type == IRTokenType.Comma)
					EatToken(IRTokenType.Comma);
			}

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, retType);
			_localMapping.Add(retVal, retOperand);

			return new IRGepInstruction(retOperand, src, indices.ToArray());
		}

		IRNegInstruction ParseNeg(string retVal)
		{
			EatToken(IRTokenType.Neg);
			IROperand op = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Colon);
			IRType type = ParseType();

			if (op.Iden == null)
				op.Type = type;

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return  new IRNegInstruction(op, retOperand);
		}

		IRIncInstruction ParseInc(string retVal)
		{
			EatToken(IRTokenType.Neg);
			IROperand op = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Colon);
			IRType type = ParseType();

			if (op.Iden == null)
				op.Type = type;

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return new IRIncInstruction(op, type, retOperand);
		}

		IRDecInstruction ParseDec(string retVal)
		{
			EatToken(IRTokenType.Neg);
			IROperand op = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Colon);
			IRType type = ParseType();

			if (op.Iden == null)
				op.Type = type;

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return new IRDecInstruction(op, type, retOperand);
		}

		IRAddInstruction ParseAdd(string retVal)
		{
			EatToken(IRTokenType.Neg);
			IROperand op0 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Comma);

			IROperand op1 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Colon);
			IRType type = ParseType();

			if (op0.Iden == null)
				op0.Type = type;
			if (op1.Iden == null)
				op1.Type = type;

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return new IRAddInstruction(op0, op1, type, retOperand);
		}

		IRSubInstruction ParseSub(string retVal)
		{
			EatToken(IRTokenType.Sub);
			IROperand op0 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Comma);

			IROperand op1 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Colon);
			IRType type = ParseType();

			if (op0.Iden == null)
				op0.Type = type;
			if (op1.Iden == null)
				op1.Type = type;

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return new IRSubInstruction(op0, op1, type, retOperand);
		}

		IRMulInstruction ParseMul(string retVal)
		{
			EatToken(IRTokenType.Mul);
			IROperand op0 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Comma);

			IROperand op1 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Colon);
			IRType type = ParseType();

			if (op0.Iden == null)
				op0.Type = type;
			if (op1.Iden == null)
				op1.Type = type;

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return new IRMulInstruction(op0, op1, type, retOperand);
		}

		IRDivInstruction ParseDiv(string retVal)
		{
			EatToken(IRTokenType.Div);
			IROperand op0 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Comma);

			IROperand op1 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Colon);
			IRType type = ParseType();

			if (op0.Iden == null)
				op0.Type = type;
			if (op1.Iden == null)
				op1.Type = type;

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return new IRDivInstruction(op0, op1, type, retOperand);
		}

		IRRemInstruction ParseRem(string retVal)
		{
			EatToken(IRTokenType.Rem);
			IROperand op0 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Comma);

			IROperand op1 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Colon);
			IRType type = ParseType();

			if (op0.Iden == null)
				op0.Type = type;
			if (op1.Iden == null)
				op1.Type = type;

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return new IRRemInstruction(op0, op1, type, retOperand);
		}

		IRShiftInstruction ParseShift(string retVal)
		{
			EatToken(IRTokenType.Rem);

			string dirStr = PeekToken().Value;
			EatToken(IRTokenType.Unknown);

			IROperand op0 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Comma);

			IROperand op1 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Colon);
			IRType type = ParseType();

			if (op0.Iden == null)
				op0.Type = type;
			if (op1.Iden == null)
				op1.Type = type;

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			IRShiftDir dir = dirStr == "l" ? IRShiftDir.Left : dirStr == "lr" ? IRShiftDir.LogicRight : IRShiftDir.ArithRight;

			return new IRShiftInstruction(dir, op0, op1, type, retOperand);
		}

		IROrInstruction ParseOr(string retVal)
		{
			EatToken(IRTokenType.Rem);
			IROperand op0 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Comma);

			IROperand op1 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Colon);
			IRType type = ParseType();

			if (op0.Iden == null)
				op0.Type = type;
			if (op1.Iden == null)
				op1.Type = type;

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return new IROrInstruction(op0, op1, type, retOperand);
		}

		IRXorInstruction ParseXor(string retVal)
		{
			EatToken(IRTokenType.Rem);
			IROperand op0 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Comma);

			IROperand op1 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Colon);
			IRType type = ParseType();

			if (op0.Iden == null)
				op0.Type = type;
			if (op1.Iden == null)
				op1.Type = type;

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return new IRXorInstruction(op0, op1, type, retOperand);
		}

		IRAndInstruction ParseAnd(string retVal)
		{
			EatToken(IRTokenType.Rem);
			IROperand op0 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Comma);

			IROperand op1 = ParseOperandFromIden(new IRType());

			EatToken(IRTokenType.Colon);
			IRType type = ParseType();

			if (op0.Iden == null)
				op0.Type = type;
			if (op1.Iden == null)
				op1.Type = type;

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return new IRAndInstruction(op0, op1, type, retOperand);
		}

		IRTruncInstruction ParseTrunc(string retVal)
		{
			EatToken(IRTokenType.Trunc);
			IROperand op = ParseOperand();

			EatToken(IRTokenType.To);
			IRType type = ParseType();

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return new IRTruncInstruction(op, type, retOperand);
		}

		IRExtInstruction ParseExt(string retVal)
		{
			EatToken(IRTokenType.Ext);

			IRExtType extType = IRExtType.Default;
			if (PeekToken().Type == IRTokenType.Unknown)
			{
				switch (PeekToken().Value)
				{
				case "s": extType = IRExtType.Signed; break;
				case "z": extType = IRExtType.Zero; break;
				}
			}

			IROperand op = ParseOperand();

			EatToken(IRTokenType.To);
			IRType type = ParseType();

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return new IRExtInstruction(extType, op, type, retOperand);
		}

		IRFptoiInstruction ParseFptoi(string retVal)
		{
			EatToken(IRTokenType.Fptoi);
			IROperand op = ParseOperand();

			EatToken(IRTokenType.To);
			IRType type = ParseType();

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return new IRFptoiInstruction(op, type, retOperand);
		}

		IRItofpInstruction ParseItofp(string retVal)
		{
			EatToken(IRTokenType.Itofp);
			IROperand op = ParseOperand();

			EatToken(IRTokenType.To);
			IRType type = ParseType();

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return new IRItofpInstruction(op, type, retOperand);
		}

		IRBitcastInstruction ParseBitcast(string retVal)
		{
			EatToken(IRTokenType.Bitcast);
			IROperand op = ParseOperand();

			EatToken(IRTokenType.To);
			IRType type = ParseType();

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return new IRBitcastInstruction(op, type, retOperand);
		}

		IRCallInstruction ParseCall(string retVal)
		{
			EatToken(IRTokenType.Call);

			string identifier = PeekToken().Value;
			EatToken(IRTokenType.GlobalValue);

			EatToken(IRTokenType.LParen);

			List<IROperand> operands = null;
			if (PeekToken().Type != IRTokenType.RParen)
			{
				operands = new List<IROperand>();
				while (PeekToken().Type != IRTokenType.RParen)
				{
					IROperand op = ParseOperand();
					operands.Add(op);

					if (PeekToken().Type == IRTokenType.Comma)
						NextToken();
				}
			}

			IRType retType = null;
			if (PeekToken().Type == IRTokenType.Colon)
			{
				EatToken(IRTokenType.Colon);
				retType = ParseType();
			}

			EatToken(IRTokenType.EoL);

			IROperand retOperand = null;
			if (retType != null)
			{
				retOperand = new IROperand(retVal, retType);
				_localMapping.Add(retVal, retOperand);
			}

			return new IRCallInstruction(identifier, operands, retType, retOperand);
		}

		IRCmpInstruction ParseCmp(string retVal)
		{
			EatToken(IRTokenType.Bitcast);

			string cmpStr = PeekToken().Value;
			NextToken();
			IRCmpType cmpType = IRHelpers.GetCmpType(cmpStr);

			IROperand op0 = ParseOperand();
			EatToken(IRTokenType.Comma);
			IROperand op1 = ParseOperand();

			EatToken(IRTokenType.To);
			IRType type = ParseType();

			EatToken(IRTokenType.EoL);

			IROperand retOperand = new IROperand(retVal, type);
			_localMapping.Add(retVal, retOperand);

			return new IRCmpInstruction(cmpType, op0, op1, type, retOperand);
		}

		IRReturnInstruction ParseReturn()
		{
			EatToken(IRTokenType.Return);

			if (PeekToken().Type == IRTokenType.EoL)
			{
				EatToken(IRTokenType.EoL);
				return new IRReturnInstruction();
			}

			IROperand operand = ParseOperand();
			EatToken(IRTokenType.EoL);

			return new IRReturnInstruction(operand);
		}

		IROperand ParseOperand()
		{
			string iden = PeekToken().Value;
			IRTokenType tokType = PeekToken().Type;

			NextToken();
			EatToken(IRTokenType.Colon);

			IRType type = ParseType();

			if (tokType == IRTokenType.LiteralVal)
			{
				long value = long.Parse(iden);
				return new IROperand(value, type);
			}
			else if (tokType == IRTokenType.HexLiteralVal)
			{
				long value = long.Parse(iden, NumberStyles.HexNumber);
				return new IROperand(value, type);
			}
			else if (tokType == IRTokenType.LocalValue)
			{
				// TODO: Check type
				return _localMapping[iden];
			}
			else if (tokType == IRTokenType.GlobalValue)
			{
				return _globalMapping[iden];
			}

			return null;
		}

		IROperand ParseOperandFromIden(IRType litType)
		{
			string iden = PeekToken().Value;
			IRTokenType tokType = PeekToken().Type;

			NextToken();

			if (tokType == IRTokenType.LiteralVal)
			{
				long value = long.Parse(iden);
				return new IROperand(value, litType);
			}
			else if (tokType == IRTokenType.LocalValue)
			{
				return _localMapping[iden];
			}
			else if (tokType == IRTokenType.GlobalValue)
			{
				return _globalMapping[iden];
			}

			return null;
		}

		IRType ParseType()
		{
			bool address = false;
			if (PeekToken().Type == IRTokenType.Caret)
			{
				address = true;
				NextToken();
			}

			IRBaseType baseType = GetIRBaseType(PeekToken().Type);
			NextToken();

			byte ptrCount = 0;
			while (PeekToken().Type == IRTokenType.Asterisk)
			{
				++ptrCount;
				NextToken();
			}

			return new IRType(address, baseType, ptrCount);
		}

		IRBaseType GetIRBaseType(IRTokenType type)
		{
			switch (type)
			{
			default:
			case IRTokenType.I8: return IRBaseType.I8;
			case IRTokenType.U8: return IRBaseType.U8;
			case IRTokenType.I16: return IRBaseType.I16;
			case IRTokenType.U16: return IRBaseType.U16;
			case IRTokenType.I32: return IRBaseType.I32;
			case IRTokenType.U32: return IRBaseType.U32;
			case IRTokenType.I64: return IRBaseType.I64;
			case IRTokenType.U64: return IRBaseType.U64;
			case IRTokenType.F32: return IRBaseType.F32;
			case IRTokenType.F64: return IRBaseType.F64;
			}
		}

		IRToken EatToken(IRTokenType type)
		{
			++_index;
			if (_index < _tokens.Count)
				return _tokens[_index];
			return null;
		}

		IRToken PeekToken()
		{
			if (_index < _tokens.Count)
				return _tokens[_index];
			return null;
		}

		IRToken NextToken()
		{
			++_index;
			if (_index < _tokens.Count)
				return _tokens[_index];
			return null;
		}

	}
}
