﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.IRBackend.Utils.PE;

// ReSharper disable InconsistentNaming

// https://docs.microsoft.com/en-us/windows/desktop/debug/pe-format
namespace MJC.IRBackend.Utils
{
	namespace PE
	{
		public struct DOSHeader
		{
			public ushort Magic;
			public ushort LastSize;
			public ushort NBlocks;
			public ushort NReloc;
			public ushort HDRSize;
			public ushort MinAlloc;
			public ushort MaxAlloc;
			public ushort SS;
			public ushort SP;
			public ushort Checksum;
			public ushort IP;
			public ushort CS;
			public ushort RelocPos;
			public ushort NoOverlay;
			public ushort Reserved1_0;
			public ushort Reserved1_1;
			public ushort Reserved1_2;
			public ushort Reserved1_3;
			public ushort OEMId;
			public ushort OEMInfo;
			public ushort Reserved2_0;
			public ushort Reserved2_1;
			public ushort Reserved2_2;
			public ushort Reserved2_3;
			public ushort Reserved2_4;
			public ushort Reserved2_5;
			public ushort Reserved2_6;
			public ushort Reserved2_7;
			public ushort Reserved2_8;
			public ushort Reserved2_9;
			public uint E_LFANEW;
		}

		public class DOSStub
		{
			public static byte[] Stub =
			{
				0x0E, 0x1F, 0xBA, 0X0E, 0x00, 0xB4, 0x09, 0xCD, 0x21, 0xB8, 0x01, 0x4C, 0xCD, 0x21, 0x54, 0x68,
				0x69, 0x73, 0x20, 0x70, 0x72, 0x6F, 0x67, 0x72, 0x6D, 0x20, 0x63, 0x61, 0x6E, 0x6E, 0x6E, 0x6F,
				0x74, 0x20, 0x62, 0x65, 0x20, 0x72, 0x75, 0x6E, 0x20, 0x69, 0x6E, 0x20, 0x44, 0x4F, 0x53, 0x20,
				0x6D, 0x6F, 0x64, 0x65, 0x2E, 0x0D, 0x0D, 0xA0, 0x24, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
			};
		}

		public struct CoffHeader
		{
			public uint Magic;
			public MachineType MachineType;
			public ushort NumSections;
			public uint Timestamp;
			public uint PtrToSymbolTable;
			public uint NumSymbols;
			public ushort OptHdrSize;
			public Characteristics Characteristics;
		}

		public struct CoffStdFieldsHeader32
		{
			public ushort Magic;
			public byte LinkerMajorVer;
			public byte LinkerMinorVer;
			public uint CodeSize;
			public uint InitDataSize;
			public uint NonInitDataSize;
			public uint EntryAddress;
			public uint CodeBase;
			public uint DataBase;
		}

		public struct CoffStdFieldsHeader64
		{
			public ushort Magic;
			public byte LinkerMajorVer;
			public byte LinkerMinorVer;
			public uint CodeSize;
			public uint InitDataSize;
			public uint NonInitDataSize;
			public uint EntryAddress;
			public uint CodeBase;
		}

		public struct CoffWinHeader32
		{
			public uint ImageBase;
			public uint SectionAlign;
			public uint FileAlign;
			public ushort OSMajorVer;
			public ushort OSMinorVer;
			public ushort ImageMajorVer;
			public ushort ImageMinorVer;
			public ushort SubSysMajorVer;
			public ushort SubSysMinorVer;
			public uint Win32Ver;
			public uint ImageSize;
			public uint HeaderSize;
			public uint Checksum;
			public SubSystem SubSystem;
			public DLLCharacteristics DllCharacteristics;
			public uint StackReserveSize;
			public uint StackCommitSize;
			public uint HeapReserveSize;
			public uint HeapCommitSize;
			public uint LoaderFlags;
			public uint NumRVAAndSizes;
		}

		public struct CoffWinHeader64
		{
			public ulong ImageBase;
			public uint SectionAlign;
			public uint FileAlign;
			public ushort OSMajorVer;
			public ushort OSMinorVer;
			public ushort ImageMajorVer;
			public ushort ImageMinorVer;
			public ushort SubSysMajorVer;
			public ushort SubSysMinorVer;
			public uint Win32Ver;
			public uint ImageSize;
			public uint HeaderSize;
			public uint Checksum;
			public SubSystem SubSystem;
			public DLLCharacteristics DllCharacteristics;
			public ulong StackReserveSize;
			public ulong StackCommitSize;
			public ulong HeapReserveSize;
			public ulong HeapCommitSize;
			public uint LoaderFlags;
			public uint NumRVAAndSizes;
		}

		public struct ImageDataDirectory
		{
			public uint Offset;
			public uint Size;
		}

		public struct CoffDataHeader
		{
			public ImageDataDirectory ExportTable; // .edata
			public ImageDataDirectory ImportTable; // .idata
			public ImageDataDirectory ResourceTable; // .rsrc
			public ImageDataDirectory ExceptionTable; // .pdata
			public ImageDataDirectory CertificateTable;
			public ImageDataDirectory BaseRelocTable;
			public ImageDataDirectory DebufTable; // .debug
			public ImageDataDirectory Architecture;
			public ImageDataDirectory GlobalPtr;
			public ImageDataDirectory TLSTable; // .tls (thread-local storage)
			public ImageDataDirectory LoadConfigTable;
			public ImageDataDirectory BoundImport;
			public ImageDataDirectory IAT; // Import address table
			public ImageDataDirectory DelayImportDescriptor;
			public ImageDataDirectory CLRRuntimeHeader; // .cormeta
			public ImageDataDirectory Reserved;
		}

		public struct SectionHeader
		{
			public ulong Name;
			public uint VirtualSize;
			public uint VirtualAddress;
			public uint RawSize;
			public uint RawAddress;
			public uint RelocPtr;
			public uint LineNumPtr;
			public ushort NumRelocs;
			public ushort NumLineNums;
			public SectionFlags Characteristics;
		}

		// https://docs.microsoft.com/en-us/windows/desktop/debug/pe-format#machine-types
		public enum MachineType : ushort
		{
			UNKOWN = 0x0000,
			AM33 = 0x01D3,
			AMD64 = 0x8664,
			ARM = 0x01c0,
			ARM64 = 0xaa64,
			ARMNT = 0x01c4,
			EBC = 0x0EBC,
			I368 = 0x014c,
			IA64 = 0x0200,
			M32R = 0x9041,
			MIPS16 = 0x0266,
			MIPSFPU = 0x0366,
			MIPSFPU16 = 0x0466,
			POWERPC = 0x01f0,
			POWERPCFP = 0x01f1,
			R4000 = 0x0166,
			RISCV32 = 0x5032,
			RISCV64 = 0x5064,
			RISCV128 = 0x5128,
			SH3 = 0x01A2,
			SH3DSP = 0x01A3,
			SH4 = 0x01A6,
			SH5 = 0x01A8,
			THUMB = 0x01C2,
			WCEMIPSV2 = 0x0169,
		}

		// https://docs.microsoft.com/en-us/windows/desktop/debug/pe-format#characteristics
		[Flags]
		public enum Characteristics : ushort
		{
			RELOCS_STRIPPED = 0x0001,           // Image only, Windows CE, and Microsoft Windows NT and later.
												// This indicates that the file does not contain base relocations and must therefore be loaded at its preferred base address.
												// If the base address is not available, the loader reports an error.
												// The default behavior of the linker is to strip base relocations from executable (EXE) files. 
			EXECUTABLE_IMAGE = 0x0002,          // Image only. This indicates that the image file is valid and can be run. If this flag is not set, it indicates a linker error. 
			LINE_NUMS_STRIPPED = 0x0004,        // COFF line numbers have been removed. This flag is deprecated and should be zero. 
			LOCAL_SYMS_STRIPPED = 0x0008,       // COFF symbol table entries for local symbols have been removed. This flag is deprecated and should be zero. 
			AGGRESSIVE_WS_TRIM = 0x0010,        // Obsolete. Aggressively trim working set. This flag is deprecated for Windows 2000 and later and must be zero. 
			LARGE_ADDRESS_AWARE = 0x0020,       // Application can handle > 2-GB addresses. 
			BYTES_REVERSER_LO = 0x0080,         // Little endian: the least significant bit (LSB) precedes the most significant bit (MSB) in memory.
												// This flag is deprecated and should be zero. 
			_32BIT_MACHINE = 0x0100,            // Machine is based on a 32-bit-word architecture. 
			DEBUG_STRIPPED = 0x0200,            // Debugging information is removed from the image file. 
			REMOVABLE_RUN_FROM_SWAP = 0x0400,   // If the image is on removable media, fully load it and copy it to the swap file. 
			NET_RUN_FROM_SWAP = 0x0800,         // If the image is on network media, fully load it and copy it to the swap file. 
			SYSTEM = 0x1000,                    // The image file is a system file, not a user program. 
			DLL = 0x2000,                       // The image file is a dynamic-link library (DLL).
												// Such files are considered executable files for almost all purposes, although they cannot be directly run. 
			UP_SYSTEM_ONLY = 0x4000,            // The file should be run only on a uniprocessor machine. 
			BYTES_REVERSED_HI = 0x8000,         // Big endian: the MSB precedes the LSB in memory. This flag is deprecated and should be zero. 
		}

		// https://docs.microsoft.com/en-us/windows/desktop/debug/pe-format#dll-characteristics
		[Flags]
		public enum DLLCharacteristics : ushort
		{
			HIGH_ENTROPY = 0x0020,
			DYNAMIC_BASE = 0x0040,
			FORCE_INTEGRITY = 0x0080,
			NX_COMPAT = 0x0100,
			NO_ISOLATION = 0x0200,
			NO_SEH = 0x0400,
			NO_BIND = 0x0800,
			APPCONTAINER = 0x1000,
			WDM_DRIVER = 0x2000,
			GUARD_CF = 0x4000,
			TERMINAL_SERVER_AWARE = 0x8000
		}

		[Flags]
		public enum SubSystem : ushort
		{
			Unknown,
			Native,
			WindowsGUI,
			WindowsCUI,
			OS2CUI,
			PosixCUI,
			NativeWindows,
			WindowsCEGUI,
			EFIApplication,
			EFIBootServiceDriver,
			EFIRuntimeDriver,
			EFIRom,
			XBox,
			WindowsBootApplication
		}

		[Flags]
		public enum SectionFlags : uint
		{
			TYPE_NO_PAD = 0x0000_0008,
			CNT_CODE = 0x0000_0020,
			CNT_INITIALIZED_DATA = 0x0000_0040,
			CNT_UNINITIALIZED_DATA = 0x0000_0080,
			LNK_OTHER = 0x0000_0100,
			LNK_INFO = 0x0000_0200,
			GRPEL = 0x0000_0800,
			MEM_PURGABLE = 0x0002_0000,
			MEM_16BIT = 0x0002_0000,
			MEM_LOCKED = 0x0004_0000,
			MEM_PRELOAD = 0x0008_0000,
			ALIGN_1BYTES = 0x0010_0000,
			ALIGN_2BYTES = 0x0020_0000,
			ALIGN_4BYTES = 0x0030_0000,
			ALIGN_8BYTES = 0x0040_0000,
			ALIGN_16BYTES = 0x0050_0000,
			ALIGN_32BYTES = 0x0060_0000,
			ALIGN_64BYTES = 0x0070_0000,
			ALIGN_128BYTES = 0x0080_0000,
			ALIGN_256BYTES = 0x0090_0000,
			ALIGN_1024BYTES = 0x00A0_0000,
			ALIGN_2048BYTES = 0x00B0_0000,
			ALIGN_4096BYTES = 0x00C0_0000,
			ALIGN_8192BYTES = 0x00D0_0000,
			LNK_RLOC_OVFL = 0x0100_0000,
			LNK_DISCARDABLE = 0x0200_0000,
			LNK_NON_CACHED = 0x0400_0000,
			LNK_NON_PAGED = 0x0800_0000,
			LNK_SHARED = 0x1000_0000,
			LNK_EXECUTE = 0x2000_0000,
			LNK_READ = 0x4000_0000,
			LNK_WRITE = 0x8000_0000,
		}
	}
}
