﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MJC.IRBackend.General;

namespace MJC.IRBackend
{
	public class IRBuilder
	{	
		private string _moduleName;

		private IRModule _module;
		private IRFunction _curFunc;
		private IRBasicBlock _curBasicBlock;

		public void Begin(string name)
		{
			_moduleName = name;
			_module = new IRModule(_moduleName);
		}

		public IRModule End()
		{
			IRModule tmp = _module;
			_module = null;
			return tmp;
		}

		public List<IROperand> BeginFunction(string funcName, IRType retType, List<IROperand> operands, IRAttributes attribs)
		{
			_curFunc = new IRFunction();
			_curFunc.Identifier = funcName;
			_curFunc.RetType = retType;
			_curFunc.Params = operands;
			_curFunc.Attribs = attribs;
			_module.Functions.Add(_curFunc);

			return operands;
		}

		public void EndFunction()
		{
			_curBasicBlock = null;
			_curFunc = null;   
		}

		public void CreateBasicBlock(string iden)
		{
			_curBasicBlock = new IRBasicBlock();
			_curBasicBlock.Label = iden;
			_curFunc.Blocks.Add(_curBasicBlock);
		}

		public void CreateType(string name, IRTypeDef typeDef)
		{
			_module.TypeDefs.Add(typeDef);
		}

		public IROperand BuildGlobal(string iden, IRType type, IRAttributes attribs)
		{
			IROperand ret = new IROperand(iden, type, true);

			IRGlobal global = new IRGlobal(iden, type, attribs);

			_module.Globals.Add(global);

			return ret;
		}

		public IROperand BuildGlobal(string iden, IRType type, IRAttributes attribs, string value)
		{
			IROperand ret = new IROperand(iden, type, true);

			IRGlobal global = new IRGlobal(iden, type, attribs);
			global.StringValue = value;
			global.HasValue = true;

			_module.Globals.Add(global);
			 
			return ret;
		}

		public IROperand BuildGlobal(string iden, IRType type, IRAttributes attribs, long value)
		{
			IROperand ret = new IROperand(iden, type, true);

			IRGlobal global = new IRGlobal(iden, type, attribs);
			global.NumValue = value;
			global.HasValue = true;

			_module.Globals.Add(global);

			return ret;
		}

		public IROperand BuildAlloca(string iden, IRType type)
		{
			IROperand ret = new IROperand(iden, new IRType(type, false, true));

			_curBasicBlock.Instructions.Add(new IRAllocaInstruction(ret, type));

			return ret;
		}

		public void BuildStore(IROperand src, IROperand dst)
		{
			_curBasicBlock.Instructions.Add(new IRStoreInstruction(src, dst));
		}

		public IROperand BuildLoad(IROperand src, string dst)
		{
			IROperand ret = new IROperand(dst, new IRType(src.Type, false));

			_curBasicBlock.Instructions.Add(new IRLoadInstruction(ret, src));

			return ret;
		}

		public IROperand BuildGep(IROperand operand, string retVar, IRType retType, params int[] indices)
		{
			IRType actRetType = new IRType(retType, forceAddress: retType.PtrCount == 0);
			IROperand ret = new IROperand(retVar, actRetType);

			_curBasicBlock.Instructions.Add(new IRGepInstruction(ret, operand, indices));

			return ret;
		}

		public IROperand BuildNeg(string retVar, IROperand operand, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRNegInstruction(operand, ret));

			return ret;
		}

		public IROperand BuildCompl(string retVar, IROperand operand, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRComplInstruction(operand, ret));

			return ret;
		}

		public IROperand BuildAdd(string retVar, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRAddInstruction(op0, op1, type, ret));

			return ret;
		}

		public IROperand BuildSub(string retVar, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRSubInstruction(op0, op1, type, ret));

			return ret;
		}

		public IROperand BuildMul(string retVar, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRMulInstruction(op0, op1, type, ret));

			return ret;
		}

		public IROperand BuildDiv(string retVar, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRDivInstruction(op0, op1, type, ret));

			return ret;
		}

		public IROperand BuildRem(string retVar, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRRemInstruction(op0, op1, type, ret));

			return ret;
		}

		public IROperand BuildShift(string retVar, IRShiftDir dir, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRShiftInstruction(dir, op0, op1, type, ret));

			return ret;
		}

		public IROperand BuildOr(string retVar, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IROrInstruction(op0, op1, type, ret));

			return ret;
		}

		public IROperand BuildXor(string retVar, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRXorInstruction(op0, op1, type, ret));

			return ret;
		}

		public IROperand BuildAnd(string retVar, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRAndInstruction(op0, op1, type, ret));

			return ret;
		}

		public IROperand BuildTrunc(string retVar, IROperand op, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRTruncInstruction(op, type, ret));

			return ret;
		}

		public IROperand BuildExt(string retVar, IRExtType extType, IROperand op, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRExtInstruction(extType, op, type, ret));

			return ret;
		}

		public IROperand BuildFptoi(string retVar, IROperand op, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRFptoiInstruction(op, type, ret));

			return ret;
		}

		public IROperand BuildItofp(string retVar, IROperand op, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRItofpInstruction(op, type, ret));

			return ret;
		}

		public IROperand BuildPtrtoi(string retVar, IROperand op, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRPtrtoiInstruction(op, type, ret));

			return ret;
		}

		public IROperand BuildItoptr(string retVar, IROperand op, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRItoptrInstruction(op, type, ret));

			return ret;
		}

		public IROperand BuildBitcast(string retVar, IROperand op, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(type));

			_curBasicBlock.Instructions.Add(new IRBitcastInstruction(op, type, ret));

			return ret;
		}

		public IROperand BuildExtractValue(string retVar, IROperand aggregate, IRType retType, params int[] indices)
		{
			IRType actRetType = new IRType(retType, forceAddress: true);
			IROperand ret = new IROperand(retVar, actRetType);
			
			_curBasicBlock.Instructions.Add(new IRExtractValueInstruction(ret, aggregate, indices));

			return ret;
		}

		public void BuildInsertValue(IROperand aggregate, IROperand value, params int[] indices)
		{
			string indicesStr = null;
			foreach (int i in indices)
			{
				if (indicesStr == null)
					indicesStr = $"{i}";
				else
					indicesStr += $", {i}";
			}

			_curBasicBlock.Instructions.Add(new IRInsertValueInstruction(aggregate, value, indices));
		}

		public IROperand BuildCall(string retVar, string identifier, List<IROperand> operands, IRType retType)
		{
			IROperand retOperand = null;
			if (retType != null)
				retOperand = new IROperand(retVar, retType);

			_curBasicBlock.Instructions.Add(new IRCallInstruction(identifier, operands, retType, retOperand));

			return retOperand;
		}

		public IROperand BuildCmp(string retVar, IRCmpType cmpType, IROperand op0, IROperand op1, IRType type)
		{
			IROperand ret = new IROperand(retVar, new IRType(IRBaseType.I1));

			_curBasicBlock.Instructions.Add(new IRCmpInstruction(cmpType, op0, op1, type, ret));

			return ret;
		}

		public void BuildRet()
		{
			_curBasicBlock.Instructions.Add(new IRReturnInstruction());
		}

		public void BuildRet(IROperand operand)
		{
			_curBasicBlock.Instructions.Add(new IRReturnInstruction(operand));
		}

		public void BuildBranch(string label)
		{
			_curBasicBlock.Instructions.Add(new IRBranchInstruction(label));
		}

		public void BuildCondBranch(IROperand condition, string trueLabel, string falseLabel)
		{
			_curBasicBlock.Instructions.Add(new IRCondBranchInstruction(condition, trueLabel, falseLabel));
		}
	}
}
