﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.IRBackend.General
{
	partial class IRHelpers
	{

		public static IRCmpType GetCmpType(string str)
		{
			switch (str)
			{
			default:
			case "eq":		return IRCmpType.Eq;
			case "ne":		return IRCmpType.Ne;
			case "gt":		return IRCmpType.Gt;
			case "ge":		return IRCmpType.Ge;
			case "lt":		return IRCmpType.Lt;
			case "le":		return IRCmpType.Le;
			case "ueq":		return IRCmpType.UEq;
			case "une":		return IRCmpType.UNe;
			case "ugt":		return IRCmpType.UGt;
			case "uge":		return IRCmpType.UGe;
			case "ult":		return IRCmpType.ULt;
			case "ule":		return IRCmpType.ULe;
			case "uno":		return IRCmpType.UNo;
			}
		}

		public static string GetString(IRCmpType type)
		{
			switch (type)
			{
			default:			return "unknown";
			case IRCmpType.Eq:	return "eq";	
			case IRCmpType.Ne:	return "ne";	
			case IRCmpType.Gt:	return "gt";	
			case IRCmpType.Ge:	return "ge";	
			case IRCmpType.Lt:	return "lt";	
			case IRCmpType.Le:	return "le";	
			case IRCmpType.UEq:	return "ueq";	
			case IRCmpType.UNe:	return "une";	
			case IRCmpType.UGt:	return "ugt";	
			case IRCmpType.UGe:	return "uge";	
			case IRCmpType.ULt:	return "ult";	
			case IRCmpType.ULe:	return "ule";	
			case IRCmpType.UNo:	return "uno";	
			}
		}

	}
}
