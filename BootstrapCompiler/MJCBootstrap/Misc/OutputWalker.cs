﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.SyntaxTree;

namespace MJC.Misc
{
	class OutputWalker : SyntaxWalkerWithTokens
	{
		protected override void VisitToken(SyntaxToken token)
		{
			Console.WriteLine(token);
			base.VisitToken(token);
		}
	}
}
