﻿using MJC.General;

namespace MJC.SyntaxTree.SemanticAnalysis.FullCheckers
{
	class DeclarationChecker : ISemanticPass
	{
		protected override void VisitDeclaration(DeclarationSyntax syntax)
		{
			if (syntax.Context.SymbolList != null)
			{
				foreach (Symbol symbol in syntax.Context.SymbolList)
				{
					
				}
			}
			else
			{
				Symbol symbol = syntax.Context.Symbol;
				if (symbol.Kind == SymbolKind.Variable)
					return;

				TextSpan symbolSpan = syntax.Names.FullSpan;
				SimpleExpressionSyntax initializer = syntax.Initializer;

				CheckInitializerForSymbol(symbol, symbolSpan, initializer);
			}
		}

		void CheckInitializerForSymbol(Symbol symbol, TextSpan symbolSpan, SimpleExpressionSyntax init)
		{
			if (!(symbol.Type is AggregateSymbolType || symbol.Type is InterfaceSymbolType))
				return;

			if (init == null)
			{
				ErrorSystem.AstError(symbolSpan, "uninitialized variable");
				return;
			}

			if (symbol.Type != init.Context.Type && 
			    !(init.Context.Type is BuiltinSymbolType builtin && builtin.Builtin == BuiltinTypes.Void))
			{
				string initType = init.Context.Type?.ToString() ?? "undefined";
				ErrorSystem.AstError(symbolSpan, $"mismatched type during initialization: was '{initType}', expected '{symbol.Type}'");
				return;
			}

			if (init is StructInitializerSyntax structInit)
			{
				CompilerContext baseTable = symbol.BaseTable;

				ScopeVariable scopeVar;
				if (structInit.TypeIden is QualifiedNameSyntax qualName)
					scopeVar = new ScopeVariable(qualName);
				else
					scopeVar = new ScopeVariable(structInit.TypeIden.Context.Identifier);
				Symbol typeSym = baseTable.FindType(symbol.Scope, scopeVar);

				if (typeSym.Children.Count > 0)
				{
					if (structInit.Elements == null)
					{
						string errStr = "initializer doesn't initialize the following variables: ";
						
						for (var i = 0; i < typeSym.Children.Count; i++)
						{
							if (i != 0)
								errStr += ", ";

							Symbol elemSymbol = typeSym.Children[0];
							errStr += elemSymbol.Identifier;

							++i;
						}

						ErrorSystem.AstError(structInit.FullSpan, errStr);
						return;
					}

					string missingParams = null;
					foreach (Symbol child in typeSym.Children)
					{
						bool found = false;
						foreach (StructInitializerElemSyntax elem in structInit.Elements)
						{
							if (child.Identifier == elem.Identifier.Context.Identifier)
							{
								found = true;
								break;
							}
						}

						if (!found)
						{
							if (missingParams == null)
								missingParams += child.Identifier;
							else
								missingParams += ", " + child.Identifier;
						}
					}

					if (missingParams != null)
					{
						string errStr = "initializer doesn't initialize the following variables: ";
						ErrorSystem.AstError(structInit.FullSpan, errStr + missingParams);
					}
				}
			}
		}

	}
}
