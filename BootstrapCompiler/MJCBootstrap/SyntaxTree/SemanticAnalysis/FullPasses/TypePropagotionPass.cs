﻿using System.Collections.Generic;
using MJC.General;

namespace MJC.SyntaxTree.SemanticAnalysis.FullPasses
{
	class TypePropagotionPass : ISemanticPass
	{
		private CompilerContext _compContext;

		private bool _idenCheck = true;
		private int _templateArgId = 0;
		private ScopeVariable _implIden;
		private Scope _implTemplateScope;

		public override void Setup(CompilerContext compilerContext)
		{
			_compContext = compilerContext;
		}

		public override void Finalize(CompilerContext compilerContext)
		{
			compilerContext.Symbols.UpdateTypes();
		}

		public override void Visit(SyntaxTree tree)
		{
			_compContext.SetCurFileImportTable(tree.FileImports);
			base.Visit(tree);
		}

		protected override void VisitAlias(AliasSyntax syntax)
		{
			//base.VisitAlias(syntax);
		}
		
		protected override void VisitArgument(ArgumentSyntax syntax)
		{
			base.VisitArgument(syntax);
			syntax.Context.Type = syntax.Expression.Context.Type;
		}

		protected override void VisitArrayLiteral(ArrayLiteralExpressionSyntax syntax)
		{
			base.VisitArrayLiteral(syntax);
		}

		protected override void VisitArrayType(ArrayTypeSyntax syntax)
		{
			base.VisitArrayType(syntax);
		}

		protected override void VisitAssert(AssertSyntax syntax)
		{
			base.VisitAssert(syntax);
		}

		protected override void VisitAssignExpression(AssignExpressionSyntax syntax)
		{
			base.VisitAssignExpression(syntax);
			syntax.Context.Type = syntax.Left.Context.Type;
		}

		protected override void VisitAssocArrayLiteral(AssocArrayLiteralExpressionSyntax syntax)
		{
			base.VisitAssocArrayLiteral(syntax);
		}

		protected override void VisitAtAttribute(AtAttributeSyntax syntax)
		{
			base.VisitAtAttribute(syntax);
		}
		
		protected override void VisitAttributedType(AttributedTypeSyntax syntax)
		{
			base.VisitAttributedType(syntax);
		}

		protected override void VisitAttributes(AttributeSyntax syntax)
		{
			base.VisitAttributes(syntax);
		}

		protected override void VisitBinaryExpression(BinaryExpressionSyntax syntax)
		{
			base.VisitBinaryExpression(syntax);

			SymbolType left = syntax.Left.Context.Type;
			SymbolType right = syntax.Right.Context.Type;

			// TODO: Pass operation
			SymbolType resultType;

			if (IsAssignToken(syntax.OperatorToken.Type))
				resultType = SymbolType.BuiltinType(BuiltinTypes.Bool);
			else
				resultType = CombineTypes(syntax.FullSpan, left, right);

			syntax.Context.Type = resultType;
		}

		protected override void VisitBlockStatement(BlockStatementSyntax syntax)
		{
			base.VisitBlockStatement(syntax);
		}

		protected override void VisitBracketedExpression(BracketedExpressionSyntax syntax)
		{
			base.VisitBracketedExpression(syntax);

			if (syntax.Expression is CommaExpressionSyntax commaExpr)
			{
				List<SymbolType> types = new List<SymbolType>();
				foreach (CommaExpressionElemSyntax elemSyntax in commaExpr.Expressions)
				{
					SymbolType type = elemSyntax.Expression.Context.Type;
					types.Add(type);
				}

				syntax.Context.Type = SymbolType.TupleType(types);
			}
			else
			{
				syntax.Context.Type = syntax.Expression.Context.Type;
			}
		}

		protected override void VisitBreak(BreakSyntax syntax)
		{
			base.VisitBreak(syntax);
		}

		protected override void VisitCase(CaseSyntax syntax)
		{
			base.VisitCase(syntax);
		}

		protected override void VisitCastExpression(CastExpressionSyntax syntax)
		{
			base.VisitCastExpression(syntax);

			syntax.Context.Type = syntax.Type.Context.Type;
		}

		protected override void VisitCCastExpression(CCastExpressionSyntax syntax)
		{
			base.VisitCCastExpression(syntax);

			syntax.Context.Type = syntax.Type.Context.Type;
		}

		protected override void VisitCommaExpression(CommaExpressionSyntax syntax)
		{
			base.VisitCommaExpression(syntax);
		}

		protected override void VisitCompilationUnit(CompilationUnitSyntax syntax)
		{
			base.VisitCompilationUnit(syntax);
		}

		protected override void VisitCompileCondition(CompileConditionalSyntax syntax)
		{
			base.VisitCompileCondition(syntax);
		}
		
		protected override void VisitContinue(ContinueSyntax syntax)
		{
			base.VisitContinue(syntax);
		}
		
		protected override void VisitDeclaration(DeclarationSyntax syntax)
		{
			// Create unpacked types for each symbols
			//base.VisitDeclaration(syntax);
			
			if (syntax.Type != null)
				VisitType(syntax.Type);
			base.VisitSimpleExpression(syntax.Initializer);

			if (syntax.Type == null && syntax.Initializer != null)
			{
				SymbolType initType = syntax.Initializer.Context.Type;

				if (initType is TupleSymbolType tupleType)
				{
					// psuedo unpack and repack
					List<SymbolType> subTypes = tupleType.SubTypes;

					if (syntax.Names is NameListSyntax nameList)
					{
						for (var i = 0; i < nameList.Names.Count; i++)
						{
							NameSyntax name = nameList.Names[i].Name;

							if (i + 1 != nameList.Names.Count)
							{
								syntax.Context.SymbolList[i].Type = subTypes[i];
							}
							else
							{
								if (subTypes.Count == i + 1)
								{
									syntax.Context.SymbolList[i].Type = subTypes[i];
								}
								else
								{
									List<SymbolType> newSubTypes = subTypes.GetRange(i, subTypes.Count - i);
									syntax.Context.SymbolList[i].Type = SymbolType.TupleType(newSubTypes);
								}
							}
						}
					}
					else
					{
						syntax.Context.Symbol.Type = tupleType;
					}

				}
				else
				{
					syntax.Context.Symbol.Type = initType;
				}
			}
			else if (syntax.Context.Symbol != null)
			{
				if (syntax.Context.Symbol.Type == null)
					syntax.Context.Symbol.Type = syntax.Type.Context.Type;
			}
			else if (syntax.Context.SymbolList != null)
			{
				foreach (Symbol symbol in syntax.Context.SymbolList)
				{
					symbol.Type = syntax.Type.Context.Type;
				}
			}

			_idenCheck = true;
		}

		protected override void VisitDefaultCase(DefaultCaseSyntax syntax)
		{
			base.VisitDefaultCase(syntax);
		}

		protected override void VisitDefer(DeferSyntax syntax)
		{
			base.VisitDefer(syntax);
		}

		protected override void VisitDelegate(DelegateSyntax syntax)
		{
			base.VisitDelegate(syntax);
		}

		protected override void VisitDeleteExpression(DeleteExpressionSyntax syntax)
		{
			base.VisitDeleteExpression(syntax);
		}
		
		protected override void VisitDoWhile(DoWhileSyntax syntax)
		{
			base.VisitDoWhile(syntax);
		}

		protected override void VisitEnum(EnumSyntax syntax)
		{
			_idenCheck = false;
			base.VisitEnum(syntax);
			UpdateTemplateDefinition(syntax.TemplateParameters, syntax);
			UpdateChildren(syntax.Context.Symbol);
		}

		protected override void VisitExpression(ExpressionSyntax syntax)
		{
			base.VisitExpression(syntax);
		}
		
		protected override void VisitFallthrough(FallthroughSyntax syntax)
		{
			base.VisitFallthrough(syntax);
		}

		protected override void VisitFor(ForSyntax syntax)
		{
			base.VisitFor(syntax);
		}

		protected override void VisitForeach(ForeachSyntax syntax)
		{
			base.VisitForeach(syntax);
		}

		protected override void VisitFunction(FunctionSyntax syntax)
		{
			_idenCheck = false;
			base.VisitFunction(syntax);

			// Update method scope
			if (_implIden != null)
			{
				syntax.Context.Symbol.Scope.LastSubScope = _implIden.Name;
			}

			// Update identifier
			if (syntax.TemplateParams != null)
			{
				UpdateTemplateDefinition(syntax.TemplateParams, syntax);
			}
		}

		protected override void VisitFunctionCall(FunctionCallExpressionSyntax syntax)
		{
			base.VisitFunctionCall(syntax);
			
			List<SymbolType> argTypes = null;
			if (syntax.Arguments != null && syntax.Arguments.Count > 0)
			{
				argTypes = new List<SymbolType>();
				foreach (ArgumentSyntax argument in syntax.Arguments)
				{
					argTypes.Add(argument.Context.Type);
				}
			}

			if (syntax.QualifiedName.SeperationToken?.Type == TokenType.Dot)
			{
				// method
				VisitName(syntax.QualifiedName.Left);
				SymbolType receiverType = syntax.QualifiedName.Left.Context.Type;

				if (receiverType != null)
				{
					SymbolType recType = receiverType;
					if (recType is MemoryLocSymbolType memLoc)
						recType = memLoc.BaseType;
					else if (recType is ReferenceSymbolType refType)
						recType = refType.BaseType;

					if (recType is AggregateSymbolType aggrType)
					{
						Scope funcScope = new Scope(aggrType.Identifier);
						ScopeVariable funcScopeVar = new ScopeVariable(funcScope, syntax.Context.Identifier);

						Symbol methodSymbol = _compContext.FindFunction(syntax.Context.Scope, funcScopeVar, receiverType, argTypes, syntax.FullSpan.Start);

						if (methodSymbol != null)
						{
							FunctionSymbolType funcType = methodSymbol.Type as FunctionSymbolType;
							syntax.Context.FuncType = funcType;
							syntax.Context.Type = funcType.ReturnType;
							syntax.Context.Symbol = methodSymbol;
							return;
						}
					}
					else if (recType is TemplateInstanceSymbolType instType)
					{
						Symbol templateSym = instType.TemplateSymbol;
						Scope funcScope = new Scope(templateSym.Identifier);
						ScopeVariable funcScopeVar = new ScopeVariable(funcScope, syntax.Context.Identifier);

						Symbol methodSymbol = _compContext.FindFunction(syntax.Context.Scope, funcScopeVar, receiverType, argTypes, syntax.FullSpan.Start);

						if (methodSymbol != null)
						{
							FunctionSymbolType funcType = methodSymbol.Type as FunctionSymbolType;
							syntax.Context.FuncType = funcType;
							syntax.Context.Type = funcType.ReturnType;
							syntax.Context.Symbol = methodSymbol;
							return;
						}
					}
				}
			}
			else // Free function
			{
				ScopeVariable funcScopeVar = new ScopeVariable(syntax.Context.Identifier);
				Symbol funcSymbol = _compContext.FindFunction(syntax.Context.Scope, funcScopeVar, null, argTypes, syntax.FullSpan.Start);

				if (funcSymbol != null)
				{
					if (funcSymbol.Type is FunctionSymbolType funcType)
					{
						syntax.Context.FuncType = funcType;
						syntax.Context.Type = funcType.ReturnType;
						syntax.Context.Symbol = funcSymbol;
					}
					else if (funcSymbol.Type is TemplateInstanceSymbolType instType)
					{
						FunctionSymbolType tmp = instType.TemplateSymbol.Type as FunctionSymbolType;
						syntax.Context.FuncType = tmp;
						syntax.Context.Type = tmp.ReturnType;
						syntax.Context.Symbol = funcSymbol;
					}
					
					return;
				}
			}

			// not found, should error
			string errorStr = "Failed to find function matching the arguments: (";
			if (argTypes != null)
			{
				for (var i = 0; i < argTypes.Count; i++)
				{
					if (i != 0)
						errorStr += ", ";
							
					SymbolType type = argTypes[i];
					errorStr += type.ToString();
				}
			}
			errorStr += ')';
			ErrorSystem.AstError(syntax.FullSpan, errorStr);
		}

		protected override void VisitTemplateConstraint(TemplateConstraintSyntax syntax)
		{
			base.VisitTemplateConstraint(syntax);
		}

		protected override void VisitGoto(GotoSyntax syntax)
		{
			base.VisitGoto(syntax);
		}

		protected override void VisitIdentifierName(IdentifierNameSyntax syntax)
		{
			//base.VisitIdentifierName(syntax);

			if (_idenCheck)
			{
				Identifier name = new IdentifierName(syntax.Identifier.Text);
				Symbol symbol = _compContext.FindDefinition(syntax.Context.Scope, name, syntax.FullSpan.Start, null);

				if (symbol == null)
				{
					ErrorSystem.AstError(syntax.FullSpan, $"Undeclared identifier {name}");
					syntax.Context.Type = SymbolType.BuiltinType(BuiltinTypes.Unkown);
				}
				else
				{
					syntax.Context.Type = symbol.Type;
					syntax.Context.Symbol = symbol;
				}
			}
		}

		protected override void VisitIdentifierType(IdentifierTypeSyntax syntax)
		{
			base.VisitIdentifierType(syntax);

			// Instantiate template instance
			if (syntax.Name is TemplateNameSyntax inst)
			{
				ProcessTemplateInstance(inst, null, syntax);
			}
		}

		protected override void VisitIf(IfSyntax syntax)
		{
			base.VisitIf(syntax);
		}

		protected override void VisitImport(ImportSyntax syntax)
		{
			bool prev = _idenCheck;
			_idenCheck = false;
			base.VisitImport(syntax);
			_idenCheck = prev;
		}

		protected override void VisitImpl(ImplSyntax syntax)
		{
			_implIden = new ScopeVariable(syntax.Type.Context.Scope.GetBaseScope(), syntax.Context.Identifier);
			_implTemplateScope = syntax.Context.Scope;
			if (syntax.Context.CompileAttribs.ContainsKey(ExtendBuiltinAttribute.Id))
			{
				VisitBlockStatement(syntax.Body);
			}
			else
			{
				base.VisitImpl(syntax);
			}
			_implTemplateScope = null;
			_implIden = null;

		}

		protected override void VisitIndexSliceExpression(IndexSliceExpressionSyntax syntax)
		{
			base.VisitIndexSliceExpression(syntax);
		}

		protected override void VisitInterface(InterfaceSyntax syntax)
		{
			_idenCheck = false;
			base.VisitInterface(syntax);
			UpdateTemplateDefinition(syntax.TemplateParameters, syntax);
		}

		protected override void VisitIsExpression(IsExpressionSyntax syntax)
		{
			base.VisitIsExpression(syntax);
		}

		protected override void VisitLabel(LabelSyntax syntax)
		{
			base.VisitLabel(syntax);
		}

		protected override void VisitLambda(ClosureSyntax syntax)
		{
			base.VisitLambda(syntax);
		}

		protected override void VisitLiteral(LiteralExpressionSyntax syntax)
		{
			base.VisitLiteral(syntax);

			BuiltinTypes type = BuiltinTypes.Unkown;
			switch (syntax.LiteralToken.Type)
			{
			case TokenType.Void:       type = BuiltinTypes.Void; break;
			case TokenType.Null:       type = BuiltinTypes.Null; break;
			case TokenType.I8Literal:  type = BuiltinTypes.I8;   break;
			case TokenType.I16Literal: type = BuiltinTypes.I16;  break;
			case TokenType.I32Literal: type = BuiltinTypes.I32;  break;
			case TokenType.I64Literal: type = BuiltinTypes.I64;  break;
			case TokenType.U8Literal:  type = BuiltinTypes.U8;   break;
			case TokenType.U16Literal: type = BuiltinTypes.U16;  break;
			case TokenType.U32Literal: type = BuiltinTypes.U32;  break;
			case TokenType.U64Literal: type = BuiltinTypes.U64;  break;
			case TokenType.F32Literal: type = BuiltinTypes.F32;  break;
			case TokenType.F64Literal: type = BuiltinTypes.F64;  break;

			// TODO: Base on value
			case TokenType.CharLiteral: type = BuiltinTypes.Char;  break; 
			case TokenType.StringLiteral: type = BuiltinTypes.StringLiteral; break;
			}

			syntax.Context.Type = SymbolType.BuiltinType(type);
		}

		protected override void VisitMixinExpression(MixinExpressionSyntax syntax)
		{
			base.VisitMixinExpression(syntax);
		}

		protected override void VisitModuleDirective(ModuleDirectiveSyntax syntax)
		{
			if (syntax.Attributes != null)
				base.VisitAttributes(syntax.Attributes);
		}

		protected override void VisitNameList(NameListSyntax syntax)
		{
			base.VisitNameList(syntax);
		}

		protected override void VisitNamespace(NamespaceSyntax syntax)
		{
			//base.VisitNamespace(syntax);
			base.VisitBlockStatement(syntax.Body);
		}

		protected override void VisitNewExpression(NewExpressionSyntax syntax)
		{
			base.VisitNewExpression(syntax);
		}

		protected override void VisitNullableType(NullableTypeSyntax syntax)
		{
			base.VisitNullableType(syntax);
		}

		protected override void VisitPackageDirective(PackageDirectiveSyntax syntax)
		{
			_idenCheck = false;
			base.VisitPackageDirective(syntax);
			_idenCheck = true;
		}

		protected override void VisitParameter(ParameterSyntax syntax)
		{
			bool prev = _idenCheck;
			_idenCheck = false;
			base.VisitParameter(syntax);
			_idenCheck = prev;
			syntax.Context.Type = syntax.Context.Type;
		}

		protected override void VisitPointerType(PointerTypeSyntax syntax)
		{
			base.VisitPointerType(syntax);
		}

		protected override void VisitPostfixExpression(PostfixExpressionSyntax syntax)
		{
			base.VisitPostfixExpression(syntax);
		}
		
		protected override void VisitBuiltinType(BuiltinTypeSyntax syntax)
		{
			base.VisitBuiltinType(syntax);
		}

		protected override void VisitPrefixExpression(PrefixExpressionSyntax syntax)
		{
			base.VisitPrefixExpression(syntax);
			
			SymbolType right = syntax.Right.Context.Type;

			// TODO: Check for custom unary ops

			// No custom operator
			switch (syntax.OperatorToken.Type)
			{
			case TokenType.And:
			{
				right = SymbolType.PointerType(right);
				break;
			}
			case TokenType.Asterisk:
			{
				if (right is PointerSymbolType ptrType)
				{
					right = ptrType.BaseType;
				}
				else
				{
					ErrorSystem.AstError(syntax.FullSpan, "Unable to dereference a non-pointer type");
				}
				break;
			}
			case TokenType.Exclaim:
			{
				right = SymbolType.BuiltinType(BuiltinTypes.Bool);
				break;
			}
			}

			syntax.Context.Type = right;

			base.VisitPrefixExpression(syntax);
		}

		protected override void VisitQualifiedName(QualifiedNameSyntax syntax)
		{
			if (_idenCheck)
			{
				_idenCheck = false;
				base.VisitQualifiedName(syntax);
				_idenCheck = true;
			}
			else
			{
				base.VisitQualifiedName(syntax);
			}
			
			// Instantiate template instance
			if (syntax.Right is TemplateNameSyntax inst)
			{
				Scope scope = new Scope(syntax.Left);
				ProcessTemplateInstance(inst, scope, syntax);
			}

			if (_idenCheck)
			{
				if (syntax.SeperationToken?.Type == TokenType.Dot)
				{
					Scope scope = new Scope(syntax);
					ScopeVariable qualScopeVar = new ScopeVariable(syntax);
					
					if (scope != qualScopeVar.Scope)
					{
						Symbol symbol;
						for (int i = scope.Names.Count; i < qualScopeVar.Scope.Names.Count; ++i)
						{
							Identifier subName = qualScopeVar.Scope.Names[i];
							symbol = _compContext.FindDefinition(syntax.Context.Scope, subName, syntax.FullSpan.Start, scope);

							if (symbol == null)
							{
								ErrorSystem.AstError(syntax.FullSpan, $"Undeclared identifier {qualScopeVar.Name}");
								syntax.Context.Type = SymbolType.BuiltinType(BuiltinTypes.Unkown);
							}
							else
							{
								SymbolType type = symbol.Type;
								SymbolType innerType = type.GetInnerType();

								// Is only a self type when 'Self' is a template instance
								if (innerType is SelfSymbolType selfType)
								{
									innerType = _compContext.FindType(syntax.Context.Scope, _implIden).Type;
								}
								if (innerType is TemplateInstanceSymbolType instType)
								{
									// TODO: full and partial specialization
									innerType = instType.TemplateSymbol.Type;

								}

								Identifier innerTypeName = innerType.GetIdentifier();
								scope = new Scope();
								scope.Names.Add(innerTypeName);
							}
						}

						symbol = _compContext.FindDefinition(syntax.Context.Scope, qualScopeVar.Name, -1, scope);

						if (symbol == null)
						{
							ErrorSystem.AstError(syntax.FullSpan, $"Undeclared identifier {qualScopeVar.Name}");
							syntax.Context.Type = SymbolType.BuiltinType(BuiltinTypes.Unkown);
						}
						else
						{
							syntax.Context.Type = symbol.Type;
							syntax.Context.Symbol = symbol;
						}
					}
					else
					{
						Symbol symbol = _compContext.FindDefinition(syntax.Context.Scope, qualScopeVar, -1);

						if (symbol == null)
						{
							ErrorSystem.AstError(syntax.FullSpan, $"Undeclared identifier {qualScopeVar.Name}");
							syntax.Context.Type = SymbolType.BuiltinType(BuiltinTypes.Unkown);
						}
						else
						{
							syntax.Context.Type = symbol.Type;
							syntax.Context.Symbol = symbol;
						}
					}
				}
				else
				{
					ScopeVariable qualScopeVar = new ScopeVariable(syntax);
					Symbol symbol = _compContext.FindDefinition(syntax.Context.Scope, qualScopeVar, syntax.FullSpan.Start);

					if (symbol == null)
					{
						ErrorSystem.AstError(syntax.FullSpan, $"Undeclared identifier {qualScopeVar.Name}");
						syntax.Context.Type = SymbolType.BuiltinType(BuiltinTypes.Unkown);
					}
					else
					{
						syntax.Context.Type = symbol.Type;
						syntax.Context.Symbol = symbol;
					}
				}
			}
		}

		protected override void VisitReturn(ReturnSyntax syntax)
		{
			base.VisitReturn(syntax);
		}

		protected override void VisitSingleAttribute(SingleAttributeSyntax syntax)
		{
			base.VisitSingleAttribute(syntax);
		}

		protected override void VisitSimpleExpression(SimpleExpressionSyntax syntax)
		{
			bool prev = _idenCheck;
			_idenCheck = true;
			base.VisitSimpleExpression(syntax);
			_idenCheck = prev;
		}

		protected override void VisitSpecialKeyword(SpecialKeywordExpressionSyntax syntax)
		{
			base.VisitSpecialKeyword(syntax);
		}

		protected override void VisitStruct(StructSyntax syntax)
		{
			_idenCheck = false;
			base.VisitStruct(syntax);
			UpdateTemplateDefinition(syntax.TemplateParameters, syntax);
			UpdateChildren(syntax.Context.Symbol);
		}

		protected override void VisitStructInitializer(StructInitializerSyntax syntax)
		{
			//base.VisitStructInitializer(syntax);
			ScopeVariable scopeVar = null;

			if (syntax.TypeIden is QualifiedNameSyntax qualifiedName)
			{
				scopeVar = new ScopeVariable(qualifiedName);
			}
			else if (syntax.TypeIden is SimpleNameSyntax simpleName)
			{
				scopeVar = new ScopeVariable();
				scopeVar.Name = Identifier.GetIdentifier(simpleName);
			}

			Symbol symbol = _compContext.FindDefinition(syntax.Context.Scope, scopeVar, -1);
			syntax.Context.Type = symbol.Type;
		}

		protected override void VisitSwitch(SwitchSyntax syntax)
		{
			base.VisitSwitch(syntax);
		}

		protected override void VisitTemplateName(TemplateNameSyntax syntax)
		{
			base.VisitTemplateName(syntax);

			// Update template instance type parameters
			TemplateInstanceName instName = syntax.Context.Identifier as TemplateInstanceName;
			for (var i = 0; i < syntax.Arguments.Count; i++)
			{
				ArgumentSyntax argument = syntax.Arguments[i];
				instName.Arguments[i].Type = argument.Context.Type;

				if (!(argument.Expression is TypeExpressionSyntax))
				{
					string funcName = $"__tavi_{_templateArgId}";
					++_templateArgId;
					argument.Context.InitFunc = funcName;
					instName.Arguments[i].ValueFunc = funcName;
				}
			}

			// Generate the symbol for the instance if none exists
			Scope scope = syntax.Context.Scope;

			// The base template symbol will always be looked for, not the specialization
			// Because of this, additional checks happen later in the analysis
			Symbol defSymbol = _compContext.FindTemplate(scope, instName, null);

			if (_compContext.FindDefinition(scope, instName, syntax.FullSpan.Start, null) == null)
			{
				instName.BaseIden = defSymbol.Identifier as TemplateDefinitionName;
				Symbol symbol = new Symbol(defSymbol.Scope, _implTemplateScope, instName, SymbolKind.TemplateInstance);
				TemplateInstanceSymbolType type = SymbolType.TemplateInstanceType(defSymbol.Scope, instName, defSymbol);
				symbol.Type = type;

				_compContext.Symbols.AddSymbol(symbol);
				syntax.Context.Symbol = symbol;
			}

			// associate template params
			//TemplateInstanceName selfInstName = selfInst.Identifier as TemplateInstanceName;
			List<Symbol> paramSyms = defSymbol.GetChildrenOfKind(SymbolKind.TemplateParam);
			for (var i = 0; i < instName.Arguments.Count; i++)
			{
				TemplateArgument argument = instName.Arguments[i];
				
				Symbol paramSym = paramSyms.Find(s =>
				{
					if (s.TemplateIdx == i)
						return true;
					return false;
				});

				argument.AssociatedBaseType = paramSym.Type;
			}
		}

		protected override void VisitTemplateParameter(TemplateParameterSyntax syntax)
		{
			_idenCheck = false;
			VisitIdentifierName(syntax.Identifier);
			if (syntax.Type != null)
			{
				VisitType(syntax.Type);
				syntax.Context.Type = syntax.Type.Context.Type;
			}
			if (syntax.SpecializedValue != null)
				VisitSimpleExpression(syntax.SpecializedValue);
			if (syntax.DefaultValue != null)
				VisitSimpleExpression(syntax.DefaultValue);

			//base.VisitTemplateParameter(syntax);
		}

		protected override void VisitTernaryExpression(TernaryExpressionSyntax syntax)
		{
			base.VisitTernaryExpression(syntax);
		}

		protected override void VisitTransmuteExpression(TransmuteExpressionSyntax syntax)
		{
			base.VisitTransmuteExpression(syntax);

			syntax.Context.Type = syntax.Type.Context.Type;
		}

		protected override void VisitTupleType(TupleTypeSyntax syntax)
		{
			base.VisitTupleType(syntax);
		}

		protected override void VisitTypedef(TypedefSyntax syntax)
		{
			base.VisitTypedef(syntax);
		}

		protected override void VisitIntrinsicExpression(IntrinsicSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
			switch (syntax.IntrinsicToken.Type)
			{
			case TokenType.Sizeof:
			case TokenType.AlignOf:
				syntax.Context.Type = SymbolType.BuiltinType(CmdLine.IsX64 ? BuiltinTypes.U64 : BuiltinTypes.USize);
				break;
			case TokenType.Typeid:
				// TODO
				break;
			}
		}

		protected override void VisitTypeof(TypeofSyntax syntax)
		{
			base.VisitTypeof(syntax);
		}

		protected override void VisitUnion(UnionSyntax syntax)
		{
			_idenCheck = false;
			base.VisitUnion(syntax);
			UpdateTemplateDefinition(syntax.TemplateParameters, syntax);
			UpdateChildren(syntax.Context.Symbol);
		}

		protected override void VisitUnitTest(UnitTestSyntax syntax)
		{
			base.VisitUnitTest(syntax);
		}

		protected override void VisitVariadicType(VariadicTypeSyntax syntax)
		{
			base.VisitVariadicType(syntax);
		}

		protected override void VisitWhile(WhileSyntax syntax)
		{
			base.VisitWhile(syntax);
		}

		protected override void VisitCompilerAtAttribute(CompilerAtAttributeSyntax syntax)
		{
			_idenCheck = false;
			base.VisitCompilerAtAttribute(syntax);
			_idenCheck = true;
		}

		protected override void VisitTypeExpression(TypeExpressionSyntax syntax)
		{
			base.VisitTypeExpression(syntax);
			syntax.Context.Type = syntax.Type.Context.Type;
		}

		protected override void VisitType(TypeSyntax syntax)
		{
			base.VisitType(syntax);

			// update AST Types
			switch (syntax)
			{
			case IdentifierTypeSyntax identifierTypeSyntax:
			{
				if (syntax.Context.Type is UnknownSymbolType unknown)
				{
					// Check if the unknown type is a param type declared in the an impl block
					Symbol sym = null;
					if (_implIden != null)
					{
						sym = _compContext.FindType(_implTemplateScope, unknown.Identifier);
					}

					if (sym == null)
						sym = _compContext.FindType(syntax.Context.Scope, unknown.Identifier);
					identifierTypeSyntax.Context.Type = sym.Type;
				}
				else if (syntax.Context.Type is SelfSymbolType selfType)
				{
					Symbol sym = _compContext.FindType(syntax.Context.Scope, selfType.Identifier);
					identifierTypeSyntax.Context.Type = sym.Type;
				}
				break;
			}
			case PointerTypeSyntax pointerTypeSyntax:
			{
				SymbolType baseType = pointerTypeSyntax.BaseType.Context.Type;
				PointerSymbolType type = SymbolType.PointerType(baseType);
				pointerTypeSyntax.Context.Type = type;
				break;
			}
			case NullableTypeSyntax nullableTypeSyntax:
			{
				SymbolType baseType = nullableTypeSyntax.BaseType.Context.Type;
				NullableSymbolType type = SymbolType.NullableType(baseType);
				nullableTypeSyntax.Context.Type = type;
				break;
			}
			case ArrayTypeSyntax arrayTypeSyntax:
			{
				SymbolType baseType = arrayTypeSyntax.BaseType.Context.Type;
				ArraySymbolType type = SymbolType.ArrayType(baseType);
				arrayTypeSyntax.Context.Type = type;
				// TODO: Handle array size
				break;
			}
			case ReferenceTypeSyntax referenceTypeSyntax:
			{
				SymbolType baseType = referenceTypeSyntax.BaseType.Context.Type;
				ReferenceSymbolType type = SymbolType.ReferenceType(baseType);
				referenceTypeSyntax.Context.Type = type;
				break;
			}
			case TupleTypeSyntax tupleTypeSyntax:
			{
				TupleSymbolType type = new TupleSymbolType();
				foreach (TupleSubTypeSyntax subType in tupleTypeSyntax.SubTypes)
				{
					type.SubTypes.Add(subType.BaseType.Context.Type);
				}
				tupleTypeSyntax.Context.Type = type;
				break;
			}
			case AttributedTypeSyntax attributedTypeSyntax:
			{
				SymbolType type = attributedTypeSyntax.BaseType.Context.Type;
				SemanticAttributes semAttribs = attributedTypeSyntax.Context.Attribs;

				TypeAttributes typeAttributes = TypeAttributes.None;
				if ((semAttribs & SemanticAttributes.Const) != SemanticAttributes.None)
					typeAttributes |= TypeAttributes.Const;
				if ((semAttribs & SemanticAttributes.Immutable) != SemanticAttributes.None)
					typeAttributes |= TypeAttributes.Immutable;
				if ((semAttribs & SemanticAttributes.Mutable) != SemanticAttributes.None)
					typeAttributes |= TypeAttributes.Mutable;

				type = SymbolType.AttributedType(type, typeAttributes);
				type.GetUpdatedType(_implTemplateScope, syntax.Context.Scope, _compContext);

				attributedTypeSyntax.Context.Type = type;
				break;
			}
			case InlineStructTypeSyntax inlineStructSyntax:
				inlineStructSyntax.Context.Type = inlineStructSyntax.StructSyntax.Context.Type;
				break;
			case InlineUnionTypeSyntax inlineUnionSyntax:
				inlineUnionSyntax.Context.Type = inlineUnionSyntax.UnionSyntax.Context.Type;
				break;
			case BitFieldTypeSyntax bitFieldSyntax:
			{
				BitFieldSymbolType bitfieldType = syntax.Context.Type as BitFieldSymbolType;

				SymbolType baseType = bitFieldSyntax.BaseType.Context.Type;
				SymbolType type = SymbolType.BitFieldType(baseType, bitfieldType.Bits);
				bitFieldSyntax.Context.Type = type;
				break;
			}
			}
			
		}

		SymbolType CombineTypes(TextSpan span, SymbolType left, SymbolType right, string op = null)
		{
			{
				if (left is BuiltinSymbolType builtinLeft && right is BuiltinSymbolType builtinRight)
				{
					return CombineBuiltin(builtinLeft, builtinRight);
				}
			}
			{
				if (left is PointerSymbolType pointerLeft && right is BuiltinSymbolType builtinRight)
				{
					BuiltinTypes baseType = builtinRight.GetBaseBuiltinType();
					if (baseType == BuiltinTypes.Bool || baseType == BuiltinTypes.Char || baseType == BuiltinTypes.String || baseType == BuiltinTypes.F32)
					{
						// No default operator, search for existing operator
					}
					else
					{
						return pointerLeft;
					}
				}
				if (left is BuiltinSymbolType builtinLeft && right is PointerSymbolType pointerRight)
				{

				}
			}

			return null;
		}

		BuiltinSymbolType CombineBuiltin(BuiltinSymbolType left, BuiltinSymbolType right)
		{
			// TODO: Incomplete
			BuiltinTypes lbase = left.GetBaseBuiltinType();
			BuiltinTypes rbase = right.GetBaseBuiltinType();

			if (lbase == BuiltinTypes.Unkown || rbase == BuiltinTypes.Unkown)
				return SymbolType.BuiltinType(BuiltinTypes.Unkown);

			if (rbase == BuiltinTypes.String || lbase == BuiltinTypes.String)
			{
				return SymbolType.BuiltinType(BuiltinTypes.String);
			}

			if (rbase == BuiltinTypes.Bool || lbase == BuiltinTypes.Bool)
			{
				return SymbolType.BuiltinType(BuiltinTypes.Bool);
			}

			int lbytes = left.GetNumBytes();
			int rbytes = right.GetNumBytes();
			int bytes = lbytes > rbytes ? lbytes : rbytes;

			if (lbase == BuiltinTypes.F32 || rbase == BuiltinTypes.F32)
				return SymbolType.BuiltinType(bytes, BuiltinTypes.F32);

			if (lbase == BuiltinTypes.U8 || rbase == BuiltinTypes.U8)
				return SymbolType.BuiltinType(bytes, BuiltinTypes.U8);

			return SymbolType.BuiltinType(bytes, BuiltinTypes.I8);
		}

		bool IsAssignToken(TokenType type)
		{
			switch (type)
			{
			case TokenType.EqualsEquals:
			case TokenType.ExclaimEquals:
			case TokenType.Greater:
			case TokenType.GreaterEquals:
			case TokenType.Less:
			case TokenType.LessEquals:
				return true;
			default:
				return false;
			}
		}

		void ProcessTemplateInstance(TemplateNameSyntax inst, Scope scope, SyntaxNode syntax)
		{
			TemplateInstanceName instName = inst.Context.Identifier as TemplateInstanceName;

			Symbol symbol = _compContext.FindDefinition(syntax.Context.Scope, instName, syntax.FullSpan.Start, null);

			Symbol templateSymbol = _compContext.FindTemplate(syntax.Context.Scope, instName, scope);
			if (symbol == null)
			{

				symbol = new Symbol(templateSymbol.Scope, _implTemplateScope, instName, SymbolKind.TemplateInstance);
				symbol.Location = syntax.FullSpan.Start;

				ScopeVariable instVar = new ScopeVariable(templateSymbol.Scope, instName);
				TemplateInstanceSymbolType type = SymbolType.TemplateInstanceType(instVar, templateSymbol);
				symbol.Type = type;

				_compContext.Symbols.AddSymbol(symbol);
			}

			// update instName's BaseIden
			TemplateDefinitionName actDef = templateSymbol.Identifier as TemplateDefinitionName;
			for (var i = 0; i < actDef.Parameters.Count; i++)
			{
				TemplateParameter parameter = actDef.Parameters[i];
				instName.BaseIden.Parameters[i] = parameter;
			}

			// TODO: Update entries for template in symbol table

			syntax.Context.Symbol = symbol;
			syntax.Context.Type = symbol.Type;
		}

		void UpdateTemplateDefinition(List<TemplateParameterSyntax> templateParams, SyntaxNode syntax)
		{
			TemplateDefinitionName defName = syntax.Context.Identifier as TemplateDefinitionName;

			for (var i = 0; i < templateParams.Count; i++)
			{
				TemplateParameterSyntax param = templateParams[i];
				if (param.Type == null) // Type parameter
				{
					SymbolType specialization = param.SpecializedValue?.Context.Type;
					SymbolType defaultT = param.DefaultValue?.Context.Type;
					
					defName.Parameters[i].TypeSpecialization = specialization;
					defName.Parameters[i].TypeDefault = defaultT;
				}
				else
				{
					SimpleExpressionSyntax specialization = param.SpecializedValue;
					SimpleExpressionSyntax defaultV = param.DefaultValue;

					defName.Parameters[i].ValueSpecializationExpr = specialization;
					defName.Parameters[i].ValueDefaultExpr = defaultV;
				}
				defName.Parameters[i].Type = param.Context.Type;
			}
		}

		void UpdateChildren(Symbol symbol)
		{
			Scope scope = new Scope(symbol.ScopeVar);
			Scope nonSpecializedScope = new Scope(symbol.Scope);
			nonSpecializedScope.Names.Add(symbol.Identifier.GetBaseIdentifier());
			SymbolTable table = _compContext.Symbols.FindTable(nonSpecializedScope);

			if (table == null)
				return;

			foreach (KeyValuePair<Identifier, List<Symbol>> pair in table.Symbols)
			{
				List<Symbol> children = pair.Value;
				foreach (Symbol child in children)
				{
					if (child.Scope == scope)
					{
						// If the child is part of a templated type, the child can have an assigned parent
						// When this is the case, a child which is actually part of a specialization,
						// teh child should be removed from the non specialized type and added to the specialized one
						if (child.Parent != null)
						{
							child.Parent.Children.Remove(child);
						}

						symbol.Children.Add(child);
						child.Parent = symbol;
					}
				}
			}
		}

	}
}
