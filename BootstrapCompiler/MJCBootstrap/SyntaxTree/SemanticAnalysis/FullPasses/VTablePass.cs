﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.SyntaxTree.SemanticAnalysis.FullPasses
{
	class VTablePass : ISemanticPass
	{
		private bool _inImpl = false;
		private ScopeVariable _vtableScopeVar;
		private Dictionary<ScopeVariable, VTable> _vTables;
		private CompilerContext _compContext;

		public override void Setup(CompilerContext compilerContext)
		{
			_compContext = compilerContext;
		}

		public override void Visit(SyntaxTree tree)
		{
			_vTables = new Dictionary<ScopeVariable, VTable>();
			base.Visit(tree);
			tree.VTables = _vTables;
			_vTables = null;
		}

		protected override void VisitPackageDirective(PackageDirectiveSyntax syntax)
		{
			//base.VisitPackageDirective(syntax);
		}

		protected override void VisitModuleDirective(ModuleDirectiveSyntax syntax)
		{
			base.VisitModuleDirective(syntax);
		}

		protected override void VisitQualifiedName(QualifiedNameSyntax syntax)
		{
			base.VisitQualifiedName(syntax);
		}

		protected override void VisitIdentifierName(IdentifierNameSyntax syntax)
		{
			base.VisitIdentifierName(syntax);
		}

		protected override void VisitTemplateName(TemplateNameSyntax syntax)
		{
			base.VisitTemplateName(syntax);
		}

		protected override void VisitNameList(NameListSyntax syntax)
		{
			base.VisitNameList(syntax);
		}

		protected override void VisitAttributes(AttributeSyntax syntax)
		{
			base.VisitAttributes(syntax);
		}

		protected override void VisitAtAttribute(AtAttributeSyntax syntax)
		{
			base.VisitAtAttribute(syntax);
		}

		protected override void VisitCompilerAtAttribute(CompilerAtAttributeSyntax syntax)
		{
			base.VisitCompilerAtAttribute(syntax);
		}

		protected override void VisitSingleAttribute(SingleAttributeSyntax syntax)
		{
			base.VisitSingleAttribute(syntax);
		}

		protected override void VisitArgument(ArgumentSyntax syntax)
		{
			base.VisitArgument(syntax);
		}

		protected override void VisitParameter(ParameterSyntax syntax)
		{
			base.VisitParameter(syntax);
		}

		protected override void VisitTemplateParameter(TemplateParameterSyntax syntax)
		{
			base.VisitTemplateParameter(syntax);
		}

		protected override void VisitType(TypeSyntax syntax)
		{
			base.VisitType(syntax);
		}

		protected override void VisitBuiltinType(BuiltinTypeSyntax syntax)
		{
			base.VisitBuiltinType(syntax);
		}

		protected override void VisitIdentifierType(IdentifierTypeSyntax syntax)
		{
			base.VisitIdentifierType(syntax);
		}

		protected override void VisitPointerType(PointerTypeSyntax syntax)
		{
			base.VisitPointerType(syntax);
		}

		protected override void VisitArrayType(ArrayTypeSyntax syntax)
		{
			base.VisitArrayType(syntax);
		}

		protected override void VisitAttributedType(AttributedTypeSyntax syntax)
		{
			base.VisitAttributedType(syntax);
		}

		protected override void VisitTupleType(TupleTypeSyntax syntax)
		{
			base.VisitTupleType(syntax);
		}

		protected override void VisitNullableType(NullableTypeSyntax syntax)
		{
			base.VisitNullableType(syntax);
		}

		protected override void VisitVariadicType(VariadicTypeSyntax syntax)
		{
			base.VisitVariadicType(syntax);
		}

		protected override void VisitTypeof(TypeofSyntax syntax)
		{
			base.VisitTypeof(syntax);
		}

		protected override void VisitInlineStruct(InlineStructTypeSyntax syntax)
		{
			base.VisitInlineStruct(syntax);
		}

		protected override void VisitInlineUnion(InlineUnionTypeSyntax syntax)
		{
			base.VisitInlineUnion(syntax);
		}

		protected override void VisitDelegate(DelegateSyntax syntax)
		{
			base.VisitDelegate(syntax);
		}

		protected override void VisitBlockStatement(BlockStatementSyntax syntax)
		{
			base.VisitBlockStatement(syntax);
		}

		protected override void VisitNamespace(NamespaceSyntax syntax)
		{
			base.VisitNamespace(syntax);
		}

		protected override void VisitImport(ImportSyntax syntax)
		{
			base.VisitImport(syntax);
		}

		protected override void VisitReceiver(ReceiverSyntax syntax)
		{
			base.VisitReceiver(syntax);
		}

		protected override void VisitFunction(FunctionSyntax syntax)
		{
			base.VisitFunction(syntax);

			if (_inImpl)
			{
				VTable table = _vTables[_vtableScopeVar];

				VTableMethod method = new VTableMethod(syntax.Context.Symbol);
				table.AddMethod(method);
			}
		}

		protected override void VisitTemplateConstraint(TemplateConstraintSyntax syntax)
		{
			base.VisitTemplateConstraint(syntax);
		}

		protected override void VisitAlias(AliasSyntax syntax)
		{
			base.VisitAlias(syntax);
		}

		protected override void VisitTypedef(TypedefSyntax syntax)
		{
			base.VisitTypedef(syntax);
		}

		protected override void VisitIfElse(IfElseSyntax syntax)
		{
			base.VisitIfElse(syntax);
		}

		protected override void VisitIf(IfSyntax syntax)
		{
			base.VisitIf(syntax);
		}

		protected override void VisitElse(ElseSyntax syntax)
		{
			base.VisitElse(syntax);
		}

		protected override void VisitWhile(WhileSyntax syntax)
		{
			base.VisitWhile(syntax);
		}

		protected override void VisitDoWhile(DoWhileSyntax syntax)
		{
			base.VisitDoWhile(syntax);
		}

		protected override void VisitFor(ForSyntax syntax)
		{
			base.VisitFor(syntax);
		}

		protected override void VisitForeach(ForeachSyntax syntax)
		{
			base.VisitForeach(syntax);
		}

		protected override void VisitSwitch(SwitchSyntax syntax)
		{
			base.VisitSwitch(syntax);
		}

		protected override void VisitCase(CaseSyntax syntax)
		{
			base.VisitCase(syntax);
		}

		protected override void VisitDefaultCase(DefaultCaseSyntax syntax)
		{
			base.VisitDefaultCase(syntax);
		}

		protected override void VisitFallthrough(FallthroughSyntax syntax)
		{
			base.VisitFallthrough(syntax);
		}

		protected override void VisitContinue(ContinueSyntax syntax)
		{
			base.VisitContinue(syntax);
		}

		protected override void VisitBreak(BreakSyntax syntax)
		{
			base.VisitBreak(syntax);
		}

		protected override void VisitReturn(ReturnSyntax syntax)
		{
			base.VisitReturn(syntax);
		}

		protected override void VisitDefer(DeferSyntax syntax)
		{
			base.VisitDefer(syntax);
		}

		protected override void VisitGoto(GotoSyntax syntax)
		{
			base.VisitGoto(syntax);
		}

		protected override void VisitLabel(LabelSyntax syntax)
		{
			base.VisitLabel(syntax);
		}

		protected override void VisitImplInterfaceFieldDef(ImplInterfaceFieldDefSyntax syntax)
		{
			base.VisitImplInterfaceFieldDef(syntax);
		}

		protected override void VisitImplInterface(ImplInterfaceSyntax syntax)
		{
			base.VisitImplInterface(syntax);
		}

		protected override void VisitImpl(ImplSyntax syntax)
		{
			_vtableScopeVar = new ScopeVariable(syntax.Type.Context.Scope, syntax.Type.Context.Identifier);
			_vtableScopeVar = _vtableScopeVar.GetBaseTemplate();
			if (!_vTables.ContainsKey(_vtableScopeVar))
			{
				// TODO: Correct mangled extensions
				string mangled = "_M" + NameMangling.MangleScopeVar(_vtableScopeVar) + "TS";
				VTable vTable = new VTable(_vtableScopeVar, mangled);
				_vTables.Add(_vtableScopeVar, vTable);
				_compContext.AddVTable(vTable);
			}

			_inImpl = true;
			base.VisitImpl(syntax);
			_inImpl = false;
		}

		protected override void VisitStruct(StructSyntax syntax)
		{
			base.VisitStruct(syntax);

			ScopeVariable scopeVar = syntax.Context.Symbol.ScopeVar;
			if (!_vTables.ContainsKey(scopeVar))
			{
				string mangled = "_M" + NameMangling.MangleScopeVar(syntax.Context.Symbol.ScopeVar) + "TS";
				VTable vTable = new VTable(scopeVar, mangled);
				_vTables.Add(scopeVar, vTable);
				_compContext.AddVTable(vTable);
			}
		}

		protected override void VisitInterface(InterfaceSyntax syntax)
		{
			base.VisitInterface(syntax);

			ScopeVariable scopeVar = syntax.Context.Symbol.ScopeVar;
			if (!_vTables.ContainsKey(scopeVar))
			{
				string mangled = "_M" + NameMangling.MangleScopeVar(syntax.Context.Symbol.ScopeVar) + "TS";
				VTable vTable = new VTable(scopeVar, mangled);
				_vTables.Add(scopeVar, vTable);
				_compContext.AddVTable(vTable);
			}
		}

		protected override void VisitUnion(UnionSyntax syntax)
		{
			base.VisitUnion(syntax);

			ScopeVariable scopeVar = syntax.Context.Symbol.ScopeVar;
			if (!_vTables.ContainsKey(scopeVar))
			{
				string mangled = "_M" + NameMangling.MangleScopeVar(syntax.Context.Symbol.ScopeVar) + "TU";
				VTable vTable = new VTable(scopeVar, mangled);
				_vTables.Add(scopeVar, vTable);
				_compContext.AddVTable(vTable);
			}
		}

		protected override void VisitEnum(EnumSyntax syntax)
		{
			base.VisitEnum(syntax);

			ScopeVariable scopeVar = syntax.Context.Symbol.ScopeVar;
			if (!_vTables.ContainsKey(scopeVar))
			{
				string mangled = "_M" + NameMangling.MangleScopeVar(syntax.Context.Symbol.ScopeVar) + "TE";
				VTable vTable = new VTable(scopeVar, mangled);
				_vTables.Add(scopeVar, vTable);
				_compContext.AddVTable(vTable);
			}
		}

		protected override void VisitUnitTest(UnitTestSyntax syntax)
		{
			base.VisitUnitTest(syntax);
		}

		protected override void VisitCompileCondition(CompileConditionalSyntax syntax)
		{
			base.VisitCompileCondition(syntax);
		}

		protected override void VisitAssert(AssertSyntax syntax)
		{
			base.VisitAssert(syntax);
		}

		protected override void VisitSimpleExpression(SimpleExpressionSyntax syntax)
		{
			base.VisitSimpleExpression(syntax);
		}

		protected override void VisitExpression(ExpressionSyntax syntax)
		{
			base.VisitExpression(syntax);
		}

		protected override void VisitCommaExpression(CommaExpressionSyntax syntax)
		{
			base.VisitCommaExpression(syntax);
		}

		protected override void VisitDeclaration(DeclarationSyntax syntax)
		{
			base.VisitDeclaration(syntax);
		}

		protected override void VisitTernaryExpression(TernaryExpressionSyntax syntax)
		{
			base.VisitTernaryExpression(syntax);
		}

		protected override void VisitBinaryExpression(BinaryExpressionSyntax syntax)
		{
			base.VisitBinaryExpression(syntax);
		}

		protected override void VisitAssignExpression(AssignExpressionSyntax syntax)
		{
			base.VisitAssignExpression(syntax);
		}

		protected override void VisitPrefixExpression(PrefixExpressionSyntax syntax)
		{
			base.VisitPrefixExpression(syntax);
		}

		protected override void VisitPostfixExpression(PostfixExpressionSyntax syntax)
		{
			base.VisitPostfixExpression(syntax);
		}

		protected override void VisitBracketedExpression(BracketedExpressionSyntax syntax)
		{
			base.VisitBracketedExpression(syntax);
		}

		protected override void VisitCastExpression(CastExpressionSyntax syntax)
		{
			base.VisitCastExpression(syntax);
		}

		protected override void VisitTransmuteExpression(TransmuteExpressionSyntax syntax)
		{
			base.VisitTransmuteExpression(syntax);
		}

		protected override void VisitCCastExpression(CCastExpressionSyntax syntax)
		{
			base.VisitCCastExpression(syntax);
		}

		protected override void VisitIndexSliceExpression(IndexSliceExpressionSyntax syntax)
		{
			base.VisitIndexSliceExpression(syntax);
		}

		protected override void VisitFunctionCall(FunctionCallExpressionSyntax syntax)
		{
			base.VisitFunctionCall(syntax);
		}

		protected override void VisitMixinExpression(MixinExpressionSyntax syntax)
		{
			base.VisitMixinExpression(syntax);
		}

		protected override void VisitIntrinsicExpression(IntrinsicSyntax syntax)
		{
			base.VisitIntrinsicExpression(syntax);
		}

		protected override void VisitIsExpression(IsExpressionSyntax syntax)
		{
			base.VisitIsExpression(syntax);
		}

		protected override void VisitNewExpression(NewExpressionSyntax syntax)
		{
			base.VisitNewExpression(syntax);
		}

		protected override void VisitDeleteExpression(DeleteExpressionSyntax syntax)
		{
			base.VisitDeleteExpression(syntax);
		}

		protected override void VisitLambda(ClosureSyntax syntax)
		{
			base.VisitLambda(syntax);
		}

		protected override void VisitLiteral(LiteralExpressionSyntax syntax)
		{
			base.VisitLiteral(syntax);
		}

		protected override void VisitArrayLiteral(ArrayLiteralExpressionSyntax syntax)
		{
			base.VisitArrayLiteral(syntax);
		}

		protected override void VisitAssocArrayLiteral(AssocArrayLiteralExpressionSyntax syntax)
		{
			base.VisitAssocArrayLiteral(syntax);
		}

		protected override void VisitStructInitializer(StructInitializerSyntax syntax)
		{
			base.VisitStructInitializer(syntax);
		}

		protected override void VisitSpecialKeyword(SpecialKeywordExpressionSyntax syntax)
		{
			base.VisitSpecialKeyword(syntax);
		}

		protected override void VisitBitField(BitFieldTypeSyntax syntax)
		{
			base.VisitBitField(syntax);
		}

		protected override void VisitTypeExpression(TypeExpressionSyntax syntax)
		{
			base.VisitTypeExpression(syntax);
		}
	}
}
