﻿using System.Collections;
using System.Collections.Generic;
using MJC.General;

namespace MJC.SyntaxTree.SemanticAnalysis.PartialPasses
{
	class TableGenPass : ISemanticPass
	{
		public SymbolTable Symbols;
		public FileImports FileImports;
		private bool _inFunction;
		private bool _inAggregate;
		private ScopeVariable _implIden;
		private Scope _implScope;
		private int _templateParamIdx;

		public TableGenPass(CompilerContext globalTable)
		{
			Symbols = globalTable.Symbols;
		}

		public override void Visit(SyntaxTree tree)
		{
			FileImports = new FileImports();
			base.Visit(tree);

			tree.Symbols = Symbols;
			tree.FileImports = FileImports;
		}

		protected override void VisitAlias(AliasSyntax syntax)
		{
			base.VisitAlias(syntax);
			
			SymbolKind kind = SymbolKind.TypeAlias;
			Identifier iden = new IdentifierName(syntax.Name.Identifier.Text);
			Symbol symbol = new Symbol(syntax.Context.Scope,_implScope, iden, kind);
			symbol.Location = syntax.FullSpan.Start;
			symbol.Type = syntax.Type.Context.Type;

			Symbols.AddSymbol(symbol);
			syntax.Context.Symbol = symbol;
		}
		
		protected override void VisitArgument(ArgumentSyntax syntax)
		{
			base.VisitArgument(syntax);
		}

		protected override void VisitArrayLiteral(ArrayLiteralExpressionSyntax syntax)
		{
			base.VisitArrayLiteral(syntax);
		}

		protected override void VisitArrayType(ArrayTypeSyntax syntax)
		{
			base.VisitArrayType(syntax);
		}

		protected override void VisitAssert(AssertSyntax syntax)
		{
			base.VisitAssert(syntax);
		}

		protected override void VisitAssignExpression(AssignExpressionSyntax syntax)
		{
			base.VisitAssignExpression(syntax);
		}

		protected override void VisitAssocArrayLiteral(AssocArrayLiteralExpressionSyntax syntax)
		{
			base.VisitAssocArrayLiteral(syntax);
		}

		protected override void VisitAtAttribute(AtAttributeSyntax syntax)
		{
			base.VisitAtAttribute(syntax);
		}
		
		protected override void VisitAttributedType(AttributedTypeSyntax syntax)
		{
			base.VisitAttributedType(syntax);
		}

		protected override void VisitAttributes(AttributeSyntax syntax)
		{
			base.VisitAttributes(syntax);
		}

		protected override void VisitBinaryExpression(BinaryExpressionSyntax syntax)
		{
			base.VisitBinaryExpression(syntax);
		}

		protected override void VisitBlockStatement(BlockStatementSyntax syntax)
		{
			base.VisitBlockStatement(syntax);
		}

		protected override void VisitBracketedExpression(BracketedExpressionSyntax syntax)
		{
			base.VisitBracketedExpression(syntax);
		}

		protected override void VisitBreak(BreakSyntax syntax)
		{
			base.VisitBreak(syntax);
		}

		protected override void VisitCase(CaseSyntax syntax)
		{
			base.VisitCase(syntax);
		}

		protected override void VisitCastExpression(CastExpressionSyntax syntax)
		{
			base.VisitCastExpression(syntax);
		}

		protected override void VisitCCastExpression(CCastExpressionSyntax syntax)
		{
			base.VisitCCastExpression(syntax);
		}

		protected override void VisitCommaExpression(CommaExpressionSyntax syntax)
		{
			base.VisitCommaExpression(syntax);
		}

		protected override void VisitCompilationUnit(CompilationUnitSyntax syntax)
		{
			base.VisitCompilationUnit(syntax);
		}

		protected override void VisitCompileCondition(CompileConditionalSyntax syntax)
		{
			base.VisitCompileCondition(syntax);
		}
		
		protected override void VisitContinue(ContinueSyntax syntax)
		{
			base.VisitContinue(syntax);
		}
		
		private delegate Symbol DeclProcessDel(Identifier iden);
		protected override void VisitDeclaration(DeclarationSyntax syntax)
		{
			base.VisitDeclaration(syntax);

			DeclProcessDel process = iden =>
			{
				SymbolType type = null;
				if (syntax.Type != null)
					type = syntax.Type.Context.Type;
				else if (syntax.Initializer != null)
				{
					TypeofSymbolType typeofType = new TypeofSymbolType();
					typeofType.Identifier = syntax.Initializer;
					type = typeofType;
				}
				else
				{
					ErrorSystem.AstError(syntax.FullSpan, $"Variable '{iden}' in scope '{syntax.Context.Scope}' declared without a type or expression to infer type from");
				}
				
				SymbolKind kind = _inFunction ? SymbolKind.LocalVar : SymbolKind.Variable;
				
				Symbol symbol = new Symbol(syntax.Context.Scope,_implScope, iden, kind);
				symbol.Location = syntax.FullSpan.Start;
				symbol.Type = type;
				Symbols.AddSymbol(symbol);
				return symbol;
			};

			if (syntax.Names is NameListSyntax nameList)
			{
				syntax.Context.SymbolList = new List<Symbol>();
				foreach (Identifier iden in syntax.Context.IdentifierList)
				{
					syntax.Context.SymbolList.Add(process(iden));
				}
			}
			else
			{
				syntax.Context.Symbol = process(syntax.Context.Identifier);
			}
		}

		protected override void VisitDefaultCase(DefaultCaseSyntax syntax)
		{
			base.VisitDefaultCase(syntax);
		}

		protected override void VisitDefer(DeferSyntax syntax)
		{
			base.VisitDefer(syntax);
		}

		protected override void VisitDelegate(DelegateSyntax syntax)
		{
			bool prev = _inFunction;
			_inFunction = true;
			base.VisitDelegate(syntax);
			_inFunction = prev;

			SymbolKind kind = SymbolKind.Delegate;
			Identifier iden = syntax.Context.Identifier;
			
			Symbol symbol = new Symbol(syntax.Context.Scope,_implScope, iden, kind);
			symbol.Location = syntax.FullSpan.Start;
			
			SymbolType returnType = syntax.ReturnType?.Context.Type;

			List<SymbolType> paramTypes = null;
			if (syntax.Parameters != null)
			{
				paramTypes = new List<SymbolType>();
				for (var i = 0; i < syntax.Parameters.Count; i++)
				{
					var parameter = syntax.Parameters[i];
					SymbolType paramType = parameter.Type.Context.Type;
					paramTypes.Add(paramType);
				}
			}

			DelegateSymbolType delType = SymbolType.DelegateType(new ScopeVariable(syntax.Context.Scope, iden), paramTypes, returnType);
			symbol.Type = delType;
			delType.Symbol = symbol;

			Symbols.AddSymbol(symbol);
			syntax.Context.Symbol = symbol;
		}

		protected override void VisitDeleteExpression(DeleteExpressionSyntax syntax)
		{
			base.VisitDeleteExpression(syntax);
		}
		
		protected override void VisitDoWhile(DoWhileSyntax syntax)
		{
			base.VisitDoWhile(syntax);
		}

		protected override void VisitEnum(EnumSyntax syntax)
		{
			_templateParamIdx = 0;
			base.VisitEnum(syntax);

			Identifier iden = syntax.Context.Identifier;
			Symbol symbol = new Symbol(syntax.Context.Scope.GetBaseScope(),_implScope, iden, SymbolKind.Enum);
			symbol.Location = syntax.FullSpan.Start;

			// Update type
			
			BuiltinSymbolType baseType = syntax.Type?.Context.Type as BuiltinSymbolType;
			if (baseType == null)
				baseType = SymbolType.BuiltinType(BuiltinTypes.I32);

			EnumSymbolType enumType = SymbolType.EnumType(new ScopeVariable(syntax.Context.Scope, iden), baseType);
			syntax.Context.Type = enumType;
			symbol.Type = enumType;
			enumType.Symbol = symbol;
			
			foreach (EnumMemberSyntax member in syntax.Members)
			{
				Symbol memberSymbol = new Symbol(member.Context.Scope,_implScope, new IdentifierName(member.Name.Identifier.Text), SymbolKind.EnumMember);
				memberSymbol.Location = member.FullSpan.Start;

				memberSymbol.Type = member.TupleType?.Context.Type;

				Symbols.AddSymbol(memberSymbol);
				member.Context.Symbol = memberSymbol;
				member.Context.Type = memberSymbol.Type;
			}

			Symbols.AddSymbol(symbol);
			syntax.Context.Symbol = symbol;
		}

		protected override void VisitExpression(ExpressionSyntax syntax)
		{
			base.VisitExpression(syntax);
		}
		
		protected override void VisitFallthrough(FallthroughSyntax syntax)
		{
			base.VisitFallthrough(syntax);
		}

		protected override void VisitFor(ForSyntax syntax)
		{
			base.VisitFor(syntax);
		}

		protected override void VisitForeach(ForeachSyntax syntax)
		{
			base.VisitForeach(syntax);
		}

		protected override void VisitFunction(FunctionSyntax syntax)
		{
			bool prev = _inFunction;
			_inFunction = true;
			base.VisitFunction(syntax);
			_inFunction = prev;
			
			SymbolKind kind = syntax.Receiver != null ? SymbolKind.Method : SymbolKind.Function;
			Identifier iden = syntax.Context.Identifier;
			Symbol symbol = new Symbol(syntax.Context.Scope.GetBaseScope(),_implScope, iden, kind);
			symbol.Location = syntax.FullSpan.Start;

			SymbolType receiverType = syntax.Receiver?.Context.Type;
			SymbolType returnType = syntax.ReturnType?.Context.Type;

			List<SymbolType> paramTypes = null;
			BitArray hasDefaultValues = null;
			if (syntax.Parameters != null)
			{
				paramTypes = new List<SymbolType>();
				hasDefaultValues = new BitArray(syntax.Parameters.Count);
				for (var i = 0; i < syntax.Parameters.Count; i++)
				{
					var parameter = syntax.Parameters[i];
					SymbolType paramType = parameter.Type?.Context.Type;

					if (parameter.VariadicToken != null)
					{
						SymbolType subType = SymbolType.BuiltinType(BuiltinTypes.Any);
						paramType = SymbolType.VariadicType(subType);
					}
					
					paramTypes.Add(paramType);
					parameter.Context.Type = paramType;

					if (parameter.EqualsToken != null)
						hasDefaultValues[i] = true;

					// Add parameter to symbol table
					Symbol paramSym = new Symbol(parameter.Context.Scope,_implScope, parameter.Context.Identifier, SymbolKind.Variable);
					paramSym.Type = paramType;

					Symbols.AddSymbol(paramSym);
				}
			}

			FunctionSymbolType funcType = SymbolType.FunctionType(receiverType, paramTypes, returnType);
			funcType.HasDefaultValues = hasDefaultValues;
			symbol.Type = funcType;
			syntax.Context.FuncType = funcType;

			Symbols.AddSymbol(symbol);
			syntax.Context.Symbol = symbol;
		}

		protected override void VisitFunctionCall(FunctionCallExpressionSyntax syntax)
		{
			base.VisitFunctionCall(syntax);
		}

		protected override void VisitTemplateConstraint(TemplateConstraintSyntax syntax)
		{
			base.VisitTemplateConstraint(syntax);
		}

		protected override void VisitGoto(GotoSyntax syntax)
		{
			base.VisitGoto(syntax);
		}

		protected override void VisitIdentifierName(IdentifierNameSyntax syntax)
		{
			base.VisitIdentifierName(syntax);
		}

		protected override void VisitIdentifierType(IdentifierTypeSyntax syntax)
		{
			base.VisitIdentifierType(syntax);
		}

		protected override void VisitIf(IfSyntax syntax)
		{
			base.VisitIf(syntax);
		}

		protected override void VisitImport(ImportSyntax syntax)
		{
			//base.VisitImport(syntax);

			string module = syntax.ImportedModule.ToString();
			List<string> symbols = null;
			if (syntax.Symbols != null)
			{
				symbols = new List<string>();
				foreach (ImportSymbolSyntax symbol in syntax.Symbols)
				{
					symbols.Add(symbol.Symbol.ToString());
				}
			}
			FileImports.AddImport(syntax.Context.Scope, syntax.FullSpan.Start, module, symbols);
		}

		protected override void VisitImpl(ImplSyntax syntax)
		{
			_implScope = syntax.Context.Scope;
			Scope scope = new Scope(_implScope.GetBaseScope());
			_implIden = new ScopeVariable(scope, syntax.Context.Identifier);
			_templateParamIdx = 0;

			base.VisitImpl(syntax);

			_implIden = null;
			_implScope = null;
		}

		protected override void VisitIndexSliceExpression(IndexSliceExpressionSyntax syntax)
		{
			base.VisitIndexSliceExpression(syntax);
		}

		protected override void VisitInterface(InterfaceSyntax syntax)
		{
			bool prev = _inAggregate;
			_inAggregate = true;
			base.VisitInterface(syntax);
			_inAggregate = prev;
			
			Identifier iden = syntax.Context.Identifier;
			Symbol symbol = new Symbol(syntax.Context.Scope.GetBaseScope(),_implScope, iden, SymbolKind.Interface);
			symbol.Location = syntax.FullSpan.Start;
			InterfaceSymbolType type = SymbolType.InterfaceType(new ScopeVariable(syntax.Context.Scope, iden));
			symbol.Type = type;
			type.Symbol = symbol;

			Symbols.AddSymbol(symbol);
			syntax.Context.Symbol = symbol;
		}

		protected override void VisitIsExpression(IsExpressionSyntax syntax)
		{
			base.VisitIsExpression(syntax);
		}

		protected override void VisitLabel(LabelSyntax syntax)
		{
			base.VisitLabel(syntax);
		}

		protected override void VisitLambda(ClosureSyntax syntax)
		{
			base.VisitLambda(syntax);
		}

		protected override void VisitLiteral(LiteralExpressionSyntax syntax)
		{
			base.VisitLiteral(syntax);
		}

		protected override void VisitMixinExpression(MixinExpressionSyntax syntax)
		{
			base.VisitMixinExpression(syntax);
		}

		protected override void VisitModuleDirective(ModuleDirectiveSyntax syntax)
		{
			base.VisitModuleDirective(syntax);
		}

		protected override void VisitNameList(NameListSyntax syntax)
		{
			base.VisitNameList(syntax);
		}

		protected override void VisitNamespace(NamespaceSyntax syntax)
		{
			base.VisitBlockStatement(syntax.Body);
		}

		protected override void VisitNewExpression(NewExpressionSyntax syntax)
		{
			base.VisitNewExpression(syntax);
		}

		protected override void VisitNullableType(NullableTypeSyntax syntax)
		{
			base.VisitNullableType(syntax);
		}

		protected override void VisitPackageDirective(PackageDirectiveSyntax syntax)
		{
			base.VisitPackageDirective(syntax);
		}

		protected override void VisitParameter(ParameterSyntax syntax)
		{
			base.VisitParameter(syntax);
		}

		protected override void VisitPointerType(PointerTypeSyntax syntax)
		{
			base.VisitPointerType(syntax);
		}

		protected override void VisitPostfixExpression(PostfixExpressionSyntax syntax)
		{
			base.VisitPostfixExpression(syntax);
		}
		
		protected override void VisitBuiltinType(BuiltinTypeSyntax syntax)
		{
			base.VisitBuiltinType(syntax);
		}

		protected override void VisitPrefixExpression(PrefixExpressionSyntax syntax)
		{
			base.VisitPrefixExpression(syntax);
		}

		protected override void VisitQualifiedName(QualifiedNameSyntax syntax)
		{
			base.VisitQualifiedName(syntax);
		}

		protected override void VisitReturn(ReturnSyntax syntax)
		{
			base.VisitReturn(syntax);
		}

		protected override void VisitReceiver(ReceiverSyntax syntax)
		{
			base.VisitReceiver(syntax);

			TypeAttributes selfTypeAttribs = TypeAttributes.None;
			if (syntax.ConstToken != null)
				selfTypeAttribs = TypeAttributes.Const;
			SymbolType type = SymbolType.SelfType(_implIden, selfTypeAttribs);

			if (syntax.RefToken != null)
			{
				type = SymbolType.ReferenceType(type);
			}
			
			Symbol symbol = new Symbol(syntax.Context.Scope,_implScope, syntax.Context.Identifier, SymbolKind.Receiver);
			symbol.Type = type;
			symbol.Location = syntax.FullSpan.Start;

			syntax.Context.Type = type;
			Symbols.AddSymbol(symbol);
		}

		protected override void VisitSingleAttribute(SingleAttributeSyntax syntax)
		{
			base.VisitSingleAttribute(syntax);
		}

		protected override void VisitSpecialKeyword(SpecialKeywordExpressionSyntax syntax)
		{
			base.VisitSpecialKeyword(syntax);
		}

		protected override void VisitStruct(StructSyntax syntax)
		{
			bool prevAgg = _inAggregate;
			_inAggregate = true;
			_templateParamIdx = 0;
			base.VisitStruct(syntax);
			_inAggregate = prevAgg;

			Identifier iden = syntax.Context.Identifier;

			if (iden is TemplateDefinitionName defName)
			{
				for (var i = 0; i < defName.Parameters.Count; i++)
				{
					TemplateParameter parameter = defName.Parameters[i];
					TemplateParameterSyntax paramSyntax = syntax.TemplateParameters[i];

					parameter.Type = paramSyntax.Context.Type;
				}
			}

			Scope scope = syntax.Context.Scope.GetBaseScope();
			
			Symbol symbol = new Symbol(scope,_implScope, iden, SymbolKind.Struct);
			symbol.Location = syntax.FullSpan.Start;
			AggregateSymbolType type = SymbolType.AggregateType(new ScopeVariable(scope, iden), AggregateTypes.Struct);
			symbol.Type = type;
			type.Symbol = symbol;

			Symbols.AddSymbol(symbol);
			syntax.Context.Symbol = symbol;
		}

		protected override void VisitStructInitializer(StructInitializerSyntax syntax)
		{
			base.VisitStructInitializer(syntax);
		}

		protected override void VisitSwitch(SwitchSyntax syntax)
		{
			base.VisitSwitch(syntax);
		}

		protected override void VisitTemplateName(TemplateNameSyntax syntax)
		{
			base.VisitTemplateName(syntax);
		}

		protected override void VisitTemplateParameter(TemplateParameterSyntax syntax)
		{
			base.VisitTemplateParameter(syntax);
			
			if (syntax.Type == null)
			{
				Identifier iden = syntax.Context.Identifier;
				Symbol symbol = new Symbol(syntax.Context.Scope,_implScope, iden, SymbolKind.TemplateParam);
				symbol.Location = syntax.FullSpan.Start;
				TemplateParamSymbolType type = SymbolType.TemplateParamType(syntax.Context.Scope, iden, _templateParamIdx);
				symbol.Type = type;
				symbol.TemplateIdx = _templateParamIdx;

				Symbols.AddSymbol(symbol);
				syntax.Context.Type = type;
			}
			else
			{
				Symbol symbol = new Symbol(syntax.Context.Scope, _implScope, syntax.Context.Identifier, SymbolKind.TemplateParam);
				symbol.Location = syntax.FullSpan.Start;
				symbol.Type = syntax.Type.Context.Type;
				symbol.TemplateIdx = _templateParamIdx;

				Symbols.AddSymbol(symbol);
				// Already propagate to the templateParam
				syntax.Context.Type = syntax.Type.Context.Type;
			}

			++_templateParamIdx;
		}

		protected override void VisitTernaryExpression(TernaryExpressionSyntax syntax)
		{
			base.VisitTernaryExpression(syntax);
		}

		protected override void VisitTransmuteExpression(TransmuteExpressionSyntax syntax)
		{
			base.VisitTransmuteExpression(syntax);
		}

		protected override void VisitTupleType(TupleTypeSyntax syntax)
		{
			base.VisitTupleType(syntax);
		}

		protected override void VisitType(TypeSyntax syntax)
		{
			base.VisitType(syntax);

			switch (syntax)
			{
			case BuiltinTypeSyntax predefinedTypeSyntax:
			{
				SymbolType type = null;
				switch (predefinedTypeSyntax.TypeToken.Type)
				{
				case TokenType.Void:		type = SymbolType.BuiltinType(BuiltinTypes.Void);	break;
				case TokenType.Bool:		type = SymbolType.BuiltinType(BuiltinTypes.Bool);	break;
				case TokenType.Byte:		type = SymbolType.BuiltinType(BuiltinTypes.I8);		break;
				case TokenType.UByte:		type = SymbolType.BuiltinType(BuiltinTypes.U8);		break;
				case TokenType.Short:		type = SymbolType.BuiltinType(BuiltinTypes.I16);	break;
				case TokenType.UShort:		type = SymbolType.BuiltinType(BuiltinTypes.U16);	break;
				case TokenType.Int:			type = SymbolType.BuiltinType(BuiltinTypes.I32);	break;
				case TokenType.UInt:		type = SymbolType.BuiltinType(BuiltinTypes.U32);	break;
				case TokenType.Long:		type = SymbolType.BuiltinType(BuiltinTypes.I64);	break;
				case TokenType.ULong:		type = SymbolType.BuiltinType(BuiltinTypes.U64);	break;
				case TokenType.ISize:		type = SymbolType.BuiltinType(BuiltinTypes.ISize);  break;
				case TokenType.USize:		type = SymbolType.BuiltinType(BuiltinTypes.USize);  break;
				case TokenType.Float:		type = SymbolType.BuiltinType(BuiltinTypes.F32);	break;
				case TokenType.Double:		type = SymbolType.BuiltinType(BuiltinTypes.F64);	break;
				case TokenType.Char:		type = SymbolType.BuiltinType(BuiltinTypes.Char);	break;
				case TokenType.WChar:		type = SymbolType.BuiltinType(BuiltinTypes.WChar);	break;
				case TokenType.Rune:		type = SymbolType.BuiltinType(BuiltinTypes.Rune);	break;
				case TokenType.String:		type = SymbolType.BuiltinType(BuiltinTypes.String);	break;
				case TokenType.SelfType:    type = SymbolType.SelfType(_implIden);				break;
				}
				predefinedTypeSyntax.Context.Type = type;
				break;
			}
			case IdentifierTypeSyntax identifierTypeSyntax:
			{
				if (identifierTypeSyntax.Name is TemplateNameSyntax templateName)
				{
					identifierTypeSyntax.Context.Type = templateName.Context.Type;
				}
				else
				{
					ScopeVariable scopeVar = null;
					if (identifierTypeSyntax.Name is QualifiedNameSyntax qualName)
						scopeVar = new ScopeVariable(qualName);
					else if (identifierTypeSyntax.Name is SimpleNameSyntax simpleName)
						scopeVar = new ScopeVariable(Identifier.GetIdentifier(simpleName));
					
					UnknownSymbolType type = SymbolType.UnknownType(scopeVar);
					identifierTypeSyntax.Context.Type = type;
				}
				
				break;
			}
			case PointerTypeSyntax pointerTypeSyntax:
			{
				SymbolType baseType = pointerTypeSyntax.BaseType.Context.Type;
				PointerSymbolType type = SymbolType.PointerType(baseType);
				pointerTypeSyntax.Context.Type = type;
				break;
			}
			case NullableTypeSyntax nullableTypeSyntax:
			{
				SymbolType baseType = nullableTypeSyntax.BaseType.Context.Type;
				NullableSymbolType type = SymbolType.NullableType(baseType);
				nullableTypeSyntax.Context.Type = type;
				break;
			}
			case ArrayTypeSyntax arrayTypeSyntax:
			{
				SymbolType baseType = arrayTypeSyntax.BaseType.Context.Type;
				ArraySymbolType type = SymbolType.ArrayType(baseType);
				arrayTypeSyntax.Context.Type = type;
				// TODO: Handle array size
				break;
			}
			case ReferenceTypeSyntax referenceTypeSyntax:
			{
				SymbolType baseType = referenceTypeSyntax.BaseType.Context.Type;
				ReferenceSymbolType type = SymbolType.ReferenceType(baseType);
				referenceTypeSyntax.Context.Type = type;
				break;
			}
			case TupleTypeSyntax tupleTypeSyntax:
			{
				TupleSymbolType type = new TupleSymbolType();
				foreach (TupleSubTypeSyntax subType in tupleTypeSyntax.SubTypes)
				{
					type.SubTypes.Add(subType.BaseType.Context.Type);
				}
				tupleTypeSyntax.Context.Type = type;
				break;
			}
			case AttributedTypeSyntax attributedTypeSyntax:
			{
				SymbolType type = attributedTypeSyntax.BaseType.Context.Type;
				SemanticAttributes semAttribs = attributedTypeSyntax.Context.Attribs;

				TypeAttributes typeAttributes = TypeAttributes.None;
				if ((semAttribs & SemanticAttributes.Const) != SemanticAttributes.None)
					typeAttributes |= TypeAttributes.Const;
				if ((semAttribs & SemanticAttributes.Immutable) != SemanticAttributes.None)
					typeAttributes |= TypeAttributes.Immutable;
				if ((semAttribs & SemanticAttributes.Mutable) != SemanticAttributes.None)
					typeAttributes |= TypeAttributes.Mutable;

				type = SymbolType.AttributedType(type, typeAttributes);

				attributedTypeSyntax.Context.Type = type;
				break;
			}
			case TypeofSyntax typeofSyntax when typeofSyntax.Expression is TypeSyntax typeSyntax:
				syntax.Context.Type = typeSyntax.Context.Type;
				break;
			case TypeofSyntax typeofSyntax:
			{
				TypeofSymbolType type = new TypeofSymbolType();
				type.Identifier = typeofSyntax.Expression;
				syntax.Context.Type = type;
				break;
			}
			case InlineStructTypeSyntax inlineStructSyntax:
				inlineStructSyntax.Context.Type = inlineStructSyntax.StructSyntax.Context.Type;
				break;
			case InlineUnionTypeSyntax inlineUnionSyntax:
				inlineUnionSyntax.Context.Type = inlineUnionSyntax.UnionSyntax.Context.Type;
				break;
			case BitFieldTypeSyntax bitFieldSyntax:
			{
				SymbolType baseType = bitFieldSyntax.BaseType.Context.Type;
				LiteralExpressionSyntax lit = bitFieldSyntax.ExprSyntax as LiteralExpressionSyntax;
				byte bits = (byte)lit.LiteralToken.Values.Integer;
				SymbolType type = SymbolType.BitFieldType(baseType, bits);
				bitFieldSyntax.Context.Type = type;
				break;
			}
			case VariadicTypeSyntax variadicSyntax:
			{
				SymbolType baseType = variadicSyntax.BaseType.Context.Type;
				SymbolType type = SymbolType.VariadicType(baseType);
				variadicSyntax.Context.Type = type;
				break;
			}
			}
		}

		protected override void VisitTypedef(TypedefSyntax syntax)
		{
			bool prev = _inAggregate;
			_inAggregate = true;
			base.VisitTypedef(syntax);
			_inAggregate = prev;

			Identifier iden = new IdentifierName(syntax.Name.Identifier.Text);
			Symbol symbol = new Symbol(syntax.Context.Scope,_implScope, iden, SymbolKind.Typedef);
			symbol.Type = syntax.Type.Context.Type;
			symbol.Location = syntax.FullSpan.Start;

			Symbols.AddSymbol(symbol);
			syntax.Context.Symbol = symbol;
		}

		protected override void VisitIntrinsicExpression(IntrinsicSyntax syntax)
		{
			base.VisitIntrinsicExpression(syntax);
		}

		protected override void VisitTypeof(TypeofSyntax syntax)
		{
			base.VisitTypeof(syntax);
		}

		protected override void VisitUnion(UnionSyntax syntax)
		{
			bool prevAgg = _inAggregate;
			_inAggregate = true;
			_templateParamIdx = 0;
			base.VisitUnion(syntax);
			_inAggregate = prevAgg;

			Identifier iden = syntax.Context.Identifier;
			Scope scope = syntax.Context.Scope.GetBaseScope();
			
			Symbol symbol = new Symbol(scope,_implScope, iden, SymbolKind.Union);
			symbol.Location = syntax.FullSpan.Start;
			AggregateSymbolType type = SymbolType.AggregateType(new ScopeVariable(syntax.Context.Scope, iden), AggregateTypes.Union);
			symbol.Type = type;
			type.Symbol = symbol;

			Symbols.AddSymbol(symbol);
			syntax.Context.Symbol = symbol;
		}

		protected override void VisitUnitTest(UnitTestSyntax syntax)
		{
			base.VisitUnitTest(syntax);
		}

		protected override void VisitVariadicType(VariadicTypeSyntax syntax)
		{
			base.VisitVariadicType(syntax);
		}

		protected override void VisitWhile(WhileSyntax syntax)
		{
			base.VisitWhile(syntax);
		}

		protected override void VisitInlineStruct(InlineStructTypeSyntax syntax)
		{
			base.VisitInlineStruct(syntax);
		}

		protected override void VisitInlineUnion(InlineUnionTypeSyntax syntax)
		{
			base.VisitInlineUnion(syntax);
		}
	}
}
