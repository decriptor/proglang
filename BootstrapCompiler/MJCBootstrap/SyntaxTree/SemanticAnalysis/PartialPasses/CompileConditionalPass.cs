﻿using System.Collections.Generic;
using MJC.General;

namespace MJC.SyntaxTree.SemanticAnalysis.PartialPasses
{
	class CompileConditionalPass : ISemanticPass
	{
		private List<string> _additionalDebug = new List<string>();
		private List<string> _additionalVersion = new List<string>();
		private long _debugValue;
		private long _versionValue;

		private List<string> _versionConditionals;
		private List<string> _debugConditionals;

		public override void Setup(List<string> versionCondVars, List<string> debugCondVars)
		{
			_versionConditionals = versionCondVars;
			_debugConditionals = debugCondVars;
		}


		public override void Visit(SyntaxTree tree)
		{
			_additionalDebug.Clear();
			_additionalVersion.Clear();
			_debugValue = 0;
			_versionValue = 0;
			base.Visit(tree);
		}

		protected override void VisitAssignExpression(AssignExpressionSyntax syntax)
		{
			if (syntax.Left is NameListSyntax nameList)
			{
				if (nameList.Names.Count == 1)
				{
					if (nameList.Names[0].Name is QualifiedNameSyntax qualifiedName &&
					    qualifiedName.SeperationToken == null &&
					    qualifiedName.Right is IdentifierNameSyntax identifierName)
					{
						string iden = identifierName.Identifier.Text;
						if (syntax.Right is NameListSyntax rNameList)
						{
							if (nameList.Names.Count == 1)
							{
								if (rNameList.Names[0].Name is QualifiedNameSyntax rQualifiedName &&
								    rQualifiedName.SeperationToken == null &&
								    rQualifiedName.Right is IdentifierNameSyntax rIdentifierName)
								{
									string idenVal = rIdentifierName.Identifier.Text;
									if (iden == "version")
									{
										_additionalVersion.Add(idenVal);
									}
									else if (iden == "debug")
									{
										_additionalDebug.Add(idenVal);
									}
								}
							}
						}
						else if (syntax.Right is LiteralExpressionSyntax litValue)
						{
							if (litValue.LiteralToken.Type == TokenType.I32Literal)
							{
								if (iden == "version")
								{
									_versionValue = litValue.LiteralToken.Values.Integer;
								}
								else if (iden == "debug")
								{
									_debugValue = litValue.LiteralToken.Values.Integer;
								}
							}
						}

					}
				}
			}

			base.VisitAssignExpression(syntax);
		}

		protected override void VisitCompileCondition(CompileConditionalSyntax syntax)
		{
			// TODO make sure this is the correct type of expression
			if (syntax.Expression is NameSyntax names)
			{
				if (names is NameListSyntax nameList)
				{
					ErrorSystem.AstError(syntax.Expression.FullSpan, "Compile conditional expression only allows 1 identifier!");
					syntax.Context.AllowCompilation = false;
				}
				else if (names is IdentifierNameSyntax identifierName)
				{
					string value = identifierName.Identifier.Text;
					if (syntax.ConditionalToken.Type == TokenType.Version)
					{
						if (!_versionConditionals.Contains(value) &&
						    !_additionalVersion.Contains(value))
						{
							syntax.Context.AllowCompilation = false;
						}
					}
					else
					{
						if (!_debugConditionals.Contains(value) &&
						    !_additionalDebug.Contains(value))
						{
							syntax.Context.AllowCompilation = false;
						}
					}
				}
				else
				{
					ErrorSystem.AstError(syntax.Expression.FullSpan, "Compile conditional expression only allows an identifier name (no qualified name or template name)!");
					syntax.Context.AllowCompilation = false;
				}
			}
			else if (syntax.Expression is LiteralExpressionSyntax literal)
			{
				if (literal.LiteralToken.Type == TokenType.I32Literal)
				{
					long value = literal.LiteralToken.Values.Integer;
					if (syntax.ConditionalToken.Type == TokenType.Version)
					{
						if (_versionValue != value)
						{
							syntax.Context.AllowCompilation = false;
						}
					}
					else
					{
						if (_debugValue != value)
						{
							syntax.Context.AllowCompilation = false;
						}
					}
				}
				else
				{
					ErrorSystem.AstError(syntax.Expression.FullSpan, "Compile conditional expression needs to be an identifier or an integer literal!");
					syntax.Context.AllowCompilation = false;
				}
			}
			else
			{
				ErrorSystem.AstError(syntax.Expression.FullSpan, "Compile conditional expression needs to be an identifier or an integer literal!");
				syntax.Context.AllowCompilation = false;
			}

			syntax.Context.AllowVisitAll = false;
			base.VisitCompileCondition(syntax);
		}

	}
}
