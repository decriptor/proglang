﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.SyntaxTree.SemanticAnalysis
{
	public class ISemanticPass : SyntaxWalker
	{
		public virtual void Setup(SyntaxTree tree)
		{

		}

		public virtual void Setup(List<string> versionCondVars, List<string> debugCondVars)
		{

		}

		public virtual void Setup(CompilerContext compilerContext)
		{

		}

		public virtual void Finalize(CompilerContext compilerContext)
		{

		}

	}
}
