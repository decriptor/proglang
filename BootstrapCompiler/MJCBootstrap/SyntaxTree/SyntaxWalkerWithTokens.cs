﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.SyntaxTree
{
	public class SyntaxWalkerWithTokens
	{
		public virtual void Visit(SyntaxTree tree)
		{
			VisitCompilationUnit(tree.CompilationUnit);
		}

		protected virtual void VisitCompilationUnit(CompilationUnitSyntax syntax)
		{
			if (syntax.Package != null)
				VisitPackageDirective(syntax.Package);
			if (syntax.Module != null)
				VisitModuleDirective(syntax.Module);
			if (syntax.Block != null)
				VisitBlockStatement(syntax.Block);
			VisitToken(syntax.EndOfFileToken);
		}

		protected virtual void VisitPackageDirective(PackageDirectiveSyntax syntax)
		{
			VisitToken(syntax.PackageToken);
			VisitName(syntax.Name);
			VisitToken(syntax.SemicolonToken);
		}

		protected virtual void VisitModuleDirective(ModuleDirectiveSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			VisitToken(syntax.ModuleToken);
			VisitIdentifierName(syntax.Name);
			VisitToken(syntax.SemicolonToken);
		}

		protected void VisitName(NameSyntax syntax)
		{
			switch (syntax)
			{
			case QualifiedNameSyntax qualifiedName:
				VisitQualifiedName(qualifiedName);
				break;
			case IdentifierNameSyntax identifierName:
				VisitIdentifierName(identifierName);
				break;
			case TemplateNameSyntax templateName:
				VisitTemplateName(templateName);
				break;
			case NameListSyntax nameList:
				VisitNameList(nameList);
				break;
			}
		}

		protected virtual void VisitQualifiedName(QualifiedNameSyntax syntax)
		{
			if (syntax.Left != null)
				VisitName(syntax.Left);
			if (syntax.SeperationToken != null)
				VisitToken(syntax.SeperationToken);
			VisitName(syntax.Right);
		}

		protected virtual void VisitIdentifierName(IdentifierNameSyntax syntax)
		{
			VisitToken(syntax.Identifier);
		}

		protected virtual void VisitTemplateName(TemplateNameSyntax syntax)
		{
			VisitToken(syntax.Identifier);
			VisitToken(syntax.OpenTemplateToken);
			syntax.Arguments?.ForEach(VisitArgument);
			VisitToken(syntax.CloseTemplateToken);
		}
		
		protected virtual void VisitNameList(NameListSyntax syntax)
		{
			foreach (NameListElemSyntax name in syntax.Names)
			{
				VisitName(name.Name);
				if (name.CommaToken != null)
					VisitToken(name.CommaToken);
			}
		}

		protected virtual void VisitAttributes(AttributeSyntax syntax)
		{
			syntax.AtAttribs?.ForEach(VisitAtAttribute);
			syntax.CompilerAtAttribs?.ForEach(VisitCompilerAtAttribute);
			syntax.SingleAttribs?.ForEach(VisitSingleAttribute);
		}

		protected virtual void VisitAtAttribute(AtAttributeSyntax syntax)
		{
			VisitToken(syntax.AtToken);
			VisitName(syntax.Name);
			if (syntax.OpenParenToken != null)
			{
				VisitToken(syntax.OpenParenToken);
				syntax.Arguments.ForEach(VisitArgument);
				VisitToken(syntax.CloseParenToken);
			}
		}

		protected virtual void VisitCompilerAtAttribute(CompilerAtAttributeSyntax syntax)
		{
			VisitToken(syntax.AtColonToken);
			VisitName(syntax.Name);
			if (syntax.OpenParenToken != null)
			{
				VisitToken(syntax.OpenParenToken);
				syntax.Arguments.ForEach(VisitArgument);
				VisitToken(syntax.CloseParenToken);
			}
		}

		protected virtual void VisitSingleAttribute(SingleAttributeSyntax syntax)
		{
			VisitToken(syntax.AttributeToken);
		}
		
		protected virtual void VisitArgument(ArgumentSyntax syntax)
		{
			if (syntax.Identifier != null)
			{
				VisitIdentifierName(syntax.Identifier);
				VisitToken(syntax.ColonToken);
			}
			VisitSimpleExpression(syntax.Expression);
			if (syntax.CommaToken != null)
				VisitToken(syntax.CommaToken);
		}

		protected virtual void VisitParameter(ParameterSyntax syntax)
		{
			VisitIdentifierName(syntax.Identifier);
			if (syntax.ColonToken != null)
				VisitToken(syntax.ColonToken);
			if (syntax.Type != null)
				VisitType(syntax.Type);
			if (syntax.VariadicToken != null)
				VisitToken(syntax.VariadicToken);
			if (syntax.EqualsToken != null)
				VisitToken(syntax.EqualsToken);
			if (syntax.DefaultValue != null)
				VisitSimpleExpression(syntax.DefaultValue);
			if (syntax.CommaToken != null)
				VisitToken(syntax.CommaToken);
		}

		protected virtual void VisitTemplateParameter(TemplateParameterSyntax syntax)
		{
			VisitIdentifierName(syntax.Identifier);
			if (syntax.CommaToken != null)
				VisitToken(syntax.CommaToken);
			if (syntax.Type != null)
				VisitType(syntax.Type);
			if (syntax.ColonEqualsToken != null)
				VisitToken(syntax.ColonEqualsToken);
			if (syntax.SpecializedValue != null)
				VisitSimpleExpression(syntax.SpecializedValue);
			if (syntax.EqualsToken != null)
				VisitToken(syntax.EqualsToken);
			if (syntax.DefaultValue != null)
				VisitSimpleExpression(syntax.DefaultValue);
			if (syntax.CommaToken != null)
				VisitToken(syntax.CommaToken);
		}

		protected virtual void VisitType(TypeSyntax syntax)
		{
			switch (syntax)
			{
			case BuiltinTypeSyntax predefinedTypeSyntax:
				VisitPredefinedType(predefinedTypeSyntax);
				break;
			case IdentifierTypeSyntax identifierTypeSyntax:
				VisitIdentifierType(identifierTypeSyntax);
				break;
			case PointerTypeSyntax pointerTypeSyntax:
				VisitPointerType(pointerTypeSyntax);
				break;
			case ArrayTypeSyntax arrayTypeSyntax:
				VisitArrayType(arrayTypeSyntax);
				break;
			case AttributedTypeSyntax attributedTypeSyntax:
				VisitAttributedType(attributedTypeSyntax);
				break;
			case TupleTypeSyntax tupleTypeSyntax:
				VisitTupleType(tupleTypeSyntax);
				break;
			case NullableTypeSyntax nullableTypeSyntax:
				VisitNullableType(nullableTypeSyntax);
				break;
			case VariadicTypeSyntax variadicTypeSyntax:
				VisitVariadicType(variadicTypeSyntax);
				break;
			case TypeofSyntax typeofSyntax:
				VisitTypeof(typeofSyntax);
				break;
			case DelegateSyntax delegateSyntax:
				VisitDelegate(delegateSyntax);
				break;
			case InlineStructTypeSyntax inlineStructSyntax:
				VisitInlineStruct(inlineStructSyntax);
				break;
			case InlineUnionTypeSyntax inlineUnionSyntax:
				VisitInlineUnion(inlineUnionSyntax);
				break;
			case BitFieldTypeSyntax bitFieldSyntax:
				VisitBitField(bitFieldSyntax);
				break;
			}
		}

		protected virtual void VisitPredefinedType(BuiltinTypeSyntax syntax)
		{
			VisitToken(syntax.TypeToken);
		}

		protected virtual void VisitIdentifierType(IdentifierTypeSyntax syntax)
		{
			VisitName(syntax.Name);
		}

		protected virtual void VisitPointerType(PointerTypeSyntax syntax)
		{
			VisitType(syntax.BaseType);
			VisitToken(syntax.PointerToken);
		}

		protected virtual void VisitArrayType(ArrayTypeSyntax syntax)
		{
			VisitType(syntax.BaseType);
			VisitToken(syntax.OpenBracketToken);
			if (syntax.SizeExpression != null)
				VisitSimpleExpression(syntax.SizeExpression);
			VisitToken(syntax.CloseBracketToken);
		}

		protected virtual void VisitAttributedType(AttributedTypeSyntax syntax)
		{
			VisitAttributes(syntax.Attributes);
			VisitToken(syntax.OpenParenToken);
			VisitType(syntax.BaseType);
			VisitToken(syntax.CloseParenToken);
		}

		protected virtual void VisitTupleType(TupleTypeSyntax syntax)
		{
			VisitToken(syntax.OpenParenToken);
			foreach (TupleSubTypeSyntax subType in syntax.SubTypes)
			{
				VisitType(subType.BaseType);
				if (subType.CommaToken != null)
					VisitToken(subType.CommaToken);
			}
			VisitToken(syntax.CloseParenToken);
		}

		protected virtual void VisitNullableType(NullableTypeSyntax syntax)
		{
			VisitType(syntax.BaseType);
			VisitToken(syntax.QuestionToken);
		}

		protected virtual void VisitVariadicType(VariadicTypeSyntax syntax)
		{
			VisitType(syntax.BaseType);
			VisitToken(syntax.VariadicToken);
		}

		protected virtual void VisitTypeof(TypeofSyntax syntax)
		{
			VisitToken(syntax.TypeofToken);
			VisitToken(syntax.OpenParenToken);
			VisitSimpleExpression(syntax.Expression);
			VisitToken(syntax.CloseParenToken);
		}

		protected virtual void VisitInlineStruct(InlineStructTypeSyntax syntax)
		{
			VisitStruct(syntax.StructSyntax);
		}

		protected virtual void VisitInlineUnion(InlineUnionTypeSyntax syntax)
		{
			VisitUnion(syntax.UnionSyntax);
		}

		protected virtual void VisitDelegate(DelegateSyntax syntax)
		{
			VisitToken(syntax.DelegateToken);
			if (syntax.Identifier != null)
				VisitIdentifierName(syntax.Identifier);
			VisitToken(syntax.OpenParenToken);
			if (syntax.Parameters != null)
			{
				foreach (ParameterSyntax parameter in syntax.Parameters)
				{
					VisitParameter(parameter);
				}
			}
			VisitToken(syntax.CloseParenToken);
			if (syntax.ArrowToken != null)
			{
				VisitToken(syntax.ArrowToken);
				VisitType(syntax.ReturnType);
			}
			if (syntax.SemicolonToken != null)
				VisitToken(syntax.SemicolonToken);
		}

		protected void VisitStatement(StatementSyntax syntax)
		{
			switch (syntax)
			{
			case BlockStatementSyntax blockStatementSyntax:
				VisitBlockStatement(blockStatementSyntax);
				break;
			case NamespaceSyntax namespaceSyntax:
				VisitNamespace(namespaceSyntax);
				break;
			case ImportSyntax importSyntax:
				VisitImport(importSyntax);
				break;
			case FunctionSyntax functionSyntax:
				VisitFunction(functionSyntax);
				break;
			case AliasSyntax aliasSyntax:
				VisitAlias(aliasSyntax);
				break;
			case TypedefSyntax typedefSyntax:
				VisitTypedef(typedefSyntax);
				break;
			case IfElseSyntax ifElseSyntax:
				VisitIfElse(ifElseSyntax);
				break;
			case IfSyntax ifSyntax:
				VisitIf(ifSyntax);
				break;
			case ElseSyntax elseSyntax:
				VisitElse(elseSyntax);
				break;
			case WhileSyntax whileSyntax:
				VisitWhile(whileSyntax);
				break;
			case DoWhileSyntax doWhileSyntax:
				VisitDoWhile(doWhileSyntax);
				break;
			case ForSyntax forSyntax:
				VisitFor(forSyntax);
				break;
			case ForeachSyntax foreachSyntax:
				VisitForeach(foreachSyntax);
				break;
			case SwitchSyntax switchSyntax:
				VisitSwitch(switchSyntax);
				break;
			case FallthroughSyntax fallthroughSyntax:
				VisitFallthrough(fallthroughSyntax);
				break;
			case ContinueSyntax continueSyntax:
				VisitContinue(continueSyntax);
				break;
			case BreakSyntax breakSyntax:
				VisitBreak(breakSyntax);
				break;
			case ReturnSyntax returnSyntax:
				VisitReturn(returnSyntax);
				break;
			case DeferSyntax deferSyntax:
				VisitDefer(deferSyntax);
				break;
			case GotoSyntax gotoSyntax:
				VisitGoto(gotoSyntax);
				break;
			case LabelSyntax labelSyntax:
				VisitLabel(labelSyntax);
				break;
			case ImplSyntax implSyntax:
				VisitImpl(implSyntax);
				break;
			case StructSyntax structSyntax:
				VisitStruct(structSyntax);
				break;
			case InterfaceSyntax interfaceSyntax:
				VisitInterface(interfaceSyntax);
				break;
			case UnionSyntax unionSyntax:
				VisitUnion(unionSyntax);
				break;
			case EnumSyntax enumSyntax:
				VisitEnum(enumSyntax);
				break;
			case UnitTestSyntax unitTestSyntax:
				VisitUnitTest(unitTestSyntax);
				break;
			case CompileConditionalSyntax compileConditionalSyntax:
				VisitCompileCondition(compileConditionalSyntax);
				break;
			case AssertSyntax assertSyntax:
				VisitAssert(assertSyntax);
				break;
			case SimpleExpressionSyntax simpleExpressionSyntax:
				VisitSimpleExpression(simpleExpressionSyntax);
				break;
			}
		}

		protected virtual void VisitBlockStatement(BlockStatementSyntax syntax)
		{
			if (syntax.OpenBraceToken != null)
				VisitToken(syntax.OpenBraceToken);
			syntax.Statements?.ForEach(VisitStatement);
			if (syntax.CloseBraceToken != null)
				VisitToken(syntax.CloseBraceToken);
		}

		protected virtual void VisitNamespace(NamespaceSyntax syntax)
		{
			VisitToken(syntax.NamespaceToken);
			VisitName(syntax.Name);
			VisitBlockStatement(syntax.Body);
		}

		protected virtual void VisitImport(ImportSyntax syntax)
		{
			if (syntax.Attribs != null)
				VisitAttributes(syntax.Attribs);
			VisitToken(syntax.ImportToken);
			VisitName(syntax.ImportedModule);
			if (syntax.AsToken != null)
			{
				VisitToken(syntax.AsToken);
				VisitIdentifierName(syntax.ModuleAlias);
			}
			if (syntax.ColonToken != null)
			{
				VisitToken(syntax.ColonToken);
				foreach (ImportSymbolSyntax symbol in syntax.Symbols)
				{
					VisitName(symbol.Symbol);
					if (symbol.AsToken != null)
					{
						VisitToken(symbol.AsToken);
						VisitIdentifierName(symbol.Alias);
					}
					if (symbol.CommaToken != null)
						VisitToken(symbol.CommaToken);
				}
			}
			VisitToken(syntax.SemicolonToken);
		}

		protected virtual void VisitReceiver(ReceiverSyntax syntax)
		{
			VisitToken(syntax.OpenParenToken);
			if (syntax.ConstToken != null)
				VisitToken(syntax.ConstToken);
			if (syntax.RefToken != null)
				VisitToken(syntax.RefToken);
			VisitToken(syntax.SelfToken);
			VisitToken(syntax.CloseParenToken);
		}

		protected virtual void VisitFunction(FunctionSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			VisitToken(syntax.FuncToken);
			if (syntax.Receiver != null)
				VisitReceiver(syntax.Receiver);
			VisitIdentifierName(syntax.Name);
			if (syntax.OpenTemplateToken != null)
			{
				VisitToken(syntax.OpenTemplateToken);
				syntax.TemplateParams?.ForEach(VisitTemplateParameter);
				VisitToken(syntax.CloseTemplateToken);
			}
			VisitToken(syntax.OpenParenToken);
			syntax.Parameters?.ForEach(VisitParameter);
			VisitToken(syntax.CloseParenToken);
			if (syntax.ArrowToken != null)
			{
				VisitToken(syntax.ArrowToken);
				VisitType(syntax.ReturnType);
			}
			if (syntax.Constraint != null)
				VisitTemplateConstraint(syntax.Constraint);
			if (syntax.Body != null)
				VisitBlockStatement(syntax.Body);
			if (syntax.SemicolonToken != null)
				VisitToken(syntax.SemicolonToken);
		}

		protected virtual void VisitTemplateConstraint(TemplateConstraintSyntax syntax)
		{
			VisitToken(syntax.IfToken);
			VisitToken(syntax.OpenParanToken);
			VisitSimpleExpression(syntax.Expression);
			VisitToken(syntax.CloseParanToken);
		}

		protected virtual void VisitAlias(AliasSyntax syntax)
		{
			VisitToken(syntax.AliasToken);
			VisitSimpleExpression(syntax.Type);
			VisitToken(syntax.AsToken);
			VisitIdentifierName(syntax.Name);
			VisitToken(syntax.SemicolonToken);
		}

		protected virtual void VisitTypedef(TypedefSyntax syntax)
		{
			VisitToken(syntax.TypedefToken);
			VisitType(syntax.Type);
			VisitToken(syntax.AsToken);
			VisitIdentifierName(syntax.Name);
			VisitToken(syntax.SemicolonToken);
		}

		protected virtual void VisitIfElse(IfElseSyntax syntax)
		{
			VisitIf(syntax.If);
			if (syntax.Else is IfSyntax ifSub)
			{
				VisitIf(ifSub);
			}
			else if (syntax.Else is ElseSyntax elseSyntax)
			{
				VisitElse(elseSyntax);
			}
		}

		protected virtual void VisitIf(IfSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			if (syntax.LeadingElse != null)
				VisitToken(syntax.LeadingElse);
			VisitToken(syntax.ElifIfToken);
			VisitToken(syntax.OpenParenToken);
			VisitSimpleExpression(syntax.Condition);
			VisitToken(syntax.CloseParenToken);
			VisitStatement(syntax.Then);
		}

		protected virtual void VisitElse(ElseSyntax syntax)
		{
			VisitToken(syntax.ElseToken);
			VisitStatement(syntax.Statement);
		}

		protected virtual void VisitWhile(WhileSyntax syntax)
		{
			VisitToken(syntax.WhileToken);
			VisitToken(syntax.OpenParenToken);
			VisitSimpleExpression(syntax.Condition);
			VisitToken(syntax.CloseParenToken);
			VisitStatement(syntax.Body);
		}

		protected virtual void VisitDoWhile(DoWhileSyntax syntax)
		{
			VisitToken(syntax.DoToken);
			VisitStatement(syntax.Body);
			VisitToken(syntax.WhileToken);
			VisitToken(syntax.OpenParenToken);
			VisitSimpleExpression(syntax.Condition);
			VisitToken(syntax.CloseParenToken);
			VisitToken(syntax.SemicolonToken);
		}

		protected virtual void VisitFor(ForSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			VisitToken(syntax.ForToken);
			VisitToken(syntax.OpenParenToken);
			VisitSimpleExpression(syntax.Initializer);
			if (syntax.SemicolonToken0 != null)
				VisitToken(syntax.SemicolonToken0);
			VisitSimpleExpression(syntax.Condition);
			VisitToken(syntax.SemicolonToken1);
			VisitSimpleExpression(syntax.Increment);
			VisitToken(syntax.CloseParenToken);
			VisitStatement(syntax.Body);
		}

		protected virtual void VisitForeach(ForeachSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			VisitToken(syntax.ForeachToken);
			VisitToken(syntax.OpenParenToken);
			VisitSimpleExpression(syntax.Values);
			VisitToken(syntax.InToken);
			VisitSimpleExpression(syntax.Source);
			VisitToken(syntax.CloseParenToken);
			VisitStatement(syntax.Body);
		}

		protected virtual void VisitSwitch(SwitchSyntax syntax)
		{
			VisitToken(syntax.SwitchToken);
			VisitToken(syntax.OpenParenToken);
			VisitSimpleExpression(syntax.Expression);
			VisitToken(syntax.CloseParenToken);
			VisitToken(syntax.OpenBraceToken);
			foreach (SimpleSwitchCaseSyntax switchCase in syntax.Cases)
			{
				if (switchCase is CaseSyntax caseSyntax)
					VisitCase(caseSyntax);
				else if (switchCase is DefaultCaseSyntax defaultCaseSyntax)
					VisitDefaultCase(defaultCaseSyntax);
			}
			VisitToken(syntax.CloseBraceToken);
		}

		protected virtual void VisitCase(CaseSyntax syntax)
		{
			VisitToken(syntax.CaseToken);
			VisitSimpleExpression(syntax.Expression);
			VisitToken(syntax.ColonToken);
			syntax.Statements?.ForEach(VisitStatement);
		}

		protected virtual void VisitDefaultCase(DefaultCaseSyntax syntax)
		{
			VisitToken(syntax.DefaultToken);
			VisitToken(syntax.ColonToken);
			syntax.Statements?.ForEach(VisitStatement);
		}

		protected virtual void VisitFallthrough(FallthroughSyntax syntax)
		{
			VisitToken(syntax.FallthroughToken);
			VisitToken(syntax.SemicolonToken);
		}

		protected virtual void VisitContinue(ContinueSyntax syntax)
		{
			VisitToken(syntax.ContinueToken);
			VisitToken(syntax.SemicolonToken);
		}

		protected virtual void VisitBreak(BreakSyntax syntax)
		{
			VisitToken(syntax.BreakToken);
			VisitToken(syntax.SemicolonToken);
		}

		protected virtual void VisitReturn(ReturnSyntax syntax)
		{
			VisitToken(syntax.ReturnToken);
			if (syntax.Expression != null)
				VisitSimpleExpression(syntax.Expression);
			VisitToken(syntax.SemicolonToken);
		}

		protected virtual void VisitDefer(DeferSyntax syntax)
		{
			VisitToken(syntax.DeferToken);
			VisitSimpleExpression(syntax.Expression);
			VisitToken(syntax.SemicolonToken);
		}

		protected virtual void VisitGoto(GotoSyntax syntax)
		{
			VisitToken(syntax.GotoToken);
			if (syntax.CaseToken != null)
				VisitToken(syntax.CaseToken);
			if (syntax.DefaultToken != null)
				VisitToken(syntax.DefaultToken);
			if (syntax.Identifier != null)
				VisitIdentifierName(syntax.Identifier);
			VisitToken(syntax.SemicolonToken);
		}

		protected virtual void VisitLabel(LabelSyntax syntax)
		{
			VisitToken(syntax.LeftColonToken);
			VisitIdentifierName(syntax.Name);
			VisitToken(syntax.RightColonToken);
		}

		protected virtual void VisitImplInterfaceFieldDef(ImplInterfaceFieldDefSyntax syntax)
		{
			VisitIdentifierName(syntax.InterfaceField);
			VisitToken(syntax.Colon);
			VisitName(syntax.AggregateField);
			if (syntax.Comma != null)
				VisitToken(syntax.Comma);
		}

		protected virtual void VisitImplInterface(ImplInterfaceSyntax syntax)
		{
			VisitName(syntax.Name);
			if (syntax.OpenParen != null)
			{
				VisitToken(syntax.OpenParen);
				syntax.FieldDefs.ForEach(VisitImplInterfaceFieldDef);
				VisitToken(syntax.CloseParen);
			}
			if (syntax.Comma != null)
				VisitToken(syntax.Comma);
		}

		protected virtual void VisitImpl(ImplSyntax syntax)
		{
			VisitToken(syntax.ImplToken);
			if (syntax.OpenTemplateToken != null)
			{
				VisitToken(syntax.OpenTemplateToken);
				syntax.TemplateParameters.ForEach(VisitTemplateParameter);
				VisitToken(syntax.CloseTemplateToken);
			}
			VisitType(syntax.Type);
			if (syntax.ColonToken != null)
			{
				VisitToken(syntax.ColonToken);
				syntax.BaseList.ForEach(VisitImplInterface);
			}
			if (syntax.Constraint != null)
				VisitTemplateConstraint(syntax.Constraint);
			VisitBlockStatement(syntax.Body);
		}

		protected virtual void VisitStruct(StructSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			VisitToken(syntax.StructToken);
			if (syntax.Name != null)
				VisitIdentifierName(syntax.Name);
			if (syntax.OpenTemplateToken != null)
			{
				VisitToken(syntax.OpenTemplateToken);
				syntax.TemplateParameters.ForEach(VisitTemplateParameter);
				VisitToken(syntax.CloseTemplateToken);
			}
			if (syntax.ColonToken != null)
			{
				VisitToken(syntax.ColonToken);
				VisitName(syntax.BaseList);
			}
			VisitBlockStatement(syntax.Body);
		}

		protected virtual void VisitInterface(InterfaceSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			VisitToken(syntax.InterfaceToken);
			VisitIdentifierName(syntax.Name);
			if (syntax.OpenTemplateToken != null)
			{
				VisitToken(syntax.OpenTemplateToken);
				syntax.TemplateParameters.ForEach(VisitTemplateParameter);
				VisitToken(syntax.CloseTemplateToken);
			}
			if (syntax.ColonToken != null)
			{
				VisitToken(syntax.ColonToken);
				VisitName(syntax.BaseList);
			}
			VisitBlockStatement(syntax.Body);
		}

		protected virtual void VisitUnion(UnionSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			VisitToken(syntax.UnionToken);
			if (syntax.Name != null)
				VisitIdentifierName(syntax.Name);
			VisitBlockStatement(syntax.Body);
		}

		protected virtual void VisitEnum(EnumSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			VisitToken(syntax.EnumToken);
			VisitIdentifierName(syntax.Name);
			if (syntax.ColonToken != null)
			{
				VisitToken(syntax.ColonToken);
				VisitType(syntax.Type);
			}
			VisitToken(syntax.OpenBraceToken);
			if (syntax.Members != null)
			{
				foreach (EnumMemberSyntax member in syntax.Members)
				{
					VisitIdentifierName(member.Name);
					if (member.TupleType != null)
					{
						VisitType(member.TupleType);
					}
					if (member.EqualsToken != null)
					{
						VisitToken(member.EqualsToken);
						VisitSimpleExpression(member.DefaultValue);
					}
					if (member.CommaToken != null)
						VisitToken(member.CommaToken);
				}
			}
			VisitToken(syntax.CloseBraceToken);
		}

		protected virtual void VisitUnitTest(UnitTestSyntax syntax)
		{
			VisitToken(syntax.UnitTestToken);
			if (syntax.Name != null)
				VisitIdentifierName(syntax.Name);
			VisitBlockStatement(syntax.Body);
		}

		protected virtual void VisitCompileCondition(CompileConditionalSyntax syntax)
		{
			if (!syntax.Context.AllowCompilation)
				return;

			VisitToken(syntax.ConditionalToken);
			VisitToken(syntax.OpenParenToken);
			VisitSimpleExpression(syntax.Expression);
			VisitToken(syntax.CloseParenToken);
			VisitBlockStatement(syntax.Body);
			if (syntax.ElseToken != null)
			{
				VisitToken(syntax.ElseToken);
				VisitStatement(syntax.ElseBody);
			}
		}

		protected virtual void VisitAssert(AssertSyntax syntax)
		{
			if (syntax.Attributes != null)
				VisitAttributes(syntax.Attributes);
			VisitToken(syntax.AssertToken);
			VisitToken(syntax.OpenParenToken);
			syntax.Arguments?.ForEach(VisitArgument);
			VisitToken(syntax.CloseParenToken);
			VisitToken(syntax.SemicolonToken);
		}
		
		protected virtual void VisitSimpleExpression(SimpleExpressionSyntax syntax)
		{
			switch (syntax)
			{
			case NameSyntax nameSyntax:
				VisitName(nameSyntax);
				break;
			case TypeSyntax typeSyntax:
				VisitType(typeSyntax);
				break;
			case ExpressionSyntax expressionSyntax:
				VisitExpression(expressionSyntax);
				break;
			case CommaExpressionSyntax commaExpression:
				VisitCommaExpression(commaExpression);
				break;
			case DeclarationSyntax declarationSyntax:
				VisitDeclaration(declarationSyntax);
				break;
			case TernaryExpressionSyntax ternaryExpressionSyntax:
				VisitTernaryExpression(ternaryExpressionSyntax);
				break;
			case BinaryExpressionSyntax binaryExpressionSyntax:
				VisitBinaryExpression(binaryExpressionSyntax);
				break;
			case AssignExpressionSyntax assignExpressionSyntax:
				VisitAssignExpression(assignExpressionSyntax);
				break;
			case PrefixExpressionSyntax prefixExpressionSyntax:
				VisitPrefixExpression(prefixExpressionSyntax);
				break;
			case PostfixExpressionSyntax postfixExpressionSyntax:
				VisitPostfixExpression(postfixExpressionSyntax);
				break;
			case BracketedExpressionSyntax bracketedExpressionSyntax:
				VisitBracketedExpression(bracketedExpressionSyntax);
				break;
			case CastExpressionSyntax castExpressionSyntax:
				VisitCastExpression(castExpressionSyntax);
				break;
			case TransmuteExpressionSyntax transmuteExpressionSyntax:
				VisitTransmuteExpression(transmuteExpressionSyntax);
				break;
			case CCastExpressionSyntax castExpressionSyntax:
				VisitCCastExpression(castExpressionSyntax);
				break;
			case IndexSliceExpressionSyntax indexSliceExpressionSyntax:
				VisitIndexSliceExpression(indexSliceExpressionSyntax);
				break;
			case FunctionCallExpressionSyntax functionCallExpressionSyntax:
				VisitFunctionCall(functionCallExpressionSyntax);
				break;
			case MixinExpressionSyntax mixinExpressionSyntax:
				VisitMixinExpression(mixinExpressionSyntax);
				break;
			case IntrinsicSyntax intrinsicSyntax:
				VisitIntrinsicExpression(intrinsicSyntax);
				break;
			case IsExpressionSyntax isExpressionSyntax:
				VisitIsExpression(isExpressionSyntax);
				break;
			case NewExpressionSyntax newExpressionSyntax:
				VisitNewExpression(newExpressionSyntax);
				break;
			case DeleteExpressionSyntax deleteExpressionSyntax:
				VisitDeleteExpression(deleteExpressionSyntax);
				break;
			case ClosureSyntax lambdaSyntax:
				VisitLambda(lambdaSyntax);
				break;
			case LiteralExpressionSyntax literalExpressionSyntax:
				VisitLiteral(literalExpressionSyntax);
				break;
			case ArrayLiteralExpressionSyntax arrayLiteralExpressionSyntax:
				VisitArrayLiteral(arrayLiteralExpressionSyntax);
				break;
			case AssocArrayLiteralExpressionSyntax assocArrayLiteralExpressionSyntax:
				VisitAssocArrayLiteral(assocArrayLiteralExpressionSyntax);
				break;
			case StructInitializerSyntax structInitializer:
				VisitStructInitializer(structInitializer);
				break;
			case SpecialKeywordExpressionSyntax specialKeywordExpressionSyntax:
				VisitSpecialKeyword(specialKeywordExpressionSyntax);
				break;
			case TypeExpressionSyntax typeExprSyntax:
				VisitTypeExpression(typeExprSyntax);
				break;
			}
		}

		protected virtual void VisitExpression(ExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
			VisitToken(syntax.SemicolonToken);
		}

		protected virtual void VisitCommaExpression(CommaExpressionSyntax syntax)
		{
			foreach (CommaExpressionElemSyntax elem in syntax.Expressions)
			{
				VisitSimpleExpression(elem.Expression);
				VisitToken(elem.CommaToken);
			}
		}

		protected virtual void VisitDeclaration(DeclarationSyntax syntax)
		{
			VisitName(syntax.Names);
			if (syntax.TypeSpecificationToken != null)
			{
				VisitToken(syntax.TypeSpecificationToken);
				VisitType(syntax.Type);
			}
			if (syntax.OpenBraceToken != null)
			{
				VisitToken(syntax.OpenBraceToken);
				foreach (PropertyGetSetSyntax getset in syntax.GetSet)
				{
					if (getset.Attributes != null)
						VisitAttributes(getset.Attributes);
					VisitToken(getset.Identifier);
					if (getset.Body != null)
						VisitBlockStatement(getset.Body);
					if (getset.SemicolonToken != null)
						VisitToken(getset.SemicolonToken);
				}
				VisitToken(syntax.CloseBraceToken);
			}
			if (syntax.AssignmentToken != null)
			{
				VisitToken(syntax.AssignmentToken);
				VisitSimpleExpression(syntax.Initializer);
			}
			VisitToken(syntax.SemicolonToken);
		}

		protected virtual void VisitTernaryExpression(TernaryExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Condition);
			VisitToken(syntax.QuestionToken);
			VisitSimpleExpression(syntax.Then);
			VisitToken(syntax.ColonToken);
			VisitSimpleExpression(syntax.Else);
		}

		protected virtual void VisitBinaryExpression(BinaryExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Left);
			VisitToken(syntax.OperatorToken);
			VisitSimpleExpression(syntax.Right);
		}

		protected virtual void VisitAssignExpression(AssignExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Left);
			VisitToken(syntax.AssignToken);
			VisitSimpleExpression(syntax.Right);
		}

		protected virtual void VisitPrefixExpression(PrefixExpressionSyntax syntax)
		{
			VisitToken(syntax.OperatorToken);
			VisitSimpleExpression(syntax.Right);
		}

		protected virtual void VisitPostfixExpression(PostfixExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Left);
			VisitToken(syntax.OperatorToken);
		}

		protected virtual void VisitBracketedExpression(BracketedExpressionSyntax syntax)
		{
			VisitToken(syntax.OpenParen);
			VisitSimpleExpression(syntax.Expression);
			VisitToken(syntax.CloseParen);
		}

		protected virtual void VisitCastExpression(CastExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
			VisitToken(syntax.AsToken);
			VisitType(syntax.Type);
		}

		protected virtual void VisitTransmuteExpression(TransmuteExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
			VisitToken(syntax.TransmuteToken);
			VisitType(syntax.Type);
		}

		protected virtual void VisitCCastExpression(CCastExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
			VisitToken(syntax.CCastToken);
			VisitType(syntax.Type);
		}

		protected virtual void VisitIndexSliceExpression(IndexSliceExpressionSyntax syntax)
		{
			VisitSimpleExpression(syntax.Expression);
			VisitToken(syntax.OpenBracketToken);
			VisitSimpleExpression(syntax.IndexSliceExpression);
			VisitToken(syntax.CloseBracketToken);
		}

		protected virtual void VisitFunctionCall(FunctionCallExpressionSyntax syntax)
		{
			VisitQualifiedName(syntax.QualifiedName);
			VisitToken(syntax.OpenParenToken);
			syntax.Arguments?.ForEach(VisitArgument);
			VisitToken(syntax.CloseParenToken);
		}

		protected virtual void VisitMixinExpression(MixinExpressionSyntax syntax)
		{
			VisitToken(syntax.MixinToken);
			VisitToken(syntax.OpenParenToken);
			VisitSimpleExpression(syntax.Expression);
			VisitToken(syntax.CloseParenToken);
		}

		protected virtual void VisitIntrinsicExpression(IntrinsicSyntax syntax)
		{
			VisitToken(syntax.IntrinsicToken);
			VisitToken(syntax.OpenParenToken);
			VisitSimpleExpression(syntax.Expression);
			VisitToken(syntax.CloseParenToken);
		}

		protected virtual void VisitIsExpression(IsExpressionSyntax syntax)
		{
			VisitToken(syntax.IsToken);
			VisitToken(syntax.OpenParen);
			VisitSimpleExpression(syntax.Expression);
			if (syntax.ColonToken != null)
				VisitToken(syntax.ColonToken);
			if (syntax.EqualsEqualsToken != null)
				VisitToken(syntax.EqualsEqualsToken);
			VisitType(syntax.Type);
			VisitToken(syntax.CloseParen);
		}

		protected virtual void VisitNewExpression(NewExpressionSyntax syntax)
		{
			VisitToken(syntax.NewToken);
			if (syntax.OpenNewParen != null)
			{
				VisitToken(syntax.OpenNewParen);
				syntax.NewArguments.ForEach(VisitArgument);
				VisitToken(syntax.CloseNewParen);
			}
			VisitType(syntax.Type);
			if (syntax.OpenTypeParen != null)
			{
				VisitToken(syntax.OpenTypeParen);
				syntax.TypeArguments.ForEach(VisitArgument);
				VisitToken(syntax.CloseTypeParen);
			}
			if (syntax.OpenArrayBracket != null)
			{
				VisitToken(syntax.OpenArrayBracket);
				VisitSimpleExpression(syntax.ArrayExpression);
				VisitToken(syntax.CloseArrayBracket);
			}
		}

		protected virtual void VisitDeleteExpression(DeleteExpressionSyntax syntax)
		{
			VisitToken(syntax.DeleteToken);
			VisitSimpleExpression(syntax.Expression);
		}

		protected virtual void VisitLambda(ClosureSyntax syntax)
		{
			VisitSimpleExpression(syntax.Identifiers);
			VisitToken(syntax.DoubleArrowToken);
			if (syntax.OpenBracketToken != null)
			{
				VisitToken(syntax.OpenBracketToken);
				foreach (ClosureCaptureSyntax capture in syntax.Captures)
				{
					if (capture.EqualsToken != null)
						VisitToken(capture.EqualsToken);
					if (capture.AndToken != null)
						VisitToken(capture.AndToken);
					if (capture.Name != null)
						VisitName(capture.Name);
					if (capture.CommaToken != null)
						VisitToken(capture.CommaToken);
				}
				VisitToken(syntax.CloseBracketToken);
			}
			VisitStatement(syntax.Body);
		}

		protected virtual void VisitLiteral(LiteralExpressionSyntax syntax) 
		{
			VisitToken(syntax.LiteralToken);
		}

		protected virtual void VisitArrayLiteral(ArrayLiteralExpressionSyntax syntax)
		{
			VisitToken(syntax.OpenBracketToken);
			foreach (ArrayLiteralElemSyntax element in syntax.Elements)
			{
				VisitSimpleExpression(element.Expression);
				VisitToken(element.CommaToken);
			}
			VisitToken(syntax.CloseBracketToken);
		}

		protected virtual void VisitAssocArrayLiteral(AssocArrayLiteralExpressionSyntax syntax)
		{
			VisitToken(syntax.OpenBracketToken);
			foreach (AssocArrayLiteralElemSyntax element in syntax.Elements)
			{
				VisitSimpleExpression(element.Left);
				VisitToken(element.ColonToken);
				VisitSimpleExpression(element.Right);
				if (element.CommaToken != null)
					VisitToken(element.CommaToken);
			}
			VisitToken(syntax.CloseBracketToken);
		}

		protected virtual void VisitStructInitializer(StructInitializerSyntax syntax)
		{
			VisitToken(syntax.OpenBraceToken);
			if (syntax.Elements != null)
			{
				foreach (StructInitializerElemSyntax element in syntax.Elements)
				{
					if (element.DotToken != null)
					{
						VisitToken(element.DotToken);
						VisitIdentifierName(element.Identifier);
						VisitToken(element.ColonToken);
					}

					VisitSimpleExpression(element.Expression);
					if (element.CommaToken != null)
						VisitToken(element.CommaToken);
				}
			}

			VisitToken(syntax.CloseBraceToken);
		}

		protected virtual void VisitSpecialKeyword(SpecialKeywordExpressionSyntax syntax)
		{
			VisitToken(syntax.SpecialKeywordToken);
		}

		protected virtual void VisitBitField(BitFieldTypeSyntax syntax)
		{
			VisitType(syntax.BaseType);
			VisitToken(syntax.DotToken);
			VisitSimpleExpression(syntax.ExprSyntax);
		}

		protected virtual void VisitTypeExpression(TypeExpressionSyntax syntax)
		{
			VisitToken(syntax.OpenExprToken);
			VisitType(syntax.Type);
			VisitToken(syntax.CloseExprToken);
		}

		protected virtual void VisitToken(SyntaxToken token)
		{
		}
	}
}
