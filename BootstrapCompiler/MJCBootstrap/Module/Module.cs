﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend;
using MJC.ILBackend.General;
using MJC.IRBackend;

namespace MJC.Module
{

	public class Module
	{
		public ModuleHeader Header = new ModuleHeader();
		public Dictionary<string, Table> Tables = new Dictionary<string, Table>();
		public List<ObjectFile> ObjectFiles = new List<ObjectFile>();

		public ILCompUnit ILCompUnit;
		public IRModule IRModule;
		public List<Symbol> Symbols = new List<Symbol>();

		public Module()
		{ }

		public Module(byte[] data)
		{
			Header = new ModuleHeader(data, 0);

			int offset = Header.GetSize();
			for (int i = 0; i < Header.NumSymbolTables; i++)
			{
				Table table = new Table(data, offset);
				Tables.Add(table.Iden, table);
				offset += table.GetSize();
			}

			for (int i = 0; i < Header.NumObjectFiles; i++)
			{
				ObjectFile file = new ObjectFile(data, offset);
				ObjectFiles.Add(file);
				offset += file.GetSize();
			}
		}

		public byte[] ToBin()
		{
			UpdateSize();
			int size = (int)Header.FileSize;
			byte[] data = new byte[size];

			byte[] headerBin = Header.ToBin();
			Array.Copy(headerBin, 0, data, 0, headerBin.Length);
			
			int offset = headerBin.Length;
			foreach (KeyValuePair<string, Table> pair in Tables)
			{
				byte[] bin = pair.Value.ToBin();
				Array.Copy(bin, 0, data, offset, bin.Length);
				offset += bin.Length;
			}

			foreach (ObjectFile file in ObjectFiles)
			{
				byte[] bin = file.ToBin();
				Array.Copy(bin, 0, data, offset, bin.Length);
				offset += bin.Length;
			}

			return data;
		}

		public uint UpdateSize()
		{
			uint size = (uint)Header.GetSize();

			foreach (KeyValuePair<string, Table> pair in Tables)
			{
				size += (uint)pair.Value.GetSize();
			}

			foreach (ObjectFile file in ObjectFiles)
			{
				size += (uint)file.GetSize();
			}

			return Header.FileSize = size;
		}

		public bool TryGetTable(string iden, out Table table)
		{
			return Tables.TryGetValue(iden, out table);
		}

		public void GenerateILCompUnit()
		{
			ILBuilder builder = new ILBuilder();
			builder.BeginRaw();
			builder.BuildModuleDirective(Header.Package, Header.Module);

			// TODO
			foreach (Symbol symbol in Symbols)
			{
				switch (symbol.Kind)
				{
				case SymbolKind.Struct:
					break;
				case SymbolKind.Interface:
					break;
				case SymbolKind.Union:
					break;
				case SymbolKind.Enum:
					break;
				case SymbolKind.EnumMember:
					break;
				case SymbolKind.Variable:
				{
					ILAttributes attribs = ILHelpers.GetAttributes(symbol, null);
					builder.BuildGlobal(symbol.MangledName, symbol.Type, "", attribs, null);
					// TODO: Value
					break;
				}
				case SymbolKind.LocalVar:
					break;
				case SymbolKind.Typedef:
					break;
				case SymbolKind.TypeAlias:
				{
					// TODO: Is this really needed?
					builder.BuildAlias(symbol.MangledName, null);
					break;
				}
				case SymbolKind.Function:
				{
					ILAttributes attribs = ILHelpers.GetAttributes(symbol, null);
					List<ILTemplateParam> templateParams = null;
					// TODO
					builder.BeginFunction(symbol.MangledName, symbol.Type as FunctionSymbolType, attribs, templateParams, false, null);
					builder.EndFunction();
					break;
				}
				case SymbolKind.Method:
				{
					ILAttributes attribs = ILHelpers.GetAttributes(symbol, null);
					List<ILTemplateParam> templateParams = null;
					// TODO
					builder.BeginFunction(symbol.MangledName, symbol.Type as FunctionSymbolType, attribs, templateParams, false, null);
					builder.EndFunction();

					FunctionSymbolType funcType = symbol.Type as FunctionSymbolType;
					string mangledReceiver = (funcType.ReceiverType as AggregateSymbolType).MangledIdentifier;
					//builder.AddVTableMethod(mangledReceiver, symbol.Identifier, funcType, symbol.MangledName); TODO
					break;
				}
				case SymbolKind.Receiver:
					break;
				case SymbolKind.Delegate:
					break;
				}
			}

			ILCompUnit = builder.End();
		}

		public void GenerateIRModule()
		{

		}
	}

	public enum ModuleFlags : ushort
	{

		// LTO
		LTOObj			= 0b0000_0000_0000_0000,
		LTOIR			= 0b0000_0000_0000_0100,
		LTOIL			= 0b0000_0000_0000_1000,
		LTOUnoptIL		= 0b0000_0000_0000_1100,

		StaticOnly		= 0b0000_0000_0001_0000,
		DynamicOnly		= 0b0000_0000_0010_0000,

		HasTemplates	= 0b0000_0001_0000_0000,
	}

	public class ModuleHeader
	{
		public static uint CorrectMagic = 0x4D4A4D80;

		public uint Magic;
		public uint FileSize;
		public ModuleFlags Flags;

		public string Package;
		public string Module;

		public short NumSymbolTables;
		public short NumObjectFiles;

		public ModuleHeader()
		{
		}

		public ModuleHeader(byte[] data, int pos)
		{
			int offset = pos;
			ByteUtils.ReadFromBuffer(data, offset, out Magic);
			offset += 4;
			ByteUtils.ReadFromBuffer(data, offset, out FileSize);
			offset += 4;
			ushort flags;
			ByteUtils.ReadFromBuffer(data, offset, out flags);
			Flags = (ModuleFlags)flags;

			offset += 2;
			ByteUtils.ReadNullTerminatedCStr(data, offset, out Package);
			offset += Package.Length + 1;
			ByteUtils.ReadNullTerminatedCStr(data, offset, out Module);

			offset += Module.Length + 1;
			ByteUtils.ReadFromBuffer(data, offset, out NumSymbolTables);
			offset += 2;
			ByteUtils.ReadFromBuffer(data, offset, out NumObjectFiles);
		}

		public byte[] ToBin()
		{
			int size = GetSize();
			byte[] data = new byte[size];

			int offset = 0;
			ByteUtils.WriteToBuffer(data, offset, Magic);
			offset += 4;
			ByteUtils.WriteToBuffer(data, offset, FileSize);
			offset += 4;
			ByteUtils.WriteToBuffer(data, offset, (uint)Flags);

			offset += 2;
			ByteUtils.WriteNullTerminatedCStr(data, offset, Package);
			offset += Package.Length + 1;
			ByteUtils.WriteNullTerminatedCStr(data, offset, Module);

			offset += Module.Length + 1;
			ByteUtils.WriteToBuffer(data, offset, NumSymbolTables);
			offset += 2;
			ByteUtils.WriteToBuffer(data, offset, NumObjectFiles);

			return data;
		}

		public int GetSize()
		{
			return 10 + Package.Length + 1 + Module.Length + 1 + 4;
		}
	}

	public class Table
	{
		public string Iden;
		public List<BaseTableEntry> Entries = new List<BaseTableEntry>();

		public Table()
		{ }

		public Table(byte[] data, int pos)
		{
			Iden += $"{(char) data[pos]}{(char) data[pos + 1]}{(char) data[pos + 2]}";

			int numEntries;
			ByteUtils.ReadFromBuffer(data, pos + 4, out numEntries);

			int offset = pos + 8;

			switch (Iden)
			{
			case "exp":
			case "pub":
			case "pak":
			case "imp":
			case "glo":
			case "ali":
			case "del":
			{
				for (int i = 0; i < numEntries; i++)
				{
					TableEntry entry = new TableEntry(data, offset);
					Entries.Add(entry);
					offset += entry.GetSize();
				}

				break;
			}
			case "str":
			case "uni":
			case "enu":
			case "adt":
			{
				for (int i = 0; i < numEntries; i++)
				{
					TypeTableEntry entry = new TypeTableEntry(data, offset);
					Entries.Add(entry);
					offset += entry.GetSize();
				}

				break;
			}
			}


			
		}

		public byte[] ToBin()
		{
			int size = 8;
			List<byte[]> entryBins = new List<byte[]>();

			foreach (BaseTableEntry entry in Entries)
			{
				byte[] bin = entry.ToBin();
				size += bin.Length;
				entryBins.Add(bin);
			}

			// Write data
			byte[] data = new byte[size];

			ByteUtils.WriteNullTerminatedCStr(data, 0, Iden);

			uint entries = (uint) Entries.Count;
			ByteUtils.WriteToBuffer(data, 4, entries);

			int offset = 8;
			foreach (byte[] bin in entryBins)
			{
				Array.Copy(bin, 0, data, offset, bin.Length);
				offset += bin.Length;
			}

			return data;
		}

		public int GetSize()
		{
			int size = 8;
			foreach (BaseTableEntry entry in Entries)
			{
				size += entry.GetSize();
			}
			return size;
		}
	}

	public class BaseTableEntry
	{
		public virtual byte[] ToBin()
		{
			return null;
		}

		public virtual int GetSize()
		{
			return 0;
		}
	}
	
	public class TableEntry : BaseTableEntry
	{
		public SymbolInfo Info;
		public string MangledName;
		public string MangledType;
		public string FileName = "";

		public TableEntry()
		{
			Info = new SymbolInfo();
		}

		public TableEntry(byte[] data, int pos)
		{
			Info = new SymbolInfo(data, pos);
			int offset = 4;
			ByteUtils.ReadNullTerminatedCStr(data, pos + offset, out MangledName);
			offset += MangledName.Length + 1;

			if ((Info.Flags & SymbolInfoFlags.Unmangled) != 0)
			{
				ByteUtils.ReadNullTerminatedCStr(data, pos + offset, out MangledType);
				offset += MangledType.Length + 1;
			}

			ByteUtils.ReadNullTerminatedCStr(data, pos + offset, out FileName);
		}

		public override byte[] ToBin()
		{
			int size = GetSize();

			byte[] data = new byte[size];

			byte[] tmp = Info.ToBin();
			ByteUtils.WriteToBuffer(data, 0, tmp);
			int offset = 4;
			ByteUtils.WriteNullTerminatedCStr(data, offset, MangledName);
			offset += MangledName.Length + 1;

			if ((Info.Flags & SymbolInfoFlags.Unmangled) != 0)
			{
				ByteUtils.WriteNullTerminatedCStr(data, offset, MangledType);
				offset += MangledType.Length + 1;
			}

			ByteUtils.WriteNullTerminatedCStr(data, offset, FileName);
			return data;
		}

		public override int GetSize()
		{
			int size = Info.GetSize() + MangledName.Length + 1 + FileName.Length + 1;

			if ((Info.Flags & SymbolInfoFlags.Unmangled) != 0)
			{
				size += MangledType.Length + 1;
			}

			return size;
		}
	}

	public class TypeTableEntry : BaseTableEntry
	{
		public SymbolInfo Info;
		public string MangledName;
		public List<string> Children = new List<string>();

		public TypeTableEntry()
		{
			Info = new SymbolInfo();
		}

		public TypeTableEntry(byte[] data, int pos)
		{
			Info = new SymbolInfo(data, pos);
			int offset = pos + 4;
			ByteUtils.ReadNullTerminatedCStr(data, offset, out MangledName);
			offset += MangledName.Length + 1;

			int numChildren;
			ByteUtils.ReadFromBuffer(data, offset, out numChildren);
			offset += 4;

			for (int i = 0; i < numChildren; i++)
			{
				string tmp;
				ByteUtils.ReadNullTerminatedCStr(data, offset, out tmp);
				offset += tmp.Length + 1;
				Children.Add(tmp);
			}
		}

		public override byte[] ToBin()
		{
			int size = GetSize();

			byte[] data = new byte[size];

			byte[] tmp = Info.ToBin();
			ByteUtils.WriteToBuffer(data, 0, tmp);
			int offset = 4;
			ByteUtils.WriteNullTerminatedCStr(data, offset, MangledName);
			offset += MangledName.Length + 1;
			ByteUtils.WriteToBuffer(data, offset, Children.Count);
			offset += 4;
			foreach (string child in Children)
			{
				ByteUtils.WriteNullTerminatedCStr(data, offset, child);
				offset += child.Length + 1;
			}
			
			return data;
		}

		public override int GetSize()
		{
			int size = 4 + MangledName.Length + 1 + 4;
			foreach (string child in Children)
			{
				size += child.Length + 1;
			}
			return size;
		}
	}

	public class ObjectFile
	{
		public string FileName;
		public byte[] Data;

		public ObjectFile()
		{ }

		public ObjectFile(byte[] data, int pos)
		{
			int offset = pos;
			ByteUtils.ReadNullTerminatedCStr(data, offset, out FileName);

			uint size;
			offset += FileName.Length + 1;
			ByteUtils.ReadFromBuffer(data, offset, out size);

			Data = new byte[size];
			offset += 4;
			Array.Copy(data, offset, Data, 0, size);
		}

		public byte[] ToBin()
		{
			int size = GetSize();
			byte[] data = new byte[size];

			ByteUtils.WriteNullTerminatedCStr(data, 0, FileName);
			int offset = FileName.Length + 1;
			ByteUtils.WriteToBuffer(data, offset, (uint) Data.Length);
			offset += 4;
			Array.Copy(Data, 0, data, offset, Data.Length);

			return data;
		}

		public int GetSize()
		{
			return FileName.Length + 1 + 4 + Data.Length;
		}
	}

	[Flags]
	public enum SymbolInfoFlags : ushort
	{
		None		= 0b0000_0000_0000_0000,
		Unmangled	= 0b0000_0000_0000_0001,
		Extern		= 0b0000_0000_0000_0010,
	}

	[Flags]
	public enum DelegateSymbolFlags : ushort
	{
		None	= 0b0000_0000_0000_0000,
		FuncPtr	= 0b0000_0000_0000_0001,
	}

	public class SymbolInfo
	{
		public SymbolInfoFlags Flags;
		public ushort TypeSpecific;

		public SymbolInfo()
		{ }

		public SymbolInfo(byte[] data, int pos)
		{
			ushort tmp;
			ByteUtils.ReadFromBuffer(data, pos, out tmp);
			Flags = (SymbolInfoFlags) tmp;
			ByteUtils.ReadFromBuffer(data, pos + 2, out TypeSpecific);
		}

		public byte[] ToBin()
		{
			byte[] data = new byte[4];

			ByteUtils.WriteToBuffer(data, 0, (ushort)Flags);
			ByteUtils.WriteToBuffer(data, 2, TypeSpecific);

			return data;
		}

		public int GetSize()
		{
			return 4;
		}
	}

}
