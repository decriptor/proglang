﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MJC.Module
{
	class ModuleImporter
	{
		public static Module Import(string file)
		{
			byte[] data = File.ReadAllBytes(file);
			Module module = new Module(data);

			return module;
		}

		public static Module Import(byte[] data)
		{
			Module module = new Module(data);

			return module;
		}

	}
}
