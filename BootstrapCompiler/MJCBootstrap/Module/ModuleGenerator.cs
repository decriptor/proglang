﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MJC.General;
using MJC.ILBackend;
using MJC.IRBackend;
using MJC.SyntaxTree.SemanticAnalysis;

namespace MJC.Module
{
	public enum LTOType
	{
		Obj,
		IR,
		IL,
		UnoptIL,
	}

	class ModuleGenerator
	{
		struct EntrySymbol
		{
			public Symbol Sym;
			public string Src;
		}

		public static Module Generate(List<SyntaxTree.SyntaxTree> trees, List<string> objFiles, string packageName, string moduleName, bool isDynamic, LTOType lto)
		{
			Module module = new Module();
			module.Header.Magic = ModuleHeader.CorrectMagic;
			module.Header.Package = packageName;
			module.Header.Module = moduleName;

			List<Identifier> baseScopeNames = new List<Identifier>();
			string[] packageParts = packageName.Split('.');
			foreach (string part in packageParts)
				baseScopeNames.Add(new IdentifierName(part));
			baseScopeNames.Add(new IdentifierName(moduleName));

			List<EntrySymbol> symbols = new List<EntrySymbol>();
			List<string> aliases = new List<string>();

			foreach (SyntaxTree.SyntaxTree tree in trees)
			{
				SymbolTable table = tree.Symbols;

				SymbolTable baseTable = table.FindTable(new Scope(baseScopeNames));
				
				Stack<SymbolTable> tableStack = new Stack<SymbolTable>();
				tableStack.Push(baseTable);

				while (tableStack.Count > 0)
				{
					SymbolTable tmp = tableStack.Pop();

					foreach (KeyValuePair<Identifier, List<Symbol>> pair in tmp.Symbols)
					{
						foreach (Symbol symbol in pair.Value)
						{
							symbols.Add(new EntrySymbol { Sym = symbol, Src = tree.FileName });
						}

						/*foreach (Symbol symbol in pair.Value)
						{
							...
							else if (symbol.CompileAttribs.ContainsKey(ForeignCompileAttribute.Id))
							{
								ForeignCompileAttribute compileAttribute = symbol.CompileAttribs[ForeignCompileAttribute.Id] as ForeignCompileAttribute;
								if (compileAttribute?.Library != null)
								{
									entry.FileName = compileAttribute.Library;
									importEntries.Add(entry);
								}

							}
						}*/
					}

					foreach (KeyValuePair<Identifier, SymbolTable> pair in tmp.SubTables)
					{
						tableStack.Push(pair.Value);
					}
				}

				ILCompUnit compUnit = tree.CompUnit;
				aliases.AddRange(compUnit.Aliases);
			}

			Dictionary<string, List<BaseTableEntry>> entries = CreateSymbolTables(symbols, lto);

			foreach (KeyValuePair<string, List<BaseTableEntry>> entry in entries)
			{
				Table table = new Table();
				table.Iden = entry.Key;
				table.Entries = entry.Value;

				module.Tables.Add(table.Iden, table);
			}

			CreateAliasTable(aliases, module);

			module.Header.NumSymbolTables = (short)module.Tables.Count;

			HashSet<string> addedFiles = new HashSet<string>();

			if (entries.ContainsKey("pub"))
			{
				List<BaseTableEntry> publicEntries = entries["pub"];
				foreach (TableEntry entry in publicEntries)
				{
					if ((entry.Info.Flags & SymbolInfoFlags.Extern) == 0 &&
					    !addedFiles.Contains(entry.FileName))
					{
						string objPath = "";
						if (!string.IsNullOrEmpty(CmdLine.IntermediateDirectory))
							objPath = CmdLine.IntermediateDirectory;

						byte[] data = File.ReadAllBytes(objPath + entry.FileName);
						ObjectFile file = new ObjectFile();
						file.FileName = entry.FileName;
						file.Data = data;
						module.ObjectFiles.Add(file);
						addedFiles.Add(entry.FileName);
					}
				}

			}

			if (entries.ContainsKey("glo"))
			{
				List<BaseTableEntry> publicEntries = entries["glo"];
				foreach (TableEntry entry in publicEntries)
				{
					if (!addedFiles.Contains(entry.FileName))
					{
						string objPath = "";
						if (!string.IsNullOrEmpty(CmdLine.IntermediateDirectory))
							objPath = CmdLine.IntermediateDirectory;

						byte[] data = File.ReadAllBytes(objPath + entry.FileName);
						ObjectFile file = new ObjectFile();
						file.FileName = entry.FileName;
						file.Data = data;
						module.ObjectFiles.Add(file);
						addedFiles.Add(entry.FileName);
					}
				}
			}

			module.Header.NumObjectFiles = (short) module.ObjectFiles.Count;
			module.UpdateSize();
			return module;
		}

		public static void ModuleOut(Module module, string file)
		{
			string dir = Path.GetDirectoryName(file);
			if (!string.IsNullOrEmpty(dir) && !Directory.Exists(dir))
				Directory.CreateDirectory(dir);

			using (FileStream fileStream = new FileStream(file, FileMode.Create))
			using (BinaryWriter writer = new BinaryWriter(fileStream))
			{
				byte[] bin = module.ToBin();
				writer.Write(bin);
			}
		}

		static Dictionary<string, List<BaseTableEntry>> CreateSymbolTables(List<EntrySymbol> symbols, LTOType lto)
		{
			Dictionary<string, List<BaseTableEntry>> entries = new Dictionary<string, List<BaseTableEntry>>();

			foreach (EntrySymbol symbol in symbols)
			{
				switch (symbol.Sym.Kind)
				{
				case SymbolKind.Struct:
					ProcessStruct(symbol, entries);
					break;
				case SymbolKind.Interface:
					break;
				case SymbolKind.Union:
					ProcessUnion(symbol, entries);
					break;
				case SymbolKind.Enum:
				{
					EnumSymbolType enumType = symbol.Sym.Type as EnumSymbolType;
					if (enumType.IsSimpleEnum)
						ProcessEnum(symbol, entries);
					else
						ProcessAdtEnum(symbol, entries);
				}
					break;
				case SymbolKind.Variable:
				{
					if (symbol.Sym.Parent == null)
						ProcessGlobal(symbol, entries, lto);
					break;
				}
				case SymbolKind.Typedef:
					break;
				case SymbolKind.TypeAlias:
					break;
				case SymbolKind.Function:
				case SymbolKind.Method:
					ProcessFunctionEntry(symbol, entries, lto);
					break;
				case SymbolKind.Delegate:
					ProcessDelegate(symbol, entries);
					break;
				}
			}

			return entries;
		}

		static void ProcessFunctionEntry(EntrySymbol entrySym, Dictionary<string, List<BaseTableEntry>> entries, LTOType lto)
		{
			string localExt;
			switch (lto)
			{
			default:
			case LTOType.Obj:
				localExt = ".o";
				break;
			case LTOType.IR:
				localExt = ".mjir";
				break;
			case LTOType.IL:
			case LTOType.UnoptIL:
				localExt = ".mjil";
				break;
			}

			Symbol symbol = entrySym.Sym;
			TableEntry entry = new TableEntry();
			entry.MangledName = symbol.MangledName;
			if (!entry.MangledName.StartsWith("_M"))
			{
				string mangledType = NameMangling.MangleType(symbol.Type);
				entry.MangledType = mangledType;
				entry.Info.Flags |= SymbolInfoFlags.Unmangled;
			}

			if (symbol.CompileAttribs.ContainsKey(ForeignCompileAttribute.Id))
			{
				entry.Info.Flags |= SymbolInfoFlags.Extern;
			}
			
			if (symbol.Visibility == SemanticVisibility.Public)
			{
				if (!entries.ContainsKey("pub"))
					entries.Add("pub", new List<BaseTableEntry>());

				List<BaseTableEntry> pubEntries = entries["pub"];
				
				if ((entry.Info.Flags & SymbolInfoFlags.Extern) == 0)
					entry.FileName = entrySym.Src + localExt;

				pubEntries.Add(entry);
			}
			else if (symbol.Visibility == SemanticVisibility.Export)
			{
				if (!entries.ContainsKey("exp"))
					entries.Add("exp", new List<BaseTableEntry>());

				List<BaseTableEntry> expEntries = entries["exp"];

				// TODO: System specific dynamic extension .dll or .so
				if ((entry.Info.Flags & SymbolInfoFlags.Extern) == 0)
					entry.FileName = entrySym.Src + ".dll";

				expEntries.Add(entry);
			}
			else if (symbol.Visibility == SemanticVisibility.Package)
			{
				if (!entries.ContainsKey("pak"))
					entries.Add("pak", new List<BaseTableEntry>());

				List<BaseTableEntry> pakEntries = entries["pak"];

				if ((entry.Info.Flags & SymbolInfoFlags.Extern) == 0)
					entry.FileName = entrySym.Src + localExt;

				pakEntries.Add(entry);
			}
		}

		static void ProcessGlobal(EntrySymbol entrySym, Dictionary<string, List<BaseTableEntry>> entries, LTOType lto)
		{
			string localExt;
			switch (lto)
			{
			default:
			case LTOType.Obj:
				localExt = ".o";
				break;
			case LTOType.IR:
				localExt = ".mjir";
				break;
			case LTOType.IL:
			case LTOType.UnoptIL:
				localExt = ".mjil";
				break;
			}

			Symbol symbol = entrySym.Sym;
			TableEntry entry = new TableEntry();
			entry.MangledName = symbol.MangledName;
			if (!entry.MangledName.StartsWith("_M"))
			{
				string mangledType = NameMangling.MangleType(symbol.Type);
				entry.MangledType = mangledType;
				entry.Info.Flags |= SymbolInfoFlags.Unmangled;
			}

			if (symbol.CompileAttribs.ContainsKey(ForeignCompileAttribute.Id))
			{
				entry.Info.Flags |= SymbolInfoFlags.Extern;
			}

			entry.FileName = entrySym.Src + localExt;

			if (!entries.ContainsKey("glo"))
				entries.Add("glo", new List<BaseTableEntry>());

			List<BaseTableEntry> gloEntries = entries["glo"];

			gloEntries.Add(entry);
		}

		static void ProcessStruct(EntrySymbol entrySym, Dictionary<string, List<BaseTableEntry>> entries)
		{
			Symbol symbol = entrySym.Sym;
			TypeTableEntry entry = new TypeTableEntry();

			entry.MangledName = symbol.MangledName;

			foreach (Symbol child in symbol.Children)
			{
				entry.Children.Add(child.MangledName);
			}

			if (!entries.ContainsKey("str"))
				entries.Add("str", new List<BaseTableEntry>());

			List<BaseTableEntry> strEntries = entries["str"];
			strEntries.Add(entry);
		}

		static void ProcessUnion(EntrySymbol entrySym, Dictionary<string, List<BaseTableEntry>> entries)
		{
			Symbol symbol = entrySym.Sym;
			TypeTableEntry entry = new TypeTableEntry();

			entry.MangledName = symbol.MangledName;

			foreach (Symbol child in symbol.Children)
			{
				entry.Children.Add(child.MangledName);
			}

			if (!entries.ContainsKey("uni"))
				entries.Add("uni", new List<BaseTableEntry>());

			List<BaseTableEntry> uniEntries = entries["uni"];
			uniEntries.Add(entry);
		}

		static void ProcessEnum(EntrySymbol entrySym, Dictionary<string, List<BaseTableEntry>> entries)
		{
			Symbol symbol = entrySym.Sym;
			TypeTableEntry entry = new TypeTableEntry();

			entry.MangledName = symbol.MangledName;

			foreach (Symbol child in symbol.Children)
			{
				entry.Children.Add(child.MangledName);
			}

			if (!entries.ContainsKey("enu"))
				entries.Add("enu", new List<BaseTableEntry>());

			List<BaseTableEntry> enuEntries = entries["enu"];
			enuEntries.Add(entry);
		}

		static void ProcessAdtEnum(EntrySymbol entrySym, Dictionary<string, List<BaseTableEntry>> entries)
		{
			Symbol symbol = entrySym.Sym;
			TypeTableEntry entry = new TypeTableEntry();

			entry.MangledName = symbol.MangledName;

			foreach (Symbol child in symbol.Children)
			{
				entry.Children.Add(child.MangledName);
			}

			if (!entries.ContainsKey("adt"))
				entries.Add("adt", new List<BaseTableEntry>());

			List<BaseTableEntry> adtEntries = entries["adt"];
			adtEntries.Add(entry);
		}

		static void ProcessDelegate(EntrySymbol entrySym, Dictionary<string, List<BaseTableEntry>> entries)
		{
			Symbol symbol = entrySym.Sym;
			TableEntry entry = new TableEntry(); // TODO: Should there be a delegate specific table entry?

			entry.MangledName = symbol.MangledName;

			DelegateSymbolFlags flags = DelegateSymbolFlags.None;
			if (symbol.CompileAttribs.ContainsKey(FuncPtrAttribute.Id))
				flags  |= DelegateSymbolFlags.FuncPtr;
			entry.Info.TypeSpecific = (ushort) flags;

			if (!entries.ContainsKey("del"))
				entries.Add("del", new List<BaseTableEntry>());

			List<BaseTableEntry> adtEntries = entries["del"];
			adtEntries.Add(entry);
		}

		static void CreateAliasTable(List<string> aliases, Module module)
		{
			if (aliases.Count == 0)
				return;

			List<BaseTableEntry> entries = new List<BaseTableEntry>();
			entries.Capacity = aliases.Count;
			foreach (string alias in aliases)
			{
				TableEntry entry = new TableEntry();
				entry.MangledName = alias;
				entry.FileName = "";
				entries.Add(entry);
			}

			Table modTable = new Table();
			modTable.Iden = "ali";
			modTable.Entries = entries;

			module.Tables.Add(modTable.Iden, modTable);
		}

	}
}
