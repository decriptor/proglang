﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend
{
	public enum ILInstructionType
	{
		Unknown,
		IntLiteral,
		FloatLiteral,
		StringLiteral,

		AllocStack,
		DeallocStack,

		Assign,
		Store,
		Load,

		FunctionRef,
		MethodRef,
		OperatorRef,

		Call,
		Builtin,
		CompilerIntrin,

		TupleUnpack,
		TuplePack,
		TupleInsert,
		TupleExtract,
		TupleElemAddr,

		StructExtract,
		StructInsert,
		StructElemAddr,
		StructTemplateValue,
		
		Destruct,

		EnumGetMemberValue,

		GlobalAddr,
		GlobalValue,

		ConvType,

		Return,
		Branch,
		CondBranch,
	}

	public class ILInstruction
	{
		public ILInstructionType InstructionType;
		public TextSpan Span;

		public bool IsTerminal()
		{
			switch (InstructionType)
			{
			case ILInstructionType.Return:
			case ILInstructionType.Branch:
			case ILInstructionType.CondBranch:
				return true;
			default:
				return false;
			}
		}

		public bool ExitsFunction()
		{
			switch (InstructionType)
			{
			case ILInstructionType.Return:
				return true;
			default:
				return false;
			}
		}

		public bool IsCompileTimeOnly()
		{
			switch (InstructionType)
			{
			case ILInstructionType.CompilerIntrin:
			case ILInstructionType.StructTemplateValue:
				return true;
			default:
				return false;
			}
		}
	}

	public class ILIntLiteralInstruction : ILInstruction
	{
		public ILIntLiteralInstruction(ILOperand retOperand, long value, TextSpan span)
		{
			Value = value;
			RetOperand = retOperand;
			InstructionType = ILInstructionType.IntLiteral;
			Span = span;
		}

		public long Value;
		public ILOperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = int_literal {Value} : ${RetOperand.Type.ToILString()}";
		}
	}

	public class ILFloatLiteralInstruction : ILInstruction
	{
		public ILFloatLiteralInstruction(ILOperand retOperand, long hexValue, TextSpan span)
		{
			HexValue = hexValue;
			RetOperand = retOperand;
			InstructionType = ILInstructionType.FloatLiteral;
			Span = span;
		}

		public long HexValue;
		public ILOperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = float_literal {HexValue:X8} : ${RetOperand.Type.ToILString()}";
		}
	}

	public class ILStringLiteralInstruction : ILInstruction
	{
		public ILStringLiteralInstruction(ILOperand retOperand, string str, TextSpan span)
		{
			String = str;
			RetOperand = retOperand;
			InstructionType = ILInstructionType.StringLiteral;
			Span = span;
		}

		public string String;
		public ILOperand RetOperand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = string_literal \"{String}\"";
		}
	}



	public class ILAllocStackInstruction : ILInstruction
	{
		public ILAllocStackInstruction(ILOperand retOperand, SymbolType type, TextSpan span)
		{
			RetOperand = retOperand;
			Type = type;
			InstructionType = ILInstructionType.AllocStack;
			Span = span;
		}

		public ILOperand RetOperand;
		public SymbolType Type;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = alloc_stack ${Type.ToILString()}";
		}
	}

	public class ILDeallocStackInstruction : ILInstruction
	{
		public ILDeallocStackInstruction(ILOperand operand)
		{
			InstructionType = ILInstructionType.DeallocStack;
			Operand = operand;
		}

		public ILOperand Operand;

		public override string ToString()
		{
			return $"dealloc_stack {Operand}";
		}
	}



	public class ILAssignInstruction : ILInstruction
	{
		public ILAssignInstruction(ILOperand src, ILOperand dst, TextSpan span)
		{
			Src = src;
			Dst = dst;
			InstructionType = ILInstructionType.Assign;
			Span = span;
		}

		public ILOperand Src;
		public ILOperand Dst;

		public override string ToString()
		{
			return $"assign %{Src.Iden} to {Dst}";
		}
	}

	public class ILStoreInstruction : ILInstruction
	{
		public ILStoreInstruction(ILOperand src, ILOperand dst, TextSpan span)
		{
			Src = src;
			Dst = dst;
			InstructionType = ILInstructionType.Store;
			Span = span;
		}

		public ILOperand Src;
		public ILOperand Dst;

		public override string ToString()
		{
			return $"store %{Src.Iden} in {Dst}";
		}
	}

	public class ILLoadInstruction : ILInstruction
	{
		public ILLoadInstruction(ILOperand retOperand, ILOperand operand, TextSpan span)
		{
			RetOperand = retOperand;
			Operand = operand;
			InstructionType = ILInstructionType.Load;
			Span = span;
		}

		public ILOperand RetOperand;
		public ILOperand Operand;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = load {Operand}";
		}
	}

	public class ILFunctionRefInstruction : ILInstruction
	{
		public ILFunctionRefInstruction(ILOperand retOperand, string mangledName, FunctionSymbolType type, TextSpan span)
		{
			RetOperand = retOperand;
			MangledName = mangledName;
			Type = type;
			InstructionType = ILInstructionType.FunctionRef;
			Span = span;
		}

		public ILOperand RetOperand;
		public string MangledName;
		public FunctionSymbolType Type;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = function_ref @{MangledName} : ${Type.ToILString()}";
		}
	}

	public class ILMethodRefInstruction : ILInstruction
	{
		public ILMethodRefInstruction(ILOperand retOperand, ILReference methodRef, FunctionSymbolType type, TextSpan span)
		{
			RetOperand = retOperand;
			MethodRef = methodRef;
			Type = type;
			InstructionType = ILInstructionType.MethodRef;
			Span = span;
		}

		public ILOperand RetOperand;
		public ILReference MethodRef;
		public FunctionSymbolType Type;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = method_ref {MethodRef} : ${Type.ToILString()}";
		}
	}

	public class ILOperatorRefInstruction : ILInstruction
	{
		public ILOperatorRefInstruction(ILOperand retOperand, ILOpLoc opLoc, ILOpType op, FunctionSymbolType type, TextSpan span)
		{
			RetOperand = retOperand;
			Loc = opLoc;
			Op = op;
			Type = type;
			InstructionType = ILInstructionType.OperatorRef;
			Span = span;
		}

		public ILOperand RetOperand;
		public ILOpLoc Loc;
		public ILOpType Op;
		public FunctionSymbolType Type;

		public override string ToString()
		{
			string opName = ILHelpers.GetIlOpName(Loc, Op).Replace('_', ' ');
			return $"%{RetOperand.Iden} = operator_ref {opName} : ${Type.ToILString()}";
		}
	}


	public class ILCallInstruction : ILInstruction
	{
		public ILCallInstruction(ILOperand retOperand, ILOperand func, List<ILOperand> parameters, SymbolType retType, TextSpan span)
		{
			RetOperand = retOperand;
			Func = func;
			Params = parameters;
			ReturnType = retType;
			InstructionType = ILInstructionType.Call;
			Span = span;
		}

		public ILOperand RetOperand;
		public ILOperand Func;
		public List<ILOperand> Params;
		public SymbolType ReturnType;

		public override string ToString()
		{
			string str;
			if (RetOperand != null)
				str = $"%{RetOperand.Iden} = call %{Func.Iden} ( ";
			else
				str = $"call %{Func.Iden} ( ";

			if (Params != null)
			{
				for (var i = 0; i < Params.Count; i++)
				{
					ILOperand operand = Params[i];
					str += $"{operand}";
					if (i != Params.Count - 1)
						str += " , ";
				}
			}

			if (ReturnType != null)
				str += $" ) : ${ReturnType.ToILString()}";
			else
				str += " )";

			return str;
		}
	}

	public class ILBuiltinInstruction : ILInstruction
	{
		public ILBuiltinInstruction(ILOperand retOperand, string builtin, List<ILOperand> parameters, SymbolType retType, TextSpan span)
		{
			RetOperand = retOperand;
			Builtin = builtin;
			Params = parameters;
			ReturnType = retType;
			InstructionType = ILInstructionType.Builtin;
			Span = span;
		}

		public ILOperand RetOperand;
		public string Builtin;
		public List<ILOperand> Params;
		public SymbolType ReturnType;

		public override string ToString()
		{
			string str;
			if (RetOperand != null)
				str = $"%{RetOperand.Iden} = builtin \"{Builtin}\" ( ";
			else
				str = $"builtin \"{Builtin}\" ( ";

			if (Params != null)
			{
				for (var i = 0; i < Params.Count; i++)
				{
					ILOperand operand = Params[i];
					str += $"{operand}";
					if (i != Params.Count - 1)
						str += " , ";
				}
			}

			if (ReturnType != null)
				str += $" ) : ${ReturnType.ToILString()}";
			else
				str += " )";

			return str;
		}
	}

	public enum ILCompilerIntrinType
	{
		Sizeof,
		Alignof
	}

	public class ILCompilerIntrinInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public ILCompilerIntrinType Intrin;
		public ILOperand Operand;
		public SymbolType Type;

		public ILCompilerIntrinInstruction(ILOperand retOperand, ILCompilerIntrinType intrin, ILOperand op, TextSpan span)
		{
			RetOperand = retOperand;
			Intrin = intrin;
			Operand = op;
			InstructionType = ILInstructionType.CompilerIntrin;
			Span = span;
		}

		public ILCompilerIntrinInstruction(ILOperand retOperand, ILCompilerIntrinType intrin, SymbolType type, TextSpan span)
		{
			RetOperand = retOperand;
			Intrin = intrin;
			Type = type;
			InstructionType = ILInstructionType.CompilerIntrin;
			Span = span;
		}

		public override string ToString()
		{
			string str = $"%{RetOperand.Iden} = compile_intrin \"";
			switch (Intrin)
			{
			case ILCompilerIntrinType.Sizeof:
				str += "sizeof";
				break;
			case ILCompilerIntrinType.Alignof:
				str += "alignof";
				break;
			}
			str += "\" ( ";

			if (Operand != null)
				str += Operand.ToString();
			else
				str += '$' + Type.ToILString();


			str += " ) : ";
			return str + '$' + RetOperand.Type.ToILString();
		}
	}



	public class ILTupleUnpackInstruction : ILInstruction
	{
		public ILTupleUnpackInstruction(List<ILOperand> retOperands, ILOperand operand, TextSpan span)
		{
			RetOperands = retOperands;
			Operand = operand;
			InstructionType = ILInstructionType.TupleUnpack;
			Span = span;
		}

		public List<ILOperand> RetOperands;
		public ILOperand Operand;

		public override string ToString()
		{
			string str = "";
			for (var i = 0; i < RetOperands.Count; i++)
			{
				ILOperand operand = RetOperands[i];
				str += $"%{operand.Iden}";
				if (i != RetOperands.Count - 1)
					str += " , ";
			}

			str += $" = tuple_unpack {Operand}";

			return str;
		}
	}

	public class ILTuplePackInstruction : ILInstruction
	{
		public ILTuplePackInstruction(ILOperand retOperand, List<ILOperand> operands, TextSpan span)
		{
			RetOperand = retOperand;
			Operands = operands;
			InstructionType = ILInstructionType.TuplePack;
			Span = span;
		}

		public ILOperand RetOperand;
		public List<ILOperand> Operands;

		public override string ToString()
		{
			string str = $"%{RetOperand.Iden} = tuple_pack ";

			for (var i = 0; i < Operands.Count; i++)
			{
				ILOperand operand = Operands[i];
				str += $"{operand}";
				if (i != Operands.Count - 1)
					str += " , ";
			}

			return str;
		}
	}

	public class ILTupleInsertInstruction : ILInstruction
	{
		public ILTupleInsertInstruction(ILOperand tuple, long index, ILOperand value, TextSpan span)
		{
			Tuple = tuple;
			Index = index;
			Value = value;
			InstructionType = ILInstructionType.TupleInsert;
			Span = span;
		}

		public ILOperand RetOperand;
		public ILOperand Tuple;
		public long Index;
		public ILOperand Value;

		public override string ToString()
		{
			return $"tuple_insert {Tuple} , {Index} , {Value}";
		}
	}

	public class ILTupleExtractInstruction : ILInstruction
	{
		public ILTupleExtractInstruction(ILOperand retOperand, ILOperand tuple, long index, TextSpan span)
		{
			RetOperand = retOperand;
			Tuple = tuple;
			Index = index;
			InstructionType = ILInstructionType.TupleExtract;
			Span = span;
		}

		public ILOperand RetOperand;
		public ILOperand Tuple;
		public long Index;
		public ILOperand Value;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = tuple_extract {Tuple} , {Index} : ${RetOperand.Type.ToILString()}";
		}
	}

	public class ILTupleElemAddrInstruction : ILInstruction
	{
		public ILTupleElemAddrInstruction(ILOperand retOperand, ILOperand tuple, long index, TextSpan span)
		{
			RetOperand = retOperand;
			Tuple = tuple;
			Index = index;
			InstructionType = ILInstructionType.TupleElemAddr;
			Span = span;
		}

		public ILOperand RetOperand;
		public ILOperand Tuple;
		public long Index;
		public ILOperand Value;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = tuple_elem_addr {Tuple} , {Index}";
		}
	}

	public class ILStructExtractInstruction : ILInstruction
	{
		public ILStructExtractInstruction(ILOperand retOperand, ILOperand structOp, ILReference elemRef, TextSpan span)
		{
			RetOperand = retOperand;
			Struct = structOp;
			ElemRef = elemRef;
			InstructionType = ILInstructionType.StructExtract;
			Span = span;
		}

		public ILOperand RetOperand;
		public ILOperand Struct;
		public ILReference ElemRef;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = struct_extract {Struct} , {ElemRef} : ${RetOperand.Type.ToILString()}";
		}
	}

	public class ILStructInsertInstruction : ILInstruction
	{
		public ILStructInsertInstruction(ILOperand structOp, ILReference elemRef, ILOperand value, TextSpan span)
		{
			Struct = structOp;
			ElemRef = elemRef;
			Value = value;
			InstructionType = ILInstructionType.StructInsert;
			Span = span;
		}

		public ILOperand Struct;
		public ILReference ElemRef;
		public ILOperand Value;

		public override string ToString()
		{
			return $"struct_insert {Struct} , {ElemRef} , {Value}";
		}
	}

	public class ILStructElemAddrInstruction : ILInstruction
	{
		public ILStructElemAddrInstruction(ILOperand retOperand, ILOperand structOp, ILReference elemRef, TextSpan span)
		{
			RetOperand = retOperand;
			Struct = structOp;
			ElemRef = elemRef;
			InstructionType = ILInstructionType.StructElemAddr;
			Span = span;
		}

		public ILOperand RetOperand;
		public ILOperand Struct;
		public ILReference ElemRef;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = struct_elem_addr {Struct} , {ElemRef} : ${RetOperand.Type.ToILString()}";
		}
	}

	public class ILStructTemplateValueInstruction : ILInstruction
	{
		public ILStructTemplateValueInstruction(ILOperand retOperand, SymbolType structType, ILReference elemRef, TextSpan span)
		{
			RetOperand = retOperand;
			StructType = structType;
			ElemRef = elemRef;
			InstructionType = ILInstructionType.StructTemplateValue;
			Span = span;
		}

		public ILOperand RetOperand;
		public SymbolType StructType;
		public ILReference ElemRef;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = struct_template_value ${StructType.ToILString()} , {ElemRef} : ${RetOperand.Type.ToILString()}";
		}
	}

	public class ILEnumGetMemberValueInstruction : ILInstruction
	{
		public ILEnumGetMemberValueInstruction(ILOperand retOperand, SymbolType type, ILReference memberRef, TextSpan span)
		{
			RetOperand = retOperand;
			Type = type;
			MemberRef = memberRef;
			InstructionType = ILInstructionType.GlobalAddr;
			Span = span;
		}

		public ILOperand RetOperand;
		public SymbolType Type;
		public ILReference MemberRef;

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = enum_get_member_value ${Type.ToILString()} , {MemberRef}";
		}
	}



	public class ILGlobalAddrInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public string Identifier;

		public ILGlobalAddrInstruction(ILOperand retOperand, string identifier, TextSpan span)
		{
			RetOperand = retOperand;
			Identifier = identifier;
			InstructionType = ILInstructionType.GlobalAddr;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = global_addr @{Identifier} : ${RetOperand.Type}";
		}
	}

	public class ILGlobalValueInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public string Identifier;

		public ILGlobalValueInstruction(ILOperand retOperand, string identifier, TextSpan span)
		{
			RetOperand = retOperand;
			Identifier = identifier;
			InstructionType = ILInstructionType.GlobalValue;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = global_value @{Identifier} : ${RetOperand.Type}";
		}
	}

	public class ILConvertTypeInstruction : ILInstruction
	{
		public ILOperand RetOperand;
		public ILOperand Operand;
		public SymbolType Type;

		public ILConvertTypeInstruction(ILOperand retOperand, ILOperand operand, SymbolType type, TextSpan span)
		{
			RetOperand = retOperand;
			Operand = operand;
			Type = type;
			InstructionType = ILInstructionType.ConvType;
			Span = span;
		}

		public override string ToString()
		{
			return $"%{RetOperand.Iden} = convert_type {Operand} to ${Type.ToILString()}";
		}
	}



	public class ILReturnInstruction : ILInstruction
	{
		public ILReturnInstruction(ILOperand operand, TextSpan span)
		{
			Operand = operand;
			InstructionType = ILInstructionType.Return;
			Span = span;
		}

		public ILOperand Operand;

		public override string ToString()
		{
			if (Operand != null)
				return $"return {Operand}";

			return "return";
		}
	}

	public class ILBranchInstruction : ILInstruction
	{
		public ILBranchInstruction(string label, TextSpan span)
		{
			Label = label;
			InstructionType = ILInstructionType.Branch;
			Span = span;
		}

		public string Label;

		public override string ToString()
		{
			return $"branch {Label}";
		}
	}

	public class ILCondBranchInstruction : ILInstruction
	{
		public ILCondBranchInstruction(ILOperand condition, string trueLabel, string falseLabel, TextSpan span)
		{
			Condition = condition;
			TrueLabel = trueLabel;
			FalseLabel = falseLabel;
			InstructionType = ILInstructionType.CondBranch;
			Span = span;
		}

		public ILOperand Condition;
		public string TrueLabel;
		public string FalseLabel;

		public override string ToString()
		{
			return $"cond_branch {Condition} , {TrueLabel} , {FalseLabel}";
		}
	}

}
