﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Interpret
{
	public enum ILInterpretType
	{
		None,
		Compile
	}

	public class ILInterpreter
	{
		private ILInterpretType _interpretType;

		public List<ILCompileInterpResult> CompileResults = null;


		public ILInterpreter(ILInterpretType type)
		{
			_interpretType = type;

			switch (_interpretType)
			{
			case ILInterpretType.Compile:
			{
				CompileResults = new List<ILCompileInterpResult>();
				break;
			}
			}
		}

		public void Interpret(ILCompUnit compUnit)
		{

			if (compUnit.Stage != ILStage.CompExec || compUnit.Stage != ILStage.Opt)
			{
				InterpCompile(compUnit);
			}

			if (_interpretType == ILInterpretType.Compile)
				return;

			// Interpret IL

		}

		void InterpCompile(ILCompUnit compUnit)
		{
			// Update enum values
			foreach (KeyValuePair<string, ILEnum> pair in compUnit.Enums)
			{
				foreach (ILEnumMember member in pair.Value.Members)
				{
					string funcName = member.EnumInit;
					ILFunction func = compUnit.GetFunction(funcName);
					ILInterpOperand retOp = InterpFunction(func, null, compUnit);
					long longVal = (retOp.Value as long?).Value;
					member.EnumVal = longVal;
				}
			}

			// Update compile time globals
			foreach (KeyValuePair<string, ILGlobal> pair in compUnit.Globals)
			{
				ILGlobal ilGlobal = pair.Value;
				if ((ilGlobal.Attributes.GlobalFlags & ILGlobalFlags.Const) != 0)
				{
					string funcName = ilGlobal.Initializer;
					ILFunction func = compUnit.GetFunction(funcName);
					ILInterpOperand retOp = InterpFunction(func, null, compUnit);

					if (retOp.Operand.Type is BuiltinSymbolType builtin)
					{
						switch (builtin.Builtin)
						{
						case BuiltinTypes.Unkown:
							break;
						case BuiltinTypes.Void:
							break;
						case BuiltinTypes.Bool:
						case BuiltinTypes.I8:
						case BuiltinTypes.I16:
						case BuiltinTypes.I32:
						case BuiltinTypes.I64:
						case BuiltinTypes.ISize:
						case BuiltinTypes.U8:
						case BuiltinTypes.U16:
						case BuiltinTypes.U32:
						case BuiltinTypes.U64:
						case BuiltinTypes.USize:
						case BuiltinTypes.Char:
						case BuiltinTypes.WChar:
						case BuiltinTypes.Rune:
						{
							long longVal = (retOp.Value as long?).Value;
							ilGlobal.Value = longVal.ToString();
							break;
						}
						case BuiltinTypes.F32:
						{
							float fltVal = (float)(retOp.Value as double?).Value;
							int hexVal = BitConverter.SingleToInt32Bits(fltVal);
							ilGlobal.Value = $"0x{hexVal:x8}";
							break;
						}
						case BuiltinTypes.F64:
						{
							double dblVal = (retOp.Value as double?).Value;
							long hexVal = BitConverter.DoubleToInt64Bits(dblVal);
							ilGlobal.Value = $"0x{hexVal:x16}";
							break;
						}
						case BuiltinTypes.String:
							break;
						case BuiltinTypes.StringLiteral:
						{
							string str = retOp.Value as string;
							ilGlobal.Value = '"' + str + '"';
							break;
						}
						case BuiltinTypes.Null:
							break;
						case BuiltinTypes.Count:
							break;
						default:
							throw new ArgumentOutOfRangeException();
						}
					}
					else if (retOp.Operand.Type is PointerSymbolType ptrType)
					{
						long longVal = (retOp.Value as long?).Value;
						ilGlobal.Value = longVal.ToString();
					}
					else if (retOp.Operand.Type is EnumSymbolType enumType && enumType.IsSimpleEnum)
					{
						long longVal = (retOp.Value as long?).Value;
						ilGlobal.Value = longVal.ToString();
					}
				}
			}

			// Replace compile time only instructions
			foreach (KeyValuePair<string, ILBlock> pair in compUnit.Blocks)
			{
				if (pair.Value is ILFunction func)
				{
					foreach (ILBasicBlock block in func.Blocks)
					{
						ReplaceCompileTimeOnlyInstructions(block, compUnit);
					}
				}
				else if (pair.Value is ILOperator op)
				{
					foreach (ILBasicBlock block in op.Blocks)
					{
						ReplaceCompileTimeOnlyInstructions(block, compUnit);
					}
				}
			}

		}

		public static ILInterpOperand InterpFunction(ILFunction function, List<ILInterpOperand> args, ILCompUnit compUnit)
		{
			Dictionary<string, ILInterpOperand> opMapping = new Dictionary<string, ILInterpOperand>();
			if (args != null)
			{
				for (var i = 0; i < args.Count; i++)
				{
					ILInterpOperand arg = args[i];
					opMapping.Add(i.ToString(), arg);
				}
			}

			ILInterpOperand lastOp = null;
			string nextBlock;
			ILBasicBlock block = function.Blocks[0];
			
			while (block != null)
			{
				(lastOp, nextBlock) = InterpBasicBlock(block, opMapping, compUnit);

				block = null;
				foreach (ILBasicBlock basicBlock in function.Blocks)
				{
					if (basicBlock.Label == nextBlock)
					{
						block = basicBlock;
						break;
					}
				}
			}

			return lastOp;
		}

		public static ILInterpOperand InterpOperator(ILOperator op, List<ILInterpOperand> args, ILCompUnit compUnit)
		{
			Dictionary<string, ILInterpOperand> opMapping = new Dictionary<string, ILInterpOperand>();
			if (args != null)
			{
				for (var i = 0; i < args.Count; i++)
				{
					ILInterpOperand arg = args[i];
					opMapping.Add(i.ToString(), arg);
				}
			}

			ILInterpOperand lastOp = null;
			string nextBlock;
			ILBasicBlock block = op.Blocks[0];

			while (block != null)
			{
				(lastOp, nextBlock) = InterpBasicBlock(block, opMapping, compUnit);

				block = null;
				foreach (ILBasicBlock basicBlock in op.Blocks)
				{
					if (basicBlock.Label == nextBlock)
					{
						block = basicBlock;
						break;
					}
				}
			}

			return lastOp;
		}

		static (ILInterpOperand, string) InterpBasicBlock(ILBasicBlock block, Dictionary<string, ILInterpOperand> opMapping, ILCompUnit compUnit)
		{
			ILInterpOperand lastOp = null;
			string blockName = null;
			for (var i = 0; i < block.Instructions.Count; i++)
			{
				ILInstruction instruction = block.Instructions[i];
				(ILInterpOperand tmp, string tmpBlock) = ILInterpInstruction.InterpInstruction(instruction, compUnit, opMapping, CmdLine.IsX64);

				if (tmp != null)
					lastOp = tmp;
				if (tmpBlock != null)
					blockName = tmpBlock;
			}
			
			return (lastOp, blockName);
		}

		static void ReplaceCompileTimeOnlyInstructions(ILBasicBlock block, ILCompUnit compUnit)
		{
			for (var i = 0; i < block.Instructions.Count; i++)
			{
				ILInstruction instruction = block.Instructions[i];
				if (instruction.IsCompileTimeOnly())
				{
					(ILInterpOperand tmp, _) = ILInterpInstruction.InterpInstruction(instruction, compUnit, null, CmdLine.IsX64);

					BuiltinSymbolType builtinType = tmp.Operand.Type as BuiltinSymbolType;
					switch (builtinType.Builtin)
					{
					case BuiltinTypes.I8:
					case BuiltinTypes.I16:
					case BuiltinTypes.I32:
					case BuiltinTypes.I64:
					case BuiltinTypes.U8:
					case BuiltinTypes.U16:
					case BuiltinTypes.U32:
					case BuiltinTypes.U64:
					case BuiltinTypes.Char:
					case BuiltinTypes.WChar:
					case BuiltinTypes.Rune:
					{
						long val = (tmp.Value as long?).Value;
						ILIntLiteralInstruction instr = new ILIntLiteralInstruction(tmp.Operand, val, instruction.Span);
						block.Instructions[i] = instr;
						break;
					}
					case BuiltinTypes.F32:
					{
						float val = (float)(tmp.Value as double?).Value;
						int hexVal = BitConverter.SingleToInt32Bits(val);
						ILFloatLiteralInstruction instr = new ILFloatLiteralInstruction(tmp.Operand, hexVal, instruction.Span);
						block.Instructions[i] = instr;
						break;
					}
					case BuiltinTypes.F64:
					{
						double val = (tmp.Value as double?).Value;
						long hexVal = BitConverter.DoubleToInt64Bits(val);
						ILFloatLiteralInstruction instr = new ILFloatLiteralInstruction(tmp.Operand, hexVal, instruction.Span);
						block.Instructions[i] = instr;
						break;
					}
					case BuiltinTypes.String:
					{
						string val = tmp.Value as string;
						ILStringLiteralInstruction instr = new ILStringLiteralInstruction(tmp.Operand, val, instruction.Span);
						block.Instructions[i] = instr;
						break;
					}
					}
				}
			}
		}

	}
}
