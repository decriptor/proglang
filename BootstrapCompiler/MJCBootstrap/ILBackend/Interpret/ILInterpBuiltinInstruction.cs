﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.ILBackend.Interpret
{
	public static partial class ILInterpInstruction
	{

		static ILInterpOperand InterpBuiltin(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			string[] builtin = instruction.Builtin.Split('.');
			switch (builtin[0])
			{
			case "neg":		return InterpBuiltinNeg		(instruction, opMapping, isX64);
			case "compl":	return InterpBuiltinCompl	(instruction, opMapping, isX64);
			case "inc":		return InterpBuiltinInc		(instruction, opMapping, isX64);
			case "dec":		return InterpBuiltinDec		(instruction, opMapping, isX64);
			case "add":		return InterpBuiltinAdd		(instruction, opMapping, isX64);
			case "sub":		return InterpBuiltinSub		(instruction, opMapping, isX64);
			case "mul":		return InterpBuiltinMul		(instruction, opMapping, isX64);
			case "div":		return InterpBuiltinDiv		(instruction, opMapping, isX64);
			case "rem":		return InterpBuiltinRem		(instruction, opMapping, isX64);
			case "shift":	return InterpBuiltinShift	(instruction, opMapping, isX64);
			case "or":		return InterpBuiltinOr		(instruction, opMapping, isX64);
			case "xor":		return InterpBuiltinXor		(instruction, opMapping, isX64);
			case "and":		return InterpBuiltinAnd		(instruction, opMapping, isX64);
			case "ext":		return InterpBuiltinExt		(instruction, opMapping, isX64);
			case "trunc":	return InterpBuiltinTrunc	(instruction, opMapping, isX64);
			case "fptoi":	return InterpBuiltinFptoi	(instruction, opMapping, isX64);
			case "itofp":	return InterpBuiltinItofp	(instruction, opMapping, isX64);
			case "ptrtoi":	return InterpBuiltinPtrtoi	(instruction, opMapping, isX64);
			case "itoptr":	return InterpBuiltinItoptr	(instruction, opMapping, isX64);
			case "bitcast":	return InterpBuiltinBitCast	(instruction, opMapping, isX64);
			case "cmp":		return InterpBuiltinCmp		(instruction, opMapping, isX64);
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinNeg(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Params[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				{
					long? srcVal = src.Value as long?;
					long val = (sbyte)(-srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I16:
				{
					long? srcVal = src.Value as long?;
					long val = (short)(-srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I32:
				{
					long? srcVal = src.Value as long?;
					long val = (int)(-srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I64:
				{
					long? srcVal = src.Value as long?;
					long val = (long)(-srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.ISize:
				{
					if (isX64)
					{
						long? srcVal = src.Value as long?;
						long val = (long)(-srcVal.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal = src.Value as long?;
						long val = (int)(-srcVal.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.F32:
				{
					double? srcVal = (src.Value as double?);
					double val = (double)(float)(-srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.F64:
				{
					double? srcVal = (src.Value as double?);
					double val = (double)(-srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinCompl(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Params[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				{
					long? srcVal = src.Value as long?;
					long val = (sbyte)(~srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I16:
				{
					long? srcVal = src.Value as long?;
					long val = (short)(~srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I32:
				{
					long? srcVal = src.Value as long?;
					long val = (int)(~srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I64:
				{
					long? srcVal = src.Value as long?;
					long val = (long)(~srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.ISize:
				{
					if (isX64)
					{
						long? srcVal = src.Value as long?;
						long val = (long)(~srcVal.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal = src.Value as long?;
						long val = (int)(~srcVal.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.U8:
				{
					long? srcVal = src.Value as long?;
					long val = (long)(byte)(~srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U16:
				{
					long? srcVal = src.Value as long?;
					long val = (long)(ushort)(~srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U32:
				{
					long? srcVal = src.Value as long?;
					long val = (long)(uint)(~srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U64:
				{
					long? srcVal = src.Value as long?;
					long val = (long)(ulong)(~srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.USize:
				{
					if (isX64)
					{
						long? srcVal = src.Value as long?;
						long val = (long)(uint)(~srcVal.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal = src.Value as long?;
						long val = (long)(ulong)(~srcVal.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinInc(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Params[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				{
					long? srcVal = src.Value as long?;
					long val = (sbyte)(srcVal.Value + 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I16:
				{
					long? srcVal = src.Value as long?;
					long val = (short)(srcVal.Value + 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I32:
				{
					long? srcVal = src.Value as long?;
					long val = (int)(srcVal.Value + 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I64:
				{
					long? srcVal = src.Value as long?;
					long val = (long)(srcVal.Value + 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.ISize:
				{
					if (isX64)
					{
						long? srcVal = src.Value as long?;
						long val = (long)(srcVal.Value + 1);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal = src.Value as long?;
						long val = (int)(srcVal.Value + 1);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.U8:
				{
					long? srcVal = src.Value as long?;
					long val = (long)(byte)(srcVal.Value + 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U16:
				{
					long? srcVal = src.Value as long?;
					long val = (long)(ushort)(srcVal.Value + 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U32:
				{
					long? srcVal = src.Value as long?;
					long val = (long)(uint)(srcVal.Value + 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U64:
				{
					long? srcVal = src.Value as long?;
					long val = (long)(ulong)(srcVal.Value + 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.USize:
				{
					if (isX64)
					{
						long? srcVal = src.Value as long?;
						long val = (long)(uint)(srcVal.Value + 1);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal = src.Value as long?;
						long val = (long)(ulong)(srcVal.Value + 1);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.F32:
				{
					double? srcVal = (src.Value as double?);
					double val = (double)(float)(srcVal.Value + 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.F64:
				{
					double? srcVal = (src.Value as double?);
					double val = (double)(srcVal.Value + 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinDec(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Params[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				{
					long? srcVal = src.Value as long?;
					long val = (sbyte)(srcVal.Value - 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I16:
				{
					long? srcVal = src.Value as long?;
					long val = (short)(srcVal.Value - 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I32:
				{
					long? srcVal = src.Value as long?;
					long val = (int)(srcVal.Value - 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I64:
				{
					long? srcVal = src.Value as long?;
					long val = (long)(srcVal.Value - 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.ISize:
				{
					if (isX64)
					{
						long? srcVal = src.Value as long?;
						long val = (long)(srcVal.Value - 1);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal = src.Value as long?;
						long val = (int)(srcVal.Value - 1);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.U8:
				{
					long? srcVal = src.Value as long?;
					long val = (long)(byte)(srcVal.Value - 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U16:
				{
					long? srcVal = src.Value as long?;
					long val = (long)(ushort)(srcVal.Value - 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U32:
				{
					long? srcVal = src.Value as long?;
					long val = (long)(uint)(srcVal.Value - 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U64:
				{
					long? srcVal = src.Value as long?;
					long val = (long)(ulong)(srcVal.Value - 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.USize:
				{
					if (isX64)
					{
						long? srcVal = src.Value as long?;
						long val = (long)(uint)(srcVal.Value - 1);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal = src.Value as long?;
						long val = (long)(ulong)(srcVal.Value - 1);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.F32:
				{
					double? srcVal = (src.Value as double?);
					double val = (double)(float)(srcVal.Value - 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.F64:
				{
					double? srcVal = (src.Value as double?);
					double val = (double)(srcVal.Value - 1);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinAdd(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Params[0];
			ILOperand op1 = instruction.Params[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (sbyte)(srcVal0.Value + srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (short)(srcVal0.Value + srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (int)(srcVal0.Value + srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I64:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(srcVal0.Value + srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.ISize:
				{
					if (isX64)
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (long)(srcVal0.Value + srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (int)(srcVal0.Value + srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.U8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(byte)(srcVal0.Value + srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(ushort)(srcVal0.Value + srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(uint)(srcVal0.Value + srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U64:
				{
					long? srcVal0 = (src0.Value as long?);
					long? srcVal1 = (src1.Value as long?);
					long val = (long)(ulong)(srcVal0.Value + srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.USize:
				{
					if (isX64)
					{
						long? srcVal0 = (src0.Value as long?);
						long? srcVal1 = (src1.Value as long?);
						long val = (long)(ulong)(srcVal0.Value + srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (long)(uint)(srcVal0.Value + srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.F32:
				{
					double? srcVal0 = (src0.Value as double?);
					double? srcVal1 = (src1.Value as double?);
					double val = (double)(float)(srcVal0.Value + srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.F64:
				{
					double? srcVal0 = (src0.Value as double?);
					double? srcVal1 = (src1.Value as double?);
					double val = (double)(srcVal0.Value + srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinSub(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Params[0];
			ILOperand op1 = instruction.Params[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (sbyte)(srcVal0.Value - srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (short)(srcVal0.Value - srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (int)(srcVal0.Value - srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I64:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(srcVal0.Value - srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.ISize:
				{
					if (isX64)
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (long)(srcVal0.Value - srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (int)(srcVal0.Value - srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.U8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(byte)(srcVal0.Value - srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(ushort)(srcVal0.Value - srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(uint)(srcVal0.Value - srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U64:
				{
					long? srcVal0 = (src0.Value as long?);
					long? srcVal1 = (src1.Value as long?);
					long val = (long)(ulong)(srcVal0.Value - srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.USize:
				{
					if (isX64)
					{
						long? srcVal0 = (src0.Value as long?);
						long? srcVal1 = (src1.Value as long?);
						long val = (long)(ulong)(srcVal0.Value - srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (long)(uint)(srcVal0.Value - srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.F32:
				{
					double? srcVal0 = (src0.Value as double?);
					double? srcVal1 = (src1.Value as double?);
					double val = (double)(float)(srcVal0.Value - srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.F64:
				{
					double? srcVal0 = (src0.Value as double?);
					double? srcVal1 = (src1.Value as double?);
					double val = (double)(srcVal0.Value - srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinMul(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Params[0];
			ILOperand op1 = instruction.Params[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (sbyte)(srcVal0.Value * srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (short)(srcVal0.Value * srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (int)(srcVal0.Value * srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I64:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(srcVal0.Value * srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.ISize:
				{
					if (isX64)
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (long)(srcVal0.Value * srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (int)(srcVal0.Value * srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.U8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(byte)(srcVal0.Value * srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(ushort)(srcVal0.Value * srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(uint)(srcVal0.Value * srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U64:
				{
					long? srcVal0 = (src0.Value as long?);
					long? srcVal1 = (src1.Value as long?);
					long val = (long)(ulong)(srcVal0.Value * srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.USize:
				{
					if (isX64)
					{
						long? srcVal0 = (src0.Value as long?);
						long? srcVal1 = (src1.Value as long?);
						long val = (long)(ulong)(srcVal0.Value * srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (long)(uint)(srcVal0.Value * srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.F32:
				{
					double? srcVal0 = (src0.Value as double?);
					double? srcVal1 = (src1.Value as double?);
					double val = (double)(float)(srcVal0.Value * srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.F64:
				{
					double? srcVal0 = (src0.Value as double?);
					double? srcVal1 = (src1.Value as double?);
					double val = (double)(srcVal0.Value * srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinDiv(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Params[0];
			ILOperand op1 = instruction.Params[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (sbyte)(srcVal0.Value / srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (short)(srcVal0.Value / srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (int)(srcVal0.Value / srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I64:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(srcVal0.Value / srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.ISize:
				{
					if (isX64)
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (long)(srcVal0.Value / srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (int)(srcVal0.Value / srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.U8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(byte)(srcVal0.Value / srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(ushort)(srcVal0.Value / srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(uint)(srcVal0.Value / srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U64:
				{
					long? srcVal0 = (src0.Value as long?);
					long? srcVal1 = (src1.Value as long?);
					long val = (long)(ulong)(srcVal0.Value / srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.USize:
				{
					if (isX64)
					{
						long? srcVal0 = (src0.Value as long?);
						long? srcVal1 = (src1.Value as long?);
						long val = (long)(ulong)(srcVal0.Value / srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (long)(uint)(srcVal0.Value / srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.F32:
				{
					double? srcVal0 = (src0.Value as double?);
					double? srcVal1 = (src1.Value as double?);
					double val = (double)(float)(srcVal0.Value / srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.F64:
				{
					double? srcVal0 = (src0.Value as double?);
					double? srcVal1 = (src1.Value as double?);
					double val = (double)(srcVal0.Value / srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinRem(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Params[0];
			ILOperand op1 = instruction.Params[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (sbyte)(srcVal0.Value % srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (short)(srcVal0.Value % srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (int)(srcVal0.Value % srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I64:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(srcVal0.Value % srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.ISize:
				{
					if (isX64)
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (long)(srcVal0.Value % srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break; ;
					}
					else
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (int)(srcVal0.Value % srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.U8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(byte)(srcVal0.Value % srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(ushort)(srcVal0.Value % srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(uint)(srcVal0.Value % srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U64:
				{
					long? srcVal0 = (src0.Value as long?);
					long? srcVal1 = (src1.Value as long?);
					long val = (long)(ulong)(srcVal0.Value % srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.USize:
				{
					if (isX64)
					{
						long? srcVal0 = (src0.Value as long?);
						long? srcVal1 = (src1.Value as long?);
						long val = (long)(ulong)(srcVal0.Value % srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (long)(uint)(srcVal0.Value % srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.F32:
				{
					double? srcVal0 = (src0.Value as double?);
					double? srcVal1 = (src1.Value as double?);
					double val = (double)(float)(srcVal0.Value % srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.F64:
				{
					double? srcVal0 = (src0.Value as double?);
					double? srcVal1 = (src1.Value as double?);
					double val = (double)(srcVal0.Value % srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinShift(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Params[0];
			ILOperand op1 = instruction.Params[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			string[] parts = instruction.Builtin.Split('.');
			string dir = parts[1];

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.U8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;

					long val = 0;
					switch (dir)
					{
					case "l":
					{
						val = (byte)srcVal0.Value << (int)srcVal1.Value;
						break;
					}
					case "lr":
					{
						val = (long)((byte)srcVal0.Value >> (int)srcVal1.Value);
						break;
					}
					case "ar":
					{
						val = (sbyte)srcVal0.Value >> (int)srcVal1.Value;
						break;
					}
					}

					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U16:
				case BuiltinTypes.I16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;

					long val = 0;
					switch (dir)
					{
					case "l":
					{
						val = (short)srcVal0.Value << (int)srcVal1.Value;
						break;
					}
					case "lr":
					{
						val = (long)((ushort)srcVal0.Value >> (int)srcVal1.Value);
						break;
					}
					case "ar":
					{
						val = (short)srcVal0.Value >> (int)srcVal1.Value;
						break;
					}
					}

					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U32:
				case BuiltinTypes.I32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;

					long val = 0;
					switch (dir)
					{
					case "l":
					{
						val = (int)srcVal0.Value << (int)srcVal1.Value;
						break;
					}
					case "lr":
					{
						val = (long)((uint)srcVal0.Value >> (int)srcVal1.Value);
						break;
					}
					case "ar":
					{
						val = (int)srcVal0.Value >> (int)srcVal1.Value;
						break;
					}
					}

					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U64:
				case BuiltinTypes.I64:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;

					long val = 0;
					switch (dir)
					{
					case "l":
					{
						val = srcVal0.Value << (int)srcVal1.Value;
						break;
					}
					case "lr":
					{
						val = (long)((ulong)srcVal0.Value >> (int)srcVal1.Value);
						break;
					}
					case "ar":
					{
						val = (long)srcVal0.Value >> (int)srcVal1.Value;
						break;
					}
					}

					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.ISize:
				case BuiltinTypes.USize:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;

					long val = 0;
					if (isX64)
					{
						switch (dir)
						{
						case "l":
						{
							val = srcVal0.Value << (int) srcVal1.Value;
							break;
						}
						case "lr":
						{
							val = (long) ((ulong) srcVal0.Value >> (int) srcVal1.Value);
							break;
						}
						case "ar":
						{
							val = (long) srcVal0.Value >> (int) srcVal1.Value;
							break;
						}
						}
					}
					else
					{
						switch (dir)
						{
						case "l":
						{
							val = (int)srcVal0.Value << (int)srcVal1.Value;
							break;
						}
						case "lr":
						{
							val = (long)((uint)srcVal0.Value >> (int)srcVal1.Value);
							break;
						}
						case "ar":
						{
							val = (int)srcVal0.Value >> (int)srcVal1.Value;
							break;
						}
						}
					}

					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinOr(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Params[0];
			ILOperand op1 = instruction.Params[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (sbyte)(srcVal0.Value | srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (short)(srcVal0.Value | srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (int)(srcVal0.Value | srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I64:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(srcVal0.Value | srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.ISize:
				{
					if (isX64)
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (long)(srcVal0.Value | srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (int)(srcVal0.Value | srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.Bool:
				case BuiltinTypes.U8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(byte)(srcVal0.Value | srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(ushort)(srcVal0.Value | srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(uint)(srcVal0.Value | srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U64:
				{
					long? srcVal0 = (src0.Value as long?);
					long? srcVal1 = (src1.Value as long?);
					long val = (long)(ulong)(srcVal0.Value | srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.USize:
				{
					if (isX64)
					{
						long? srcVal0 = (src0.Value as long?);
						long? srcVal1 = (src1.Value as long?);
						long val = (long)(ulong)(srcVal0.Value | srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (long)(uint)(srcVal0.Value | srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinXor(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Params[0];
			ILOperand op1 = instruction.Params[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (sbyte)(srcVal0.Value ^ srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (short)(srcVal0.Value ^ srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (int)(srcVal0.Value ^ srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I64:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(srcVal0.Value ^ srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.ISize:
				{
					if (isX64)
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (long)(srcVal0.Value ^ srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (int)(srcVal0.Value ^ srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.Bool:
				case BuiltinTypes.U8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(byte)(srcVal0.Value ^ srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(ushort)(srcVal0.Value ^ srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(uint)(srcVal0.Value ^ srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U64:
				{
					long? srcVal0 = (src0.Value as long?);
					long? srcVal1 = (src1.Value as long?);
					long val = (long)(ulong)(srcVal0.Value ^ srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.USize:
				{
					if (isX64)
					{
						long? srcVal0 = (src0.Value as long?);
						long? srcVal1 = (src1.Value as long?);
						long val = (long)(ulong)(srcVal0.Value ^ srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (long)(uint)(srcVal0.Value ^ srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinAnd(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Params[0];
			ILOperand op1 = instruction.Params[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (sbyte)(srcVal0.Value & srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (short)(srcVal0.Value & srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (int)(srcVal0.Value & srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I64:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(srcVal0.Value & srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.ISize:
				{
					if (isX64)
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (long)(srcVal0.Value & srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (int)(srcVal0.Value & srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				case BuiltinTypes.Bool:
				case BuiltinTypes.U8:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(byte)(srcVal0.Value & srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U16:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(ushort)(srcVal0.Value & srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U32:
				{
					long? srcVal0 = src0.Value as long?;
					long? srcVal1 = src1.Value as long?;
					long val = (long)(uint)(srcVal0.Value & srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U64:
				{
					long? srcVal0 = (src0.Value as long?);
					long? srcVal1 = (src1.Value as long?);
					long val = (long)(ulong)(srcVal0.Value & srcVal1.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.USize:
				{
					if (isX64)
					{
						long? srcVal0 = (src0.Value as long?);
						long? srcVal1 = (src1.Value as long?);
						long val = (long)(ulong)(srcVal0.Value & srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
					else
					{
						long? srcVal0 = src0.Value as long?;
						long? srcVal1 = src1.Value as long?;
						long val = (long)(uint)(srcVal0.Value & srcVal1.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
						break;
					}
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinExt(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Params[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;
			bool zeroExtend = instruction.Builtin.Contains("z");

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				{
					long? srcVal = src.Value as long?;
					long val = srcVal.Value;
					if (zeroExtend && val < 0)
					{
						val = val & 0x7f;
						val |= 0x80;
					}
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I16:
				{
					long? srcVal = src.Value as long?;
					long val = srcVal.Value;
					if (zeroExtend && val < 0)
					{
						val = val & 0x7fff;
						val |= 0x8000;
					}
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I32:
				{
					long? srcVal = src.Value as long?;
					long val = srcVal.Value;
					if (zeroExtend && val < 0)
					{
						val = val & 0x7fff_ffff;
						val |= 0x8000_0000;
					}
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					break;
				}
				case BuiltinTypes.ISize:
				{
					if (isX64)
					{
						ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					}
					else
					{
						long? srcVal = src.Value as long?;
						long val = srcVal.Value;
						if (zeroExtend && val < 0)
						{
							val = val & 0x7fff_ffff;
							val |= 0x8000_0000;
						}
						ret = new ILInterpOperand(instruction.RetOperand, val);
					}
					break;
				}
				case BuiltinTypes.Bool:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
					ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					break;
				case BuiltinTypes.F32:
				case BuiltinTypes.F64:
					ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					break;
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinTrunc(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Params[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.Bool:
				{
					long? srcVal = src.Value as long?;
					long val = srcVal.Value & 0x1;
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I8:
				{
					long? srcVal = src.Value as long?;
					long val = (sbyte) (srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I16:
				{
					long? srcVal = src.Value as long?;
					long val = (short)(srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I32:
				{
					long? srcVal = src.Value as long?;
					long val = (int)(srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					break;
				}
				case BuiltinTypes.ISize:
				{
					if (isX64)
					{
						ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					}
					else
					{
						long? srcVal = src.Value as long?;
						long val = (int)(srcVal.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
					}
					break;
				}
				case BuiltinTypes.U8:
				{
					long? srcVal = src.Value as long?;
					long val = (byte)(srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U16:
				{
					long? srcVal = src.Value as long?;
					long val = (ushort)(srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U32:
				{
					long? srcVal = src.Value as long?;
					long val = (uint)(srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					break;
				}
				case BuiltinTypes.USize:
				{
					if (isX64)
					{
						ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					}
					else
					{
						long? srcVal = src.Value as long?;
						long val = (int)(srcVal.Value);
						ret = new ILInterpOperand(instruction.RetOperand, val);
					}
					break;
				}
				case BuiltinTypes.F32:
				{
					double? srcVal = (src.Value as double?);
					double val = (float)(srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.F64:
					ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					break;
				case BuiltinTypes.Char:
				{
					long? srcVal = src.Value as long?;
					long val = (byte)(srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.WChar:
				{
					long? srcVal = src.Value as long?;
					long val = (ushort)(srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.Rune:
				{
					long? srcVal = src.Value as long?;
					long val = (uint)(srcVal.Value);
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinFptoi(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Params[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I32:
				{
					double? srcVal = (src.Value as double?);
					long val = (int)srcVal.Value;
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I64:
				{
					double? srcVal = (src.Value as double?);
					long val = (long)srcVal.Value;
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U32:
				{
					double? srcVal = (src.Value as double?);
					long val = (uint)srcVal.Value;
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U64:
				{
					double? srcVal = (src.Value as double?);
					long val = (long)(ulong)srcVal.Value;
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinItofp(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Params[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.F32:
				{
					long? srcVal = src.Value as long?;
					double val = (float)srcVal.Value;
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.F64:
				{
					long? srcVal = src.Value as long?;
					double val = (double)srcVal.Value;
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinPtrtoi(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Params[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				{
					long? srcVal = (src.Value as long?);
					long val = (sbyte)srcVal.Value;
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U8:
				{
					long? srcVal = (src.Value as long?);
					long val = (byte)srcVal.Value;
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I16:
				{
					long? srcVal = (src.Value as long?);
					long val = (short)srcVal.Value;
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U16:
				{
					long? srcVal = (src.Value as long?);
					long val = (ushort)srcVal.Value;
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I32:
				{
					long? srcVal = (src.Value as long?);
					long val = (int)srcVal.Value;
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.U32:
				{
					long? srcVal = (src.Value as long?);
					long val = (uint)srcVal.Value;
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				case BuiltinTypes.I64:
				case BuiltinTypes.U64:
				{
					ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinItoptr(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Params[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (op.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				{
					ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					break;
				}
				case BuiltinTypes.I64:
				case BuiltinTypes.U64:
				{
					long? srcVal = (src.Value as long?);
					long val = isX64 ? srcVal.Value : (uint)srcVal.Value;
					ret = new ILInterpOperand(instruction.RetOperand, val);
					break;
				}
				}
			}

			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinBitCast(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op = instruction.Params[0];
			ILInterpOperand src = opMapping[op.Iden];
			ILInterpOperand ret = null;

			if (instruction.RetOperand.Type is BuiltinSymbolType builtinRet)
			{
				if (op.Type is PointerSymbolType)
				{
					ret = new ILInterpOperand(instruction.RetOperand, src.Value);
				}
				else if (op.Type is BuiltinSymbolType builtinOp)
				{
					if (builtinOp.Builtin == BuiltinTypes.F64)
					{
						double srcVal = (src.Value as double?).Value;
						long val = BitConverter.DoubleToInt64Bits(srcVal);
						ret = new ILInterpOperand(instruction.RetOperand, val);
					}
					else if (builtinOp.Builtin == BuiltinTypes.F32)
					{
						double srcVal = (src.Value as double?).Value;
						long val = BitConverter.SingleToInt32Bits((float)srcVal);
						ret = new ILInterpOperand(instruction.RetOperand, val);
					}
					else if (builtinRet.Builtin == BuiltinTypes.F64)
					{
						long srcVal = (src.Value as long?).Value;
						double val = BitConverter.Int64BitsToDouble(srcVal);
						ret = new ILInterpOperand(instruction.RetOperand, val);
					}
					else if (builtinRet.Builtin == BuiltinTypes.F32)
					{
						long srcVal = (src.Value as long?).Value;
						double val = BitConverter.Int32BitsToSingle((int)srcVal);
						ret = new ILInterpOperand(instruction.RetOperand, val);
					}
					else
					{
						ret = new ILInterpOperand(instruction.RetOperand, src.Value);
					}
				}
			}
			else if (instruction.RetOperand.Type is PointerSymbolType || op.Type is PointerSymbolType)
			{
				ret = new ILInterpOperand(instruction.RetOperand, src.Value);
			}


			if (ret != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}

			return null;
		}

		static ILInterpOperand InterpBuiltinCmp(ILBuiltinInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILOperand op0 = instruction.Params[0];
			ILOperand op1 = instruction.Params[1];
			ILInterpOperand src0 = opMapping[op0.Iden];
			ILInterpOperand src1 = opMapping[op1.Iden];
			bool bRes = false;

			string opName = instruction.Builtin.Split('.')[1];

			if (op0.Type is BuiltinSymbolType builtin)
			{
				switch (builtin.Builtin)
				{
				case BuiltinTypes.Bool:
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				case BuiltinTypes.Char:
				case BuiltinTypes.WChar:
				case BuiltinTypes.Rune:
				{
					long srcVal0 = (src0.Value as long?).Value;
					long srcVal1 = (src1.Value as long?).Value;
					
					switch (opName)
					{
					case "eq":
						bRes = srcVal0 == srcVal1;
						break;
					case "ne":
						bRes = srcVal0 == srcVal1;
						break;
					case "gt":
						bRes = srcVal0 == srcVal1;
						break;
					case "gte":
						bRes = srcVal0 == srcVal1;
						break;
					case "lt":
						bRes = srcVal0 == srcVal1;
						break;
					case "lte":
						bRes = srcVal0 == srcVal1;
						break;
					}
					break;
				}
				case BuiltinTypes.F32:
				case BuiltinTypes.F64:
				{
					double srcVal0 = (src0.Value as double?).Value;
					double srcVal1 = (src1.Value as double?).Value;

					switch (opName)
					{
					case "eq":
						bRes = srcVal0 == srcVal1;
						break;
					case "ne":
						bRes = srcVal0 == srcVal1;
						break;
					case "gt":
						bRes = srcVal0 == srcVal1;
						break;
					case "gte":
						bRes = srcVal0 == srcVal1;
						break;
					case "lt":
						bRes = srcVal0 == srcVal1;
						break;
					case "lte":
						bRes = srcVal0 == srcVal1;
						break;
					}
					break;
				}
				}
			}
			else if (op0.Type is PointerSymbolType)
			{
				long srcVal0 = (src0.Value as long?).Value;
				long srcVal1 = (src1.Value as long?).Value;

				switch (opName)
				{
				case "eq":
					bRes = srcVal0 == srcVal1;
					break;
				case "ne":
					bRes = srcVal0 == srcVal1;
					break;
				case "gt":
					bRes = srcVal0 == srcVal1;
					break;
				case "gte":
					bRes = srcVal0 == srcVal1;
					break;
				case "lt":
					bRes = srcVal0 == srcVal1;
					break;
				case "lte":
					bRes = srcVal0 == srcVal1;
					break;
				}
			}

			ILInterpOperand ret = new ILInterpOperand(instruction.RetOperand, (long)(bRes ? 1 : 0));
			opMapping.Add(instruction.RetOperand.Iden, ret);
			return null;
		}
	}
}
