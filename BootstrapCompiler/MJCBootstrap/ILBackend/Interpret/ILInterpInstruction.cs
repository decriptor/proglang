﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Interpret
{
	public static partial class ILInterpInstruction
	{

		public static (ILInterpOperand, string) InterpInstruction(ILInstruction instruction, ILCompUnit compUnit, Dictionary<string, ILInterpOperand> opMapping, bool isX64)
		{
			ILInterpOperand ret = null;
			string nextBlock = null;

			switch (instruction.InstructionType)
			{
			case ILInstructionType.IntLiteral:			ret = InterpIntLiteral(instruction as ILIntLiteralInstruction, opMapping); break;
			case ILInstructionType.FloatLiteral:		ret = InterpFloatLiteral(instruction as ILFloatLiteralInstruction, opMapping); break;
			case ILInstructionType.StringLiteral:		ret = InterpStringLiteral(instruction as ILStringLiteralInstruction, opMapping); break;
			case ILInstructionType.AllocStack:			ret = InterpAllocStack(instruction as ILAllocStackInstruction, opMapping);	break;
			case ILInstructionType.DeallocStack:		ret = null;	break;
			case ILInstructionType.Assign:				ret = null;	break;
			case ILInstructionType.Store:				InterpStore(instruction as ILStoreInstruction, opMapping);	break;
			case ILInstructionType.Load:				ret = InterpLoad(instruction as ILLoadInstruction, opMapping);	break;
			case ILInstructionType.OperatorRef:			ret = InterpOperatorRef(instruction as ILOperatorRefInstruction, opMapping, compUnit); break;
			case ILInstructionType.FunctionRef:			ret = InterpFunctionRef(instruction as ILFunctionRefInstruction, opMapping, compUnit); break;
			case ILInstructionType.MethodRef:			ret = null; break;
			case ILInstructionType.Call:				ret = InterpCall(instruction as ILCallInstruction, opMapping, compUnit); break;
			case ILInstructionType.Builtin:				ret = InterpBuiltin(instruction as ILBuiltinInstruction, opMapping, isX64); break;
			case ILInstructionType.TupleUnpack:			ret = null;	break;
			case ILInstructionType.TuplePack:			ret = null;	break;
			case ILInstructionType.TupleInsert:			ret = null;	break;
			case ILInstructionType.TupleExtract:		ret = null;	break;
			case ILInstructionType.TupleElemAddr:		ret = null;	break;
			case ILInstructionType.StructExtract:		ret = null;	break;
			case ILInstructionType.StructInsert:		ret = null;	break;
			case ILInstructionType.StructElemAddr:		ret = null;	break;
			case ILInstructionType.StructTemplateValue: ret = InterpStructTemplateValue(instruction as ILStructTemplateValueInstruction, compUnit); break;
			case ILInstructionType.Destruct:			ret = null; break;
			case ILInstructionType.EnumGetMemberValue:	ret = InterpEnumGetMemberValue(instruction as ILEnumGetMemberValueInstruction, compUnit, opMapping); break;
			case ILInstructionType.ConvType:			ret = InterpConvType(instruction as ILConvertTypeInstruction, opMapping); break;
			case ILInstructionType.Return:				ret = InterpReturn(instruction as ILReturnInstruction, opMapping); break;
			case ILInstructionType.Branch:				nextBlock = InterpBranch(instruction as ILBranchInstruction);	break;
			case ILInstructionType.CondBranch:			nextBlock = InterpCondBranch(instruction as ILCondBranchInstruction, opMapping); break;
			}

			return (ret, nextBlock);
		}

		static ILInterpOperand InterpIntLiteral(ILIntLiteralInstruction instruction, Dictionary<string, ILInterpOperand> opMapping)
		{
			ILInterpOperand ret;
			ret = new ILInterpOperand(instruction.RetOperand, instruction.Value);
			opMapping.Add(instruction.RetOperand.Iden, ret);
			return ret;
		}

		static ILInterpOperand InterpFloatLiteral(ILFloatLiteralInstruction instruction, Dictionary<string, ILInterpOperand> opMapping)
		{
			double floatVal;
			if ((instruction.RetOperand.Type as BuiltinSymbolType).Builtin == BuiltinTypes.F32)
				floatVal = BitConverter.Int32BitsToSingle((int)instruction.HexValue);
			else
				floatVal = BitConverter.Int64BitsToDouble(instruction.HexValue);
			
			ILInterpOperand ret = new ILInterpOperand(instruction.RetOperand, floatVal);
			opMapping.Add(instruction.RetOperand.Iden, ret);
			return ret;
		}

		static ILInterpOperand InterpStringLiteral(ILStringLiteralInstruction instruction, Dictionary<string, ILInterpOperand> opMapping)
		{
			ILInterpOperand ret = new ILInterpOperand(instruction.RetOperand, instruction.String);
			opMapping.Add(instruction.RetOperand.Iden, ret);
			return ret;
		}

		static ILInterpOperand InterpAllocStack(ILAllocStackInstruction instruction, Dictionary<string, ILInterpOperand> opMapping)
		{
			ILInterpOperand ret = null;
			if (instruction.Type is BuiltinSymbolType builtin)
			{
				switch (builtin.Builtin)
				{
				case BuiltinTypes.Bool:
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
				case BuiltinTypes.Char:
				case BuiltinTypes.WChar:
				case BuiltinTypes.Rune:
					ret = new ILInterpOperand(instruction.RetOperand, 0L);
					break;
				case BuiltinTypes.F32:
				case BuiltinTypes.F64:
					ret = new ILInterpOperand(instruction.RetOperand, 1.0);
					break;
				case BuiltinTypes.String:
					ret = new ILInterpOperand(instruction.RetOperand, (string)null);
					break;
				}
			}


			opMapping.Add(instruction.RetOperand.Iden, ret);
			return ret;
		}

		static void InterpStore(ILStoreInstruction instruction, Dictionary<string, ILInterpOperand> opMapping)
		{
			ILInterpOperand dst = opMapping[instruction.Dst.Iden];
			ILInterpOperand src = opMapping[instruction.Src.Iden];

			dst.Value = src.Value;
		}

		static ILInterpOperand InterpLoad(ILLoadInstruction instruction, Dictionary<string, ILInterpOperand> opMapping)
		{
			ILInterpOperand src = opMapping[instruction.Operand.Iden];

			ILInterpOperand ret = new ILInterpOperand(instruction.RetOperand, src.Value);
			opMapping.Add(instruction.RetOperand.Iden, ret);
			return ret;
		}

		static ILInterpOperand InterpFunctionRef(ILFunctionRefInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, ILCompUnit compUnit)
		{
			string opName = instruction.MangledName;

			// 1. Lookup builtin function
			ILFunction function = compUnit.Builtins.GetFunction(opName);
			// 2. If no function was found, lookup local function
			if (function == null)
			{
				function = compUnit.GetFunction(opName);
			}
			// 4. If no function was found, lookup function in other module compile units
			if (function == null)
			{
				foreach (ILCompUnit refCompUnit in compUnit.RefCompUnits)
				{
					function = refCompUnit.GetFunction(opName);
					if (function != null)
						break;
				}
			}
			// 4. If no function was found, we can't handle binary formats
			if (function == null)
			{
				// TODO: Error
			}

			ILInterpOperand ret = new ILInterpOperand(instruction.RetOperand, function);
			opMapping.Add(instruction.RetOperand.Iden, ret);
			return ret;
		}

		static ILInterpOperand InterpOperatorRef(ILOperatorRefInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, ILCompUnit compUnit)
		{
			string opName = ILHelpers.GetTypedIlOpName(instruction);

			// 1. Lookup builtin operator
			ILOperator op = compUnit.Builtins.GetOperator(opName);
			// 2. If no op was found, lookup local operator
			if (op == null)
			{
				op = compUnit.GetOperator(opName);
			}
			// 4. If no op was found, lookup operator in other module compile units
			if (op == null)
			{
				foreach (ILCompUnit refCompUnit in compUnit.RefCompUnits)
				{
					op = refCompUnit.GetOperator(opName);
					if (op != null)
						break;
				}
			}
			// 4. If no operator was found, we can't handle binary formats
			if (op == null)
			{
				// TODO: Error
			}

			ILInterpOperand ret = new ILInterpOperand(instruction.RetOperand, op);
			opMapping.Add(instruction.RetOperand.Iden, ret);
			return ret;
		}

		static ILInterpOperand InterpCall(ILCallInstruction instruction, Dictionary<string, ILInterpOperand> opMapping, ILCompUnit compUnit)
		{
			ILInterpOperand callee = opMapping[instruction.Func.Iden];

			List<ILInterpOperand> args = null;
			if (instruction.Params != null)
			{
				args = new List<ILInterpOperand>();
				foreach (ILOperand param in instruction.Params)
				{
					ILInterpOperand interpOp = opMapping[param.Iden];
					args.Add(interpOp);
				}
			}

			ILInterpOperand ret = null;
			if (callee.Value is ILFunction function)
			{
				ret = ILInterpreter.InterpFunction(function, args, compUnit);
				if (ret != null)
					ret = new ILInterpOperand(instruction.RetOperand, ret.Value);
			}
			else if (callee.Value is ILOperator op)
			{
				ret = ILInterpreter.InterpOperator(op, args, compUnit);
				if (ret != null)
					ret = new ILInterpOperand(instruction.RetOperand, ret.Value);
			}

			if (instruction.RetOperand != null)
			{
				opMapping.Add(instruction.RetOperand.Iden, ret);
				return ret;
			}
			return null;
		}



		static ILInterpOperand InterpStructTemplateValue(ILStructTemplateValueInstruction instruction, ILCompUnit compUnit)
		{
			ILReference paramRef = instruction.ElemRef;
			Identifier paramIden = paramRef.Element;

			TemplateInstanceSymbolType instType = instruction.StructType as TemplateInstanceSymbolType;

			// Get param index
			TemplateDefinitionName defName = instType.TemplateSymbol.Identifier as TemplateDefinitionName;
			int idx;
			for (idx = 0; idx < defName.Parameters.Count; idx++)
			{
				if (defName.Parameters[idx].ValueName == paramIden.GetSimpleName())
					break;
			}

			ILTemplateInstance instance = compUnit.TemplateInstances[instType.Iden];
			ILTemplateArg arg = instance.Arguments[idx];

			if (arg.Type is BuiltinSymbolType builtinType)
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.Char:
				case BuiltinTypes.WChar:
				case BuiltinTypes.Rune:
				{
					long val = long.Parse(arg.Value);
					return new ILInterpOperand(instruction.RetOperand, val);
				}
				case BuiltinTypes.F32:
				{
					return new ILInterpOperand(instruction.RetOperand, null);
				}
				case BuiltinTypes.F64:
				{
					return new ILInterpOperand(instruction.RetOperand, null);
				}
				case BuiltinTypes.String:
				{
					return new ILInterpOperand(instruction.RetOperand, null);
				}
				}
			}

			return null;
		}


		static ILInterpOperand InterpEnumGetMemberValue(ILEnumGetMemberValueInstruction instruction, ILCompUnit compUnit, Dictionary<string, ILInterpOperand> opMapping)
		{
			ILReference memberRef = instruction.MemberRef;

			ILEnum ilEnum = compUnit.GetEnum((instruction.Type as EnumSymbolType).MangledIdentifier);
			ILEnumMember member = ilEnum.GetMember(memberRef.Element.GetSimpleName());

			ILInterpOperand ret = new ILInterpOperand(instruction.RetOperand, member.EnumVal.Value);
			opMapping.Add(ret.Operand.Iden, ret);
			return ret;
		}

		static ILInterpOperand InterpConvType(ILConvertTypeInstruction instruction, Dictionary<string, ILInterpOperand> opMapping)
		{
			ILInterpOperand op = opMapping[instruction.Operand.Iden];
			ILInterpOperand ret = new ILInterpOperand(instruction.RetOperand, op.Value);
			opMapping.Add(ret.Operand.Iden, ret);
			return ret;
		}

		static ILInterpOperand InterpReturn(ILReturnInstruction instruction, Dictionary<string, ILInterpOperand> opMapping)
		{
			if (instruction.Operand == null)
				return null;

			return opMapping[instruction.Operand.Iden];
		}

		static string InterpBranch(ILBranchInstruction instruction)
		{
			return instruction.Label;
		}

		static string InterpCondBranch(ILCondBranchInstruction instruction, Dictionary<string, ILInterpOperand> opMapping)
		{
			ILInterpOperand op = opMapping[instruction.Condition.Iden];
			long val = (op.Value as long?).Value;
			return val == 0 ? instruction.FalseLabel : instruction.TrueLabel;
		}

	}
}
