﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.ILBackend.Interpret
{
	public class ILInterpOperand
	{
		public ILOperand Operand;

		public object Value;

		public ILInterpOperand(ILOperand operand, object value)
		{
			Operand = operand;
			Value = value;
		}
	}
}
