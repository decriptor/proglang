﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend
{

	public class ILOperand
	{
		public ILOperand(string iden, SymbolType type)
		{
			Iden = iden;
			Type = type;
		}

		public ILOperand(ILOperand other)
		{
			Iden = other.Iden;
			Type = other.Type;
		}

		public string Iden;
		public SymbolType Type;

		public override string ToString()
		{
			return $"%{Iden} : ${Type.ToILString()}";
		}
	}

	public class ILReference
	{
		public ILReference(Identifier refBase, Identifier element)
		{
			Base = refBase;
			Element = element;
		}

		public Identifier Base;
		public Identifier Element;

		public override string ToString()
		{
			return $"#{Base}.{Element}";
		}
	}

	public enum ILAggregateType
	{
		Struct,
		Union
	}

	public class ILAggregateVariable
	{
		public Identifier Identifier;
		public string MangledIdentifier;
		public SymbolType Type;

		public ILAggregateVariable(Identifier iden, string mangledName, SymbolType type)
		{
			Identifier = iden;
			MangledIdentifier = mangledName;
			Type = type;
		}

		public override string ToString()
		{
			return $"%{MangledIdentifier} : ${Type.ToILString()}";
		}
	}

	public class ILAggregate
	{
		public Symbol Symbol;

		public ILAggregateType Type;

		public ScopeVariable Identifier;
		public ScopeVariable BaseIdentifier;
		public string MangledIdentifier;

		public ILAttributes Attributes;

		public List<ILTemplateParam> TemplateParams;
		public ILAggregate BaseAggregate;
		
		public List<ILAggregateVariable> Variables = new List<ILAggregateVariable>();

		public ILAggregate(ILAggregateType aggregateType, ScopeVariable identifier, string mangleName, Symbol symbol, ILAttributes attribs, List<ILTemplateParam> templateParams, ILAggregate baseAggregate = null)
		{
			Type = aggregateType;
			Identifier = identifier;
			BaseIdentifier = BaseAggregate?.Identifier ?? identifier.GetBaseTemplate();
			MangledIdentifier = mangleName;
			Symbol = symbol;
			Attributes = attribs;
			TemplateParams = templateParams;
		}
	}

	/*public class ILMethod
	{
		public Identifier Iden;
		public FunctionSymbolType Type;
		public string FuncIden;

		public ILMethod(Identifier iden, FunctionSymbolType type, string funcIden)
		{
			Iden = iden;
			Type = type;
			FuncIden = funcIden;
		}

		public override string ToString()
		{
			return $"{Iden} ${Type} : {FuncIden}";
		}
	}

	public class ILVTable
	{
		public ScopeVariable AggregateIden;
		public string MangledIden;
		public List<ILMethod> Methods = new List<ILMethod>();

		public ILVTable(ScopeVariable aggregateIden, string mangled)
		{
			AggregateIden = aggregateIden;
			MangledIden = mangled;
		}

		public ILMethod GetMethod(string name)
		{
			foreach (ILMethod method in Methods)
			{
				if (method.Iden.ToString() == name)
					return method;
			}

			return null;
		}

		public ILMethod GetMethod(string name, FunctionSymbolType funcType)
		{
			foreach (ILMethod method in Methods)
			{
				if (method.Iden.ToString() == name &&
				    method.Type == funcType)
					return method;
			}

			return null;
		}
	}*/

	public class ILTypedef
	{
		public SymbolType Type;
		public string Identifier;

		public ILTypedef(SymbolType type, string identifier)
		{
			Type = type;
			Identifier = identifier;
		}
	}

	public class ILEnumMember
	{
		public string Identifier;
		public string EnumInit;
		public long? EnumVal = null;
		public SymbolType AdtType;

		public ILEnumMember(string iden, string enumInit, SymbolType adtType)
		{
			Identifier = iden;
			EnumInit = enumInit;
			AdtType = adtType;
		}
	}

	public class ILEnum
	{
		public string Identifier;
		public string MangledIdentifier;
		public SymbolType BaseType;
		public List<ILEnumMember> Members = new List<ILEnumMember>();
		public bool IsADT;


		public ILEnum(string identifier, string mangledIdentifier, SymbolType baseType)
		{
			Identifier = identifier;
			MangledIdentifier = mangledIdentifier;
			BaseType = baseType;
		}

		public void AddMember(ILEnumMember member)
		{
			Members.Add(member);

			if (!IsADT)
			{
				IsADT = member.AdtType != null;
			}
		}

		public ILEnumMember GetMember(string iden)
		{
			foreach (ILEnumMember member in Members)
			{
				if (member.Identifier == iden)
					return member;
			}

			return null;
		}
	}

	public class ILDelegate
	{
		public string MangledIdentifier;
		public List<SymbolType> ParamTypes;
		public SymbolType ReturnType;
		public bool AsFuncPtr;

		public ILDelegate(string mangledName, List<SymbolType> paramTypes, SymbolType returnType, bool asFuncPtr)
		{
			MangledIdentifier = mangledName;
			ParamTypes = paramTypes;
			ReturnType = returnType;
			AsFuncPtr = asFuncPtr;
		}
	}

	public class ILGlobal
	{
		public string MangledIdentifier;
		public SymbolType Type;
		public string Initializer;
		public string Value;
		public ILAttributes Attributes;

		public ILGlobal(string mangledName, SymbolType type, string initializer, ILAttributes attribs)
		{
			MangledIdentifier = mangledName;
			Type = type;
			Initializer = initializer;
			Attributes = attribs;
		}
	}

	public class ILTypeAlias
	{
		public string MangledAlias;

		public ILTypeAlias(string mangledAlias)
		{
			MangledAlias = mangledAlias;
		}
	}

}
