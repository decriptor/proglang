﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Builtin
{
	class BuiltinModule
	{

		public static ILCompUnit GenerateBuiltins()
		{
			ILCompUnit compUnit = new ILCompUnit();
			compUnit.ILModule = "MJay";
			compUnit.Stage = ILStage.Canonical;

			List<SymbolType> sigInts = new List<SymbolType>
			{
				SymbolType.BuiltinType(BuiltinTypes.I8),
				SymbolType.BuiltinType(BuiltinTypes.I16),
				SymbolType.BuiltinType(BuiltinTypes.I32),
				SymbolType.BuiltinType(BuiltinTypes.I64),
			};

			List<SymbolType> unsigInts = new List<SymbolType>
			{
				SymbolType.BuiltinType(BuiltinTypes.U8),
				SymbolType.BuiltinType(BuiltinTypes.U16),
				SymbolType.BuiltinType(BuiltinTypes.U32),
				SymbolType.BuiltinType(BuiltinTypes.U64),
			};

			List<SymbolType> fps = new List<SymbolType>
			{
				SymbolType.BuiltinType(BuiltinTypes.F32),
				SymbolType.BuiltinType(BuiltinTypes.F64),
			};

			SymbolType boolType = SymbolType.BuiltinType(BuiltinTypes.Bool);

			List<SymbolType> chars = new List<SymbolType>
			{
				SymbolType.BuiltinType(BuiltinTypes.Char),
				SymbolType.BuiltinType(BuiltinTypes.WChar),
				SymbolType.BuiltinType(BuiltinTypes.Rune),
			};

			List<SymbolType> integers = new List<SymbolType>();
			integers.AddRange(sigInts);
			integers.AddRange(unsigInts);

			List<SymbolType> arith = new List<SymbolType>();
			arith.AddRange(sigInts);
			arith.AddRange(unsigInts);
			arith.AddRange(fps);

			List<SymbolType> builtins = new List<SymbolType>();
			builtins.AddRange(arith);
			builtins.AddRange(chars);
			builtins.Add(boolType);

			// unary plus
			foreach (SymbolType type in arith)
			{
				ILOperator op = GenerateUnaryDummy(ILOpLoc.Prefix, ILOpType.Add, type);
				compUnit.AddBlock(op);
			}

			// unary minus
			foreach (SymbolType type in sigInts)
			{
				ILOperator op = GenerateUnary(ILOpLoc.Prefix, ILOpType.Sub, "neg", type);
				compUnit.AddBlock(op);
			}
			foreach (SymbolType type in fps)
			{
				ILOperator op = GenerateUnary(ILOpLoc.Prefix, ILOpType.Sub, "neg", type);
				compUnit.AddBlock(op);
			}

			// unary complement
			foreach (SymbolType type in integers)
			{
				ILOperator op = GenerateUnary(ILOpLoc.Prefix, ILOpType.Concat, "compl", type);
				compUnit.AddBlock(op);
			}

			// unary not
			{
				ILOperator op = GenerateUnary(ILOpLoc.Prefix, ILOpType.Concat, "not", boolType);
				compUnit.AddBlock(op);
			}

			// prefix inc
			foreach (SymbolType type in arith)
			{
				ILOperator op = GenerateUnary(ILOpLoc.Prefix, ILOpType.Inc, "inc", type);
				compUnit.AddBlock(op);
			}

			// postfix inc
			foreach (SymbolType type in arith)
			{
				ILOperator op = GenerateUnary(ILOpLoc.Postfix, ILOpType.Inc, "inc", type);
				compUnit.AddBlock(op);
			}

			// prefix dec
			foreach (SymbolType type in arith)
			{
				ILOperator op = GenerateUnary(ILOpLoc.Prefix, ILOpType.Dec, "dec", type);
				compUnit.AddBlock(op);
			}

			// postfix dec
			foreach (SymbolType type in arith)
			{
				ILOperator op = GenerateUnary(ILOpLoc.Postfix, ILOpType.Dec, "dec", type);
				compUnit.AddBlock(op);
			}

			// TODO: not
			{
				ILOperator op = GenerateUnary(ILOpLoc.Prefix, ILOpType.Not, "neg", boolType);
				compUnit.AddBlock(op);
			}


			// infix add
			foreach (SymbolType type in arith)
			{
				ILOperator op = GenerateBinary(ILOpType.Add, "add", type);
				compUnit.AddBlock(op);
			}

			// infix sub
			foreach (SymbolType type in arith)
			{
				ILOperator op = GenerateBinary(ILOpType.Sub, "sub", type);
				compUnit.AddBlock(op);
			}

			// infix mul
			foreach (SymbolType type in arith)
			{
				ILOperator op = GenerateBinary(ILOpType.Mul, "mul", type);
				compUnit.AddBlock(op);
			}

			// infix div
			foreach (SymbolType type in arith)
			{
				ILOperator op = GenerateBinary(ILOpType.Div, "div", type);
				compUnit.AddBlock(op);
			}

			// infix rem
			foreach (SymbolType type in arith)
			{
				ILOperator op = GenerateBinary(ILOpType.Rem, "rem", type);
				compUnit.AddBlock(op);
			}

			// infix shift.l
			foreach (SymbolType type in integers)
			{
				ILOperator op = GenerateBinary(ILOpType.Shl, "shift.l", type);
				compUnit.AddBlock(op);
			}

			// infix shift.lr (>>)
			foreach (SymbolType type in integers)
			{
				ILOperator op = GenerateBinary(ILOpType.LShr, "shift.lr", type);
				compUnit.AddBlock(op);
			}

			// infix shift.ar (>>>)
			foreach (SymbolType type in integers)
			{
				ILOperator op = GenerateBinary(ILOpType.AShr, "shift.ar", type);
				compUnit.AddBlock(op);
			}

			// infix or
			foreach (SymbolType type in integers)
			{
				ILOperator op = GenerateBinary(ILOpType.BOr, "or", type);
				compUnit.AddBlock(op);
			}

			// infix xor
			foreach (SymbolType type in integers)
			{
				ILOperator op = GenerateBinary(ILOpType.BXor, "xor", type);
				compUnit.AddBlock(op);
			}

			// infix and
			foreach (SymbolType type in integers)
			{
				ILOperator op = GenerateBinary(ILOpType.BAnd, "and", type);
				compUnit.AddBlock(op);
			}

			// infix assign
			foreach (SymbolType type in arith)
			{
				ILOperator op = GenerateAssignSimple(ILOpType.Assign, type);
				compUnit.AddBlock(op);
			}

			// infix add assign
			foreach (SymbolType type in arith)
			{
				ILOperator op = GenerateAssign(ILOpType.AddAssign, "add", type);
				compUnit.AddBlock(op);
			}

			// infix sub assign
			foreach (SymbolType type in arith)
			{
				ILOperator op = GenerateAssign(ILOpType.SubAssign, "sub", type);
				compUnit.AddBlock(op);
			}

			// infix mul assign
			foreach (SymbolType type in arith)
			{
				ILOperator op = GenerateAssign(ILOpType.MulAssign, "mul", type);
				compUnit.AddBlock(op);
			}

			// infix div assign
			foreach (SymbolType type in arith)
			{
				ILOperator op = GenerateAssign(ILOpType.DivAssign, "div", type);
				compUnit.AddBlock(op);
			}

			// infix rem assign
			foreach (SymbolType type in arith)
			{
				ILOperator op = GenerateAssign(ILOpType.RemAssign, "rem", type);
				compUnit.AddBlock(op);
			}

			// infix shift.l assign
			foreach (SymbolType type in integers)
			{
				ILOperator op = GenerateAssign(ILOpType.ShlAssign, "shift.l", type);
				compUnit.AddBlock(op);
			}

			// infix shift.lr (>>) assign
			foreach (SymbolType type in integers)
			{
				ILOperator op = GenerateAssign(ILOpType.LShrAssign, "shift.lr", type);
				compUnit.AddBlock(op);
			}

			// infix shift.ar (>>>) assign
			foreach (SymbolType type in integers)
			{
				ILOperator op = GenerateAssign(ILOpType.AShrAssign, "shift.ar", type);
				compUnit.AddBlock(op);
			}

			// infix or assign
			foreach (SymbolType type in integers)
			{
				ILOperator op = GenerateAssign(ILOpType.OrAssign, "or", type);
				compUnit.AddBlock(op);
			}

			// infix xor assign
			foreach (SymbolType type in integers)
			{
				ILOperator op = GenerateAssign(ILOpType.XorAssign, "xor", type);
				compUnit.AddBlock(op);
			}

			// infix and assign
			foreach (SymbolType type in integers)
			{
				ILOperator op = GenerateAssign(ILOpType.AndAssign, "and", type);
				compUnit.AddBlock(op);
			}


			// infix eq
			foreach (SymbolType type in builtins)
			{
				ILOperator op = GenerateBinaryBool(ILOpType.Eq, "cmp.eq", type, boolType);
				compUnit.AddBlock(op);
			}

			// infix ne
			foreach (SymbolType type in builtins)
			{
				ILOperator op = GenerateBinaryBool(ILOpType.Ne, "cmp.ne", type, boolType);
				compUnit.AddBlock(op);
			}

			// infix gt
			foreach (SymbolType type in builtins)
			{
				ILOperator op = GenerateBinaryBool(ILOpType.Lt, "cmp.gt", type, boolType);
				compUnit.AddBlock(op);
			}

			// infix gte
			foreach (SymbolType type in builtins)
			{
				ILOperator op = GenerateBinaryBool(ILOpType.Lte, "cmp.gte", type, boolType);
				compUnit.AddBlock(op);
			}

			// infix lt
			foreach (SymbolType type in builtins)
			{
				ILOperator op = GenerateBinaryBool(ILOpType.Gt, "cmp.lt", type, boolType);
				compUnit.AddBlock(op);
			}

			// infix lte
			foreach (SymbolType type in builtins)
			{
				ILOperator op = GenerateBinaryBool(ILOpType.Gte, "cmp.lte", type, boolType);
				compUnit.AddBlock(op);
			}

			// cast static to i1
			foreach (SymbolType type in integers)
			{
				ILOperator op = GenerateCastToBool(type, boolType);
				compUnit.AddBlock(op);
			}

			// cast static from i1
			foreach (SymbolType type in integers)
			{
				ILOperator op = GenerateCast(boolType, type, "ext");
				compUnit.AddBlock(op);
			}

			// cast static (integer to integer)
			foreach (SymbolType from in integers)
			{
				int fromSize = (from as BuiltinSymbolType).GetNumBytes();
				foreach (SymbolType to in integers)
				{
					if (to == from)
						continue;

					int toSize = (to as BuiltinSymbolType).GetNumBytes();

					ILOperator op;
					if (fromSize == toSize)
					{
						op = GenerateConvType(from, to);
					}
					else if (fromSize < toSize)
					{
						op = GenerateCast(from, to, "ext");
					}
					else
					{
						op = GenerateCast(from, to, "trunc");
					}
					compUnit.AddBlock(op);
				}
			}

			// cast static (integer to char)
			foreach (SymbolType from in integers)
			{
				int fromSize = (from as BuiltinSymbolType).GetNumBytes();
				foreach (SymbolType to in chars)
				{
					if (to == from)
						continue;

					int toSize = (to as BuiltinSymbolType).GetNumBytes();

					ILOperator op;
					if (fromSize == toSize)
					{
						op = GenerateConvType(from, to);
					}
					else if (fromSize < toSize)
					{
						op = GenerateCast(from, to, "ext");
					}
					else
					{
						op = GenerateCast(from, to, "trunc");
					}
					compUnit.AddBlock(op);
				}
			}

			// cast static (char to integer)
			foreach (SymbolType from in chars)
			{
				int fromSize = (from as BuiltinSymbolType).GetNumBytes();
				foreach (SymbolType to in integers)
				{
					if (to == from)
						continue;

					int toSize = (to as BuiltinSymbolType).GetNumBytes();

					ILOperator op;
					if (fromSize == toSize)
					{
						op = GenerateConvType(from, to);
					}
					else if (fromSize < toSize)
					{
						op = GenerateCast(from, to, "ext");
					}
					else
					{
						op = GenerateCast(from, to, "trunc");
					}
					compUnit.AddBlock(op);
				}
			}

			// cast static (fp to fp)
			foreach (SymbolType from in fps)
			{
				int fromSize = (from as BuiltinSymbolType).GetNumBytes();
				foreach (SymbolType to in fps)
				{
					if (to == from)
						continue;

					int toSize = (to as BuiltinSymbolType).GetNumBytes();

					ILOperator op;
					if (fromSize == toSize)
					{
						op = GenerateConvType(from, to);
					}
					else if (fromSize < toSize)
					{
						op = GenerateCast(from, to, "ext");
					}
					else
					{
						op = GenerateCast(from, to, "trunc");
					}
					compUnit.AddBlock(op);
				}
			}

			// cast static (fp to integer)
			foreach (SymbolType from in fps)
			{
				foreach (SymbolType to in integers)
				{
					if (to == from)
						continue;

					ILOperator op = GenerateCast(from, to, "fptoi");
					compUnit.AddBlock(op);
				}
			}

			// cast static (integer to fp)
			foreach (SymbolType from in integers)
			{
				foreach (SymbolType to in fps)
				{
					if (to == from)
						continue;

					ILOperator op = GenerateCast(from, to, "itofp");
					compUnit.AddBlock(op);
				}
			}

			return compUnit;
		}

		static ILOperator GenerateUnaryDummy(ILOpLoc loc, ILOpType opType, SymbolType type)
		{
			string opName = ILHelpers.GetIlOpName(loc, opType);
			ILOperand op = new ILOperand("0", type);
			List<ILOperand> operands = new List<ILOperand> { op };

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ILOperator ilOperator = new ILOperator(opName, operands, type, attribs);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);
			
			basicBlock.Instructions.Add(new ILReturnInstruction(op, null));

			return ilOperator;
		}

		static ILOperator GenerateUnary(ILOpLoc loc, ILOpType opType, string unaryBuiltin, SymbolType type)
		{
			string opName = ILHelpers.GetIlOpName(loc, opType);
			ILOperand op = new ILOperand("0", type);
			List<ILOperand> operands = new List<ILOperand>{ op };

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ILOperator ilOperator = new ILOperator(opName, operands, type, attribs);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);

			ILOperand retOp = new ILOperand("1", type);
			basicBlock.Instructions.Add(new ILBuiltinInstruction(retOp, unaryBuiltin, operands, type, null));
			basicBlock.Instructions.Add(new ILReturnInstruction(retOp, null));

			return ilOperator;
		}

		static ILOperator GenerateBinary(ILOpType opType, string binaryBuiltin, SymbolType type)
		{
			string opName = ILHelpers.GetIlOpName(ILOpLoc.Infix, opType);
			ILOperand op0 = new ILOperand("0", type);
			ILOperand op1 = new ILOperand("1", type);
			List<ILOperand> operands = new List<ILOperand> { op0, op1 };

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ILOperator ilOperator = new ILOperator(opName, operands, type, attribs);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);

			ILOperand retOp = new ILOperand("2", type);
			basicBlock.Instructions.Add(new ILBuiltinInstruction(retOp, binaryBuiltin, operands, type, null));
			basicBlock.Instructions.Add(new ILReturnInstruction(retOp, null));

			return ilOperator;
		}

		static ILOperator GenerateBinaryBool(ILOpType opType, string binaryBuiltin, SymbolType type, SymbolType boolType)
		{
			string opName = ILHelpers.GetIlOpName(ILOpLoc.Infix, opType);
			ILOperand op0 = new ILOperand("0", type);
			ILOperand op1 = new ILOperand("1", type);
			List<ILOperand> operands = new List<ILOperand> { op0, op1 };

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ILOperator ilOperator = new ILOperator(opName, operands, boolType, attribs);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);

			ILOperand retOp = new ILOperand("2", boolType);
			basicBlock.Instructions.Add(new ILBuiltinInstruction(retOp, binaryBuiltin, operands, boolType, null));
			basicBlock.Instructions.Add(new ILReturnInstruction(retOp, null));

			return ilOperator;
		}

		static ILOperator GenerateAssignSimple(ILOpType opType, SymbolType type)
		{
			string opName = ILHelpers.GetIlOpName(ILOpLoc.Infix, opType);
			SymbolType memType = SymbolType.MemoryLocType(type);
			ILOperand op0 = new ILOperand("0", memType);
			ILOperand op1 = new ILOperand("1", type);
			List<ILOperand> operands = new List<ILOperand> { op0, op1 };

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ILOperator ilOperator = new ILOperator(opName, operands, type, attribs);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);

			basicBlock.Instructions.Add(new ILStoreInstruction(op1, op0, null));
			ILOperand retOp = new ILOperand("2", type);
			basicBlock.Instructions.Add(new ILLoadInstruction(retOp, op0, null));
			basicBlock.Instructions.Add(new ILReturnInstruction(retOp, null));

			return ilOperator;
		}

		static ILOperator GenerateAssign(ILOpType opType, string binaryBuiltin, SymbolType type)
		{
			string opName = ILHelpers.GetIlOpName(ILOpLoc.Infix, opType);
			SymbolType memType = SymbolType.MemoryLocType(type);
			ILOperand op0 = new ILOperand("0", memType);
			ILOperand op1 = new ILOperand("1", type);
			List<ILOperand> operands = new List<ILOperand> {op0, op1};

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ILOperator ilOperator = new ILOperator(opName, operands, type, attribs);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);

			ILOperand op2 = new ILOperand("2", type);
			basicBlock.Instructions.Add(new ILLoadInstruction(op2, op0, null));

			ILOperand op3 = new ILOperand("3", type);
			List<ILOperand> builtinOps = new List<ILOperand>{op2, op1};
			basicBlock.Instructions.Add(new ILBuiltinInstruction(op3, binaryBuiltin, builtinOps, type, null));
			basicBlock.Instructions.Add(new ILStoreInstruction(op3, op0, null));

			ILOperand retOp = new ILOperand("4", type);
			basicBlock.Instructions.Add(new ILLoadInstruction(retOp, op0, null));
			basicBlock.Instructions.Add(new ILReturnInstruction(retOp, null));

			return ilOperator;
		}

		static ILOperator GenerateCastToBool(SymbolType type, SymbolType boolType)
		{
			string opName = ILHelpers.GetIlOpName(ILOpLoc.Infix, ILOpType.Static);
			ILOperand op0 = new ILOperand("0", type);
			List<ILOperand> operands = new List<ILOperand> { op0 };

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ILOperator ilOperator = new ILOperator(opName, operands, boolType, attribs);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);

			ILOperand op1 = new ILOperand("1", type);
			basicBlock.Instructions.Add(new ILIntLiteralInstruction(op1, 0, null));
			ILOperand retOp = new ILOperand("2", boolType);
			basicBlock.Instructions.Add(new ILBuiltinInstruction(retOp, "cmp.ne", new List<ILOperand>{ op0, op1 }, boolType, null));
			basicBlock.Instructions.Add(new ILReturnInstruction(retOp, null));

			return ilOperator;
		}

		static ILOperator GenerateCast(SymbolType from, SymbolType to, string convBuiltin)
		{
			string opName = ILHelpers.GetIlOpName(ILOpLoc.Cast, ILOpType.Static);
			ILOperand op = new ILOperand("0", from);
			List<ILOperand> operands = new List<ILOperand> { op };

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ILOperator ilOperator = new ILOperator(opName, operands, to, attribs);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);

			ILOperand retOp = new ILOperand("1", to);
			basicBlock.Instructions.Add(new ILBuiltinInstruction(retOp, convBuiltin, operands, to, null));
			basicBlock.Instructions.Add(new ILReturnInstruction(retOp, null));

			return ilOperator;
		}
		
		static ILOperator GenerateConvType(SymbolType from, SymbolType to)
		{
			string opName = ILHelpers.GetIlOpName(ILOpLoc.Cast, ILOpType.Static);
			ILOperand op = new ILOperand("0", from);
			List<ILOperand> operands = new List<ILOperand> { op };

			ILAttributes attribs = new ILAttributes();
			attribs.Inline = ILInline.Always;

			ILOperator ilOperator = new ILOperator(opName, operands, to, attribs);
			ILBasicBlock basicBlock = new ILBasicBlock("entry");
			ilOperator.Blocks.Add(basicBlock);

			ILOperand retOp = new ILOperand("1", to);
			basicBlock.Instructions.Add(new ILConvertTypeInstruction(retOp, op, to, null));
			basicBlock.Instructions.Add(new ILReturnInstruction(retOp, null));

			return ilOperator;
		}

	}
}
