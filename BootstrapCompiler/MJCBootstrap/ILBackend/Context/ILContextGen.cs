﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Context
{
	public static class ILContextGen
	{
		public static void ProcessModule(ILCompUnit compUnit, CompilerContext compilerContext)
		{
			compUnit.Context = new ILCompUnitContext();
			compUnit.Context.CompUnit = compUnit;
			compUnit.Context.CompilerContext = compilerContext;

			foreach (KeyValuePair<string, ILBlock> pair in compUnit.Blocks)
			{
				switch (pair.Value)
				{
				case ILFunction function:
					ProcessFunction(function, compUnit);
					break;
				case ILOperator op:
					ProcessOperator(op);
					break;
				}
			}
		}

		public static void ProcessFunction(ILFunction function, ILCompUnit compUnit)
		{
			// Skip template definitions
			if (function.IsUninstantiated)
				return;

			function.Context = compUnit.Context.CreateContext();

			if (function.Operands != null)
			{
				foreach (ILOperand op in function.Operands)
				{
					ILVarContext context = new ILVarContext(op, ILVarType.Param);
					function.Context.AddVarContext(context);
				}
			}

			foreach (ILBasicBlock block in function.Blocks)
			{
				ProcessBasicBlock(block, function.Context);
			}
		}

		public static void ProcessOperator(ILOperator op)
		{
			foreach (ILBasicBlock block in op.Blocks)
			{
				ProcessBasicBlock(block, op.Context);
			}
		}

		public static void ProcessBasicBlock(ILBasicBlock basicBlock, ILContext funcContext)
		{
			foreach (ILInstruction instruction in basicBlock.Instructions)
			{
				switch (instruction.InstructionType)
				{
				case ILInstructionType.Unknown:
					break;
				case ILInstructionType.IntLiteral:
					GenIntLiteral(instruction as ILIntLiteralInstruction, funcContext);
					break;
				case ILInstructionType.FloatLiteral:
					GenFloatLiteral(instruction as ILFloatLiteralInstruction, funcContext);
					break;
				case ILInstructionType.StringLiteral:
					GenStringLiteral(instruction as ILStringLiteralInstruction, funcContext);
					break;
				case ILInstructionType.AllocStack:
					GenAllocStack(instruction as ILAllocStackInstruction, funcContext);
					break;
				case ILInstructionType.Assign:
					GenAssign(instruction as ILAssignInstruction, funcContext);
					break;
				case ILInstructionType.Load:
					GenLoad(instruction as ILLoadInstruction, funcContext);
					break;
				case ILInstructionType.OperatorRef:
					GenOperatorRef(instruction as ILOperatorRefInstruction, funcContext);
					break;
				case ILInstructionType.FunctionRef:
					GenFunctionRef(instruction as ILFunctionRefInstruction, funcContext);
					break;
				case ILInstructionType.MethodRef:
					GenMethodRef(instruction as ILMethodRefInstruction, funcContext);
					break;
				case ILInstructionType.Call:
					GenCall(instruction as ILCallInstruction, funcContext);
					break;
				case ILInstructionType.Builtin:
					GenBuiltin(instruction as ILBuiltinInstruction, funcContext);
					break;
				case ILInstructionType.StructExtract:
					GenStructExtract(instruction as ILStructExtractInstruction, funcContext);
					break;
				case ILInstructionType.StructInsert:
					GenStructInsert(instruction as ILStructInsertInstruction, funcContext);
					break;
				case ILInstructionType.StructElemAddr:
					GenStructElemAddr(instruction as ILStructElemAddrInstruction, funcContext);
					break;
				case ILInstructionType.EnumGetMemberValue:
					GenEnumGetMemberValue(instruction as ILEnumGetMemberValueInstruction, funcContext);
					break;
				case ILInstructionType.GlobalAddr:
					GenGlobalAddr(instruction as ILGlobalAddrInstruction, funcContext);
					break;
				case ILInstructionType.GlobalValue:
					GenGlobalValue(instruction as ILGlobalValueInstruction, funcContext);
					break;
				case ILInstructionType.ConvType:
					GenConvType(instruction as ILConvertTypeInstruction, funcContext);
					break;

				case ILInstructionType.Return:
					GenReturn(instruction as ILReturnInstruction, funcContext);
					break;
				}
			}
		}

		public static void GenIntLiteral(ILIntLiteralInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);
		}

		public static void GenFloatLiteral(ILFloatLiteralInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);
		}

		public static void GenStringLiteral(ILStringLiteralInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);
		}

		public static void GenAllocStack(ILAllocStackInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Stack);
			context.AddVarContext(varContext);
		}

		public static void GenAssign(ILAssignInstruction instruction, ILContext context)
		{
			ILVarContext src = context.GetVarContext(instruction.Src.Iden);
			ILVarContext dst = context.GetVarContext(instruction.Dst.Iden);

			dst.Dependents.Add(src.Operand.Iden);
			++src.UseCount;
		}

		public static void GenLoad(ILLoadInstruction instruction, ILContext context)
		{
			ILVarContext src = context.GetVarContext(instruction.Operand.Iden);
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);

			varContext.Dependents.Add(src.Operand.Iden);
			++src.UseCount;

			context.AddVarContext(varContext);
		}

		public static void GenOperatorRef(ILOperatorRefInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);
		}

		public static void GenFunctionRef(ILFunctionRefInstruction instruction, ILContext context)
		{
			if (instruction.RetOperand == null)
				return;

			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);

			context.AddVarContext(varContext);

			ILCompUnit compUnit = context.Parent.CompUnit;
			if (compUnit.GetFunction(instruction.MangledName) == null &&
			    !context.Parent.ExternalFunctions.ContainsKey(instruction.MangledName))
			{
				ILFunction func = compUnit.GetExternalFunction(instruction.MangledName);
				context.Parent.ExternalFunctions.Add(instruction.MangledName, func.GetDeclaration());
			}
		}

		public static void GenMethodRef(ILMethodRefInstruction instruction, ILContext context)
		{
			if (instruction.RetOperand == null)
				return;

			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);

			context.AddVarContext(varContext);
		}

		public static void GenCall(ILCallInstruction instruction, ILContext context)
		{
			ILVarContext varContext = null;
			if (instruction.RetOperand != null)
			{
				varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);

				context.AddVarContext(varContext);
			}

			ILVarContext func = context.GetVarContext(instruction.Func.Iden);
			++func.UseCount;

			if (varContext == null)
				return;

			varContext.Dependents.Add(varContext.Operand.Iden);

			if (instruction.Params != null)
			{
				foreach (ILOperand operand in instruction.Params)
				{
					ILVarContext param = context.GetVarContext(operand.Iden);
					++param.UseCount;
					varContext.Dependents.Add(operand.Iden);
				}
			}

		}

		public static void GenBuiltin(ILBuiltinInstruction instruction, ILContext context)
		{
			if (instruction.RetOperand != null)
			{
				ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
				context.AddVarContext(varContext);
			}
		}

		public static void GenStructInsert(ILStructInsertInstruction instruction, ILContext context)
		{
			ILVarContext srcContext = context.GetVarContext(instruction.Value.Iden);
			++srcContext.UseCount;
		}

		public static void GenStructExtract(ILStructExtractInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);
		}

		public static void GenStructElemAddr(ILStructElemAddrInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Stack);
			context.AddVarContext(varContext);
		}

		public static void GenEnumGetMemberValue(ILEnumGetMemberValueInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Stack);
			context.AddVarContext(varContext);
		}

		public static void GenGlobalAddr(ILGlobalAddrInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Stack);
			context.AddVarContext(varContext);

			ILCompUnitContext parent = context.Parent;
			if (parent.CompUnit.GetGlobal(instruction.Identifier) == null &&
			    !parent.ExternalGlobals.ContainsKey(instruction.Identifier))
			{
				ILGlobal tmp = parent.CompUnit.GetExternalGlobal(instruction.Identifier);
				ILAttributes attribs = new ILAttributes(tmp.Attributes, true);
				ILGlobal global = new ILGlobal(tmp.MangledIdentifier, tmp.Type, tmp.Initializer, attribs);
				global.Value = tmp.Value;
				parent.ExternalGlobals.Add(instruction.Identifier, global);
			}
		}

		public static void GenGlobalValue(ILGlobalValueInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Stack);
			context.AddVarContext(varContext);

			ILCompUnitContext parent = context.Parent;
			if (parent.CompUnit.GetGlobal(instruction.Identifier) == null &&
			    !parent.ExternalGlobals.ContainsKey(instruction.Identifier))
			{
				ILGlobal tmp = parent.CompUnit.GetExternalGlobal(instruction.Identifier);
				ILAttributes attribs = new ILAttributes(tmp.Attributes, true);
				ILGlobal global = new ILGlobal(tmp.MangledIdentifier, tmp.Type, tmp.Initializer, attribs);
				global.Value = tmp.Value;
				parent.ExternalGlobals.Add(instruction.Identifier, global);
			}
		}

		public static void GenConvType(ILConvertTypeInstruction instruction, ILContext context)
		{
			ILVarContext varContext = new ILVarContext(instruction.RetOperand, ILVarType.Intermediate);
			context.AddVarContext(varContext);
		}

		public static void GenReturn(ILReturnInstruction instruction, ILContext context)
		{
			if (instruction.Operand != null)
			{
				ILVarContext varContext = context.GetVarContext(instruction.Operand.Iden);
				++varContext.UseCount;
			}
		}

	}
}
