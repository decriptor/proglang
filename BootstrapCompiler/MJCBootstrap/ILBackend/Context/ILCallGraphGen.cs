﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Context
{
	public static class ILCallGraphGen
	{
		public static void ProcessModule(ILCompUnit compUnit)
		{
			compUnit.Context.CallGraph = new ILCallGraph();

			foreach (KeyValuePair<string, ILBlock> pair in compUnit.Blocks)
			{
				switch (pair.Value)
				{
				case ILFunction function:
					ProcessFunction(function);
					break;
				case ILOperator op:
					ProcessOperator(op);
					break;
				}
			}
		}

		public static void ProcessFunction(ILFunction function)
		{
			// Skip template definitions
			if (function.IsUninstantiated)
				return;

			ILCompUnitContext parentContext = function.Context.Parent;
			function.Context.CallGraphNode = parentContext.CallGraph.AddNode(function);

			foreach (ILBasicBlock block in function.Blocks)
			{
				ProcessBasicBlock(block, function.Context);
			}
		}

		public static void ProcessOperator(ILOperator op)
		{
			ILCompUnitContext parentContext = op.Context.Parent;
			op.Context.CallGraphNode = parentContext.CallGraph.AddNode(op);

			foreach (ILBasicBlock block in op.Blocks)
			{
				ProcessBasicBlock(block, op.Context);
			}
		}

		public static void ProcessBasicBlock(ILBasicBlock block, ILContext funcContext)
		{
			Dictionary<string, ILInstruction> refs = new Dictionary<string, ILInstruction>();
			ILCallGraph callgraph = funcContext.Parent.CallGraph;
			ILCompUnit compUnit = funcContext.Parent.CompUnit;

			ILSubGraphNode curSubNode = funcContext.CallGraphNode.CreateSubNode(block);

			foreach (ILInstruction instruction in block.Instructions)
			{
				switch (instruction.InstructionType)
				{
				case ILInstructionType.FunctionRef:
				{
					ILFunctionRefInstruction funcRef = instruction as ILFunctionRefInstruction;
					refs.Add(funcRef.RetOperand.Iden, funcRef);
					break;
				}
				case ILInstructionType.MethodRef:
				{
					ILMethodRefInstruction funcRef = instruction as ILMethodRefInstruction;
					refs.Add(funcRef.RetOperand.Iden, funcRef);
					break;
				}
				case ILInstructionType.OperatorRef:
				{
					ILOperatorRefInstruction opRef = instruction as ILOperatorRefInstruction;
					refs.Add(opRef.RetOperand.Iden, opRef);
					break;
				}
				case ILInstructionType.Call:
				{
					ILCallInstruction call = instruction as ILCallInstruction;
					ILInstruction callRef = refs[call.Func.Iden];

					if (callRef is ILFunctionRefInstruction funcRef)
					{
						var node = callgraph.GetNode(funcRef.MangledName);
						if (node == null)
						{
							ILFunction func = compUnit.GetFunction(funcRef.MangledName);
							if (func == null)
								func = compUnit.GetExternalFunction(funcRef.MangledName);
							node = callgraph.AddNode(func);
						}

						curSubNode.AddCall(node);
					}
					else if (callRef is ILMethodRefInstruction methodRef)
					{
						// Lookup method mangled name in VTable
						SymbolType recType = methodRef.Type.ReceiverType;
						if (recType is ReferenceSymbolType refType)
							recType = refType.BaseType;

						VTable vTable;
						if (recType is TemplateInstanceSymbolType tmp)
						{
							vTable = compUnit.GetVTable(tmp.Iden);
						}
						else
						{
							vTable = compUnit.GetVTable(recType.ToILString());
						}

						string methodName = null;
						if (recType is TemplateInstanceSymbolType instType)
						{
							List<VTable> vtableVariants = compUnit.GetVTableVariants(instType.Iden.GetBaseTemplate());
							TemplateInstanceName instName = instType.Iden.Name as TemplateInstanceName;
							TemplateVTables tables = TemplateHelpers.GetValidVTables(vtableVariants, instName);
							ILTemplateInstance instance = compUnit.TemplateInstances[instType.Iden];
							
							VTableMethod method = null;
							if (tables.FullSpecTable != null)
							{
								method = tables.FullSpecTable.GetMethod(methodRef.MethodRef.Element, methodRef.Type, instance);
							}
							else if (tables.PartSpecTables.Count != 0)
							{
								// Temp
								foreach (VTable table in tables.PartSpecTables)
								{
									method = table.GetMethod(methodRef.MethodRef.Element, methodRef.Type, instance);
									if (method != null)
										break;
								}
							}
							else
							{
								method = tables.BaseTable.GetMethod(methodRef.MethodRef.Element, methodRef.Type, instance);
							}
							methodName = method.MangledFunc;
						}
						else
						{
							VTableMethod method = vTable.GetMethod(methodRef.MethodRef.Element, methodRef.Type);
							methodName = method.MangledFunc;
						}

						var node = callgraph.GetNode(methodName);
						if (node == null)
						{
							ILFunction func = compUnit.GetFunction(methodName);
							node = callgraph.AddNode(func);
						}

						curSubNode.AddCall(node);
					}
					else if (callRef is ILOperatorRefInstruction opRef)
					{
						/*string typedName = ILHelpers.GetTypedIlOpName(opRef);
						var node = callgraph.GetNode(typedName);
						if (node == null)
						{
							ILOperator op = compUnit.GetOperator(typedName);
							node = callgraph.AddNode(op);
						}

						_curSubNode.AddCall(node);*/
					}

					break;
				}
				}
			}

			// Continue depending on last instruction
			if (block.IsTerminal())
			{
				// TODO
			}
		}


	}
}
