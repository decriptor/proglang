﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;
using MJC.ILBackend.General;

namespace MJC.ILBackend
{
	public class ILBlock
	{
		public ILContext Context;
	}

	public class ILBasicBlock : ILBlock
	{
		public string Label;
		public List<ILInstruction> Instructions = new List<ILInstruction>();

		public ILBasicBlock(string label)
		{
			Label = label;
		}

		public bool IsTerminal()
		{
			if (Instructions.Count == 0)
				return false;
			return Instructions.Last().IsTerminal();
		}

		public bool ExitsFunction()
		{
			if (Instructions.Count == 0)
				return false;
			return Instructions.Last().ExitsFunction();
		}
	}

	public class ILFunction : ILBlock
	{
		public string Identifier;
		public SymbolType ReturnType;

		public ILAttributes Attributes;

		public List<ILOperand> Operands;

		public List<ILTemplateParam> TemplateParams;
		public bool IsUninstantiated = false;

		public List<ILBasicBlock> Blocks = new List<ILBasicBlock>();

		public ILFunction(string identifier, List<ILOperand> operands, SymbolType retType, ILAttributes attribs, List<ILTemplateParam> templateParams, bool isUninstantiated)
		{
			Identifier = identifier;
			Operands = operands;
			ReturnType = retType;
			Attributes = attribs;
			TemplateParams = templateParams;
			IsUninstantiated = isUninstantiated;
		}

		public ILFunction GetDeclaration()
		{
			ILAttributes attribs = new ILAttributes(Attributes, true);
			return new ILFunction(Identifier, Operands, ReturnType, attribs, TemplateParams, IsUninstantiated);
		}
	}

	public class ILOperator : ILBlock
	{
		public string Identifier;
		public SymbolType ReturnType;

		public ILAttributes Attributes;

		public List<ILOperand> Operands = new List<ILOperand>();

		public List<ILBasicBlock> Blocks = new List<ILBasicBlock>();

		public ILOperator(string identifier, List<ILOperand> operands, SymbolType retType, ILAttributes attribs)
		{
			Identifier = identifier;
			Operands = operands;
			ReturnType = retType;
			Attributes = attribs;
		}

		public string GetTypedIdentifier()
		{
			return ILHelpers.GetTypedIlOpName(this);
		}
	}
}
