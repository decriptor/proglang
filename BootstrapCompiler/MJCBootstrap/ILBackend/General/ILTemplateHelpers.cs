﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.ILBackend.General
{
	public static class ILTemplateHelpers
	{
		public static ILAggregate GetBestBaseAggregate(ILTemplateInstance instance, ILCompUnit compUnit)
		{
			ScopeVariable baseIden = instance.Iden.GetBaseTemplate();
			List<ILAggregate> variants = compUnit.GetAggregateVariants(baseIden);

			if (variants.Count == 1)
				return variants[0];

			ILAggregate bestVariant = null;
			BitArray bestMatch = null;
			int count = instance.Arguments.Count;
			foreach (ILAggregate variant in variants)
			{
				BitArray specParams = new BitArray(count);
				for (var i = 0; i < variant.TemplateParams.Count; i++)
				{
					ILTemplateParam param = variant.TemplateParams[i];
					ILTemplateArg arg = instance.Arguments[i];


					if (param.ValueName == null)
					{
						if (param.TypeSpecialization != null)
						{
							if (param.TypeSpecialization != arg.Type)
								continue;

							specParams[i] = true;
						}
					}
					else
					{
						if (param.ValueSpecialization != null)
						{
							if (param.ValueSpecialization != arg.Value)
								continue;

							specParams[i] = true;
						}
					}
				}

				if (bestVariant == null)
				{
					bestVariant = variant;
					bestMatch = specParams;
				}
				else if (specParams.NumBitsSet() > bestMatch.NumBitsSet()) // TODO: Be more specific
				{
					bestVariant = variant;
					bestMatch = specParams;
				}

			}

			return bestVariant;
		}

		public static SymbolType GetInstancedType(SymbolType type, ILTemplateInstance instance)
		{
			switch (type)
			{
			case BuiltinSymbolType builtinType:
				return builtinType;
			case PointerSymbolType ptrType:
			{
				SymbolType baseType = GetInstancedType(ptrType.BaseType, instance);
				return SymbolType.PointerType(baseType, ptrType.Attributes);
			}
			case ReferenceSymbolType refType:
			{
				SymbolType baseType = GetInstancedType(refType.BaseType, instance);
				return SymbolType.ReferenceType(baseType, refType.Attributes);
			}
			case MemoryLocSymbolType memLocType:
			{
				SymbolType baseType = GetInstancedType(memLocType.BaseType, instance);
				return SymbolType.MemoryLocType(baseType, memLocType.Attributes);
			}
			case AggregateSymbolType aggrType:
			{
				Identifier iden = aggrType.GetIdentifier();

				if (iden is TemplateDefinitionName defName)
				{
					TemplateInstanceName instName = new TemplateInstanceName(defName.Iden, defName.Parameters.Count);
					for (var i = 0; i < defName.Parameters.Count; i++)
					{
						TemplateParameter param = defName.Parameters[i];
						ILTemplateArg ilArg = instance.Arguments[param.Index];
						instName.Arguments[i].Type = ilArg.Type;
					}
					ScopeVariable scopeVar = new ScopeVariable(aggrType.Identifier.Scope, instName);
					return SymbolType.TemplateInstanceType(scopeVar, aggrType.Symbol, type.Attributes);
				}

				return aggrType;
			}
			case FunctionSymbolType funcType:
			{
				List<SymbolType> paramTypes = null;
				if (funcType.ParamTypes != null)
				{
					paramTypes = new List<SymbolType>();
					foreach (SymbolType baseParamType in funcType.ParamTypes)
					{
						SymbolType paramType = GetInstancedType(baseParamType, instance);
						paramTypes.Add(paramType);
					}
				}

				SymbolType recType = GetInstancedType(funcType.ReceiverType, instance);
				SymbolType resType = GetInstancedType(funcType.ReturnType, instance);

				return SymbolType.FunctionType(recType, paramTypes, resType, funcType.Attributes);
			}
			case TemplateParamSymbolType templateParamType:
			{
				int idx = templateParamType.Index;
				ILTemplateArg arg = instance.Arguments[idx];
				return arg.Type;
			}
			case TemplateInstanceSymbolType templateInstType:
			{
				//if (templateInstType.Symbol.Identifier == instance.Type.TemplateSymbol.SelfInstance.Identifier)
				//	return instance.Type;

				TemplateInstanceName typeName = templateInstType.Iden.Name as TemplateInstanceName;

				List<Symbol> paramSyms = templateInstType.TemplateSymbol.GetChildrenOfKind(SymbolKind.TemplateParam);

				bool isSelfInst = true;
				for (var i = 0; i < instance.Arguments.Count; i++)
				{
					ILTemplateArg instArg = instance.Arguments[i];
					TemplateArgument typeArg = typeName.Arguments[i];

					Symbol paramSym = paramSyms.Find(s =>
					{
						if (s.TemplateIdx == i)
							return true;
						return false;
					});

					if (instArg.ValueFunc == null)
					{
						if (typeArg.Type is TemplateParamSymbolType tparamType)
						{
							if (tparamType.Index != i)
								isSelfInst = false;
						}
						else
						{
							TemplateParamSymbolType paramType = paramSym.Type as TemplateParamSymbolType;
							if (paramType != typeArg.Type && typeArg.AssociatedBaseType == null)
								isSelfInst = false;

							// Check associated type
							if (typeArg.AssociatedBaseType != null)
							{
								if (paramType != typeArg.AssociatedBaseType)
									isSelfInst = false;
							}
						}

					}
					else
					{
						if (paramSym.Type is TemplateParamSymbolType)
							isSelfInst = false;
					}
				}


				if (isSelfInst)
					return instance.Type;
				return templateInstType;
			}
			default:
				return null;
			}
		}

		public static ILOperand GetInstancedOperand(ILOperand operand, ILTemplateInstance instance)
		{
			SymbolType type = GetInstancedType(operand.Type, instance);
			return new ILOperand(operand.Iden, type);
		}

		public static void UpdateDefaultValues(TemplateInstanceName instName, TemplateDefinitionName defName)
		{
			if (instName.Arguments.Count == defName.Parameters.Count)
				return;

			for (int i = instName.Arguments.Count; i < defName.Parameters.Count; i++)
			{
				TemplateParameter param = defName.Parameters[i];
				TemplateArgument argument;

				if (param.ValueName != null)
				{
					argument = new TemplateArgument(i, param.Type, param.ValueDefault);
				}
				else
				{
					argument = new TemplateArgument(i, param.TypeDefault);
				}

				instName.Arguments.Add(argument);
			}
		}
	}
}
