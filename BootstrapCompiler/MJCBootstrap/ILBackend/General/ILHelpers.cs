﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;

namespace MJC.ILBackend.General
{
	public static partial class ILHelpers
	{
		public static string GetTypedIlOpName(ILOpLoc loc, ILOpType type, List<SymbolType> paramTypes, SymbolType retType)
		{
			string iden = GetIlOpName(loc, type) + '(';

			for (var i = 0; i < paramTypes.Count; i++)
			{
				iden += paramTypes[i].ToString();
				if (i + 1 != paramTypes.Count)
					iden += ",";
			}

			iden += "):";
			iden += retType.ToString();

			return iden;
		}

		public static string GetTypedIlOpName(ILOperatorRefInstruction op)
		{
			string iden = GetIlOpName(op.Loc, op.Op) + '(';

			FunctionSymbolType opType = op.Type as FunctionSymbolType;
			for (var i = 0; i < opType.ParamTypes.Count; i++)
			{
				iden += opType.ParamTypes[i].ToString();
				if (i + 1 != opType.ParamTypes.Count)
					iden += ",";
			}

			iden += "):";
			iden += (op.RetOperand.Type as FunctionSymbolType).ReturnType.ToString();

			return iden;
		}

		public static string GetTypedIlOpName(ILOperator op)
		{
			string iden = op.Identifier + '(';

			for (var i = 0; i < op.Operands.Count; i++)
			{
				iden += op.Operands[i].Type.ToString();
				if (i + 1 != op.Operands.Count)
					iden += ",";
			}

			iden += "):";
			iden += op.ReturnType.ToString();

			return iden;
		}
	}
}
