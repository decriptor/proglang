﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.ILBackend.Parse
{
	public enum ILTokenType
	{
		Unknown,

		DotStage,
		DotModule,
		DotImport,
		DotFunc,
		DotOperator,

		Indent,
		EoL,

		LocalValue,
		GlobalValue,

		LiteralVal,
		HexLiteralVal,

		LParen,
		RParen,
		LBrace,
		RBrace,
		LBracket,
		RBracket,
		Dollar,
		Comma,

		Plus,
		PlusPlus,
		PlusEquals,
		Minus,
		MinusMinus,
		MinusEquals,
		Asterisk,
		AsteriskEquals,
		Slash,
		SlashEquals,
		Or,
		OrEquals,
		OrOr,
		And,
		AndEquals,
		AndAnd,
		Caret,
		CaretEquals,
		CaretCaret,
		CaretCaretEquals,
		Exclaim,
		ExclaimEquals,
		ExclaimColon,
		Equals,
		EqualsEquals,
		Less,
		LessEquals,
		LessLess,
		LessLessEquals,
		Greater,
		GreaterEquals,
		GreaterGreater,
		GreaterGreaterEquals,
		Modulo,
		ModuloEquals,
		Tilde,
		TildeEquals,
		Colon,
		Question,
		QuestionQuestion,
		QuestionDot,
		Arrow,

		IntLiteral,
		FloatLiteral,
		StringLiteral,

		AllocStack,

		Assign,
		Store,
		Load,

		OperatorRef,
		Call,
		Builtin,

		PackTuple,
		UnpackTuple,

		ConvType,

		Return,
		Branch,
		CondBranch,

		To,
		In,
		Prefix,
		Infix,
		Postfix,
		Cast,
	}

	class ILToken
	{
		public ILTokenType Type;
		public string Value;

		public ILToken(ILTokenType type, string value = null)
		{
			Type = type;
			Value = value;
		}

		public override string ToString()
		{
			string str = Type.ToString();
			if (Value != null)
				str += " | " + Value;
			return str;
		}
	}
}
