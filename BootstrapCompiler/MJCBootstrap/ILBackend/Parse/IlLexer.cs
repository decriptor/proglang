﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MJC.ILBackend.Parse
{
	class ILLexer
	{

		private Dictionary<string, ILTokenType> _dotTokens = new Dictionary<string, ILTokenType>
		{
			{ "stage"			, ILTokenType.DotStage },
			{ "module"			, ILTokenType.DotModule },
			{ "import"			, ILTokenType.DotImport },
			{ "func"			, ILTokenType.DotFunc },
			{ "operator"		, ILTokenType.DotOperator },
		};

		private Dictionary<string, ILTokenType> _instructions = new Dictionary<string, ILTokenType>
		{
			{ "int_literal"		, ILTokenType.IntLiteral },
			{ "float_literal"	, ILTokenType.FloatLiteral },
			{ "string_literal"	, ILTokenType.StringLiteral },

			{ "alloc_stack"		, ILTokenType.AllocStack },

			{ "assign"			, ILTokenType.Assign },
			{ "store"			, ILTokenType.Store },
			{ "load"			, ILTokenType.Load },

			{ "operator_ref"	, ILTokenType.OperatorRef },
			{ "call"			, ILTokenType.Call },
			{ "builtin"			, ILTokenType.Builtin },

			{ "tuple_pack"      , ILTokenType.PackTuple },
			{ "tuple_unpack"    , ILTokenType.UnpackTuple },

			{"convert_type"		, ILTokenType.ConvType },

			{ "return"			, ILTokenType.Return },
			{ "br"				, ILTokenType.Branch },
			{ "cond_br"			, ILTokenType.CondBranch },

			{ "to"				, ILTokenType.To },
			{ "in"				, ILTokenType.In },
			{ "prefix"			, ILTokenType.Prefix },
			{ "infix"			, ILTokenType.Infix },
			{ "postfix"			, ILTokenType.Postfix },
			{ "cast"			, ILTokenType.Cast },
		};

		public List<ILToken> Lex(string source)
		{
			int offset = 0;
			List<ILToken> tokens = new List<ILToken>();

			while (offset < source.Length)
			{
				switch (source[offset])
				{
				case '\r':
				case '\n':
				{
					if (source[offset] == '\n')
						tokens.Add(new ILToken(ILTokenType.EoL));
					++offset;
					break;
				}
				case ' ':
				{
					if (tokens.Count > 0 && tokens.Last().Type == ILTokenType.EoL)
					{
						tokens.Add(new ILToken(ILTokenType.Indent));
						while (source[offset] == ' ')
						{
							++offset;
						}
					}
					else
					{
						++offset;
					}

					break;
				}
				case '.':
				{
					++offset;
					int end = offset;
					while (Char.IsLetter(source[end]))
					{
						++end;
					}

					string tmp = source.Substring(offset, end - offset);
					if (_dotTokens.ContainsKey(tmp))
						tokens.Add(new ILToken(_dotTokens[tmp]));
					else
						tokens.Add(new ILToken(ILTokenType.Unknown, tmp));

					offset = end;
					break;
				}
				case '\t':
				{
					tokens.Add(new ILToken(ILTokenType.Indent));
					++offset;
					break;
				}
				case '(':
				{
					tokens.Add(new ILToken(ILTokenType.LParen));
					++offset;
					break;
				}
				case ')':
				{
					tokens.Add(new ILToken(ILTokenType.RParen));
					++offset;
					break;
				}
				case '{':
				{
					tokens.Add(new ILToken(ILTokenType.LBrace));
					++offset;
					break;
				}
				case '}':
				{
					tokens.Add(new ILToken(ILTokenType.RBrace));
					++offset;
					break;
				}
				case '[':
				{
					tokens.Add(new ILToken(ILTokenType.LBracket));
					++offset;
					break;
				}
				case ']':
				{
					tokens.Add(new ILToken(ILTokenType.RBracket));
					++offset;
					break;
				}
				case '$':
				{
					tokens.Add(new ILToken(ILTokenType.Dollar));
					++offset;
					break;
				}
				case ':':
				{
					tokens.Add(new ILToken(ILTokenType.Colon));
					++offset;
					break;
				}
				case ',':
				{
					tokens.Add(new ILToken(ILTokenType.Comma));
					++offset;
					break;
				}
				case '+':
				{
					if (source[offset + 1] == '+')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.PlusPlus, "++"));
					}
					else if (source[offset + 1] == '=')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.PlusEquals, "+="));
					}
					else
					{
						tokens.Add(new ILToken(ILTokenType.Plus, "+"));
					}

					++offset;
					break;
				}
				case '-':
				{
					if (source[offset + 1] == '-')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.MinusMinus, "--"));
					}
					else if(source[offset + 1] == '=')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.MinusEquals, "-="));
					}
					else if (source[offset + 1] == '>')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.Arrow, "->"));
					}
					else
					{
						tokens.Add(new ILToken(ILTokenType.Minus, "-"));
					}

					++offset;
					break;
				}
				case '/':
				{
					if (source[offset + 1] == '/')
					{
						offset += 2;
						while (source[offset] != '\r' && source[offset] != '\n')
						{
							++offset;
						}

						if (source[offset] != '\n')
							++offset;
					}
					else if (source[offset + 1] == '=')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.SlashEquals, "/="));
					}
					else
					{
						tokens.Add(new ILToken(ILTokenType.Slash, "/"));
					}

					++offset;
					break;
				}
				case '*':
				{
					if (source[offset + 1] == '=')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.AsteriskEquals, "*="));
					}
					else
					{
						tokens.Add(new ILToken(ILTokenType.Asterisk, "*"));
					}

					++offset;
					break;
				}
				case '|':
				{
					if (source[offset + 1] == '=')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.OrEquals, "|="));
					}
					else if (source[offset + 1] == '|')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.OrOr, "||"));
					}
					else
					{
						tokens.Add(new ILToken(ILTokenType.Or, "|"));
					}

					++offset;
					break;
				}
				case '&':
				{
					if (source[offset + 1] == '=')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.AndEquals, "&="));
					}
					else if (source[offset + 1] == '&')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.AndAnd, "&&"));
					}
					else
					{
						tokens.Add(new ILToken(ILTokenType.And, "&"));
					}

					++offset;
					break;
				}
				case '^':
				{
					if (source[offset + 1] == '=')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.CaretEquals, "^="));
					}
					else if (source[offset + 1] == '&')
					{
						if (source[offset + 2] == '=')
						{
							tokens.Add(new ILToken(ILTokenType.CaretCaretEquals, "^^="));
							++offset;
						}
						else
						{
							tokens.Add(new ILToken(ILTokenType.CaretCaret, "^^"));
						}
						++offset;
					}
					else
					{
						tokens.Add(new ILToken(ILTokenType.Caret, "^"));
					}

					++offset;
					break;
				}
				case '!':
				{
					if (source[offset + 1] == '=')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.ExclaimEquals, "!="));
					}
					else if (source[offset + 1] == '.')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.ExclaimColon, "!."));
					}
					else
					{
						tokens.Add(new ILToken(ILTokenType.Exclaim, "!"));
					}

					++offset;
					break;
				}
				case '=':
				{
					if (source[offset + 1] == '=')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.EqualsEquals, "=="));
					}
					else
					{
						tokens.Add(new ILToken(ILTokenType.Equals, "="));
					}

					++offset;
					break;
				}
				case '<':
				{
					if (source[offset + 1] == '=')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.LessEquals, "<="));
					}
					else if (source[offset + 1] == '<')
					{
						if (source[offset + 2] == '=')
						{
							tokens.Add(new ILToken(ILTokenType.LessLessEquals, "<<="));
							++offset;
						}
						else
						{
							tokens.Add(new ILToken(ILTokenType.LessLess, "<<"));
						}
						++offset;
					}
					else
					{
						tokens.Add(new ILToken(ILTokenType.Less, "<"));
					}

					++offset;
					break;
				}
				case '>':
				{
					if (source[offset + 1] == '=')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.GreaterEquals, ">="));
					}
					else if (source[offset + 1] == '>')
					{
						if (source[offset + 2] == '=')
						{
							tokens.Add(new ILToken(ILTokenType.GreaterGreaterEquals, ">>="));
							++offset;
						}
						else
						{
							tokens.Add(new ILToken(ILTokenType.GreaterGreater, ">>"));
						}
						++offset;
					}
					else
					{
						tokens.Add(new ILToken(ILTokenType.Greater, ">"));
					}

					++offset;
					break;
				}
				case '~':
				{
					if (source[offset + 1] == '=')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.TildeEquals, "~="));
					}
					else
					{
						tokens.Add(new ILToken(ILTokenType.Tilde, "~"));
					}

					++offset;
					break;
				}
				case '?':
				{
					if (source[offset + 1] == '?')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.QuestionQuestion, "??"));
					}
					else if (source[offset + 1] == '.')
					{
						++offset;
						tokens.Add(new ILToken(ILTokenType.QuestionDot, "?."));
					}
					else
					{
						tokens.Add(new ILToken(ILTokenType.Question, "?"));
					}

					++offset;
					break;
				}
				case '%':
				{
					if (source[offset + 1] == '=')
					{
						offset += 2;
						tokens.Add(new ILToken(ILTokenType.ModuloEquals, "%="));
					}
					else if (source[offset + 1] == ' ')
					{
						offset += 2;
						tokens.Add(new ILToken(ILTokenType.Modulo, "%"));
					}
					else
					{
						++offset;
						int end = offset;
						while (!Char.IsWhiteSpace(source[end]))
						{
							++end;
						}

						string tmp = source.Substring(offset, end - offset);
						tokens.Add(new ILToken(ILTokenType.LocalValue, tmp));

						offset = end;
					}

					break;
				}
				case '@':
				{
					++offset;
					int end = offset;
					while (!Char.IsWhiteSpace(source[end]))
					{
						++end;
					}

					string tmp = source.Substring(offset, end - offset);
					tokens.Add(new ILToken(ILTokenType.GlobalValue, tmp));

					offset = end;
					break;
				}
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				{
					ILTokenType type = ILTokenType.LiteralVal;
					if (source[offset] == '0' && source[offset + 1] == 'x')
					{
						type = ILTokenType.HexLiteralVal;
						offset += 2;
					}

					int end = offset;
					while (Char.IsDigit(source[end]) || (type == ILTokenType.HexLiteralVal && "abcdef".IndexOf(Char.ToLower(source[end])) != -1))
					{
						++end;
					}

					string tmp = source.Substring(offset, end - offset);
					tokens.Add(new ILToken(type, tmp));

					offset = end;
					break;
				}
				case '"':
				{
					int end = source.IndexOf('"', offset + 1);
					while (end != -1 && source[end - 1] == '\\')
					{
						end = source.IndexOf('"', offset + 1);
					}

					if (end == -1)
						return tokens;

					string text = source.Substring(offset, end - offset + 1);
					tokens.Add(new ILToken(ILTokenType.StringLiteral, text));

					offset = end + 1;
					break;
				}
				default:
				{
					int end = offset;
					string specialChars = "_.+-*/%|&!=<>[]";

					while (Char.IsLetterOrDigit(source[end]) || specialChars.IndexOf(source[end]) != -1)
					{
						++end;
					}

					string tmp = source.Substring(offset, end - offset);
					
					if (_instructions.ContainsKey(tmp))
						tokens.Add(new ILToken(_instructions[tmp]));
					else
						tokens.Add(new ILToken(ILTokenType.Unknown, tmp));

					offset = end;
					break;
				}
				}
			}

			return tokens;
		}

	}
}
