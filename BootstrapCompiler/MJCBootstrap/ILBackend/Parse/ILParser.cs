﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;
using MJC.SyntaxTree;

// TODO: Not all instructions, etc are implemented

namespace MJC.ILBackend.Parse
{
	// No error checking, since IL should be error free
	class ILParser
	{
		private List<ILToken> _tokens;
		private int _index;

		private Dictionary<string, ILOperand> _localOperands = new Dictionary<string, ILOperand>();
		private Dictionary<string, ILOperand> _globalOperands = new Dictionary<string, ILOperand>();

		public ILCompUnit Parse(List<ILToken> tokens)
		{
			_tokens = tokens;
			_index = 0;

			ILCompUnit compUnit = new ILCompUnit();
			
			while (_index < tokens.Count)
			{
				switch (tokens[_index].Type)
				{
				case ILTokenType.DotStage:
				{
					compUnit.Stage = ParseStage();
					break;
				}
				case ILTokenType.DotModule:
				{
					NextToken();
					compUnit.ILModule = PeekToken().Value;
					EatToken(ILTokenType.Unknown);
					EatToken(ILTokenType.EoL);
					break;
				}
				case ILTokenType.DotImport:
				{
					NextToken();
					compUnit.ImportNames.Add(PeekToken().Value);
					EatToken(ILTokenType.Unknown);
					EatToken(ILTokenType.EoL);
					break;
				}
				case ILTokenType.DotFunc:
				{
					ILFunction func = ParseFunction();
					compUnit.AddBlock(func);
					break;
				}
				case ILTokenType.DotOperator:
				{
					ILOperator op = ParseOperator();
					compUnit.AddBlock(op);
					break;
				}
				default:
				{
					++_index;
					break;
				}
				}

				_localOperands.Clear();
			}


			return compUnit;
		}

		ILStage ParseStage()
		{
			EatToken(ILTokenType.DotStage);

			ILStage stage = ILStage.Invalid;
			switch (PeekToken().Value)
			{
			case "raw": stage = ILStage.Raw; break;
			case "canonical": stage = ILStage.Canonical; break;
			case "comp_exec": stage = ILStage.CompExec; break;
			case "opt": stage = ILStage.Opt; break;
			}

			NextToken();
			EatToken(ILTokenType.EoL);

			return stage;
		}

		ILFunction ParseFunction()
		{

			EatToken(ILTokenType.DotFunc);

			ILAttributes attribs = ParseAttributes();

			string identifier = PeekToken().Value;
			EatToken(ILTokenType.GlobalValue);

			EatToken(ILTokenType.LParen);

			List<ILOperand> operands = null;
			if (PeekToken().Type != ILTokenType.RParen)
			{
				operands = new List<ILOperand>();
				bool comma = true;
				while (comma)
				{
					string iden = PeekToken().Value;
					EatToken(ILTokenType.LocalValue);

					EatToken(ILTokenType.Colon);

					SymbolType type = ParseType();
					ILOperand operand = new ILOperand(iden, type);
					operands.Add(operand);
					_localOperands.Add(iden, operand);

					comma = PeekToken().Type == ILTokenType.Comma;
					if (comma)
						NextToken();
				}
			}
			EatToken(ILTokenType.RParen);

			EatToken(ILTokenType.Arrow);

			SymbolType returnType = ParseType();

			EatToken(ILTokenType.LBrace);
			EatToken(ILTokenType.EoL);

			ILFunction func = new ILFunction(identifier, operands, returnType, attribs, null, false);

			while (PeekToken().Type != ILTokenType.RBrace)
			{
				ILBasicBlock block = ParseBasicBlock();
				func.Blocks.Add(block);
			}
			NextToken();
			EatToken(ILTokenType.EoL);

			return func;
		}

		ILOperator ParseOperator()
		{
			EatToken(ILTokenType.DotOperator);

			ILAttributes attributes = ParseAttributes();

			string identifier = PeekToken().Value;
			EatToken(ILTokenType.GlobalValue);

			EatToken(ILTokenType.LParen);

			List<ILOperand> operands = new List<ILOperand>();
			if (PeekToken().Type != ILTokenType.RParen)
			{
				bool comma = true;
				while (comma)
				{
					string iden = PeekToken().Value;
					EatToken(ILTokenType.LocalValue);

					EatToken(ILTokenType.Colon);

					SymbolType type = ParseType();
					ILOperand operand = new ILOperand(iden, type);
					operands.Add(operand);
					_localOperands.Add(iden, operand);

					comma = PeekToken().Type == ILTokenType.Comma;
					if (comma)
						NextToken();
				}
			}
			EatToken(ILTokenType.RParen);

			SymbolType returnType = null;
			if (PeekToken().Type == ILTokenType.Arrow)
			{
				EatToken(ILTokenType.Arrow);

				returnType = ParseType();
			}

			EatToken(ILTokenType.LBrace);
			EatToken(ILTokenType.EoL);

			ILOperator op = new ILOperator(identifier, operands, returnType, attributes);

			while (PeekToken().Type != ILTokenType.RBrace)
			{
				ILBasicBlock block = ParseBasicBlock();
				op.Blocks.Add(block);
			}
			NextToken();
			EatToken(ILTokenType.EoL);

			return op;
		}

		ILBasicBlock ParseBasicBlock()
		{
			string label = PeekToken().Value;
			ILBasicBlock block = new ILBasicBlock(label);
			EatToken(ILTokenType.Unknown);
			EatToken(ILTokenType.Colon);
			do
			{
				EatToken(ILTokenType.EoL);
			} 
			while (PeekToken().Type == ILTokenType.EoL);

			while (PeekToken().Type == ILTokenType.Indent)
			{
				ILInstruction instruction = ParseInstruction();
				block.Instructions.Add(instruction);

				while (PeekToken().Type == ILTokenType.EoL)
					EatToken(ILTokenType.EoL);
			}

			return block;
		}

		ILInstruction ParseInstruction()
		{
			EatToken(ILTokenType.Indent);

			string localValue = null;
			List<string> localValues = null;
			
			while (PeekToken().Type != ILTokenType.EoL)
			{
				switch (PeekToken().Type)
				{
				case ILTokenType.LocalValue:
				{
					string tmp = PeekToken().Value;
					EatToken(ILTokenType.LocalValue);
					EatToken(ILTokenType.Equals);

					if (localValue == null)
					{
						localValue = tmp;
					}
					else
					{
						if (localValues == null)
							localValues = new List<string>();

						localValues.Add(localValue);
						localValues.Add(tmp);
					}

					break;
				}
				case ILTokenType.IntLiteral:
				case ILTokenType.FloatLiteral:
				case ILTokenType.StringLiteral:
				{
					ILInstruction tmp = ParseLiteralInstruction(localValue);
					EatToken(ILTokenType.EoL);
					return tmp;
				}

				case ILTokenType.AllocStack:
					return ParseAllocStackInstruction(localValue);

				case ILTokenType.Assign:
					return ParseAssignInstruction();
				case ILTokenType.Store:
					return ParseStoreInstruction();
				case ILTokenType.Load:
					return ParseLoadInstruction(localValue);

				case ILTokenType.OperatorRef:
					return ParseOperatorRefInstruction(localValue);

				case ILTokenType.Call:
					return ParseCallInstruction(localValue);
				case ILTokenType.Builtin:
					return ParseBuiltinInstruction(localValue);

				case ILTokenType.PackTuple:
					return ParsePackTupleInstruction(localValue);
				case ILTokenType.UnpackTuple:
				{
					if (localValues == null)
						localValues = new List<string>{ localValue };
					return ParseUnpackTupleInstruction(localValues);
				}

				case ILTokenType.ConvType:
					return ParseConvertType(localValue);

				case ILTokenType.Return:
					return ParseReturnInstruction();
				case ILTokenType.Branch:
					return ParseBranchInstruction();
				case ILTokenType.CondBranch:
					return ParseCondBranchInstruction();

				default:
				{
					NextToken();
					break;
				}
				}
			}

			return null;
		}

		ILInstruction ParseLiteralInstruction(string variable)
		{
			switch (PeekToken().Type)
			{
			case ILTokenType.IntLiteral:
			{
				NextToken();
				long val = long.MaxValue;
				if (PeekToken().Type == ILTokenType.LiteralVal)
				{
					val = long.Parse(PeekToken().Value);
				}
				EatToken(ILTokenType.LiteralVal);
				EatToken(ILTokenType.Colon);

				SymbolType type = ParseType();

				ILOperand operand = new ILOperand(variable, type);
				_localOperands.Add(variable, operand);
				return new ILIntLiteralInstruction(operand, val, null);
			}
			case ILTokenType.FloatLiteral:
			{
				NextToken();
				long val = long.MaxValue;
				if (PeekToken().Type == ILTokenType.HexLiteralVal)
				{
					string tmp = PeekToken().Value;
					val = long.Parse(tmp, NumberStyles.HexNumber);
				}
				EatToken(ILTokenType.LiteralVal);
				EatToken(ILTokenType.Colon);

				SymbolType type = ParseType();

				ILOperand operand = new ILOperand(variable, type);
				_localOperands.Add(variable, operand);
				return new ILFloatLiteralInstruction(operand, val, null);
			}
			case ILTokenType.StringLiteral:
			{
				NextToken();
				string val = null;
				if (PeekToken().Type == ILTokenType.HexLiteralVal)
				{
					val = PeekToken().Value.Trim('"');
				}
				EatToken(ILTokenType.LiteralVal);

				ILOperand operand = new ILOperand(variable, SymbolType.BuiltinType(BuiltinTypes.String));
				_localOperands.Add(variable, operand);
				return new ILStringLiteralInstruction(operand, val, null);
			}
			default: return null;
			}
		}

		ILAllocStackInstruction ParseAllocStackInstruction(string retVar)
		{
			EatToken(ILTokenType.AllocStack);
			SymbolType type = ParseType();
			
			EatToken(ILTokenType.EoL);

			ILOperand retOperand = new ILOperand(retVar, SymbolType.MemoryLocType(type));
			_localOperands.Add(retVar, retOperand);

			return new ILAllocStackInstruction(retOperand, type, null);
		}

		ILAssignInstruction ParseAssignInstruction()
		{
			EatToken(ILTokenType.Assign);
			ILOperand src = ParseOperandFromIden();
			EatToken(ILTokenType.To);
			ILOperand dst = ParseOperand();
			EatToken(ILTokenType.EoL);

			return new ILAssignInstruction(src, dst, null);
		}

		ILStoreInstruction ParseStoreInstruction()
		{
			EatToken(ILTokenType.Store);
			ILOperand src = ParseOperandFromIden();
			EatToken(ILTokenType.To);
			ILOperand dst = ParseOperand();
			EatToken(ILTokenType.EoL);

			return new ILStoreInstruction(src, dst, null);
		}

		ILLoadInstruction ParseLoadInstruction(string retVar)
		{
			EatToken(ILTokenType.Load);
			ILOperand operand = ParseOperand();
			EatToken(ILTokenType.EoL);

			SymbolType retType = (operand.Type as MemoryLocSymbolType)?.BaseType;
			ILOperand retOperand = new ILOperand(retVar, retType);
			_localOperands.Add(retVar, retOperand);

			return new ILLoadInstruction(retOperand, operand, null);
		}

		ILOperatorRefInstruction ParseOperatorRefInstruction(string retVar)
		{
			EatToken(ILTokenType.OperatorRef);
			string locStr = PeekToken().Type.ToString().ToLower();
			ILOpLoc opLoc = ILHelpers.GetILOpLoc(locStr);
			EatToken(ILTokenType.Unknown);
			string opStr = PeekToken().Value;
			ILOpType op = ILHelpers.GetILOpType(opStr);
			NextToken();
			SymbolType retType = ParseType();
			
			ILOperand retOperand = new ILOperand(retVar, retType);
			_localOperands.Add(retVar, retOperand);

			return new ILOperatorRefInstruction(retOperand, opLoc, op, retType as FunctionSymbolType, null);
		}

		ILCallInstruction ParseCallInstruction(string retVar)
		{
			EatToken(ILTokenType.Call);
			string func = PeekToken().Value;
			EatToken(ILTokenType.GlobalValue);

			EatToken(ILTokenType.LParen);

			List<ILOperand> parameters = null;
			if (PeekToken().Type != ILTokenType.RParen)
			{
				parameters = ParseOperandList();
			}

			EatToken(ILTokenType.RParen);

			SymbolType returnType = null;
			if (PeekToken().Type == ILTokenType.Colon)
			{
				EatToken(ILTokenType.Colon);

				returnType = ParseType();
			}

			ILOperand retOperand = null;
			if (retVar != null)
			{
				retOperand = new ILOperand(retVar, returnType);
				_localOperands.Add(retVar, retOperand);
			}

			return new ILCallInstruction(retOperand, _localOperands[func], parameters, returnType, null);
		}

		ILBuiltinInstruction ParseBuiltinInstruction(string retVar)
		{
			EatToken(ILTokenType.Call);
			string builtin = PeekToken().Value.Trim('"');
			EatToken(ILTokenType.StringLiteral);

			EatToken(ILTokenType.LParen);

			List<ILOperand> parameters = null;
			if (PeekToken().Type != ILTokenType.RParen)
			{
				parameters = ParseOperandList();
			}

			EatToken(ILTokenType.RParen);

			SymbolType returnType = null;
			ILOperand retOperand = null;
			if (PeekToken().Type == ILTokenType.Colon)
			{
				EatToken(ILTokenType.Colon);
				returnType = ParseType();

				retOperand = new ILOperand(retVar, returnType);
				_localOperands.Add(retVar, retOperand);
			}

			return new ILBuiltinInstruction(retOperand, builtin, parameters, returnType, null);
		}

		ILTuplePackInstruction ParsePackTupleInstruction(string retVar)
		{
			EatToken(ILTokenType.PackTuple);

			List<ILOperand> parameters = ParseOperandList();

			EatToken(ILTokenType.EoL);

			List<SymbolType> subTypes = new List<SymbolType>();
			foreach (ILOperand operand in parameters)
			{
				subTypes.Add(operand.Type);
			}

			ILOperand retOperand = new ILOperand(retVar, SymbolType.TupleType(subTypes));
			return new ILTuplePackInstruction(retOperand, parameters, null);
		}

		ILTupleUnpackInstruction ParseUnpackTupleInstruction(List<string> retVars)
		{
			EatToken(ILTokenType.UnpackTuple);

			ILOperand operand = ParseOperand();

			List<ILOperand> retOperands = new List<ILOperand>();
			for (var i = 0; i < retVars.Count; i++)
			{
				string retVar = retVars[i];
				SymbolType type = (operand.Type as TupleSymbolType).SubTypes[i];

				ILOperand retOperand = new ILOperand(retVar, type);
				retOperands.Add(retOperand);
				_localOperands.Add(retVar, retOperand);
			}

			return new ILTupleUnpackInstruction(retOperands, operand, null);
		}

		ILConvertTypeInstruction ParseConvertType(string retVar)
		{
			EatToken(ILTokenType.ConvType);
			ILOperand operand = ParseOperand();
			EatToken(ILTokenType.To);
			SymbolType type = ParseType();
			ILOperand retOperand = new ILOperand(retVar, type);
			EatToken(ILTokenType.EoL);

			_localOperands.Add(retVar, retOperand);

			return new ILConvertTypeInstruction(retOperand, operand, type, null);
		}

		ILReturnInstruction ParseReturnInstruction()
		{
			EatToken(ILTokenType.Return);
			ILOperand operand = ParseOperand();
			EatToken(ILTokenType.EoL);

			return new ILReturnInstruction(operand, null);
		}

		ILBranchInstruction ParseBranchInstruction()
		{
			EatToken(ILTokenType.Branch);
			string label = PeekToken().Value;
			EatToken(ILTokenType.Unknown);
			EatToken(ILTokenType.EoL);

			return new ILBranchInstruction(label, null);
		}

		ILCondBranchInstruction ParseCondBranchInstruction()
		{
			EatToken(ILTokenType.CondBranch);
			ILOperand condition = ParseOperand();

			EatToken(ILTokenType.Comma);

			string trueLabel = PeekToken().Value;
			EatToken(ILTokenType.Unknown);

			EatToken(ILTokenType.Comma);

			string falseLabel = PeekToken().Value;
			EatToken(ILTokenType.Unknown);

			EatToken(ILTokenType.EoL);

			return new ILCondBranchInstruction(condition, trueLabel, falseLabel, null);
		}

		ILOperand ParseOperand()
		{
			ILToken tok = PeekToken();
			ILOperand operand = null;
			if (tok.Type == ILTokenType.LocalValue)
			{
				operand = _localOperands[tok.Value];
				NextToken();
			}
			else if (tok.Type == ILTokenType.GlobalValue)
			{
				operand = _globalOperands[tok.Value];
				NextToken();
			}
			EatToken(ILTokenType.Colon);
			SymbolType type = ParseType();

			// TODO: Check type

			return operand;
		}

		ILOperand ParseOperandFromIden()
		{
			ILToken tok = PeekToken();
			ILOperand operand = null;
			if (tok.Type == ILTokenType.LocalValue)
			{
				operand = _localOperands[tok.Value];
				NextToken();
			}
			else if (tok.Type == ILTokenType.GlobalValue)
			{
				operand = _globalOperands[tok.Value];
				NextToken();
			}
			return operand;
		}

		List<ILOperand> ParseOperandList()
		{
			List<ILOperand> operands = new List<ILOperand>();

			bool comma = true;
			while (comma)
			{
				operands.Add(ParseOperand());

				comma = PeekToken().Type == ILTokenType.Comma;
				if (comma)
					NextToken();
			}

			return operands;
		}

		SymbolType ParseType()
		{
			EatToken(ILTokenType.Dollar);
			return ParseSimpleType();
		}

		SymbolType ParseSimpleType()
		{
			ILToken tok = PeekToken();
			SymbolType type = null;
			switch (tok.Type)
			{
			case ILTokenType.Unknown:
				switch (tok.Value)
				{
				case "i1":		type =  SymbolType.BuiltinType(BuiltinTypes.Bool); break;
				case "i8":		type =  SymbolType.BuiltinType(BuiltinTypes.I8); break;
				case "i16":		type =  SymbolType.BuiltinType(BuiltinTypes.I16); break;
				case "i32":		type =  SymbolType.BuiltinType(BuiltinTypes.I32); break;
				case "i64":		type =  SymbolType.BuiltinType(BuiltinTypes.I64); break;
				case "u8":		type =  SymbolType.BuiltinType(BuiltinTypes.U8); break;
				case "u16":		type =  SymbolType.BuiltinType(BuiltinTypes.U16); break;
				case "u32":		type =  SymbolType.BuiltinType(BuiltinTypes.U32); break;
				case "u64":		type =  SymbolType.BuiltinType(BuiltinTypes.U64); break;
				case "f32":		type =  SymbolType.BuiltinType(BuiltinTypes.F32); break;
				case "f64":		type =  SymbolType.BuiltinType(BuiltinTypes.F64); break;
				case "char":	type =  SymbolType.BuiltinType(BuiltinTypes.Char); break;
				case "wchar":	type =  SymbolType.BuiltinType(BuiltinTypes.WChar); break;
				case "rune":	type =  SymbolType.BuiltinType(BuiltinTypes.Rune); break;
				case "string":	type =  SymbolType.BuiltinType(BuiltinTypes.String); break;
				}

				NextToken();
				break;
			case ILTokenType.LocalValue:
			case ILTokenType.GlobalValue:
				type = SymbolType.UnknownType(new Scope(), new IdentifierName(tok.Value));
				NextToken();
				break;
			case ILTokenType.LParen:
			{
				EatToken(ILTokenType.LParen);

				if (PeekToken().Type == ILTokenType.RParen)
				{
					NextToken();
					return null;
				}

				bool comma = true;
				List<SymbolType> subTypes = new List<SymbolType>();
				while (comma)
				{
					subTypes.Add(ParseSimpleType());
					comma = PeekToken().Type == ILTokenType.Comma;
					if (comma)
						EatToken(ILTokenType.Comma);
				}

				EatToken(ILTokenType.RParen);

				SymbolType receiverType = null;
				if (PeekToken().Type == ILTokenType.LParen)
				{
					receiverType = subTypes[0];
					subTypes.Clear();

					comma = true;
					while (comma)
					{
						subTypes.Add(ParseSimpleType());
						comma = PeekToken().Type == ILTokenType.Comma;
						if (comma)
							EatToken(ILTokenType.Comma);
					}
				}

				if (PeekToken().Type == ILTokenType.Arrow)
				{
					EatToken(ILTokenType.Arrow);
					EatToken(ILTokenType.LParen);
					SymbolType returnType = null;
					if (PeekToken().Type != ILTokenType.RParen)
						returnType = ParseSimpleType();
					EatToken(ILTokenType.RParen);

					type = SymbolType.FunctionType(receiverType, subTypes, returnType);
				}
				else
				{
					type = SymbolType.TupleType(subTypes);
				}

				break;
			}
			case ILTokenType.LBracket:
			{
				NextToken();
				ulong size = ulong.MaxValue;
				if (PeekToken().Type == ILTokenType.IntLiteral)
				{
					size = ulong.Parse(EatToken(ILTokenType.IntLiteral).Value);
				}
				EatToken(ILTokenType.RBracket);
				type = ParseSimpleType();
				type = SymbolType.ArrayType(type, size);
				break;
			}
			case ILTokenType.Asterisk:
			{
				NextToken();
				type = ParseSimpleType();
				type = SymbolType.PointerType(type);
				break;
			}
			case ILTokenType.Question:
			{
				NextToken();
				type = ParseSimpleType();
				type = SymbolType.NullableType(type);
				break;
			}
			case ILTokenType.Caret:
			{
				NextToken();
				type = ParseSimpleType();
				type = SymbolType.MemoryLocType(type);
				break;
			}
			}

			return type;
		}

		ILAttributes ParseAttributes()
		{
			ILAttributes attributes = new ILAttributes();

			while (PeekToken().Type == ILTokenType.Unknown)
			{
				switch (PeekToken().Value)
				{
				case "noinline":
					attributes.Inline = ILInline.Never;
					break;
				case "inline":
					attributes.Inline = ILInline.Prefered;
					break;
				case "alwaysinline":
					attributes.Inline = ILInline.Always;
					break;
				}

				NextToken();
			}

			return attributes;
		}

		ILToken EatToken(ILTokenType type)
		{
			++_index;
			if (_index < _tokens.Count)
				return _tokens[_index];
			return null;
		}

		ILToken PeekToken()
		{
			if (_index < _tokens.Count)
				return _tokens[_index];
			return null;
		}

		ILToken NextToken()
		{
			++_index;
			if (_index < _tokens.Count)
				return _tokens[_index];
			return null;
		}

	}
}
