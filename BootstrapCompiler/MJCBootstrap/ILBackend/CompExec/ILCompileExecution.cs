﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.ILBackend.Interpret;

namespace MJC.ILBackend.CompExec
{
	public class ILCompileExecution
	{

		private ILInterpreter _interpreter = new ILInterpreter(ILInterpretType.Compile);

		public void Process(ILCompUnit compUnit)
		{
			_interpreter.Interpret(compUnit);

			List<ILCompileInterpResult> compResults = _interpreter.CompileResults;
		}

	}
}
