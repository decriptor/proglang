﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.ILBackend.Opt
{
	public abstract class IILOptimizePass : ILPass
	{
		public abstract void Setup(List<ILCompUnit> refCompUnits, ILCompUnit builtins);
	}
}
