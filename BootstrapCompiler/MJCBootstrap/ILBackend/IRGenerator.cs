﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MJC.General;
using MJC.ILBackend.General;
using MJC.IRBackend;
using MJC.IRBackend.General;

namespace MJC.ILBackend
{
	public class IRGenerator
	{
		private IRBuilder _builder = new IRBuilder();
		
		private int _stringCounter = 0;

		private Dictionary<string, IROperand> _localMapping = new Dictionary<string, IROperand>();

		private Dictionary<string, IRTypeDef> _typeDefinitions = new Dictionary<string, IRTypeDef>();
		private Dictionary<string, IRTypeDef> _irTuples = new Dictionary<string, IRTypeDef>();
		private Dictionary<string, IRType> _ilSimpleEnums = new Dictionary<string, IRType>();
		private Dictionary<string, IRType> _ilFuncPtrs = new Dictionary<string, IRType>();

		private Dictionary<string, string> _funcIdens = new Dictionary<string, string>();

		private Dictionary<string, ILAggregate> _ilAggregates = new Dictionary<string, ILAggregate>();
		private Dictionary<string, ILDelegate> _ilDelegates = new Dictionary<string, ILDelegate>();
		private Dictionary<string, VTable> _ilVTables = new Dictionary<string, VTable>();
		private Dictionary<string, ILTypedef> _ilTypedefs = new Dictionary<string, ILTypedef>();

		public IRModule GenerateIR(ILCompUnit compUnit, string irPath)
		{
			string name = Path.GetFileNameWithoutExtension(irPath);
			_builder.Begin(name);

			GenTypes(compUnit);

			foreach (KeyValuePair<string, VTable> pair in compUnit.VTablesMangled)
			{
				VTable vTable = pair.Value;
				_ilVTables.Add(vTable.MangledIden, vTable);
			}

			foreach (ILTypedef typedef in compUnit.Typedefs)
			{
				_ilTypedefs.Add(typedef.Identifier, typedef);
			}

			foreach (KeyValuePair<string, ILGlobal> pair in compUnit.Globals)
			{
				GenGlobal(pair.Value);
			}

			// External globals
			foreach (KeyValuePair<string, ILGlobal> pair in compUnit.Context.ExternalGlobals)
			{
				GenGlobal(pair.Value);
			}

			// External functions
			foreach (KeyValuePair<string, ILFunction> pair in compUnit.Context.ExternalFunctions)
			{
				GenFunction(pair.Value);
			}

			foreach (KeyValuePair<string, ILBlock> pair in compUnit.Blocks)
			{
				if (pair.Value is ILFunction function)
					GenFunction(function);
			}

			foreach (KeyValuePair<string, IRTypeDef> pair in _irTuples)
			{
				_builder.CreateType(pair.Key, pair.Value);
			}

			IRModule module = _builder.End();
			IRWriter.Write(module, irPath);

			return module;
		}

		void GenGlobal(ILGlobal ilGlobal)
		{
			IRType type = GenIRTypeFromType(ilGlobal.Type);
			IRAttributes attribs = GenIRAttribs(ilGlobal.Attributes);


			if (ilGlobal.Type is BuiltinSymbolType builtin &&
			    (builtin.Builtin == BuiltinTypes.String ||
			     builtin.Builtin == BuiltinTypes.StringLiteral))
			{
				string value = ilGlobal.Value.Trim('"');

				// TODO: Use correct char type
				type = new IRType(new IRType(IRBaseType.U8), (ulong)(value.Length + 1));

				_builder.BuildGlobal(ilGlobal.MangledIdentifier, type, attribs, value);
			}
			else if (ilGlobal.Value != null)
			{
				long value;
				if (ilGlobal.Value.StartsWith("-"))
				{
					value = long.Parse(ilGlobal.Value);
				}
				else
				{
					if (ilGlobal.Value.StartsWith("0x"))
					{
						string hex = ilGlobal.Value.Substring(2);
						value = (long) Convert.ToUInt64(hex, 16);
					}
					else
					{
						value = (long)ulong.Parse(ilGlobal.Value);
					}

				}

				_builder.BuildGlobal(ilGlobal.MangledIdentifier, type, attribs, value);
			}
			else
			{
				_builder.BuildGlobal(ilGlobal.MangledIdentifier, type, attribs);
			}
			
		}

		void GenFunction(ILFunction function)
		{
			// Skip un-instantiated templates
			if (function.IsUninstantiated)
				return;

			IRType retType = GenIRTypeFromType(function.ReturnType);

			List<IROperand> operands = null;

			if (function.Operands != null)
			{
				operands = new List<IROperand>();
				foreach (ILOperand operand in function.Operands)
				{
					IRType type = GenIRTypeFromType(operand.Type);
					IROperand op = new IROperand(operand.Iden, type);
					operands.Add(op);
					_localMapping.Add(op.Iden, op);
				}
			}

			IRAttributes attribs = function.Attributes != null ? GenIRAttribs(function.Attributes) : null;
			_builder.BeginFunction(function.Identifier, retType, operands, attribs);

			foreach (ILBasicBlock block in function.Blocks)
			{
				GenBasicBlock(block);
			}

			_builder.EndFunction();

			_localMapping.Clear();
			_funcIdens.Clear();
		}

		void GenBasicBlock(ILBasicBlock basicBlock)
		{
			_builder.CreateBasicBlock(basicBlock.Label);

			foreach (ILInstruction instruction in basicBlock.Instructions)
			{
				switch (instruction.InstructionType)
				{
				case ILInstructionType.IntLiteral:
				{
					ILIntLiteralInstruction tmp = instruction as ILIntLiteralInstruction;
					if (tmp == null) break;

					IROperand operand = new IROperand(tmp.Value, GenIRTypeFromType(tmp.RetOperand.Type));
					_localMapping.Add(tmp.RetOperand.Iden, operand);
					break;
				}
				case ILInstructionType.FloatLiteral:
				{
					ILFloatLiteralInstruction tmp = instruction as ILFloatLiteralInstruction;
					if (tmp == null) break;

					IROperand operand = new IROperand(tmp.HexValue, GenIRTypeFromType(tmp.RetOperand.Type));
					_localMapping.Add(tmp.RetOperand.Iden, operand);
					break;
				}
				case ILInstructionType.StringLiteral:
				{
					ILStringLiteralInstruction tmp = instruction as ILStringLiteralInstruction;
					if (tmp == null) break;

					// TODO: Figure out whether to use 'char', 'wchar' or 'rune'
					// TODO: Figure out better string naming scheme
					IRType type = new IRType(new IRType(IRBaseType.U8), (ulong)tmp.String.Length + 1);
					IRAttributes attribs = new IRAttributes();
					attribs.GlobalFlags = IRGlobalFlags.Const;
					attribs.Linkage = IRLinkage.Private;
					IROperand operand = _builder.BuildGlobal($"__str{_stringCounter++}", type, attribs, tmp.String);
					_localMapping.Add(tmp.RetOperand.Iden, operand);

					break;
				}
				case ILInstructionType.AllocStack:
					GenAllocStack(instruction as ILAllocStackInstruction);
					break;
				case ILInstructionType.Assign:
					break;
				case ILInstructionType.Store:
					GenStore(instruction as ILStoreInstruction);
					break;
				case ILInstructionType.Load:
					GenLoad(instruction as ILLoadInstruction);
					break;


				case ILInstructionType.FunctionRef:
					GenFunctionRef(instruction as ILFunctionRefInstruction);
					break;
				case ILInstructionType.MethodRef:
					// Should be processed in the IL
					break;
				case ILInstructionType.OperatorRef:
					GenOperatorRef(instruction as ILOperatorRefInstruction);
					break;


				case ILInstructionType.Call:
					GenCall(instruction as ILCallInstruction);
					break;
				case ILInstructionType.Builtin:
					GenBuiltin(instruction as ILBuiltinInstruction);
					break;

				case ILInstructionType.TuplePack:
					GenPackTuple(instruction as ILTuplePackInstruction);
					break;
				case ILInstructionType.TupleUnpack:
					GenUnpackTuple(instruction as ILTupleUnpackInstruction);
					break;

				case ILInstructionType.StructElemAddr:
					GenStructElemAddr(instruction as ILStructElemAddrInstruction);
					break;

				case ILInstructionType.GlobalAddr:
					GenGlobalAddr(instruction as ILGlobalAddrInstruction);
					break;
				case ILInstructionType.GlobalValue:
					GenGlobalValue(instruction as ILGlobalValueInstruction);
					break;

				case ILInstructionType.ConvType:
					GenConvType(instruction as ILConvertTypeInstruction);
					break;

				case ILInstructionType.Return:
					GenReturn(instruction as ILReturnInstruction);
					break;
				case ILInstructionType.Branch:
					GenBranch(instruction as ILBranchInstruction);
					break;
				case ILInstructionType.CondBranch:
					GenCondBranch(instruction as ILCondBranchInstruction);
					break;
				default:
					break;
				}
			}
		}

		void GenAllocStack(ILAllocStackInstruction instruction)
		{
			IROperand operand = _builder.BuildAlloca(instruction.RetOperand.Iden, GenIRTypeFromType(instruction.Type));
			_localMapping.Add(instruction.RetOperand.Iden, operand);
		}

		void GenStore(ILStoreInstruction instruction)
		{
			IROperand dst = _localMapping[instruction.Dst.Iden];

			IRType dstType = dst.Type;
			IRType srcType = new IRType();
			srcType.BaseType = dstType.BaseType;
			srcType.PtrCount = dstType.PtrCount;

			IROperand src = _localMapping[instruction.Src.Iden];

			_builder.BuildStore(src, dst);
		}

		void GenLoad(ILLoadInstruction instruction)
		{
			IROperand src = _localMapping[instruction.Operand.Iden]; ;

			IROperand operand = _builder.BuildLoad(src, instruction.RetOperand.Iden);

			_localMapping.Add(instruction.RetOperand.Iden, operand);
		}

		void GenBuiltin(ILBuiltinInstruction instruction)
		{
			string[] builtins = instruction.Builtin.Split('.');

			switch (builtins[0])
			{
			case "neg":
			{
				ILOperand ilOp = instruction.Params[0];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IROperand retOperand = _builder.BuildNeg(instruction.RetOperand.Iden, _localMapping[ilOp.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			case "compl":
			{
				ILOperand ilOp = instruction.Params[0];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IROperand retOperand = _builder.BuildCompl(instruction.RetOperand.Iden, _localMapping[ilOp.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			case "add":
			{
				ILOperand ilOp0 = instruction.Params[0];
				ILOperand ilOp1 = instruction.Params[1];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IROperand retOperand = _builder.BuildAdd(instruction.RetOperand.Iden, _localMapping[ilOp0.Iden], _localMapping[ilOp1.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			case "sub":
			{
				ILOperand ilOp0 = instruction.Params[0];
				ILOperand ilOp1 = instruction.Params[1];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IROperand retOperand = _builder.BuildSub(instruction.RetOperand.Iden, _localMapping[ilOp0.Iden], _localMapping[ilOp1.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			case "mul":
			{
				ILOperand ilOp0 = instruction.Params[0];
				ILOperand ilOp1 = instruction.Params[1];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IROperand retOperand = _builder.BuildMul(instruction.RetOperand.Iden, _localMapping[ilOp0.Iden], _localMapping[ilOp1.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			case "div":
			{
				ILOperand ilOp0 = instruction.Params[0];
				ILOperand ilOp1 = instruction.Params[1];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IROperand retOperand = _builder.BuildDiv(instruction.RetOperand.Iden, _localMapping[ilOp0.Iden], _localMapping[ilOp1.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			case "rem":
			{
				ILOperand ilOp0 = instruction.Params[0];
				ILOperand ilOp1 = instruction.Params[1];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IROperand retOperand = _builder.BuildRem(instruction.RetOperand.Iden, _localMapping[ilOp0.Iden], _localMapping[ilOp1.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			case "shift":
			{
				ILOperand ilOp0 = instruction.Params[0];
				ILOperand ilOp1 = instruction.Params[1];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IRShiftDir dir = builtins[1] == "l" ? IRShiftDir.Left : builtins[1] == "lr" ? IRShiftDir.LogicRight : IRShiftDir.ArithRight;
				IROperand retOperand = _builder.BuildShift(instruction.RetOperand.Iden, dir, _localMapping[ilOp0.Iden], _localMapping[ilOp1.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			case "or":
			{
				ILOperand ilOp0 = instruction.Params[0];
				ILOperand ilOp1 = instruction.Params[1];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IROperand retOperand = _builder.BuildOr(instruction.RetOperand.Iden, _localMapping[ilOp0.Iden], _localMapping[ilOp1.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			case "xor":
			{
				ILOperand ilOp0 = instruction.Params[0];
				ILOperand ilOp1 = instruction.Params[1];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IROperand retOperand = _builder.BuildXor(instruction.RetOperand.Iden, _localMapping[ilOp0.Iden], _localMapping[ilOp1.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			case "and":
			{
				ILOperand ilOp0 = instruction.Params[0];
				ILOperand ilOp1 = instruction.Params[1];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IROperand retOperand = _builder.BuildAnd(instruction.RetOperand.Iden, _localMapping[ilOp0.Iden], _localMapping[ilOp1.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}

			case "trunc":
			{
				ILOperand ilOp = instruction.Params[0];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IROperand retOperand = _builder.BuildTrunc(instruction.RetOperand.Iden, _localMapping[ilOp.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			case "ext":
			{
				ILOperand ilOp = instruction.Params[0]; 
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				
				IRExtType extType = IRExtType.Default;
				if (builtins.Length == 2)
					extType = builtins[1][0] == 's' ? IRExtType.Signed : builtins[1][0] == 'z' ? IRExtType.Zero : IRExtType.Default;

				IROperand retOperand = _builder.BuildExt(instruction.RetOperand.Iden, extType, _localMapping[ilOp.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);

				break;
			}
			case "fptoi":
			{
				ILOperand ilOp = instruction.Params[0];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IROperand retOperand = _builder.BuildFptoi(instruction.RetOperand.Iden, _localMapping[ilOp.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			case "itofp":
			{
				ILOperand ilOp = instruction.Params[0];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IROperand retOperand = _builder.BuildItofp(instruction.RetOperand.Iden, _localMapping[ilOp.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			case "ptrtoi":
			{
				ILOperand ilOp = instruction.Params[0];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IROperand retOperand = _builder.BuildPtrtoi(instruction.RetOperand.Iden, _localMapping[ilOp.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			case "itoptr":
			{
				ILOperand ilOp = instruction.Params[0];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IROperand retOperand = _builder.BuildItoptr(instruction.RetOperand.Iden, _localMapping[ilOp.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			case "bitcast":
			{
				ILOperand ilOp = instruction.Params[0];
				IRType retType = GenIRTypeFromType(instruction.ReturnType);
				IROperand retOperand = _builder.BuildBitcast(instruction.RetOperand.Iden, _localMapping[ilOp.Iden], retType);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			case "cmp":
			{
				ILOperand ilOp0 = instruction.Params[0];
				ILOperand ilOp1 = instruction.Params[1];
				IRType type = GenIRTypeFromType(ilOp0.Type);
				IRCmpType cmpType = IRHelpers.GetCmpType(builtins[1]);
				IROperand retOperand = _builder.BuildCmp(instruction.RetOperand.Iden, cmpType, _localMapping[ilOp0.Iden], _localMapping[ilOp1.Iden], type);
				_localMapping.Add(instruction.RetOperand.Iden, retOperand);
				break;
			}
			}
		}

		void GenPackTuple(ILTuplePackInstruction instruction)
		{
			IRType retType = GenIRTypeFromType(instruction.RetOperand.Type);

			string retvar = instruction.RetOperand.Iden;

			IROperand retAddr = _builder.BuildAlloca(retvar + ".addr", retType);

			for (var i = 0; i < instruction.Operands.Count; i++)
			{
				ILOperand ilOperand = instruction.Operands[i];
				IROperand src = _localMapping[ilOperand.Iden];
				
				_builder.BuildInsertValue(retAddr, src, i);
			}

			IROperand ret = _builder.BuildLoad(retAddr, retvar);
			_localMapping.Add(ret.Iden, ret);
		}

		void GenUnpackTuple(ILTupleUnpackInstruction instruction)
		{
			IROperand operand = _localMapping[instruction.Operand.Iden];

			if (!operand.Type.Address && operand.Type.PtrCount == 0)
			{
				IROperand tmp = _builder.BuildAlloca(operand.Iden + ".addr", operand.Type);
				_builder.BuildStore(operand, tmp);
				operand = tmp;
			}

			for (var i = 0; i < instruction.RetOperands.Count; i++)
			{
				ILOperand retOperand = instruction.RetOperands[i];
				IRType retType = GenIRTypeFromType(retOperand.Type);

				IROperand ret = _builder.BuildExtractValue(retOperand.Iden + ".addr", operand, retType, i);
				_localMapping.Add(ret.Iden, ret);
			}
		}

		void GenStructElemAddr(ILStructElemAddrInstruction instruction)
		{
			SymbolType type = instruction.Struct.Type.GetInnerType();

			ILAggregate aggregate = _ilAggregates[type.ToILString()];
			if (aggregate.Identifier.Name is IdentifierName idenName &&
			    idenName != instruction.ElemRef.Base)
				return;
			if (!(instruction.ElemRef.Base is TemplateDefinitionName def) || 
			    !(aggregate.Identifier.Name is TemplateInstanceName inst) || 
			    !TemplateHelpers.IsTemplateSpecialization(def, inst.BaseIden))
				return;

			int index = aggregate.Variables.FindIndex(v => v.Identifier == instruction.ElemRef.Element);

			ILOperand retVar = instruction.RetOperand;
			IRType retType = GenIRTypeFromType(retVar.Type);
			IROperand addr = _builder.BuildGep(_localMapping[instruction.Struct.Iden], retVar.Iden, retType, 0, index);
			_localMapping.Add(addr.Iden, addr);
		}

		void GenFunctionRef(ILFunctionRefInstruction instruction)
		{
			_funcIdens.Add(instruction.RetOperand.Iden, instruction.MangledName);
		}

		void GenOperatorRef(ILOperatorRefInstruction instruction)
		{
			// TODO
		}

		void GenCall(ILCallInstruction instruction)
		{
			List<IROperand> operands = null;
			if (instruction.Params != null && instruction.Params.Count > 0)
			{
				operands = new List<IROperand>();
				foreach (ILOperand ilOperand in instruction.Params)
				{
					IROperand op = _localMapping[ilOperand.Iden];
					operands.Add(op);
				}
			}

			IRType retType = GenIRTypeFromType(instruction.ReturnType);

			string callName = _funcIdens[instruction.Func.Iden];
			IROperand ret = _builder.BuildCall(instruction.RetOperand?.Iden, callName, operands, retType);

			if (ret != null)
			{
				_localMapping.Add(ret.Iden, ret);
			}
		}

		void GenGlobalAddr(ILGlobalAddrInstruction instruction)
		{
			IRType type = GenIRTypeFromType(instruction.RetOperand.Type);
			IROperand operand = new IROperand(instruction.Identifier, type, true);
			_localMapping.Add(instruction.RetOperand.Iden, operand);
		}

		void GenGlobalValue(ILGlobalValueInstruction instruction)
		{
			IRType type = GenIRTypeFromType(instruction.RetOperand.Type);
			IRType memType = new IRType(type, forceAddress: true);
			IROperand operand = new IROperand(instruction.Identifier, memType, true);
			IROperand ret = _builder.BuildLoad(operand, instruction.Identifier + "_value");
			_localMapping.Add(instruction.RetOperand.Iden, ret);
		}

		void GenConvType(ILConvertTypeInstruction convertType)
		{
			// Just interpret operand as a different type

			IROperand srcOperand = _localMapping[convertType.Operand.Iden];

			IRType type = GenIRTypeFromType(convertType.Type);

			if (srcOperand.Type.BaseType == IRBaseType.Array && type.PtrCount > 0)
			{
				IROperand op = _builder.BuildGep(srcOperand, convertType.RetOperand.Iden, type, new[] {0, 0});
				_localMapping.Add(convertType.RetOperand.Iden, op);
			}
			else
			{
				if (srcOperand.Iden == null)
				{
					IROperand op = new IROperand(srcOperand.IntLiteral, type);
					_localMapping.Add(convertType.RetOperand.Iden, op);
				}
				else
				{
					IROperand op = new IROperand(srcOperand.Iden, type, srcOperand.Global);
					_localMapping.Add(convertType.RetOperand.Iden, op);
				}
			}
			
		}


		void GenReturn(ILReturnInstruction instruction)
		{
			if (instruction.Operand == null)
			{
				_builder.BuildRet();
			}
			else
			{
				_builder.BuildRet(_localMapping[instruction.Operand.Iden]);
			}
		}

		void GenBranch(ILBranchInstruction instruction)
		{
			_builder.BuildBranch(instruction.Label);
		}

		void GenCondBranch(ILCondBranchInstruction instruction)
		{
			IROperand cond = _localMapping[instruction.Condition.Iden];
			_builder.BuildCondBranch(cond, instruction.TrueLabel, instruction.FalseLabel);
		}

		IRType GenIRTypeFromType(SymbolType type)
		{
			Stack<SymbolType> subTypes = new Stack<SymbolType>();
			SymbolType subType = type;
			while (subType != null)
			{
				subTypes.Push(subType);
				subType = subType.GetBaseType();
			}

			if (subTypes.Count == 0)
				return null;

			SymbolType tmp = subTypes.Pop();

			IRType irType;
			switch (tmp)
			{
			case BuiltinSymbolType builtinType:
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.Bool:		irType = new IRType(IRBaseType.I1);				break;
				case BuiltinTypes.I8:		irType = new IRType(IRBaseType.I8);				break;
				case BuiltinTypes.I16:		irType = new IRType(IRBaseType.I16);			break;
				case BuiltinTypes.I32:		irType = new IRType(IRBaseType.I32);			break;
				case BuiltinTypes.I64:		irType = new IRType(IRBaseType.I64);			break;
				case BuiltinTypes.U8:		irType = new IRType(IRBaseType.U8);				break;
				case BuiltinTypes.U16:		irType = new IRType(IRBaseType.U16);			break;
				case BuiltinTypes.U32:		irType = new IRType(IRBaseType.U32);			break;
				case BuiltinTypes.U64:		irType = new IRType(IRBaseType.U64);			break;
				case BuiltinTypes.ISize:	irType = new IRType(CmdLine.IsX64 ? IRBaseType.I64 : IRBaseType.I32);	break;
				case BuiltinTypes.USize:	irType = new IRType(CmdLine.IsX64 ? IRBaseType.U64 : IRBaseType.U32);	break;
				case BuiltinTypes.F32:		irType = new IRType(IRBaseType.F32);			break;
				case BuiltinTypes.F64:		irType = new IRType(IRBaseType.F64);			break;
				case BuiltinTypes.Char:		irType = new IRType(IRBaseType.U8);				break;
				case BuiltinTypes.WChar:	irType = new IRType(IRBaseType.U16);			break;
				case BuiltinTypes.Rune:		irType = new IRType(IRBaseType.U32);			break;  
				case BuiltinTypes.Null:		irType = new IRType(false, IRBaseType.U8, 1);	break;
				case BuiltinTypes.Void:		irType = new IRType(IRBaseType.U8);             break;
				case BuiltinTypes.String:	irType = new IRType("string");					break;
				default:
					return null;
				}
				break;
			}
			case AggregateSymbolType aggregateType:
			{
				irType = new IRType(aggregateType.MangledIdentifier);
				break;
			}
			case EnumSymbolType enumType:
			{
				IRType simpleEnumType;
				if (_ilSimpleEnums.TryGetValue(enumType.MangledIdentifier, out simpleEnumType))
				{
					irType = simpleEnumType;
				}
				else
				{
					irType = new IRType(enumType.MangledIdentifier);
				}

				break;
			}
			case DelegateSymbolType delegateType:
			{
				IRType funcPtr;
				if (_ilFuncPtrs.TryGetValue(delegateType.MangledIdentifier, out funcPtr))
				{
					irType = funcPtr;
				}
				else
				{
					irType = new IRType(delegateType.MangledIdentifier);
				}

				break;
			}
			case TupleSymbolType tupleType:
			{
				string typeName = "__tup_" + NameMangling.MangleType(tupleType);

				// Create tuple if it doesn't exist yet
				if (!_irTuples.ContainsKey(typeName))
				{
					IRTypeDef tupDef = GenTupleTypeDefinition(tupleType);
					_irTuples.Add(typeName, tupDef);
				}

				irType = new IRType(typeName);
				break;
			}
			case TemplateInstanceSymbolType instType:
			{
				irType = new IRType(instType.MangledName);
				break;
			}
			// TODO: other types
			default:
				irType = null;
				break;
			}

			SymbolType tmpOut;
			while (subTypes.TryPop(out tmpOut))
			{
				switch (tmpOut)
				{
				case PointerSymbolType _:
				case ReferenceSymbolType _:
					++irType.PtrCount;
					break;
				case MemoryLocSymbolType _:
					irType.Address = true;
					break;
				case ArraySymbolType arrayType when arrayType.ArraySize == ulong.MaxValue:
					++irType.PtrCount;
					break;
				case ArraySymbolType arrayType:
					irType = new IRType(irType, arrayType.ArraySize);
					break;
				}
			}

			return irType;
		}

		void GenTypes(ILCompUnit compUnit)
		{
			foreach (KeyValuePair<string, ILEnum> pair in compUnit.Enums)
			{
				ILEnum ilEnum = pair.Value;

				// TODO
				// Skip un-instantiated templates
				//if (ilEnum.TemplateParams != null)
				//	continue;

				if (ilEnum.IsADT)
				{
					// TODO
				}
				else
				{
					IRType type = GenIRTypeFromType(ilEnum.BaseType);
					_ilSimpleEnums.Add(ilEnum.MangledIdentifier, type);
				}
			}

			foreach (KeyValuePair<string, ILDelegate> pair in compUnit.Delegates)
			{
				ILDelegate del = pair.Value;

				// TODO
				// Skip un-instantiated templates
				//if (del.TemplateParams != null)
				//	continue;

				if (del.AsFuncPtr)
				{
					string mangledName = del.MangledIdentifier;
					_ilFuncPtrs.Add(mangledName, new IRType(false, IRBaseType.U8, 1));
					_ilDelegates.Add(mangledName, del);
				}
				else
				{
					// TODO
				}

				
			}

			foreach (KeyValuePair<string, ILAggregate> pair in compUnit.AggregatesMangled)
			{
				ILAggregate aggregate = pair.Value;

				// Skip un-instantiated templates
				if (aggregate.Identifier.Name is TemplateDefinitionName)
					continue;

				List<IRType> subTypes = new List<IRType>();
				foreach (ILAggregateVariable variable in aggregate.Variables)
				{
					IRType subType = GenIRTypeFromType(variable.Type);
					subTypes.Add(subType);
				}

				IRTypeDefKind kind = aggregate.Type == ILAggregateType.Struct ? IRTypeDefKind.Struct : IRTypeDefKind.Union;
				IRTypeDef typeDef = new IRTypeDef(aggregate.MangledIdentifier, subTypes, kind);
				
				string mangledName = aggregate.MangledIdentifier;
				_typeDefinitions.Add(mangledName, typeDef);
				_ilAggregates.Add(mangledName, aggregate);
			}

			if (_typeDefinitions.Count > 0)
			{
				foreach (KeyValuePair<string, IRTypeDef> def in _typeDefinitions)
				{
					_builder.CreateType(def.Key, def.Value);
				}
			}

		}

		IRTypeDef GenTupleTypeDefinition(TupleSymbolType tupleType)
		{
			List<IRType> types = new List<IRType>();
			foreach (SymbolType subType in tupleType.SubTypes)
			{
				IRType type = GenIRTypeFromType(subType);
				types.Add(type);
			}

			string iden = "__tup_" + NameMangling.MangleType(tupleType);
			return new IRTypeDef(iden, types, IRTypeDefKind.Struct);
		}

		IRAttributes GenIRAttribs(ILAttributes ilAttribs)
		{
			IRAttributes attribs = new IRAttributes();

			switch (ilAttribs.Linkage)
			{
			case ILLinkage.Export:			attribs.Linkage = IRLinkage.Export;			break;
			case ILLinkage.Import:			attribs.Linkage = IRLinkage.Import;			break;
			case ILLinkage.Public:			attribs.Linkage = IRLinkage.Public;			break;
			case ILLinkage.PublicExternal:	attribs.Linkage = IRLinkage.PublicExternal;	break;
			case ILLinkage.Hidden:			attribs.Linkage = IRLinkage.Hidden;			break;
			case ILLinkage.HiddenExternal:	attribs.Linkage = IRLinkage.HiddenExternal;	break;
			case ILLinkage.Shared:			attribs.Linkage = IRLinkage.Shared;			break;
			case ILLinkage.Private:			attribs.Linkage = IRLinkage.Private;		break;
			}

			attribs.Alignment = ilAttribs.Alignment;

			switch (ilAttribs.CallConv)
			{
			case ILCallConv.None:		attribs.CallConv = IRCallConv.None;		break;
			case ILCallConv.MJay:		attribs.CallConv = IRCallConv.MJay;		break;
			case ILCallConv.C:			attribs.CallConv = IRCallConv.C;		break;
			case ILCallConv.Windows:	attribs.CallConv = IRCallConv.Windows;	break;
			}

			if (ilAttribs.GlobalFlags != ILGlobalFlags.None)
			{
				if ((ilAttribs.GlobalFlags & ILGlobalFlags.Const) != 0)
					attribs.GlobalFlags |= IRGlobalFlags.Const;
			}

			return attribs;
		}

	}
}
