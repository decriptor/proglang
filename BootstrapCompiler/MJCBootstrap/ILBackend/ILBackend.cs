﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using MJC.General;
using MJC.ILBackend.Builtin;
using MJC.ILBackend.Canonical;
using MJC.ILBackend.CompExec;
using MJC.ILBackend.Context;
using MJC.ILBackend.Opt;
using MJC.ILBackend.Parse;
using MJC.IRBackend;

namespace MJC.ILBackend
{
	class ILBackend
	{
		private ILLexer _ilLexer = new ILLexer();
		private ILParser _ilParser = new ILParser();
		
		private ILCanonicalizer _canonicalizer = new ILCanonicalizer();
		private ILOptimization _optimization = new ILOptimization();

		private IRGenerator _irGenerator = new IRGenerator();

		private List<ILCompUnit> _refCompUnits = new List<ILCompUnit>();
		private ILCompUnit _builtins;

		private ILCompileExecution _compExec = new ILCompileExecution();

		public ILBackend()
		{
			// Generate builtin module
			_builtins = BuiltinModule.GenerateBuiltins();
			
			// Setup canonicalize passes
			_canonicalizer.AddPass(new ILTemplateInitPass());
			_canonicalizer.AddPass(new ILBoilerplatePass());
			_canonicalizer.AddPass(new ILGlobalInitPass());
			_canonicalizer.AddPass(new ILBlockTerminationPass());
			_canonicalizer.AddPass(new ILStackDeallocationPass());
			_canonicalizer.AddPass(new ILInsertExtractReplacePass());
			_canonicalizer.AddPass(new ILInstructionCanonicalizationPass());
			_canonicalizer.AddPass(new ILNullCastPass());
			_canonicalizer.AddPass(new ILStaticDispatchPass());

			// Setup optimization passes
			_optimization.AddPass(new ILRemEnumMemFuncPass());
			_optimization.AddPass(new ILRemGlobalConstFuncPass());
			_optimization.AddPass(new ILInlinePass());
		}

		ILCompUnit ParseCompUnit(string ilSource)
		{
			List<ILToken> tokens = _ilLexer.Lex(ilSource);
			ILCompUnit compUnit = _ilParser.Parse(tokens);

			return compUnit;
		}

		bool CanonicalizePass(ILCompUnit compUnit, string ilFile)
		{
			bool success = _canonicalizer.Canonicalize(compUnit);
			ILWriter.Write(compUnit, ilFile);

			return success;
		}

		void Optimize(List<ILCompUnit> compUnits)
		{
			_optimization.Setup(_refCompUnits, _builtins);
			_optimization.Process(compUnits);
		}

		void CompileExec(ILCompUnit compUnit, string ilFile)
		{
			_compExec.Process(compUnit);
			compUnit.Stage = ILStage.CompExec;
			ILWriter.Write(compUnit, ilFile);
		}

		public (List<string>, List<IRModule>) Execute(CompilerContext compileContext, List<string> ilFiles, List<ILCompUnit> compUnits)
		{
			List<string> irFiles = new List<string>();
			List<IRModule> irModules = new List<IRModule>();
			Stopwatch timer = new Stopwatch();

			if (CmdLine.Stats)
				timer.Start();

			if (compUnits == null)
			{
				compUnits = new List<ILCompUnit>();

				foreach (string ilFile in ilFiles)
				{
					string source = File.ReadAllText(ilFile);
					ILCompUnit compUnit = ParseCompUnit(source);
					compUnits.Add(compUnit);
					_refCompUnits.Add(compUnit);
				}
			}

			foreach (ILCompUnit compUnit in compUnits)
			{
				compUnit.Builtins = _builtins;
				compUnit.RefCompUnits = _refCompUnits;
			}

			if (CmdLine.Stats)
				timer.Restart();

			foreach (ILCompUnit compUnit in compUnits)
			{
				ILContextGen.ProcessModule(compUnit, compileContext);
			}

			if (CmdLine.Stats)
			{
				timer.Stop();
				Console.WriteLine($"IL: context creation took {timer.Elapsed.TotalMilliseconds}ms");
			}

			if (CmdLine.Stats)
				timer.Restart();

			foreach (ILCompUnit compUnit in compUnits)
			{
				ILCallGraphGen.ProcessModule(compUnit);
			}

			if (CmdLine.Stats)
			{
				timer.Stop();
				Console.WriteLine($"IL: call graph creation took {timer.Elapsed.TotalMilliseconds}ms");
			}

			if (CmdLine.Stats)
				timer.Restart();

			bool success = false;
			for (var i = 0; i < compUnits.Count; i++)
			{
				ILCompUnit compUnit = compUnits[i];
				string ilFile = ilFiles[i];

				success |= CanonicalizePass(compUnit, ilFile);
			}

			if (CmdLine.Stats)
			{
				timer.Stop();
				Console.WriteLine($"IL: canonicalization took {timer.Elapsed.TotalMilliseconds}ms");
			}

			if (!success)
			{
				ErrorSystem.Fatal("An error has occured during IL canonicalization");
			}

			if (CmdLine.Stats)
				timer.Restart();

			for (var i = 0; i < compUnits.Count; i++)
			{
				ILCompUnit compUnit = compUnits[i];
				string ilFile = ilFiles[i];

				CompileExec(compUnit, ilFile);
			}

			if (CmdLine.Stats)
			{
				timer.Stop();
				Console.WriteLine($"IL: compile exec took {timer.Elapsed.TotalMilliseconds}ms");
			}

			if (CmdLine.Stats)
				timer.Restart();
			
			Optimize(compUnits);

			if (CmdLine.Stats)
			{
				timer.Stop();
				Console.WriteLine($"IL: optimization took {timer.Elapsed.TotalMilliseconds}ms");
			}

			for (var i = 0; i < compUnits.Count; i++)
			{
				ILCompUnit compUnit = compUnits[i];
				string ilFile = ilFiles[i];

				ILWriter.Write(compUnit, ilFile);
			}

			if (CmdLine.Stats)
				timer.Restart();

			for (var i = 0; i < compUnits.Count; i++)
			{
				ILCompUnit compUnit = compUnits[i];
				string irFile = Path.ChangeExtension(ilFiles[i], "mjir");

				IRModule module = _irGenerator.GenerateIR(compUnit, irFile);
				irModules.Add(module);
				irFiles.Add(irFile);
			}

			if (CmdLine.Stats)
			{
				timer.Stop();
				Console.WriteLine($"IL: generating IR took {timer.Elapsed.TotalMilliseconds}ms");
			}

			return (irFiles, irModules);
		}

	}
}
