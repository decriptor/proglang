﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;

namespace MJC.ILBackend.Canonical
{
	class ILInstructionCanonicalizationPass : ILCanonicalizePass
	{
		public override void ProcessBasicBlock(ILBasicBlock basicBlock, ILContext context)
		{
			for (var i = 0; i < basicBlock.Instructions.Count; i++)
			{
				ILInstruction instruction = basicBlock.Instructions[i];
				if (instruction is ILAssignInstruction assign)
				{
					ILOperand srcVar = assign.Dst;
					ILVarContext srcContext = context.GetVarContext(srcVar.Iden);

					switch (srcContext.VarType)
					{
					case ILVarType.Stack:
						ILStoreInstruction store = new ILStoreInstruction(assign.Src, assign.Dst, assign.Span);
						basicBlock.Instructions[i] = store;
						break;
					}
				}
			}
		}
	}
}
