﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;

namespace MJC.ILBackend.Canonical
{
	class ILInsertExtractReplacePass : ILCanonicalizePass
	{
		public override void ProcessBasicBlock(ILBasicBlock basicBlock, ILContext funcContext)
		{
			//base.ProcessBasicBlock(basicBlock, funcContext);

			List<ILInstruction> instructions = basicBlock.Instructions;
			for (var i = 0; i < instructions.Count; i++)
			{
				ILInstruction instr = instructions[i];
				switch (instr.InstructionType)
				{
				case ILInstructionType.StructInsert:
				{
					ILStructInsertInstruction structInsert = instr as ILStructInsertInstruction;
					TextSpan span = instr.Span;

					string iden = structInsert.Struct.Iden + '_' +
					              structInsert.ElemRef.Element + '_' +
					              structInsert.Value.Iden + "_addr";

					SymbolType retType = SymbolType.MemoryLocType(structInsert.Value.Type);
					ILOperand addressOp = new ILOperand(iden, retType);
					instructions[i] = new ILStructElemAddrInstruction(addressOp, structInsert.Struct, structInsert.ElemRef, span);

					++i;
					instructions.Insert(i, new ILAssignInstruction(structInsert.Value, addressOp, span));

					// Update context
					ILVarContext context = new ILVarContext(addressOp, ILVarType.Stack);
					context.Dependents.Add(structInsert.Value.Iden);
					context.UseCount = 1;
					funcContext.Variables.Add(iden, context);

					break;
				}
				case ILInstructionType.StructExtract:
				{
					ILStructExtractInstruction structExtract = instr as ILStructExtractInstruction;
					TextSpan span = instr.Span;

					string iden = structExtract.Struct.Iden + '_' +
					              structExtract.ElemRef.Element + "_addr";

					SymbolType retType = SymbolType.MemoryLocType(structExtract.RetOperand.Type);
					ILOperand addressOp = new ILOperand(iden, retType);
					instructions[i] = new ILStructElemAddrInstruction(addressOp, structExtract.Struct, structExtract.ElemRef, span);

					++i;
					instructions.Insert(i, new ILLoadInstruction(structExtract.RetOperand, addressOp, span));

					// Update context
					ILVarContext context = new ILVarContext(addressOp, ILVarType.Stack);
					context.UseCount = 1;
					funcContext.Variables.Add(iden, context);

					ILVarContext retContext = funcContext.GetVarContext(structExtract.RetOperand.Iden);
					retContext.Dependents.Add(iden);

					break;
				}
				}
			}
		}
	}
}
