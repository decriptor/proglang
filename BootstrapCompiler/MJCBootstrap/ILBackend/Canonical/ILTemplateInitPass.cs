﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;
using MJC.ILBackend.General;
using MJC.ILBackend.Interpret;

namespace MJC.ILBackend.Canonical
{
	// TODO: allow templated methods in templated aggregates
	class ILTemplateInitPass : ILCanonicalizePass
	{
		public override void ProcessModule(ILCompUnit compUnit)
		{
			//base.ProcessModule(compUnit);

			// Update all template aggregate specializations
			foreach (KeyValuePair<ScopeVariable, ILAggregate> pair in compUnit.Aggregates)
			{
				ILAggregate aggr = pair.Value;
				if (aggr.TemplateParams != null)
					ResolveValueSpecializationAndDefault(aggr, compUnit);
			}
			
			// Instantiate all used template instances
			Dictionary<ScopeVariable, ILTemplateInstance> instantiated = new Dictionary<ScopeVariable, ILTemplateInstance>();
			foreach (KeyValuePair<ScopeVariable, ILTemplateInstance> pair in compUnit.TemplateInstances)
			{
				ILTemplateInstance instance = pair.Value;

				// Update the default values of the template instance
				ILTemplateHelpers.UpdateDefaultValues(instance.Iden.Name as TemplateInstanceName, instance.Type.TemplateSymbol.Identifier as TemplateDefinitionName);

				if (instance.IsTypeInstance())
				{
					InstantiateType(instance, compUnit);
				}
				else // func inst
				{
					string mangledBase = instance.Type.TemplateSymbol.MangledName;
					string mangled = instance.MangledName;

					// If function is already instantiated, skip it
					if (compUnit.GetFunction(mangled) != null ||
					    compUnit.GetExternalFunction(mangled) != null)
						continue;

					ILFunction baseFunction = compUnit.GetFunction(mangledBase);
					InstantiateFunction(instance, baseFunction, compUnit);
				}

				instantiated.Add(pair.Key, pair.Value);
			}

			compUnit.TemplateInstances = instantiated;

			// Instantiate all used []T (Array<T>) and [N]T (Array<T,N>) instances

		}

		void InstantiateType(ILTemplateInstance instance, ILCompUnit compUnit)
		{
			// Resolve value parameters
			ResolveValueParameters(instance, compUnit);
			TemplateInstanceName instName = instance.Iden.Name as TemplateInstanceName;
			for (var i = 0; i < instance.Arguments.Count; i++)
			{
				ILTemplateArg ilArg = instance.Arguments[i];
				if (ilArg.Value != null)
				{
					TemplateArgument instArg = instName.Arguments[i];
					TemplateDefinitionName defName =  instance.Type.TemplateSymbol.Identifier as TemplateDefinitionName;

					instArg.Value = ilArg.Value;
					instArg.Type = defName.Parameters[i].Type;
				}
			}

			// Generate symbol
			Symbol symbol = new Symbol(instance.Iden.Scope, null, instance.Iden.Name, SymbolKind.TemplateInstance);
			symbol.Type = instance.Type;

			// Generate mangled name
			string mangled = NameMangling.Mangle(symbol);
			instance.MangledName = mangled;
			symbol.MangledName = mangled;
			instance.Type.MangledName = mangled;

			// If function is already instantiated, skip it
			if (compUnit.GetAggregate(mangled) != null ||
			    compUnit.GetEnum(mangled) != null)
				return;

			Symbol templateSymbol = instance.Type.TemplateSymbol;
			if (templateSymbol.Kind == SymbolKind.Struct ||
			    templateSymbol.Kind == SymbolKind.Union)
			{
				ILAggregate baseAggr = ILTemplateHelpers.GetBestBaseAggregate(instance, compUnit);
				InstantiateAggregate(instance, baseAggr, compUnit);

				// vtable
				InstantiateVTables(instance, compUnit, baseAggr);
			}
		}

		void InstantiateAggregate(ILTemplateInstance instance, ILAggregate baseAggr, ILCompUnit compUnit)
		{
			ILAggregate aggregate = new ILAggregate(baseAggr.Type, instance.Iden, instance.MangledName, instance.Type.TemplateSymbol, baseAggr.Attributes, null, baseAggr);

			foreach (ILAggregateVariable baseVar in baseAggr.Variables)
			{
				SymbolType varType = ILTemplateHelpers.GetInstancedType(baseVar.Type, instance);
				string mangledVar = "_M" + NameMangling.MangleScopeVar(instance.Iden) + NameMangling.MangleIdentifier(baseVar.Identifier) + 'V' + NameMangling.MangleType(varType);

				ILAggregateVariable aggrVar = new ILAggregateVariable(baseVar.Identifier, mangledVar, varType);
				aggregate.Variables.Add(aggrVar);
			}

			compUnit.AddAggregate(aggregate, true);
		}

		void InstantiateVTables(ILTemplateInstance instance, ILCompUnit compUnit, ILAggregate baseAggregate)
		{
			VTable vTable = new VTable(instance.Iden, instance.MangledName);
			List<VTableMethod> methods = TemplateHelpers.GetInstanceMethods(instance.Iden, instance.Type.TemplateSymbol, compUnit.VTablesVariants);

			foreach (VTableMethod baseMethod in methods)
			{
				FunctionSymbolType funcType = ILTemplateHelpers.GetInstancedType(baseMethod.Type, instance) as FunctionSymbolType;

				string mangledMethod = "_M" + NameMangling.MangleScopeVar(instance.Iden) + NameMangling.MangleIdentifier(baseMethod.Iden) + NameMangling.MangleType(funcType);
				VTableMethod method = new VTableMethod(baseMethod.Iden, funcType, mangledMethod);

				vTable.AddMethod(method);
				if (compUnit.GetFunction(mangledMethod) != null)
					continue;

				ILFunction baseFunc = compUnit.GetFunction(baseMethod.MangledFunc);
				InstantiateFunction(instance, baseFunc, compUnit, mangledMethod);
			}

			compUnit.AddVTable(vTable, true);
			compUnit.Context.CompilerContext.AddVTable(vTable);
		}

		void InstantiateFunction(ILTemplateInstance instance, ILFunction baseFunction, ILCompUnit compUnit, string mangledMethod = null)
		{
			List<ILOperand> operands = new List<ILOperand>();
			Dictionary<string, ILOperand> opMapping = new Dictionary<string, ILOperand>();
			foreach (ILOperand baseOp in baseFunction.Operands)
			{
				SymbolType type = ILTemplateHelpers.GetInstancedType(baseOp.Type, instance);
				ILOperand op = new ILOperand(baseOp.Iden, type);
				operands.Add(op);
				opMapping.Add(op.Iden, op);
			}

			SymbolType retType = ILTemplateHelpers.GetInstancedType(baseFunction.ReturnType, instance);
			ILFunction function = new ILFunction(mangledMethod ?? instance.MangledName, operands, retType, baseFunction.Attributes, null, false);

			foreach (ILBasicBlock baseBB in baseFunction.Blocks)
			{
				ILBasicBlock bb = InstantiateBasicBlock(instance, baseBB, function, opMapping);
				function.Blocks.Add(bb);
			}

			compUnit.AddBlock(function);

			// Generate context and callgraph
			ILContextGen.ProcessFunction(function, compUnit);
			ILCallGraphGen.ProcessFunction(function);
		}

		ILBasicBlock InstantiateBasicBlock(ILTemplateInstance instance, ILBasicBlock baseBB, ILFunction function, Dictionary<string, ILOperand> opMapping)
		{
			ILBasicBlock bb = new ILBasicBlock(baseBB.Label);
			

			foreach (ILInstruction baseInstr in baseBB.Instructions)
			{
				ILInstruction instr = null;

				switch (baseInstr.InstructionType)
				{
				case ILInstructionType.IntLiteral:
				{
					ILIntLiteralInstruction baseIntLit = baseInstr as ILIntLiteralInstruction;

					ILOperand op = ILTemplateHelpers.GetInstancedOperand(baseIntLit.RetOperand, instance);
					instr = new ILIntLiteralInstruction(op, baseIntLit.Value, baseIntLit.Span);
					opMapping.Add(op.Iden, op);
					break;
				}
				case ILInstructionType.FloatLiteral:
				{
					ILFloatLiteralInstruction baseFloatLit = baseInstr as ILFloatLiteralInstruction;

					ILOperand op = ILTemplateHelpers.GetInstancedOperand(baseFloatLit.RetOperand, instance);
					instr = new ILFloatLiteralInstruction(op, baseFloatLit.HexValue, baseFloatLit.Span);
					opMapping.Add(op.Iden, op);
					break;
				}
				case ILInstructionType.StringLiteral:
				{
					ILStringLiteralInstruction baseStrLit = baseInstr as ILStringLiteralInstruction;

					ILOperand op = ILTemplateHelpers.GetInstancedOperand(baseStrLit.RetOperand, instance);
					instr = new ILStringLiteralInstruction(op, baseStrLit.String, baseStrLit.Span);
					opMapping.Add(op.Iden, op);
					break;
				}
				case ILInstructionType.AllocStack:
				{
					ILAllocStackInstruction baseAlloc = baseInstr as ILAllocStackInstruction;

					SymbolType type = ILTemplateHelpers.GetInstancedType(baseAlloc.Type, instance);
					ILOperand op = new ILOperand(baseAlloc.RetOperand.Iden, SymbolType.MemoryLocType(type));
					instr = new ILAllocStackInstruction(op, type, baseAlloc.Span);
					opMapping.Add(op.Iden, op);
					break;
				}
				case ILInstructionType.DeallocStack:
				{
					ILDeallocStackInstruction baseDealloc = baseInstr as ILDeallocStackInstruction;

					ILOperand op = opMapping[baseDealloc.Operand.Iden];
					instr = new ILDeallocStackInstruction(op);
					break;
				}
				case ILInstructionType.Assign:
				{
					ILAssignInstruction baseAssign = baseInstr as ILAssignInstruction;

					ILOperand src = opMapping[baseAssign.Src.Iden];
					ILOperand dst = opMapping[baseAssign.Dst.Iden];
					instr = new ILAssignInstruction(src, dst, baseAssign.Span);
					break;
				}
				case ILInstructionType.Store:
				{
					ILStoreInstruction baseStore = baseInstr as ILStoreInstruction;

					ILOperand src = opMapping[baseStore.Src.Iden];
					ILOperand dst = opMapping[baseStore.Dst.Iden];
					instr = new ILStoreInstruction(src, dst, baseStore.Span);
					break;
				}
				case ILInstructionType.Load:
				{
					ILLoadInstruction baseLoad = baseInstr as ILLoadInstruction;

					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseLoad.RetOperand, instance);
					ILOperand op = opMapping[baseLoad.Operand.Iden];
					instr = new ILLoadInstruction(ret, op, baseLoad.Span);
					opMapping.Add(ret.Iden, ret);
					break;
				}
				case ILInstructionType.FunctionRef:
					break;
				case ILInstructionType.MethodRef:
					break;
				case ILInstructionType.OperatorRef:
				{
					
					break;
				}
				case ILInstructionType.Call:
				{
					ILCallInstruction baseCall = baseInstr as ILCallInstruction;

					ILOperand ret = baseCall.RetOperand != null ? ILTemplateHelpers.GetInstancedOperand(baseCall.RetOperand, instance) : null;
					ILOperand func = opMapping[baseCall.Func.Iden];
					List<ILOperand> ops = null;
					if (baseCall.Params != null)
					{
						ops = new List<ILOperand>();
						foreach (ILOperand param in baseCall.Params)
						{
							ops.Add(new ILOperand(param));
						}
					}

					instr = new ILCallInstruction(ret, func, ops, baseCall.ReturnType, baseCall.Span);
					if (ret != null)
						opMapping.Add(ret.Iden, ret);

					break;
				}
				case ILInstructionType.Builtin:
				{
					ILBuiltinInstruction baseBuiltin = baseInstr as ILBuiltinInstruction;

					ILOperand ret = baseBuiltin.RetOperand != null ? ILTemplateHelpers.GetInstancedOperand(baseBuiltin.RetOperand, instance) : null;
					List<ILOperand> ops = null;
					if (baseBuiltin.Params != null)
					{
						ops = new List<ILOperand>();
						foreach (ILOperand param in baseBuiltin.Params)
						{
							ops.Add(new ILOperand(param));
						}
					}

					instr = new ILBuiltinInstruction(ret, baseBuiltin.Builtin, ops, baseBuiltin.ReturnType, baseBuiltin.Span);
					if (ret != null)
						opMapping.Add(ret.Iden, ret);

					break;
				}
				case ILInstructionType.CompilerIntrin:
				{
					break;
				}
				case ILInstructionType.TupleUnpack:
				{
					break;
				}
				case ILInstructionType.TuplePack:
				{
					break;
				}
				case ILInstructionType.TupleInsert:
				{
					break;
				}
				case ILInstructionType.TupleExtract:
				{
					break;
				}
				case ILInstructionType.TupleElemAddr:
				{
					break;
				}
				case ILInstructionType.StructExtract:
				{
					ILStructExtractInstruction baseInsert = baseInstr as ILStructExtractInstruction;

					ILOperand ret = ILTemplateHelpers.GetInstancedOperand(baseInsert.RetOperand, instance);
					opMapping.Add(ret.Iden, ret);
					ILOperand str = opMapping[baseInsert.Struct.Iden];

					instr = new ILStructExtractInstruction(ret, str, baseInsert.ElemRef, baseInsert.Span);
					break;
				}
				case ILInstructionType.StructInsert:
				{
					ILStructInsertInstruction baseInsert = baseInstr as ILStructInsertInstruction;

					ILOperand str = opMapping[baseInsert.Struct.Iden];
					ILOperand val = opMapping[baseInsert.Value.Iden];
					instr = new ILStructInsertInstruction(str, baseInsert.ElemRef, val, baseInsert.Span);
					break;
				}
				case ILInstructionType.StructElemAddr:
				{
					break;
				}
				case ILInstructionType.Destruct:
				{
					break;
				}
				case ILInstructionType.EnumGetMemberValue:
				{
					break;
				}
				case ILInstructionType.GlobalAddr:
				{
					break;
				}
				case ILInstructionType.GlobalValue:
				{
					break;
				}
				case ILInstructionType.ConvType:
				{
					break;
				}
				case ILInstructionType.Return:
				{
					ILReturnInstruction baseRet = baseInstr as ILReturnInstruction;

					ILOperand op = baseRet.Operand != null ? opMapping[baseRet.Operand.Iden] : null;
					instr = new ILReturnInstruction(op, baseRet.Span);
					break;
				}
				case ILInstructionType.Branch:
				{
					break;
				}
				case ILInstructionType.CondBranch:
				{
					break;
				}
				default:
					instr = null;
					break;
				}


				bb.Instructions.Add(instr);
			}

			return bb;
		}

		void ResolveValueParameters(ILTemplateInstance instance, ILCompUnit compUnit)
		{
			foreach (ILTemplateArg arg in instance.Arguments)
			{
				if (arg.ValueFunc != null)
				{
					ILFunction func = compUnit.GetFunction(arg.ValueFunc);
					ILInterpOperand interpOp = ILInterpreter.InterpFunction(func, null, compUnit);

					arg.Value = interpOp.Value.ToString();

					switch (interpOp.Value)
					{
					case long _:
					case double _:
						arg.Value = interpOp.Value.ToString();
						break;
					case string sval:
						arg.Value = '"' + sval + '"';
						break;
					}
				}
			}
		}

		void ResolveValueSpecializationAndDefault(ILAggregate aggr, ILCompUnit compUnit)
		{
			TemplateDefinitionName defName = aggr.Identifier.Name as TemplateDefinitionName;
			for (var i = 0; i < aggr.TemplateParams.Count; i++)
			{
				ILTemplateParam ilParam = aggr.TemplateParams[i];
				TemplateParameter param = defName.Parameters[i];
				if (ilParam.ValueSpecializationFunc != null && string.IsNullOrEmpty(ilParam.ValueSpecialization))
				{
					ILFunction func = compUnit.GetFunction(ilParam.ValueSpecializationFunc);
					ILInterpOperand interpOp = ILInterpreter.InterpFunction(func, null, compUnit);

					ilParam.ValueSpecialization = interpOp.Value.ToString();

					switch (interpOp.Value)
					{
					case long _:
					case double _:
						ilParam.ValueSpecialization = interpOp.Value.ToString();
						break;
					case string sval:
						ilParam.ValueSpecialization = '"' + sval + '"';
						break;
					}
					param.ValueSpecialization = ilParam.ValueSpecialization;
				}
				if (ilParam.ValueDefaultFunc != null && string.IsNullOrEmpty(ilParam.ValueDefault))
				{
					ILFunction func = compUnit.GetFunction(ilParam.ValueDefaultFunc);
					ILInterpOperand interpOp = ILInterpreter.InterpFunction(func, null, compUnit);

					ilParam.ValueSpecialization = interpOp.Value.ToString();

					switch (interpOp.Value)
					{
					case long _:
					case double _:
						ilParam.ValueDefault = interpOp.Value.ToString();
						break;
					case string sval:
						ilParam.ValueDefault = '"' + sval + '"';
						break;
					}
					param.ValueDefault = ilParam.ValueDefault;
				}
			}
		}

	}
}
