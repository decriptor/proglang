﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;

namespace MJC.ILBackend.Canonical
{
	class ILNullCastPass : ILCanonicalizePass
	{
		public override void ProcessBasicBlock(ILBasicBlock basicBlock, ILContext funcContext)
		{
			for (var i = 0; i < basicBlock.Instructions.Count; i++)
			{
				ILInstruction ilInstruction = basicBlock.Instructions[i];
				if (ilInstruction is ILOperatorRefInstruction refInstruction)
				{
					var paramType = refInstruction.Type.ParamTypes[0];

					if (paramType is BuiltinSymbolType builtin && builtin.Builtin == BuiltinTypes.Null)
					{
						ILCallInstruction call = basicBlock.Instructions[i + 1] as ILCallInstruction;
						if (call == null)
							continue;

						TextSpan span = call.Span;

						basicBlock.Instructions.RemoveAt(i); // Remove operator ref
						basicBlock.Instructions.RemoveAt(i); // Remove call

						basicBlock.Instructions.Insert(i, new ILConvertTypeInstruction(call.RetOperand, call.Params[0], call.ReturnType, span));
					}
				}
			}
		}
	}
}
