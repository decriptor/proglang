﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using MJC.ILBackend.Context;

namespace MJC.ILBackend.Canonical
{
	class ILStackDeallocationPass : ILCanonicalizePass
	{
		public override void ProcessBasicBlock(ILBasicBlock basicBlock, ILContext funcContext)
		{
			if (basicBlock.ExitsFunction())
			{
				// Traverse callgraph and blocks to find non-deallocated variables

				ILCallGraphNode callGraphNode = funcContext.CallGraphNode;

				ILSubGraphNode subNode = callGraphNode.GetSubNode(basicBlock);

				Queue<Stack<ILOperand>> allocOperands = new Queue<Stack<ILOperand>>();

				Queue<ILSubGraphNode> predecessors = new Queue<ILSubGraphNode>();
				predecessors.Enqueue(subNode);

				while (predecessors.Count > 0)
				{
					ILSubGraphNode curNode = predecessors.Dequeue();
					Stack<ILOperand> curOpStack = new Stack<ILOperand>();

					foreach (ILInstruction instruction in curNode.Block.Instructions)
					{
						if (instruction is ILAllocStackInstruction allocStack)
						{
							curOpStack.Push(allocStack.RetOperand);
						}
						else if (instruction is ILDeallocStackInstruction deallocStack)
						{
							if (curOpStack.Count == 0)
							{
								curOpStack = allocOperands.Dequeue();
							}

							ILOperand top = curOpStack.Peek();
							Debug.Assert(top == deallocStack.Operand);
							curOpStack.Pop();
						}
					}

					allocOperands.Enqueue(curOpStack);

					foreach (ILSubGraphNode predecessor in curNode.Predecessors)
					{
						predecessors.Enqueue(predecessor);
					}
				}

				while (allocOperands.Count > 0)
				{
					Stack<ILOperand> ops = allocOperands.Dequeue();

					while (ops.Count > 0)
					{
						ILDeallocStackInstruction deallocStack = new ILDeallocStackInstruction(ops.Pop());
						basicBlock.Instructions.Insert(basicBlock.Instructions.Count - 1, deallocStack);
					}
				}
			}
		}
	}
}
