﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.ILBackend.Context;

namespace MJC.ILBackend.Canonical
{
	class ILCanonicalizer
	{
		private List<ILCanonicalizePass> _passes = new List<ILCanonicalizePass>();

		public void AddPass(ILCanonicalizePass pass)
		{
			_passes.Add(pass);
		}

		public bool Canonicalize(ILCompUnit compUnit)
		{
			foreach (ILCanonicalizePass pass in _passes)
			{
				pass.ProcessModule(compUnit);

				if (pass.ErrorOccured)
					return false;
			}

			compUnit.Stage = ILStage.Canonical;
			return true;
		}
	}
}
