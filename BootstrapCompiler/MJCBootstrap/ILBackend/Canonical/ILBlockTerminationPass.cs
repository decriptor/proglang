﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.ILBackend.Context;

namespace MJC.ILBackend.Canonical
{
	class ILBlockTerminationPass : ILCanonicalizePass
	{
		public override void ProcessBasicBlock(ILBasicBlock basicBlock, ILContext context)
		{
			ILInstruction last = basicBlock.Instructions.LastOrDefault();

			if (!basicBlock.IsTerminal())
			{
				basicBlock.Instructions.Add(new ILReturnInstruction(null, null));
			}
		}
	}
}
