﻿using System.Collections.Generic;
using MJC.General;
using MJC.ILBackend.Context;
using MJC.ILBackend.General;

namespace MJC.ILBackend.Canonical
{
	// Generate the required boilerplate
	class ILBoilerplatePass : ILCanonicalizePass
	{
		public override void ProcessModule(ILCompUnit compUnit)
		{
			bool hasGlobals = GenGlobals(compUnit);

			// If a module is created, skip '_start'
			if (CmdLine.GenerateModule)
				return;

			GenUUStart(compUnit, hasGlobals);

			//base.ProcessModule(compUnit);
		}

		bool GenGlobals(ILCompUnit compUnit)
		{
			// Are there globals that need to be initialized?
			bool hasGlobals = false;
			foreach (KeyValuePair<string, ILGlobal> ilGlobal in compUnit.Globals)
			{
				if ((ilGlobal.Value.Attributes.GlobalFlags & ILGlobalFlags.Constant) == 0)
				{
					hasGlobals = true;
					break;
				}
			}

			// When there are, pregen the _init_globals and _deinit_globals functions
			if (hasGlobals)
			{
				ILAttributes attributes = new ILAttributes();
				attributes.Linkage = ILLinkage.Hidden;

				// Create initializer for globals
				ILFunction initFunc = new ILFunction("_init_globals", null, null, attributes, null, false);
				ILBasicBlock bb = new ILBasicBlock("entry");
				initFunc.Blocks.Add(bb);
				compUnit.AddBlock(initFunc);

				ILContextGen.ProcessFunction(initFunc, compUnit);
				ILCallGraphGen.ProcessFunction(initFunc);

				// Create de-initializer for globals
				ILFunction deinitFunc = new ILFunction("_deinit_globals", null, null, attributes, null, false);
				bb = new ILBasicBlock("entry");
				deinitFunc.Blocks.Add(bb);
				compUnit.AddBlock(deinitFunc);

				ILContextGen.ProcessFunction(deinitFunc, compUnit);
				ILCallGraphGen.ProcessFunction(deinitFunc);
			}

			return hasGlobals;
		}

		void GenUUStart(ILCompUnit compUnit, bool hasGlobals)
		{
			ILAttributes attributes = new ILAttributes();
			attributes.Linkage = ILLinkage.Public;

			// Generate __start
			ILFunction startProc = new ILFunction("_start", null, null, attributes, null, false);

			ILBasicBlock bb = new ILBasicBlock("entry");
			startProc.Blocks.Add(bb);

			FunctionSymbolType funcType = new FunctionSymbolType(null, null, null, TypeAttributes.None);

			if (hasGlobals)
			{
				ILOperand initGlobalOp = new ILOperand("initGlobalFunc", funcType);
				bb.Instructions.Add(new ILFunctionRefInstruction(initGlobalOp, "_init_globals", funcType, null));
				bb.Instructions.Add(new ILCallInstruction(null, initGlobalOp, null, null, null));
			}

			ILOperand mainOp = new ILOperand("mainFunc", funcType);
			bb.Instructions.Add(new ILFunctionRefInstruction(mainOp, "_main", funcType, null));
			bb.Instructions.Add(new ILCallInstruction(null, mainOp, null, null, null));

			if (hasGlobals)
			{
				ILOperand deinitGlobalOp = new ILOperand("deinitGlobalFunc", funcType);
				bb.Instructions.Add(new ILFunctionRefInstruction(deinitGlobalOp, "_deinit_globals", funcType, null));
				bb.Instructions.Add(new ILCallInstruction(null, deinitGlobalOp, null, null, null));
			}

			compUnit.AddBlock(startProc);

			ILContextGen.ProcessFunction(startProc, compUnit);
			ILCallGraphGen.ProcessFunction(startProc);
		}
	}
}
