﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;
using MJC.ILBackend.General;

namespace MJC.ILBackend
{
	public enum ILStage
	{
		Invalid,
		Raw,
		Canonical,
		CompExec,
		Opt
	}

	public class ILCompUnit
	{
		public ILStage Stage;
		public string ILModule;

		public List<string> ImportNames = new List<string>();

		public ILCompUnit Builtins;
		public List<ILCompUnit> RefCompUnits;
		public List<ILCompUnit> Imports = new List<ILCompUnit>();
		
		public Dictionary<string, ILBlock> Blocks = new Dictionary<string, ILBlock>();
		public Dictionary<ScopeVariable, ILAggregate> Aggregates = new Dictionary<ScopeVariable, ILAggregate>();
		public Dictionary<string, ILAggregate> AggregatesMangled = new Dictionary<string, ILAggregate>();
		public Dictionary<ScopeVariable, List<ILAggregate>> AggregatesVariants = new Dictionary<ScopeVariable, List<ILAggregate>>();
		public Dictionary<ScopeVariable, VTable> VTables = new Dictionary<ScopeVariable, VTable>();
		public Dictionary<ScopeVariable, List<VTable>> VTablesVariants = new Dictionary<ScopeVariable, List<VTable>>();
		public Dictionary<string, VTable> VTablesMangled = new Dictionary<string, VTable>();
		public Dictionary<string, ILDelegate> Delegates = new Dictionary<string, ILDelegate>();
		public List<ILTypedef> Typedefs = new List<ILTypedef>();
		public Dictionary<string, ILEnum> Enums = new Dictionary<string, ILEnum>();
		public Dictionary<string, ILGlobal> Globals = new Dictionary<string, ILGlobal>();
		public Dictionary<ScopeVariable, ILTemplateInstance> TemplateInstances = new Dictionary<ScopeVariable, ILTemplateInstance>();
		public List<string> Aliases = new List<string>();

		public ILCompUnitContext Context;

		public void AddVTable(VTable vtable, bool isTemplateInstance = false)
		{
			VTables.Add(vtable.Iden, vtable);

			ScopeVariable baseScopeVar = vtable.Iden.GetBaseTemplate();
			if (!isTemplateInstance && baseScopeVar.Name is TemplateDefinitionName)
			{
				if (!VTablesVariants.ContainsKey(baseScopeVar))
					VTablesVariants.Add(baseScopeVar, new List<VTable>());

				VTablesVariants[baseScopeVar].Add(vtable);
			}

			if (!(vtable.Iden.Name is TemplateDefinitionName))
				VTablesMangled.Add(vtable.MangledIden, vtable);
		}

		public VTable GetVTable(ScopeVariable iden)
		{
			VTables.TryGetValue(iden, out VTable res);
			return res;
		}

		public VTable GetVTable(string iden)
		{
			VTablesMangled.TryGetValue(iden, out VTable res);
			return res;
		}

		public List<VTable> GetVTableVariants(ScopeVariable iden)
		{
			VTablesVariants.TryGetValue(iden, out List<VTable> tables);
			return tables;
		}

		public void AddBlock(ILBlock block)
		{
			if (block is ILFunction func)
			{
				Blocks.Add(func.Identifier, block);
			}
			else if (block is ILOperator op)
			{
				Blocks.Add(op.GetTypedIdentifier(), op);
			}
		}

		public ILFunction GetFunction(string name)
		{
			ILBlock block;
			if (Blocks.TryGetValue(name, out block))
			{
				return block as ILFunction;
			}

			return null;
		}

		public ILFunction GetExternalFunction(string name)
		{
			foreach (ILCompUnit compUnit in RefCompUnits)
			{
				ILFunction func = compUnit.GetFunction(name);
				if (func != null)
				{
					return func;
				}
			}

			foreach (ILCompUnit compUnit in Imports)
			{
				ILFunction func = compUnit.GetFunction(name);
				if (func != null)
				{
					return func;
				}
			}

			return null;
		}

		public void RemoveFunction(string name)
		{
			Blocks.Remove(name);
		}

		public ILOperator GetOperator(string name)
		{
			ILBlock block;
			if (Blocks.TryGetValue(name, out block))
			{
				return block as ILOperator;
			}

			return null;
		}

		public void AddAggregate(ILAggregate aggregate, bool isTemplateInstance = false)
		{
			Aggregates.Add(aggregate.Identifier, aggregate);

			if (!isTemplateInstance)
			{
				if (!AggregatesVariants.ContainsKey(aggregate.BaseIdentifier))
					AggregatesVariants.Add(aggregate.BaseIdentifier, new List<ILAggregate>());

				AggregatesVariants[aggregate.BaseIdentifier].Add(aggregate);
			}

			if (aggregate.TemplateParams == null)
				AggregatesMangled.Add(aggregate.MangledIdentifier, aggregate);
		}

		public ILAggregate GetAggregate(ScopeVariable iden)
		{
			bool found = Aggregates.TryGetValue(iden, out ILAggregate res);
			return found ? res : null;
		}

		public ILAggregate GetAggregate(string name)
		{
			bool found = AggregatesMangled.TryGetValue(name, out ILAggregate res);
			return found ? res : null;
		}

		public List<ILAggregate> GetAggregateVariants(ScopeVariable iden)
		{
			bool found = AggregatesVariants.TryGetValue(iden, out List<ILAggregate> res);
			return found ? res : null;
		}

		public void AddEnum(ILEnum ilEnum)
		{
			Enums.Add(ilEnum.MangledIdentifier, ilEnum);
		}

		public ILEnum GetEnum(string name)
		{
			bool found = Enums.TryGetValue(name, out var res);
			return found ? res : null;
		}

		public void AddDelegate(ILDelegate del)
		{
			Delegates.Add(del.MangledIdentifier, del);
		}

		public void AddGlobal(ILGlobal global)
		{
			Globals.Add(global.MangledIdentifier, global);
		}

		public ILGlobal GetGlobal(string iden)
		{
			if (Globals.TryGetValue(iden, out var global))
			{
				return global;
			}
			return null;
		}

		public ILGlobal GetExternalGlobal(string name)
		{
			foreach (ILCompUnit compUnit in RefCompUnits)
			{
				ILGlobal func = compUnit.GetGlobal(name);
				if (func != null)
				{
					return func;
				}
			}

			foreach (ILCompUnit compUnit in Imports)
			{
				ILGlobal func = compUnit.GetGlobal(name);
				if (func != null)
				{
					return func;
				}
			}

			return null;
		}

		public void AddTemplateInstance(ILTemplateInstance instance)
		{
			TemplateInstances.Add(instance.Iden, instance);
		}
	}
}
