﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.General;
using MJC.ILBackend.Context;

namespace MJC.ILBackend
{
	public abstract class ILPass
	{
		protected ILCompUnit _compUnit;

		public virtual void ProcessModule(ILCompUnit compUnit)
		{
			_compUnit = compUnit;

			foreach (KeyValuePair<ScopeVariable, VTable> pair in _compUnit.VTables)
			{
				ProcessVTable(pair.Value);
			}

			foreach (KeyValuePair<string, ILBlock> pair in compUnit.Blocks)
			{
				switch (pair.Value)
				{
				case ILFunction function:
					ProcessFunction(function);
					break;
				case ILOperator op:
					ProcessOperator(op);
					break;
				}
			}
		}


		public virtual void ProcessFunction(ILFunction function)
		{
			// Skip uninstantiated function
			if (function.IsUninstantiated)
				return;

			foreach (ILBasicBlock block in function.Blocks)
			{
				ProcessBasicBlock(block, function.Context);
			}
		}

		public virtual void ProcessOperator(ILOperator op)
		{
			foreach (ILBasicBlock block in op.Blocks)
			{
				ProcessBasicBlock(block, op.Context);
			}
		}

		public virtual void ProcessBasicBlock(ILBasicBlock basicBlock, ILContext funcContext)
		{
		}

		public virtual void ProcessVTable(VTable vTable)
		{

		}

	}
}
