﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.General
{
	public class CompilerContext
	{
		public SymbolTable Symbols;
		public ImportTable Imports;
		public FileImports FileImport;
		public Dictionary<ScopeVariable, List<VTable>> VTables = new Dictionary<ScopeVariable, List<VTable>>();

		public CompilerContext()
		{
			Symbols = new SymbolTable(this);
			Imports = new ImportTable(this);
		}

		public void SetCurFileImportTable(FileImports importTable)
		{
			FileImport = importTable;
		}

		public Symbol FindDefinition(Scope scope, Identifier name, int location, Scope qualifiedScope)
		{
			Symbol symbol = Symbols.FindDefinition(scope, name, location, qualifiedScope);
			if (symbol != null)
				return symbol;

			// Not defined in module, search in imports
			return FileImport != null ? Imports.FindDefinition(scope, name, location, qualifiedScope, FileImport) : null;
		}

		public Symbol FindDefinition(Scope scope, ScopeVariable scopeVar, int location)
		{
			return FindDefinition(scope, scopeVar.Name, location, scopeVar.Scope);
		}

		public Symbol FindFunction(Scope scope, Identifier funcName, SymbolType recType, List<SymbolType> argTypes, Scope qualifiedScope, int location)
		{
			Symbol symbol = Symbols.FindFunctionWithParameters(scope, funcName, recType, argTypes, qualifiedScope);
			if (symbol != null)
				return symbol;

			// Not defined in module, search in imports
			return FileImport != null ? Imports.FindFunction(scope, funcName, recType, argTypes, qualifiedScope, location, FileImport) : null;
		}

		public Symbol FindFunction(Scope scope, ScopeVariable scopeVar, SymbolType recType, List<SymbolType> argTypes, int location)
		{
			return FindFunction(scope, scopeVar.Name, recType, argTypes, scopeVar.Scope, location);
		}

		public Symbol FindType(Scope scope, Identifier name, Scope qualifiedScope = null)
		{
			Symbol symbol = Symbols.FindType(scope, name, qualifiedScope);
			if (symbol != null)
				return symbol;

			// Not defined in module, search in imports
			return FileImport != null ? Imports.FindType(scope, name, qualifiedScope, FileImport) : null;
		}

		public Symbol FindType(Scope scope, ScopeVariable scopeVariable)
		{
			return FindType(scope, scopeVariable.Name, scopeVariable.Scope);
		}

		public Symbol FindTemplate(Scope scope, TemplateInstanceName name, Scope qualifiedScope)
		{
			Symbol symbol = Symbols.FindTemplate(scope, name, qualifiedScope);
			if (symbol != null)
				return symbol;

			return FileImport != null ? Imports.FindTemplate(scope, name, qualifiedScope, FileImport) : null;
		}

		public Symbol FindTemplate(Scope scope, ScopeVariable scopeVar)
		{
			if (scopeVar.Name is TemplateInstanceName instName)
				return FindTemplate(scope, instName, scopeVar.Scope);
			return null;
		}

		public void AddVTable(VTable table)
		{
			ScopeVariable baseScopeVar = table.Iden.GetBaseTemplate();
			if (!VTables.ContainsKey(baseScopeVar))
				VTables.Add(baseScopeVar, new List<VTable>());

			VTables[baseScopeVar].Add(table);
		}

		public VTable GetVTable(ScopeVariable scopeVar)
		{
			ScopeVariable baseScope = scopeVar.GetBaseTemplate();
			bool res = VTables.TryGetValue(baseScope, out List<VTable> tables);
			if (!res)
				return null;

			foreach (VTable table in tables)
			{
				if (table.Iden == scopeVar)
					return table;
			}

			return null;
		}

		public List<VTable> GetTemplateVTables(ScopeVariable scopeVar)
		{
			VTables.TryGetValue(scopeVar, out List<VTable> tables);
			return tables;
		}

		public void LoadImportModules()
		{
			Imports.LoadModules();
		}

		public void GenerateImportedILCompUnits()
		{
			Imports.GenerateImportedILCompUnits();
		}

		public string PrepareForLinking()
		{
			return Imports.PrepareForLinking();
		}

		public void CleanupAfterLinking()
		{
			Imports.CleanupAfterLinking();
		}
	}
}
