﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.SyntaxTree.SemanticAnalysis;

namespace MJC.General
{
	public enum SymbolKind
	{
		Unknown,
		Struct,
		Interface,
		Union,
		Enum,
		EnumMember,
		Variable,
		LocalVar,
		Typedef,
		TypeAlias,
		Function,
		Method,
		Receiver,
		Delegate,
		TemplateParam,
		TemplateInstance,
	}

	public class Symbol
	{
		public Identifier Identifier;
		public SymbolKind Kind;
		public SymbolType Type;
		public int Location;
		public string MangledName;
		public Scope ImplScope;
		public Scope Scope;
		public ScopeVariable ScopeVar;
		public CompilerContext BaseTable;

		public SemanticVisibility Visibility;
		public SemanticAttributes Attribs;

		public Dictionary<string, CompileAttribute> CompileAttribs = new Dictionary<string, CompileAttribute>();

		public Symbol Parent;
		public List<Symbol> Children = new List<Symbol>();
		public int TemplateIdx = -1;

		public Symbol(Scope scope, Scope implScope, Identifier identifier, SymbolKind kind)
		{
			Identifier = identifier;
			ImplScope = implScope;
			Scope = scope;
			ScopeVar = new ScopeVariable(scope, identifier);
			Kind = kind;
		}

		public void UpdateType()
		{
			if (Type == null)
				return;

			switch (Type)
			{
			case TypeofSymbolType typeofSyntax:
				Type = typeofSyntax.Identifier.Context.Type;
				break;
			case AggregateSymbolType aggregate when aggregate.Symbol == null:
				aggregate.Symbol = BaseTable.FindDefinition(Scope, aggregate.Identifier, -1);
				break;
			case InterfaceSymbolType interfaceType when interfaceType.Symbol == null:
				interfaceType.Symbol = BaseTable.FindDefinition(Scope, interfaceType.Identifier, -1);
				break;
			case EnumSymbolType enumType:
			{
				if (enumType.BaseType == null)
				{
					Symbol symbol = BaseTable.FindDefinition(Scope, enumType.Identifier, -1);
					Type = symbol.Type;
				}

				break;
			}
			case UnknownSymbolType unknownType:
			{
				Symbol symbol = BaseTable.FindDefinition(Scope, unknownType.Identifier, -1);
				Type = symbol.Type;
				break;
			}
			case SelfSymbolType selfType:
			{
				Symbol symbol = BaseTable.FindDefinition(Scope, selfType.Identifier, -1);
				Type = symbol.Type;
				break;
			}
			default:
				Type = Type.GetUpdatedType(ImplScope, Scope, BaseTable);
				break;
			}
		}

		public bool IsType()
		{
			switch (Kind)
			{
			case SymbolKind.Struct:
			case SymbolKind.Interface:
			case SymbolKind.Union:
			case SymbolKind.Enum:
			case SymbolKind.Typedef:
			case SymbolKind.TypeAlias:
			case SymbolKind.Delegate:
			case SymbolKind.TemplateParam:
			case SymbolKind.TemplateInstance:
				return true;
			default:
				return false;
			}
		}

		public override string ToString()
		{
			return ScopeVar.ToString();
		}

		public List<Symbol> GetChildrenWithType<T>()
		{
			List<Symbol> symbols = new List<Symbol>();
			foreach (Symbol child in Children)
			{
				if (child.Type is T)
					symbols.Add(child);
			}
			return symbols;
		}

		public List<Symbol> GetChildrenOfKind(SymbolKind kind)
		{
			List<Symbol> symbols = new List<Symbol>();
			foreach (Symbol child in Children)
			{
				if (child.Kind == kind)
					symbols.Add(child);
			}
			return symbols;
		}
	}
}
