﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MJC.SyntaxTree;

namespace MJC.General
{
	// Name mangling is independent from the compiler and can be changed with another implementation, depending on future ABI changes

	public static class NameMangling
	{
		// TODO: Add backtracking
		public static string Mangle(Symbol symbol)
		{
			if (symbol.MangledName != null)
				return symbol.MangledName;

			if (symbol.Identifier is IdentifierName idenName && 
			    idenName.Iden == "main" && 
			    symbol.Kind == SymbolKind.Function)
				return "_main";

			string mangled = MangleName(symbol);

			switch (symbol.Kind)
			{
			case SymbolKind.Struct:
				mangled += "TS";
				break;
			case SymbolKind.Interface:
				mangled += "TI";
				break;
			case SymbolKind.Union:
				mangled += "TU";
				break;
			case SymbolKind.Enum:
				mangled += "TE";
				break;
			case SymbolKind.Delegate:
				mangled += "TD";
				break;
			case SymbolKind.Variable:
				mangled += MangleType(symbol.Type, true);
				break;
			case SymbolKind.EnumMember:
			{
				if (symbol.Type != null)
					mangled += MangleType(symbol.Type);
				break;
			}
			case SymbolKind.Typedef:
				mangled += "TT";
				mangled += MangleType(symbol.Type);
				break;
			case SymbolKind.TypeAlias:
				mangled += "TL" + MangleType(symbol.Type);
				break;
			case SymbolKind.Function:
			case SymbolKind.Method:
				mangled += MangleFunctionType(symbol.Type as FunctionSymbolType);
				break;
			case SymbolKind.TemplateInstance:
				mangled += "T__I";
				break;
			}

			return mangled;
		}

		public static string MangleName(Symbol symbol)
		{
			Scope scope = symbol.Scope;
			string mangled = "_M";
			foreach (Identifier name in scope.Names)
			{
				mangled += MangleIdentifier(name);
			}

			return mangled + MangleIdentifier(symbol.Identifier);
		}

		public static string MangleScopeVar(ScopeVariable scopeVar)
		{
			Scope scope = scopeVar.Scope;
			string mangled = "";
			foreach (Identifier name in scope.Names)
			{
				mangled += MangleIdentifier(name);
			}

			return mangled + MangleIdentifier(scopeVar.Name);
		}

		public static string MangleIdentifier(Identifier iden)
		{
			if (iden is IdentifierName idenName)
			{
				return $"{idenName.Iden.Length}{idenName.Iden}";
			}
			else if (iden is TemplateInstanceName inst)
			{
				string mangled = $"__I{inst.Iden.Length}{inst.Iden}{inst.Arguments.Count}";
				foreach (TemplateArgument arg in inst.Arguments)
				{
					if (arg.Value == null)
					{
						mangled += 'T';
						mangled += MangleType(arg.Type);
					}
					else
					{
						mangled += 'V';
						mangled += MangleType(arg.Type);
						mangled += arg.Value.Length;
						mangled += '_';
						mangled += arg.Value;
						mangled += 'Z';
					}
				} 
				return mangled;
			}
			else if (iden is TemplateDefinitionName def)
			{
				string mangled = $"__T{def.Iden.Length}{def.Iden}{def.Parameters.Count}";
				foreach (TemplateParameter param in def.Parameters)
				{
					if (param.ValueName == null)
					{
						mangled += "T" + MangleType(param.Type);
						if (param.TypeSpecialization != null)
							mangled += 'G' + MangleType(param.TypeSpecialization);
						if (param.TypeDefault != null)
							mangled += 'H' + MangleType(param.TypeDefault);
					}
					else
					{
						mangled += $"V{param.ValueName.Length}{param.ValueName}" + MangleType(param.Type);
						if (param.ValueSpecialization != null)
							mangled += $"G{param.ValueSpecialization.Length}_{param.ValueSpecialization}";
						if (param.ValueDefault != null)
							mangled += $"H{param.ValueDefault.Length}_{param.ValueDefault}";
					}
				}
				return mangled;
			}

			return "";
		}

		private static string MangleFunctionType(FunctionSymbolType type)
		{
			bool isMethod = type.ReceiverType != null;
			string mangled = isMethod ? "M" : "F";

			// TODO: Calling convention
			mangled += 'm'; // <- Temp

			// TODO: Function attributes

			if (type.ReceiverType != null)
			{
				mangled += 'N';
				mangled += MangleType(type.ReceiverType);
			}

			if (type.ParamTypes != null && type.ParamTypes.Count > 0)
			{
				foreach (SymbolType param in type.ParamTypes)
				{
					// TODO: parameter attributes

					mangled += MangleType(param);
				}
			}

			SymbolType last = type.ParamTypes?.LastOrDefault();
			if (last is VariadicSymbolType)
				mangled += 'Y';
			else
				mangled += 'Z';

			if (type.ReturnType != null)
				mangled += MangleType(type.ReturnType);

			return mangled;
		}

		public static string MangleType(SymbolType type, bool isValue = false)
		{
			string mangled = isValue ? "V" : "";

			if (type.Attributes != TypeAttributes.None)
			{
				// TODO: Incomplete
				if ((type.Attributes & TypeAttributes.Const) != 0)
					mangled += "No";
				if ((type.Attributes & TypeAttributes.Immutable) != 0)
					mangled += "Ny";
			}

			switch (type)
			{
			case FunctionSymbolType functionType:
			{
				mangled += MangleFunctionType(functionType);
				break;
			}
			case ArraySymbolType arrayType:
			{
				mangled += 'A';
				mangled += MangleType(arrayType.BaseType);
				break;
			}
			case PointerSymbolType pointerType:
			{
				mangled += 'P';
				mangled += MangleType(pointerType.BaseType);
				break;
			}
			case TupleSymbolType tupleType:
			{
				mangled += 'M';
				foreach (SymbolType subType in tupleType.SubTypes)
				{
					mangled += MangleType(subType);
				}
				mangled += 'Z';
				break;
			}
			case BuiltinSymbolType builtinType:
			{
				switch (builtinType.Builtin)
				{
				case BuiltinTypes.Bool:
					mangled += 'b';
					break;
				case BuiltinTypes.I8:
					mangled += 'g';
					break;
				case BuiltinTypes.I16:
					mangled += 's';
					break;
				case BuiltinTypes.I32:
					mangled += 'i';
					break;
				case BuiltinTypes.I64:
					mangled += 'l';
					break;
				case BuiltinTypes.U8:
					mangled += 'h';
					break;
				case BuiltinTypes.U16:
					mangled += 't';
					break;
				case BuiltinTypes.U32:
					mangled += 'j';
					break;
				case BuiltinTypes.U64:
					mangled += 'm';
					break;
				case BuiltinTypes.F32:
					mangled += 'f';
					break;
				case BuiltinTypes.F64:
					mangled += 'd';
					break;
				case BuiltinTypes.Char:
					mangled += 'c';
					break;
				case BuiltinTypes.WChar:
					mangled += 'w';
					break;
				case BuiltinTypes.Rune:
					mangled += 'r';
					break;
				case BuiltinTypes.String:
				case BuiltinTypes.StringLiteral:
					mangled += 'u';
					break;
				case BuiltinTypes.Void:
					mangled += 'v';
					break;
				}

				break;
			}
			case AggregateSymbolType aggregateType:
			{
				switch (aggregateType.Type)
				{
				case AggregateTypes.Struct:
					mangled += 'S';
					break;
				case AggregateTypes.Union:
					mangled += 'U';
					break;
				}

				if (string.IsNullOrEmpty(aggregateType.MangledIdentifier))
				{
					Symbol aggregateSym = aggregateType.Symbol;
					aggregateSym.MangledName = Mangle(aggregateSym);
				}

				string mangleIden = aggregateType.MangledIdentifier;
				mangled += mangleIden.Remove(mangleIden.Length - 2);
				break;
			}
			case InterfaceSymbolType interfaceType:
			{
				mangled += 'I';

				if (interfaceType.MangledIdentifier == null)
				{
					Symbol aggregateSym = interfaceType.Symbol;
					aggregateSym.MangledName = Mangle(aggregateSym);
				}

				string mangleIden = interfaceType.MangledIdentifier;
				mangled += mangleIden.Remove(mangleIden.Length - 2);
				break;
			}
			case EnumSymbolType enumType:
			{
				mangled += 'E';

				if (enumType.MangledIdentifier == null)
				{
					Symbol aggregateSym = enumType.Symbol;
					aggregateSym.MangledName = Mangle(aggregateSym);
				}

				string mangleIden = enumType.MangledIdentifier;
				mangled += mangleIden.Remove(mangleIden.Length - 2);
				break;
			}
			case MemoryLocSymbolType memoryLoc:
				// A MemoryLoc is interpreted as a pointer for the name mangling
				mangled += 'P' + MangleType(memoryLoc.BaseType);
				break;
			case DelegateSymbolType delegateType:
			{
				mangled += 'D';
				string mangleIden = delegateType.MangledIdentifier;
				mangled += mangleIden.Remove(mangleIden.Length - 2);
				break;
			}
			case BitFieldSymbolType bitfield:
			{
				mangled += 'B';
				mangled += MangleType(bitfield.BaseType);
				mangled += bitfield.Bits.ToString();
				mangled += 'Z';
				break;
			}
			case TemplateInstanceSymbolType instType:
			{
				mangled += "J";
				mangled += MangleScopeVar(instType.Iden);
				break;
			}
			case TemplateParamSymbolType paramType:
			{
				mangled += 'K';
				mangled += MangleIdentifier(paramType.Iden.Name);
				break;
			}
			}

			return mangled;
		}


		public static void Demangle(string mangled, out Scope scope, out Symbol symbol)
		{
			scope = new Scope();
			symbol = null;

			if (!mangled.StartsWith("_M"))
				return;

			// demangle name
			int pos = 2;
			Identifier iden = null;
			while (pos < mangled.Length && (char.IsDigit(mangled[pos]) || mangled[pos] == '_'))
			{
				if (iden != null)
					scope.Names.Add(iden);
				iden = DemangleIdentifier(mangled, ref pos);
			}

			// Demangle type
			(SymbolType type, SymbolKind kind) = ExtractType(mangled, ref pos);

			symbol = new Symbol(new Scope(scope), null, iden, kind);
			symbol.Type = type;
			symbol.MangledName = mangled;
		}

		public static ScopeVariable MangledToScopeVariable(string mangled, ref int pos)
		{
			if (mangled[pos] == '_' && mangled[pos + 1] == 'M')
				pos += 2;

			Identifier iden = null;
			List<Identifier> scopeNames = new List<Identifier>();
			while (pos < mangled.Length && char.IsDigit(mangled[pos]))
			{
				if (iden != null)
					scopeNames.Add(iden);
				iden = DemangleIdentifier(mangled, ref pos);
			}
			if (scopeNames.Count > 0)
				return new ScopeVariable(new Scope(scopeNames), iden);
			return new ScopeVariable(iden);
		}

		public static Identifier DemangleIdentifier(string mangled, ref int offset)
		{
			if (mangled[offset] == '_' && mangled[offset + 1] == 'M')
				offset += 2;

			if (mangled[offset] == '_' && mangled[offset + 1] == '_' && mangled[offset + 2] == 'I')
			{
				offset += 3;
				int len = GetElemLength(mangled, ref offset);
				string name = mangled.Substring(offset, len);
				offset += len;
				
				int numArgs = GetElemLength(mangled, ref offset);
				TemplateInstanceName iden = new TemplateInstanceName(name, numArgs);
				for (int i = 0; i < numArgs; i++)
				{
					char c = mangled[offset];
					++offset;
					if (c == 'V')
					{
						(SymbolType type, _) = ExtractType(mangled, ref offset);

						int valLen = GetElemLength(mangled, ref offset);
						++offset; // skip '_'
						string value = mangled.Substring(offset, valLen);
						offset += valLen;

						TemplateArgument arg = new TemplateArgument(i, type, value);
						iden.Arguments[i] = arg;
					}
					else if (c == 'T')
					{
						(SymbolType type, _) = ExtractType(mangled, ref offset);
						TemplateArgument arg = new TemplateArgument(i, type);
						iden.Arguments[i] = arg;
					}
				}

				return iden;
			}
			else if (mangled[offset] == '_' && mangled[offset + 1] == '_' && mangled[offset + 2] == 'T')
			{
				offset += 3;
				int len = GetElemLength(mangled, ref offset);
				string name = mangled.Substring(offset, len);
				offset += len;

				int numParams = GetElemLength(mangled, ref offset);
				TemplateDefinitionName iden = new TemplateDefinitionName(name, numParams);
				for (int i = 0; i < numParams; i++)
				{
					char c = mangled[offset];
					++offset;
					if (c == 'V')
					{
						int valLen = GetElemLength(mangled, ref offset);
						string paramName = mangled.Substring(offset, valLen);
						offset += valLen;

						(SymbolType type, _) = ExtractType(mangled, ref offset);

						string valueSepcialization = null;
						if (mangled[offset] == 'G')
						{
							++offset;
							valLen = GetElemLength(mangled, ref offset);
							++offset; // skip '_'
							valueSepcialization = mangled.Substring(offset, valLen);
							offset += valLen;
						}

						string valueDefault = null;
						if (mangled[offset] == 'H')
						{
							++offset;
							valLen = GetElemLength(mangled, ref offset);
							++offset; // skip '_'
							valueDefault = mangled.Substring(offset, valLen);
							offset += valLen;
						}

						TemplateParameter param = new TemplateParameter(paramName, type, i, valueSepcialization, valueDefault);
						iden.Parameters[i] = param;
					}
					else if (c == 'T')
					{
						(SymbolType type, _) = ExtractType(mangled, ref offset);

						SymbolType typeSpecialization = null;
						if (mangled[offset] == 'G')
						{
							++offset;
							(typeSpecialization, _) = ExtractType(mangled, ref offset);
						}

						SymbolType typeDefault = null;
						if (mangled[offset] == 'H')
						{
							++offset;
							(typeDefault, _) = ExtractType(mangled, ref offset);
						}

						TemplateParameter param = new TemplateParameter(type, i, typeSpecialization, typeDefault);
					}
				}

				return iden;
			}
			else
			{
				int len = GetElemLength(mangled, ref offset);
				string name = mangled.Substring(offset, len);
				offset += len;

				return new IdentifierName(name);
			}
		}

		public static int GetElemLength(string mangled, ref int offset)
		{
			string count = "";
			while (Char.IsDigit(mangled[offset]))
			{
				count += mangled[offset];
				++offset;
			}
			return Int32.Parse(count);
		}

		public static (SymbolType, SymbolKind) ExtractType(string mangled, ref int start)
		{
			// Special case: enum member
			if (start == mangled.Length)
				return (null, SymbolKind.EnumMember);

			if (start == -1)
			{
				int idx = 2;
				while (true)
				{
					string count = "";
					while (Char.IsDigit(mangled[idx]))
					{
						count += mangled[idx++];
					}
					if (count.Length == 0)
						break;

					int len = Int32.Parse(count);

					idx += len;
				}
				start = idx;
			}
			
			switch (mangled[start])
			{
			case 'F':
			{
				++start;
				char callConv = mangled[start];
				++start;

				SymbolType recieverType = null;
				if (mangled[start] == 'N')
				{
					++start;
					(recieverType, _) = ExtractType(mangled, ref start);
				}

				List<SymbolType> argTypes = IsMangleParamClose(mangled[start]) ? null : new List<SymbolType>();
				while (!IsMangleParamClose(mangled[start]))
				{
					(SymbolType type, _) = ExtractType(mangled, ref start);
					argTypes.Add(type);
				}
				++start;

				SymbolType retType = null;
				if (start < mangled.Length)
				{
					(retType, _) = ExtractType(mangled, ref start);
				}

				return (SymbolType.FunctionType(recieverType, argTypes, retType), recieverType == null ? SymbolKind.Function : SymbolKind.Method);
			}
			case 'V':
			{
				++start;
				(SymbolType resType, _) = ExtractType(mangled, ref start);
				return (resType, SymbolKind.Variable);
			}
			case 'A':
			{
				++start;
				(SymbolType subType, _) = ExtractType(mangled, ref start);
				return (SymbolType.ArrayType(subType), SymbolKind.Unknown);
			}
			case 'P':
			{
				++start;
				(SymbolType subType, _) = ExtractType(mangled, ref start);
				return (SymbolType.PointerType(subType), SymbolKind.Unknown);
			}
			case 'R':
			{
				++start;
				(SymbolType subType, _) = ExtractType(mangled, ref start);
				return (SymbolType.ReferenceType(subType), SymbolKind.Unknown);
			}
			case 'M':
			{
				++start;
				List<SymbolType> subTypes = new List<SymbolType>();
				while (mangled[start] != 'Z')
				{
					(SymbolType subType, _) = ExtractType(mangled, ref start);
					subTypes.Add(subType);
				}
				++start;
				return (SymbolType.TupleType(subTypes), SymbolKind.Unknown);
			}
			case 'N':
			{
				++start;
				if (start == mangled.Length)
					return (null, SymbolKind.Receiver);
				(SymbolType receiverType, _) = ExtractType(mangled, ref start);
				return (receiverType, SymbolKind.Receiver);
			}
			case 'S':
			{
				++start;
				if (start == mangled.Length)
					return (null, SymbolKind.Struct);
				ScopeVariable scopeVar = MangledToScopeVariable(mangled, ref start);
				return (SymbolType.AggregateType(scopeVar, AggregateTypes.Struct), SymbolKind.Struct);
			}
			case 'I':
			{
				++start;
				if (start == mangled.Length)
					return (null, SymbolKind.Interface);
				ScopeVariable scopeVar = MangledToScopeVariable(mangled, ref start);
				return (SymbolType.InterfaceType(scopeVar), SymbolKind.Interface);
			}
			case 'U':
			{
				++start;
				if (start == mangled.Length)
					return (null, SymbolKind.Union);
				ScopeVariable scopeVar = MangledToScopeVariable(mangled, ref start);
				return (SymbolType.AggregateType(scopeVar, AggregateTypes.Union), SymbolKind.Union);
			}
			case 'E':
			{
				++start;
				if (start == mangled.Length)
					return (null, SymbolKind.Enum);
				ScopeVariable scopeVar = MangledToScopeVariable(mangled, ref start);
				return (SymbolType.EnumType(scopeVar, null), SymbolKind.Enum);
			}
			case 'D':
			{
				++start;
				if (start == mangled.Length)
					return (null, SymbolKind.Delegate);
				ScopeVariable scopeVar = MangledToScopeVariable(mangled, ref start);
				return (SymbolType.DelegateType(scopeVar), SymbolKind.Delegate);
			}
			case 'B':
			{
				++start;
				(SymbolType baseType, _) = ExtractType(mangled, ref start);
				string bitsStr = "";
				while (mangled[start] != 'Z')
				{
					bitsStr += mangled[start];
					++start;
				}
				byte bits = byte.Parse(bitsStr);
				return (SymbolType.BitFieldType(baseType, bits), SymbolKind.Unknown);
			}
			case 'T':
			{
				++start;
				switch (mangled[start])
				{
				case 'S':
					++start;
					return (null, SymbolKind.Struct);
				case 'U':
					++start;
					return (null, SymbolKind.Union);
				case 'I':
					++start;
					return (null, SymbolKind.Interface);
				case 'E':
					++start;
					return (null, SymbolKind.Enum);
				case 'D':
					++start;
					return (null, SymbolKind.Delegate);
				default:
					return (null, SymbolKind.Unknown);
				}
			}
			default:
			{
				BuiltinSymbolType builtin = GetBuiltin(mangled[start]);
				++start;
				return (builtin, SymbolKind.Unknown);
			}
			}
		}


		private static bool IsMangleParamClose(char c)
		{
			return c == 'Z' || c == 'X' || c == 'Y';
		}

		private static BuiltinSymbolType GetBuiltin(char c)
		{
			switch (c)
			{
				case 'v': return SymbolType.BuiltinType(BuiltinTypes.Void);
				case 'g': return SymbolType.BuiltinType(BuiltinTypes.I8);
				case 'h': return SymbolType.BuiltinType(BuiltinTypes.U8);
				case 's': return SymbolType.BuiltinType(BuiltinTypes.I16);
				case 't': return SymbolType.BuiltinType(BuiltinTypes.U16);
				case 'i': return SymbolType.BuiltinType(BuiltinTypes.I32);
				case 'j': return SymbolType.BuiltinType(BuiltinTypes.U32);
				case 'l': return SymbolType.BuiltinType(BuiltinTypes.I64);
				case 'm': return SymbolType.BuiltinType(BuiltinTypes.U64);
				case 'f': return SymbolType.BuiltinType(BuiltinTypes.F32);
				case 'd': return SymbolType.BuiltinType(BuiltinTypes.F64);
				case 'b': return SymbolType.BuiltinType(BuiltinTypes.Bool);
				case 'c': return SymbolType.BuiltinType(BuiltinTypes.Char);
				case 'w': return SymbolType.BuiltinType(BuiltinTypes.WChar);
				case 'r': return SymbolType.BuiltinType(BuiltinTypes.Rune);
				case 'u': return SymbolType.BuiltinType(BuiltinTypes.String);
				case 'n': return SymbolType.BuiltinType(BuiltinTypes.Null);
				default: return null;
			}
		}


	}
}
