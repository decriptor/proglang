﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MJC.General
{
	public enum InlineMode
	{
		None,
		OnlyAlways,
	}

	public static class CompileSettings
	{
		public static InlineMode Inline = InlineMode.OnlyAlways;
	}
}
