﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MJC.Module;
using MJC.SyntaxTree.SemanticAnalysis;

// TODO: import aliases
namespace MJC.General
{
	public class FileImportDirective
	{
		public bool IsStatic; // requires full scope
		public bool IsPublic; // exposed to module
		public bool AllSymbols;
		public List<string> RequestedSymbols;
		public string ModuleName;
		public string PackageName;
		public int FileLocation;
		public Scope ContainingScope;
	}

	public class FileImports
	{
		public List<FileImportDirective> Directives = new List<FileImportDirective>();
		public HashSet<string> UsedModules = new HashSet<string>();

		public void AddImport(Scope scope, int location, string module, List<string> symbols = null)
		{
			FileImportDirective directive = new FileImportDirective();
			int lastDot = module.LastIndexOf('.');
			if (lastDot == -1)
			{
				directive.ModuleName = module;
			}
			else
			{
				directive.PackageName = module.Substring(0, lastDot);
				directive.ModuleName = module.Substring(lastDot + 1);
			}

			directive.FileLocation = location;
			directive.ContainingScope = scope;

			if (symbols == null)
			{
				directive.AllSymbols = true;
			}
			else
			{
				directive.RequestedSymbols.AddRange(symbols);
			}
			Directives.Add(directive);
		}

		public void AddUsedModule(FileImportDirective directive)
		{
			string fullIden = "";
			if (!string.IsNullOrEmpty(directive.PackageName))
				fullIden += directive.PackageName + '.';
			fullIden += directive.ModuleName;

			if (!UsedModules.Contains(fullIden))
				UsedModules.Add(fullIden);
		}
	}

	public class ImportTable
	{
		private List<FileImports> _fileImports = new List<FileImports>();
		private Dictionary<string, string[]> _moduleIdens = new Dictionary<string, string[]>();
		private SymbolTable _symbolTable;
		private List<string> _tempObjFiles = new List<string>();

		public Dictionary<string, Module.Module> Modules = new Dictionary<string, Module.Module>();

		public ImportTable(CompilerContext globalTable)
		{
			_symbolTable = new SymbolTable(globalTable);
		}
		
		public void AddFileImports(FileImports imports)
		{
			_fileImports.Add(imports);

			foreach (FileImportDirective directive in imports.Directives)
			{
				string fullIden = "";
				if (!string.IsNullOrEmpty(directive.PackageName))
					fullIden += directive.PackageName + '.';
				fullIden += directive.ModuleName;

				if (!_moduleIdens.ContainsKey(fullIden))
					_moduleIdens.Add(fullIden, new []{ directive.PackageName, directive.ModuleName });
			}
		}

		public Symbol FindDefinition(Scope scope, Identifier iden, int location, Scope baseScope, FileImports fileImports)
		{
			// Get all available directives from the current scope
			List<FileImportDirective>[] availableDirectives = GetAvailableDirectives(scope, location, fileImports);
			// Check if symbol is available from current scope
			for (int i = availableDirectives.Length - 1; i >= 0; i--)
			{
				List<FileImportDirective> directives = availableDirectives[i];
				if (directives == null)
					continue;

				foreach (FileImportDirective directive in directives)
				{
					List<Identifier> subScopes = new List<Identifier>();
					if (!string.IsNullOrEmpty(directive.PackageName))
					{
						string[] subNames = directive.PackageName.Split('.');
						foreach (string subName in subNames)
							subScopes.Add(new IdentifierName(subName));
					}
					subScopes.Add(new IdentifierName(directive.ModuleName));

					Scope directiveScope = new Scope(subScopes);

					Symbol symbol = _symbolTable.FindDefinition(directiveScope, iden, -1, baseScope);
					if (symbol != null)
					{
						fileImports.AddUsedModule(directive);
						return symbol;
					}
				}
			}

			return null;
		}

		public Symbol FindFunction(Scope scope, Identifier funcName, SymbolType recType, List<SymbolType> argTypes, Scope qualifiedScope, int location, FileImports fileImports)
		{
			// Get all available directives from the current scope
			List<FileImportDirective>[] availableDirectives = GetAvailableDirectives(scope, location, fileImports);
			// Check if symbol is available from current scope
			for (int i = availableDirectives.Length - 1; i >= 0; i--)
			{
				List<FileImportDirective> directives = availableDirectives[i];
				if (directives == null)
					continue;

				foreach (FileImportDirective directive in directives)
				{
					List<Identifier> subScopes = new List<Identifier>();
					if (!string.IsNullOrEmpty(directive.PackageName))
					{
						string[] subNames = directive.PackageName.Split('.');
						foreach (string subName in subNames)
							subScopes.Add(new IdentifierName(subName));
					}
					subScopes.Add(new IdentifierName(directive.ModuleName));

					Scope directiveScope = new Scope(subScopes);

					Symbol symbol = _symbolTable.FindFunctionWithParameters(directiveScope, funcName, recType, argTypes, qualifiedScope);
					if (symbol != null)
					{
						fileImports.AddUsedModule(directive);
						return symbol;
					}
				}
			}

			return null;
		}

		public Symbol FindType(Scope scope, Identifier iden, Scope baseScope, FileImports fileImports)
		{
			// Get all available directives from the current scope
			List<FileImportDirective>[] availableDirectives = GetAvailableDirectives(scope, -1, fileImports);
			// Check if symbol is available from current scope
			for (int i = availableDirectives.Length - 1; i >= 0; i--)
			{
				List<FileImportDirective> directives = availableDirectives[i];
				if (directives == null)
					continue;

				foreach (FileImportDirective directive in directives)
				{
					List<Identifier> subScopes = new List<Identifier>();
					if (!string.IsNullOrEmpty(directive.PackageName))
					{
						string[] subNames = directive.PackageName.Split('.');
						foreach (string subName in subNames)
							subScopes.Add(new IdentifierName(subName));
					}
					subScopes.Add(new IdentifierName(directive.ModuleName));

					Scope directiveScope = new Scope(subScopes);

					Symbol symbol = _symbolTable.FindType(directiveScope, iden, baseScope);
					if (symbol != null)
					{
						fileImports.AddUsedModule(directive);
						return symbol;
					}
				}
			}

			return null;
		}

		public Symbol FindTemplate(Scope scope, TemplateInstanceName iden, Scope baseScope, FileImports fileImports)
		{
			// Get all available directives from the current scope
			List<FileImportDirective>[] availableDirectives = GetAvailableDirectives(scope, -1, fileImports);
			// Check if symbol is available from current scope
			for (int i = availableDirectives.Length - 1; i >= 0; i--)
			{
				List<FileImportDirective> directives = availableDirectives[i];
				if (directives == null)
					continue;

				foreach (FileImportDirective directive in directives)
				{
					List<Identifier> subScopes = new List<Identifier>();
					if (!string.IsNullOrEmpty(directive.PackageName))
					{
						string[] subNames = directive.PackageName.Split('.');
						foreach (string subName in subNames)
							subScopes.Add(new IdentifierName(subName));
					}
					subScopes.Add(new IdentifierName(directive.ModuleName));

					Scope directiveScope = new Scope(subScopes);

					Symbol symbol = _symbolTable.FindTemplate(directiveScope, iden, baseScope);
					if (symbol != null)
					{
						fileImports.AddUsedModule(directive);
						return symbol;
					}
				}
			}

			return null;
		}

		List<FileImportDirective>[] GetAvailableDirectives(Scope scope, int location, FileImports fileImports)
		{
			List<FileImportDirective>[] availableDirectives = new List<FileImportDirective>[scope.Names.Count];
			foreach (FileImportDirective directive in fileImports.Directives)
			{
				if (scope.ContainsScope(directive.ContainingScope))
				{
					if (directive.FileLocation > location)
						continue;

					int depth = directive.ContainingScope.Names.Count - 1;

					if (availableDirectives[depth] == null)
						availableDirectives[depth] = new List<FileImportDirective>();

					availableDirectives[depth].Add(directive);
				}
			}
			return availableDirectives;
		}

		public void LoadModules()
		{
			// Extract expected modules
			Dictionary<string, string[]> expectedModules = new Dictionary<string, string[]>();
			foreach (KeyValuePair<string, string[]> moduleIden in _moduleIdens)
			{
				expectedModules.Add(moduleIden.Key, moduleIden.Value);
			}

			// Get available modules
			List<string> availableModules = new List<string>();
			foreach (string file in Directory.EnumerateFiles(Directory.GetCurrentDirectory()))
			{
				if (file.EndsWith(".mjm"))
				{
					availableModules.Add(file);
				}
			}

			foreach (string dir in CmdLine.IncludeDirs)
			{
				foreach (string file in Directory.EnumerateFiles(Directory.GetCurrentDirectory()))
				{
					if (file.EndsWith(".mjm"))
					{
						availableModules.Add(file);
					}
				}
			}

			// load modules
			foreach (string moduleFile in availableModules)
			{
				byte[] data = File.ReadAllBytes(moduleFile);

				// Load header and check for module or package name
				ModuleHeader header = new ModuleHeader(data, 0);

				string fullIden = "";
				if (!string.IsNullOrEmpty(header.Package))
					fullIden += header.Package + '.';
				fullIden += header.Module;

				if (expectedModules.ContainsKey(fullIden))
				{
					Module.Module module = ModuleImporter.Import(data);
					Modules.Add(fullIden, module);
					expectedModules.Remove(fullIden);
				}
			}

			if (expectedModules.Count > 0)
			{
				// TODO: Error
			}

			// extract symbols from module
			foreach (KeyValuePair<string, Module.Module> pair in Modules)
			{
				Module.Module module = pair.Value;
				List<Identifier> scopeNames = new List<Identifier>();
				if (!string.IsNullOrEmpty(module.Header.Package))
				{
					string[] subNames = module.Header.Package.Split('.');
					foreach (string subName in subNames)
						scopeNames.Add(new IdentifierName(subName));
				}
				scopeNames.Add(new IdentifierName(module.Header.Module));
				Scope moduleScope = new Scope(scopeNames);

				// functions

				// TODO
				if (module.Header.Package == "__TODO__")
				{
					if (module.TryGetTable("pak", out Table pakTable))
					{
						foreach (BaseTableEntry baseTableEntry in pakTable.Entries)
						{
							TableEntry entry = baseTableEntry as TableEntry;
							NameMangling.Demangle(entry.MangledName, out _, out Symbol symbol);
							_symbolTable.AddSymbol(symbol);
						}
					}
				}

				if (module.TryGetTable("pub", out Table pubTable))
				{
					foreach (BaseTableEntry baseTableEntry in pubTable.Entries)
					{
						TableEntry entry = baseTableEntry as TableEntry;
						NameMangling.Demangle(entry.MangledName, out _, out Symbol symbol);

						if (symbol == null)
						{
							int start = 0;
							(SymbolType type, SymbolKind kind) = NameMangling.ExtractType(entry.MangledType, ref start);

							symbol = new Symbol(moduleScope, null, new IdentifierName(entry.MangledName), kind);
							symbol.Type = type;
						}

						symbol.MangledName = entry.MangledName;
						symbol.Visibility = SemanticVisibility.Public;
						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);
					}
				}

				if (module.TryGetTable("exp", out Table expTable))
				{
					foreach (BaseTableEntry baseTableEntry in expTable.Entries)
					{
						TableEntry entry = baseTableEntry as TableEntry;
						NameMangling.Demangle(entry.MangledName, out _, out Symbol symbol);

						if (symbol == null)
						{
							int start = 0;
							(SymbolType type, SymbolKind kind) = NameMangling.ExtractType(entry.MangledType, ref start);

							symbol = new Symbol(moduleScope, null, new IdentifierName(entry.MangledName), kind);
							symbol.Type = type;
						}

						symbol.MangledName = entry.MangledName;
						symbol.Visibility = SemanticVisibility.Import;
						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);
					}
				}

				// globals
				if (module.TryGetTable("glo", out Table gloTable))
				{
					foreach (BaseTableEntry baseTableEntry in gloTable.Entries)
					{
						TableEntry entry = baseTableEntry as TableEntry;
						NameMangling.Demangle(entry.MangledName, out _, out Symbol symbol);
						symbol.MangledName = entry.MangledName;
						symbol.Visibility = SemanticVisibility.Import;
						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);
					}
				}

				// structures
				if (module.TryGetTable("str", out Table strTable))
				{
					foreach (BaseTableEntry baseTableEntry in strTable.Entries)
					{
						TypeTableEntry entry = baseTableEntry as TypeTableEntry;
						NameMangling.Demangle(entry.MangledName, out _, out Symbol symbol);
						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);

						foreach (string child in entry.Children)
						{
							NameMangling.Demangle(child, out _, out Symbol childSymbol);
							_symbolTable.AddSymbol(childSymbol);
							module.Symbols.Add(childSymbol);
						}
					}
				}

				// unions
				if (module.TryGetTable("uni", out Table uniTable))
				{
					foreach (BaseTableEntry baseTableEntry in uniTable.Entries)
					{
						TypeTableEntry entry = baseTableEntry as TypeTableEntry;
						NameMangling.Demangle(entry.MangledName, out _, out Symbol symbol);
						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);

						foreach (string child in entry.Children)
						{
							NameMangling.Demangle(child, out _, out Symbol childSymbol);
							_symbolTable.AddSymbol(childSymbol);
							module.Symbols.Add(childSymbol);
						}
					}
				}

				// simple enum
				if (module.TryGetTable("enu", out Table enuTable))
				{
					foreach (BaseTableEntry baseTableEntry in enuTable.Entries)
					{
						TypeTableEntry entry = baseTableEntry as TypeTableEntry;
						NameMangling.Demangle(entry.MangledName, out _, out Symbol symbol);
						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);

						foreach (string child in entry.Children)
						{
							NameMangling.Demangle(child, out _, out Symbol childSymbol);
							_symbolTable.AddSymbol(childSymbol);
							module.Symbols.Add(childSymbol);
						}
					}
				}

				// adt enum
				if (module.TryGetTable("adt", out Table adtTable))
				{
					foreach (BaseTableEntry baseTableEntry in adtTable.Entries)
					{
						TypeTableEntry entry = baseTableEntry as TypeTableEntry;
						NameMangling.Demangle(entry.MangledName, out _, out Symbol symbol);
						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);

						foreach (string child in entry.Children)
						{
							NameMangling.Demangle(child, out _, out Symbol childSymbol);
							_symbolTable.AddSymbol(childSymbol);
							module.Symbols.Add(childSymbol);
						}
					}
				}

				// delegate
				// TODO: may change table type
				if (module.TryGetTable("del", out Table delTable))
				{
					foreach (BaseTableEntry baseTableEntry in delTable.Entries)
					{
						TableEntry entry = baseTableEntry as TableEntry;
						NameMangling.Demangle(entry.MangledName, out _, out Symbol symbol);

						DelegateSymbolFlags flags = (DelegateSymbolFlags) entry.Info.TypeSpecific;

						if ((flags & DelegateSymbolFlags.FuncPtr) != 0)
							symbol.CompileAttribs.Add(FuncPtrAttribute.Id, new FuncPtrAttribute());


						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);
					}
				}

				// aliases
				if (module.TryGetTable("ali", out Table aliTable))
				{
					foreach (BaseTableEntry baseTableEntry in aliTable.Entries)
					{
						TableEntry entry = baseTableEntry as TableEntry;
						NameMangling.Demangle(entry.MangledName, out _, out Symbol symbol);
						_symbolTable.AddSymbol(symbol);
						module.Symbols.Add(symbol);
					}
				}
			}

		}


		public void GenerateImportedILCompUnits()
		{
			foreach (KeyValuePair<string, Module.Module> pair in Modules)
			{
				pair.Value.GenerateILCompUnit();
			}
		}

		public string PrepareForLinking()
		{
			string interDir = CmdLine.IntermediateDirectory ?? "";
			string cmdLine = "";
			foreach (KeyValuePair<string, Module.Module> pair in Modules)
			{
				Module.Module module = pair.Value;

				foreach (ObjectFile file in module.ObjectFiles)
				{
					string path = interDir + file.FileName;
					using (FileStream stream = new FileStream(path, FileMode.Create))
					using (BinaryWriter writer = new BinaryWriter(stream))
					{
						writer.Write(file.Data);
					}
					_tempObjFiles.Add(path);

					cmdLine += ' ' + path;
				}
			}

			return cmdLine;
		}

		public void CleanupAfterLinking()
		{
			foreach (string objFile in _tempObjFiles)
			{
				File.Delete(objFile);
			}
			_tempObjFiles.Clear();
		}


	}

}
