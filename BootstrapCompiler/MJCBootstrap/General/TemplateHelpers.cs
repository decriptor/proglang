﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.ILBackend;
using MJC.ILBackend.General;

namespace MJC.General
{
	public enum TemplateValueParamCategory
	{
		Unknown,
		Bool,
		Integer,
		FloatingPoint,
		Char,
		String,
	}

	public static class TemplateHelpers
	{
		public static SymbolType GetTemplateBaseType(SymbolType type, Symbol symbol)
		{
			// TODO: Complete
			switch (type)
			{
			case ReferenceSymbolType refType:
			{
				SymbolType baseType = GetTemplateBaseType(refType.BaseType, symbol);
				return SymbolType.ReferenceType(baseType, type.Attributes);
			}
			case PointerSymbolType ptrType:
			{
				SymbolType baseType = GetTemplateBaseType(ptrType.BaseType, symbol);
				return SymbolType.PointerType(baseType, type.Attributes);
			}
			case ArraySymbolType arrType:
			{
				SymbolType baseType = GetTemplateBaseType(arrType.BaseType, symbol);
				return SymbolType.ArrayType(baseType, arrType.ArraySize, type.Attributes);
			}
			case NullableSymbolType nullType:
			{
				SymbolType baseType = GetTemplateBaseType(nullType.BaseType, symbol);
				return SymbolType.NullableType(baseType, type.Attributes);
			}
			case TemplateInstanceSymbolType instType:
			{
				return instType.TemplateSymbol.Type;
			}
			case FunctionSymbolType funcType:
			{
				SymbolType recType = GetTemplateBaseType(funcType.ReceiverType, symbol);
				SymbolType retType = GetTemplateBaseType(funcType.ReturnType, symbol);
				List<SymbolType> paramTypes = null;
				if (funcType.ParamTypes != null)
				{
					paramTypes = new List<SymbolType>();
					foreach (SymbolType paramType in funcType.ParamTypes)
					{
						SymbolType tmp = GetTemplateBaseType(paramType, symbol);
						paramTypes.Add(tmp);
					}
				}

				return SymbolType.FunctionType(recType, paramTypes, retType, type.Attributes);
			}
			case TemplateParamSymbolType paramType:
			{
				List<Symbol> paramSyms = symbol.GetChildrenWithType<TemplateParamSymbolType>();
				Symbol paramSym = paramSyms.Find(s =>
				{
					TemplateParamSymbolType tpType = s.Type as TemplateParamSymbolType;
					if (tpType.Index == paramType.Index)
						return true;
					return false;
				});

				return paramSym.Type;
			}

			default:
				return type;
			}
		}

		public static bool DoTemplateDefInstMatch(TemplateInstanceName inst, TemplateDefinitionName def)
		{
			if (inst.Arguments.Count != def.Parameters.Count)
				return false;

			Dictionary<SymbolType, SymbolType> typeMapping = new Dictionary<SymbolType, SymbolType>();

			for (int i = 0; i < inst.Arguments.Count; i++)
			{
				TemplateArgument arg = inst.Arguments[i];
				TemplateParameter param = def.Parameters[i];

				if (param.ValueName != null)
				{
					// TODO
				}
				else
				{
					// If specialized, check if the types match
					if (param.TypeSpecialization != null && arg.Type != param.TypeSpecialization)
						return false;

					// If a specialization, where a template param gets replaced by another, occurs,
					// check if both given params are the same type,
					// else add the types to the mapping, in case template param is reused
					if (typeMapping.TryGetValue(param.Type, out SymbolType type))
					{
						if (type != arg.Type)
							return false;
					}
					else
					{
						typeMapping.Add(param.Type, arg.Type);
					}
				}
			}

			return true;
		}

		public static bool IsTemplateSpecialization(TemplateDefinitionName spec, TemplateDefinitionName def)
		{
			if (spec.Parameters.Count != def.Parameters.Count)
				return false;
			
			for (int i = 0; i < spec.Parameters.Count; i++)
			{
				TemplateParameter specParam = spec.Parameters[i];
				TemplateParameter param = def.Parameters[i];

				if (param.ValueName != null)
				{
					// TODO
				}
				else
				{
					if (specParam.Type != param.Type)
						return false;
				}
			}

			return true;
		}

		public static TemplateVTables GetValidVTables(List<VTable> tables, TemplateInstanceName instName)
		{
			TemplateVTables vTables = new TemplateVTables();

			foreach (VTable table in tables)
			{
				TemplateDefinitionName defName = table.Iden.Name as TemplateDefinitionName;
				if (TemplateHelpers.DoTemplateDefInstMatch(instName, defName))
				{
					bool isFullSpec = true, isPartialSpec = false, isValid = true;

					for (var i = 0; i < defName.Parameters.Count; i++)
					{
						TemplateParameter parameter = defName.Parameters[i];
						TemplateArgument arg = instName.Arguments[i];
						if (parameter.TypeSpecialization != null)
						{
							if (parameter.TypeSpecialization != arg.Type)
							{
								isValid = false;
								break;
							}

							isPartialSpec = true;
						}
						else if (parameter.ValueSpecialization != null)
						{
							if (parameter.ValueSpecialization != arg.Value)
							{
								isValid = false;
								break;
							}

							isPartialSpec = true;
						}
						else
						{
							isFullSpec = false;
						}
					}

					if (isValid)
					{
						if (isFullSpec)
							vTables.FullSpecTable = table;
						else if (isPartialSpec)
							vTables.PartSpecTables.Add(table);
						else
							vTables.BaseTable = table;
					}
				}
			}

			return vTables;
		}

		public static List<VTableMethod> GetInstanceMethods(ScopeVariable instScopeVar, Symbol baseSymbol, Dictionary<ScopeVariable, List<VTable>> vtables)
		{
			List<VTable> vtableOverloads = vtables[baseSymbol.ScopeVar];
			TemplateInstanceName instName = instScopeVar.Name as TemplateInstanceName;

			// Find all valid vtables
			TemplateVTables validTables = GetValidVTables(vtableOverloads, instName);
			List<VTableMethod> methods = new List<VTableMethod>();
			HashSet<Identifier> specialFunctions = new HashSet<Identifier>();

			// TODO: Method templates
			// Add fully specialized
			if (validTables.FullSpecTable != null)
			{
				foreach (KeyValuePair<Identifier, List<VTableMethod>> pair in validTables.FullSpecTable.Methods)
				{
					foreach (VTableMethod method in pair.Value)
					{
						if (method.Iden is IdentifierName idenName && idenName.Iden.StartsWith("__"))
						{
							specialFunctions.Add(idenName);
							methods.Add(method);
						}
						else
						{
							methods.Add(method);
						}

					}
				}
			}

			// Add unoverloaded specialized tables
			if (validTables.PartSpecTables.Count > 0)
			{
				foreach (VTable table in validTables.PartSpecTables)
				{
					foreach (KeyValuePair<Identifier, List<VTableMethod>> pair in table.Methods)
					{
						foreach (VTableMethod method in pair.Value)
						{
							if (method.Iden is IdentifierName idenName && idenName.Iden.StartsWith("__"))
							{
								if (!specialFunctions.Contains(idenName))
								{
									specialFunctions.Add(idenName);
									methods.Add(method);
								}
							}
							else
							{
								bool alreadyExists = false;
								foreach (VTableMethod addedMethod in methods)
								{
									if (addedMethod.Iden == method.Iden)
									{
										SymbolType baseAddedType = GetTemplateBaseType(addedMethod.Type, baseSymbol);
										SymbolType baseType = GetTemplateBaseType(method.Type, baseSymbol);

										if (baseAddedType == baseType)
											alreadyExists = true;
									}
								}

								if (!alreadyExists)
									methods.Add(method);
							}
						}
					}
				}
			}

			// Handle base table
			foreach (KeyValuePair<Identifier, List<VTableMethod>> pair in validTables.BaseTable.Methods)
			{
				foreach (VTableMethod method in pair.Value)
				{
					if (method.Iden is IdentifierName idenName && idenName.Iden.StartsWith("__"))
					{
						if (!specialFunctions.Contains(idenName))
						{
							specialFunctions.Add(idenName);
							methods.Add(method);
						}
					}
					else
					{
						bool alreadyExists = false;
						foreach (VTableMethod addedMethod in methods)
						{
							if (addedMethod.Iden == method.Iden)
							{
								SymbolType baseAddedType = GetTemplateBaseType(addedMethod.Type, baseSymbol);
								SymbolType baseType = GetTemplateBaseType(method.Type, baseSymbol);

								if (baseAddedType == baseType)
									alreadyExists = true;
							}
						}

						if (!alreadyExists)
							methods.Add(method);
					}
				}
			}

			return methods;
		}

		public static TemplateValueParamCategory CategorizeType(SymbolType type)
		{
			switch (type)
			{
			case BuiltinSymbolType builtin:
			{
				switch (builtin.Builtin)
				{
				case BuiltinTypes.Bool:
					return TemplateValueParamCategory.Bool;
				case BuiltinTypes.I8:
				case BuiltinTypes.I16:
				case BuiltinTypes.I32:
				case BuiltinTypes.I64:
				case BuiltinTypes.ISize:
				case BuiltinTypes.U8:
				case BuiltinTypes.U16:
				case BuiltinTypes.U32:
				case BuiltinTypes.U64:
				case BuiltinTypes.USize:
					return TemplateValueParamCategory.Integer;
				case BuiltinTypes.F32:
				case BuiltinTypes.F64:
					return TemplateValueParamCategory.FloatingPoint;
				case BuiltinTypes.Char:
				case BuiltinTypes.WChar:
				case BuiltinTypes.Rune:
					return TemplateValueParamCategory.Char;
				case BuiltinTypes.String:
				case BuiltinTypes.StringLiteral:
					return TemplateValueParamCategory.String;
				case BuiltinTypes.Null:
				case BuiltinTypes.Any:
				default:
					return TemplateValueParamCategory.Unknown;
				}
			}
			default:
				return TemplateValueParamCategory.Unknown;
			}
		}

		public static bool IsValidTemplate(TemplateDefinitionName defName, List<SymbolType> funcParamTypes = null)
		{
			if (funcParamTypes == null)
			{
				// When no function parameters are passed to the template, if any template parameter has a default value, they need to be at the end of the template parameter list

				bool encounteredDefault = false;
				foreach (TemplateParameter param in defName.Parameters)
				{
					if (param.HasDefaultValue())
					{
						encounteredDefault = true;
						continue;
					}

					if (encounteredDefault)
						return false;
				}
			}
			else
			{
				// When function parameters are passed the the template, for any template parameter with no default value, encountered after the first template parameters with a default value, the template parameter needs to be able to be inferred from the function parameters

				bool encounteredDefault = false;
				foreach (TemplateParameter param in defName.Parameters)
				{
					if (param.HasDefaultValue())
					{
						encounteredDefault = true;
						continue;
					}

					if (encounteredDefault)
					{
						if (param.ValueName != null)
						{
							// TODO
						}
						else
						{
							bool found = false;
							foreach (SymbolType paramType in funcParamTypes)
							{
								List<TemplateParamSymbolType> subTypes = paramType.GetSubTypesOf<TemplateParamSymbolType>();
								foreach (TemplateParamSymbolType subType in subTypes)
								{
									if (subType == param.Type)
									{
										found = true;
										break;
									}
								}

								if (found)
									break;
							}

							if (!found)
								return false;
						}
					}
				}
			}

			return true;
		}

		// TODO: Let work with function parameters
		public static bool IsCompatible(TemplateDefinitionName def, TemplateInstanceName inst)
		{
			int defIdx = 0, instIdx = 0;
			while(defIdx < def.Parameters.Count)
			{
				TemplateParameter param = def.Parameters[defIdx];
				TemplateArgument arg;
				inst.Arguments.TryGetValue(instIdx, out arg);

				if (arg == null)
				{
					if (!param.HasDefaultValue())
						return false;

					++defIdx;
				}
				else
				{
					if (param.ValueName != null)
					{
						TemplateValueParamCategory paramCat = CategorizeType(param.Type);
						TemplateValueParamCategory argCat = CategorizeType(arg.Type);

						if (paramCat != argCat)
							return false;
					}
					else
					{
						// Both are types, check whether any specialization exists
						if (param.TypeSpecialization != null && param.TypeSpecialization != arg.Type)
							return false;
					}

					++defIdx;
					++instIdx;
				}
			}

			return true;
		}
	}
}
