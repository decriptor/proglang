﻿using System;
using System.Collections.Generic;
using System.Text;
using MJC.ILBackend.General;

namespace MJC.General
{
	public class VTable
	{
		public ScopeVariable Iden;
		public string MangledIden;
		public Dictionary<Identifier, List<VTableMethod>> Methods = new Dictionary<Identifier, List<VTableMethod>>();

		public VTable(ScopeVariable iden, string mangled)
		{
			Iden = iden;
			MangledIden = mangled;
		}

		public void AddMethod(VTableMethod method)
		{
			if (!Methods.ContainsKey(method.Iden))
				Methods.Add(method.Iden, new List<VTableMethod>());

			Methods[method.Iden].Add(method);
		}

		public List<VTableMethod> GetMethods(Identifier iden)
		{
			Methods.TryGetValue(iden, out List<VTableMethod> methods);
			return methods;
		}

		public VTableMethod GetMethod(Identifier iden, FunctionSymbolType funcType)
		{
			bool res = Methods.TryGetValue(iden, out List<VTableMethod> methods);
			if (!res)
				return null;

			foreach (VTableMethod method in methods)
			{
				if (method.Type == funcType)
					return method;
			}

			return null;
		}

		public VTableMethod GetMethod(Identifier iden, FunctionSymbolType funcType, ILTemplateInstance instance)
		{
			bool res = Methods.TryGetValue(iden, out List<VTableMethod> methods);
			if (!res)
				return null;

			foreach (VTableMethod method in methods)
			{
				SymbolType tmp = ILTemplateHelpers.GetInstancedType(method.Type, instance);
				if (tmp == funcType)
					return method;
			}

			return null;
		}
	}

	public class VTableMethod
	{
		public Identifier Iden;
		public FunctionSymbolType Type;
		public Symbol Symbol;

		private string _mangledName;

		public string MangledFunc
		{
			get
			{
				return Symbol?.MangledName ?? _mangledName;
			}
			set { _mangledName = MangledFunc; }
		}

		public VTableMethod(Symbol symbol)
		{
			Symbol = symbol;
			Iden = symbol.Identifier;
			Type = symbol.Type as FunctionSymbolType;
		}

		public VTableMethod(Identifier iden, FunctionSymbolType funcType, string mangled)
		{
			Iden = iden;
			Type = funcType;
			_mangledName = mangled;
		}

		public override string ToString()
		{
			return $"{Iden} {Type} : @{MangledFunc}";
		}
	}

	public class TemplateVTables
	{
		public VTable BaseTable;
		public List<VTable> PartSpecTables = new List<VTable>();
		public VTable FullSpecTable;
	}
}
