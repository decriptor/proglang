# Errors

// Fatal error
E0000: Unknown error
E0001: Error count exceded limit
E0002: Unexpected end of file
E0003: Cannot open file 'file'

E0100: Unrecognized flag 'string' in 'option'
E0101: No input file specified
E0102: Multiple input files specified

E0200: Invalid integer constant expression

// Lexical errors
E0900: Block comment not ended before end of file
E0901: Newline in string constant

// Syntax errors

E1000: Package is already defined.
E1001: Invalid package specifier, encountered unexpected token 'token'
E1010: Module is already defined.
E1011: Invalid package specifier, encountered unexpected token 'token'

E1500: newline in constant

# Linker errors

# Warning