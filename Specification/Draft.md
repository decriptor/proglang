# MJay programming language specification

Note that this file is heavily WIP, formatting and specification are subject to frequent changes

Note that any explenation following the grammar section has been made to be human readable, so does not contain the full grammar, so for the correct representations, explicitly reference the lexical and grammar sections.

# Index
1. Introduction
2. Lexical
    1. Source code
    2. Lexical definitions
        1. Character set
        2. End Of File
        3. End Of Line
        4. WhiteSpace
        5. Comments
        6. Tokens
        7. Identifiers
        8. String literals
        9. Char literals
        10. Integer literals
        11. Floating point literals
        12. Keywords
        13. Built-in attribute keywords
        14. Special keywords
3. Grammar
    1. File structure
    2. Packages, modules and namespaces
    3. Imports
    4. Declarations
    5. Expressions
    6. Statements
    7. Functions
    8. Classes
    9. Structures
    10. Interfaces
    11. Unions
    12. Enums
    13. Properties
    14. Attributes
    15. Extensions
    16. Templates
    17. Mixin templates
    18. Unittests
    19. Compile-time expressions
    20. Pragma
    21. Inline assembly
    22. ABI
4. Packages, Modules and Namespaces
28. Application Binary Interface
    1. Name mangling
        1. Qualified name
        2. Qualified identifier
        3. Templates identifier
        4. Default mangles values
        5. Back references
        6. Qualified type data
29. Standard library
31. Appendices

# 1. Introduction

MJay is a general purpose programmming language that has both low and high level language features.
This document lays out the specification for this language

MJay is inspired and heavily based on the following languages: D, C++ and C#, with a modern syntax.

# 2. Lexical
Lexical analysis is independent of the syntax parsing and sementic analysis. The Lexer's job is to split up the source code  into tokens, which are then used during the compilation process. The lexical grammer describes the systax of these tokens.

## 2.1. Source code
Source code can be stored using the following formats
- ASCII
- UTF-8
- UTF-16BE
- UTF-16LE
- UTF-32BE
- UTF-32LE

The following UTF BOMs (Byte Order Marks) can be present at the start of the source

|  Format  |     BOM     |
|----------|-------------|
| UTF-8    | EF BB BF    |
| UTF-16BE | FE FF       |
| UTF-16LE | FF FE       |
| UTF-32BE | 00 00 FE FF |
| UTF-32LE | FF FE 00 00 |
| ASCII    | no BOM      |

When no BOM is present, the file is expected to start with a character smaller that U+0000007F


Source code is required to made up by the following character sets:
- WhiteSpace
- EndOfLine
- EndOfFile
- Comments
- Tokens
- Special tokens

## 2.2. Lexical definitions

### 2.2.1. Character set
    character-set ::= Any string of unicode characters

### 2.2.2. End Of File
    end-of-file ::=  End of bytestream (file)

### 2.2.3. End Of Line
    eol ::= "\u000A"                ;(\n)
            "\u000D" |              ;(\r)
            ("\u000D" "\u000A") |   ;(\r\n)
            "\u2028" |              ;(line seperator)
            "\u2029" |              ;(paragraph separator)

### 2.2.4. White Space
    white-space ::= <space>+
  
    space ::= "\u0009" |
              "\u000B" |
              "\u000C" |
              "\u0020"

### 2.2.5. Comments
    comment ::= <block-comment> |
                <nested-block-comment> |
                <line-comment> |

    NestedBlockComment ::=  "/*" (<nested-block-comment> | <characters>) "*/"

    LineComment ::= "//" <characters> <eol>

### 2.2.6. Tokens
    token ::= 
        <identifier> |
        <string-literal> |
        <character-literal> |
        <integer-literal> |
        <float-literal> |
        <keywords> |
        <attribute-keywords> |
        "+" |
        "++" |
        "+=" |
        "-" |
        "--" |
        "-=" |
        "->" |
        "*" |
        "*=" |
        "/" |
        "/=" |
        "|" |
        "|=" |
        "|| |
        "&" |
        "&=" |
        "&&" |
        "%" |
        "%=" |
        "~" |
        "~=" |
        "!" |
        "!=" |
        "!<" |
        "!:" |
        "=" |
        "==" |
        "=>" |
        ":"" |
        ":=" |
        "?" |
        "??" |
        "?." |
        "^" |
        "^=" |
        "^^" |
        "^^=" |
        "<" |
        "<=" |
        "<<" |
        "<<=" |
        ">" |
        ">=" |
        ">>" |
        ">>=" |
        "." |
        ".." |
        "..." |
        "(" |
        ")" |
        "{" |
        "}" |
        "[" |
        "]" |
        "," |
        ";" |
        "@"
    
### 2.2.7. Identifier
    identifier ::= <identifier-start> <identifier-char>*

    identifier-start ::= "_" | <letter>

    identifier-char ::= <identifier-start> | <digit>

@TODO: Move
Identifiers starting with __ (double underscore) are reserved for the compiler and core language. Check specification for more info about core language identifier

### 2.2.8. String Literals
    string-literal ::= <string> | 
                       <multiline-string> |
                       <wysiwyg-string> |
                       <interpolated-string>

    string ::= """ <string-character>* """
        " StringCharacters "

    string-character ::= <character> |
                         <escape-sequence> |
                         <eol>

    EscapeSequence ::= "\"" |
                       "\\" |
                       "\x" <hexadecimal>

    wysiwyg-string ::= ("@"" <character>* """) |
                       ("`" <character>* "`")

    interpolated-string ::= "$"" <nterpolated-character>* """

    interpolated-character ::= <string-character> |
                               <interpolated-value> |
                               "{{" |
                               "}}"

    interpolated-value ::= "{" <expression> "}"

### 2.2.9. Character Literals
    character-literal ::= "'" (<character> | <escape-sequence>) "'"

### 2.2.10. Integer Literals
    integer-literal ::= <integer> [<integer-suffix>]

    integer ::= <decimal-integer> |
                <binary-integer> |
                <hexadecimal-integer> |
                <octal-integer>

    IntegerSuffix ::= "l" |
                      "L" |
                      "u" |
                      "U" |
                      "ul" |
                      "Ul" |
                      "uL" |
                      "UL" |
                      "lu" |
                      "Lu" |
                      "lU" |
                      "LU"

    decimal-integer ::= <digit> |
                        (<non-zero-digit> <digit>*)

    binary-integer :: <binary-prefix> <binary-digit>

    binary-prefix ::= "0b" | "0B"

    hexadecimal-integer ::= <hex-prefix> <hexadecimal-digit>*

    hex-prefix ::= "0x" | "0X"

    OctalInteger ::= "0" <octal-digit>*

    <binary-digit> ::= "0" | "1"
    <octal-digit> ::= <binary-digit> | "2" | "3" | "4" | "5" | "6" | "7"
    <digit> ::= <octal-digit> | "8" | "9"
    <digit> ::= <octal-digit> | "8" | "9"
    <hexadecimal-digit> ::= <digit> | "a" | "A" | "b" | "B" | "c" | "C" | "d" | "D" | "e" | "E" | "f" | "F"

Integer literals can be group using a single quote (') or an underscore

```c++
20'000              // Grouped decimal
20_000              // Grouped decimal
0xFFFF'FFFF         // Grouped hexadecimal
0xFFFF_FFFF         // Grouped hexadecimal
0b10101010'11001100 // Grouped binary
0b10101010_11001100 // Grouped binary
```

### 2.2.11. Floating Point Literals
    float-literal ::= ( <float> [<float-suffix>] ) | ( <integer> <float-suffix> )
    
    Float ::= [<integer>] . [ <digit>+ [<exponent>] ]

    float-suffix ::= "f" | "F"

    exponent ::= ( ( "e" | "E" ) [ "+" | "-" ] ) <digit>+

### 2.2.12. Keywords

    keywords ::= "abstract"
                 "alias" |
                 "align" |
                 "alignof" |
                 "as" |
                 "asm" |
                 "assert" |

                 "bool" |
                 "break" |
                 "byte" |

                 "case" |
                 "catch" |
                 "cconst" |
                 "char" |
                 "class" |
                 "const" |
                 "continue" |

                 "debug" |
                 "default" |
                 "defer" |
                 "delegate" |
                 "delete" |
                 "do" |
                 "double" |

                 "else" |
                 "elif" |
                 "enum" |
                 "export" |
                 "extension" |
                 "extern" |

                 "fallthrough" |
                 "false" |
                 "final" |
                 "finally" |
                 "float" |
                 "for" |
                 "foreach" |
                 "func" |

                 "goto" |

                 "if" |
                 "interface" |
                 "immutable" |
                 "import" |
                 "int" |
                 "internal" |
                 "is" |
                 "in" |
                 "inout" |
                 "invariant" |

                 "lazy" |
                 "long" |

                 "mixin" |
                 "module" |
                 "mutable" |

                 "namespace" |
                 "new" |
                 "null" |

                 "out" |
                 "override" |

                 "package" |
                 "pragma" |
                 "private" |
                 "protected" |
                 "public" |

                 "ref" |
                 "return" |
                 "rune" |

                 "shared" |
                 "short" |
                 "sizeof" |
                 "static" |
                 "string" |
                 "struct" |
                 "super" |
                 "switch" |
                 "synchronized" |

                 "template" |
                 "this" |
                 "throw" |
                 "throws" |
                 "transmute" |
                 "true" |
                 "try" |
                 "typedef" |
                 "typeid" |
                 "typeof" |

                 "ubyte" |
                 "uint" |
                 "ulong" |
                 "union" |
                 "unittest" |
                 "ushort" |

                 "version" |
                 "virtual" |
                 "void" |

                 "wchar" |
                 "while" |

                 "__global"

### 2.2.13. Built-in attribute keywords
    attribute-keywords ::= "deprecated"

### 2.2.14. Special tokens
    special-tokens ::= "__FILE__" |
                       "__FILE_FULL_PATH__" |
                       "__PACKAGE__" |
                       "__MODULE__" |
                       "__NAMESPACE__" |
                       "__LINE__" |
                       "__FUNC__" |
                       "__PRETTY_FUNC__"

# 3. Grammar
This section contains the grammar specification of the language. A source file should only contain grammar that follows these rules.

Identifiers surrounded by [...]opt are optional identifier, note that there are no spaces in an optional identifier

## 3.1. File structure
    file ::= [<package>] [<module>] <decldef>*

    decldef ::= <namespace-decl>
              | <import-decl>
              | <declaration>
              | <func-decl>
              | <delegate-decl>
              | <pragma-decl>
              | <aggregate-decl>
              | <struct-invariant>
              | <class-invariant>
              | <constructor-decl>
              | <destructor-decl>
              | <post-blit>
              | <allocator>
              | <dealllocator>
              | <extension>
              | <template-mixin-decl>
              | <template-mixin>
              | <static-assert>
              | <unittest>
              | <attribute-specifier>

    decl-block ::= ( ["{"] <decldef>* ["}"] ) | ( "{" "}" )

    symbol-identifier ::= <identifier> { "." <identifier> }

## 3.2. Package, Module and Namespace
    package ::= "package" <package-identifier> ";"
    package-identifier ::= identifier { "." <identifier> }

    module ::= "module" <identifier> ";"

    namespace-decl ::= "namespace" <namespace-identifier> <decl-block>

## 3.3. Imports
    import-decl ::= [<import-modifiers>] "import" <import-list> ";"
                  | [<import-modifiers>] "import" "package" <package-import-list> ";"

    import-modifiers ::= ["public"] ["static"]

    import-list ::= <import-alias> { "," <import-alias> }

    import-alias ::= <import-identifier> [ "as" <identifier> ]

    import-identifier ::= <module-identifier> [ ":" <import-binding> { "," <import-binding> } ]

    import-binding ::= <symbol-identifier> [ as <identifier> ]

    package-import-list ::= <package-identifier> { "," <package-identifier> }

    module-identifier ::= [ <package-identifier> "." ] <identifier>

## 3.4. Declarations
    declaration ::= <variable-decl>
                  | <enum-decl>
                  | <alias-decl>
                  | <typedef-decl>
                  | <aggregate-decl>

    variable-decl ::= <explicit-declarator>
                    | <implicit-declarator>

    explicit-declarator ::= <declarators> ":" <decl-type> [ "=" <intializers> ] ";"

    implicit-declarator ::= <declarators> ":=" <initializers> ";"

    declarators ::= <identifier> { "," <identifier> }

    decl-type ::= <type> [ "." <integer-literal> ] ; with optional int, type becomes a bit set

    type ::= ( [<type-modifier>*] <basic-type> [<template-arguments>] ) ; no "?" yet
           | <delegate-type>
           | <tuple-type>

    type-modifier ::= "cconst"
                    | "const"
                    | "immutable"
                    | "static"
                    | "ref"
                    | <attribute>

    basic-type ::= <basic-type-2> [<basic-type-ext>*]
                 | <tuple-type>

    basic-type-2 ::= <base-type>
                   | ( ["."] <type-identifier> )
                   | ( <typeof> [ "." <symbol-identifier> ] )

    basic-type-ext ::= "*"
                     | ( "[" "]" )
                     | ( "[" <type> "]" )
                     | ( "[" <assign-expression> [ ".." <assign-expression> ] "]" )
                     | "..."

    base-type ::= "bool"
                | "byte"
                | "ubyte"
                | "short"
                | "ushort"
                | "int"
                | "uint"
                | "long"
                | "ulong"
                | "float"
                | "double"
                | "char"
                | "wchar"
                | "rune"
                | "string"

    type-identifier ::= ( <identifier> [ "." <type-identifier> ] )
                      | ( [ <type-identifier> "." ] <template-instance> [ "." <type-identifier> ] )

    tuple-type ::= "(" <type> { "." <type> } ")"

---
    Typeof:
        typeof ( Expression )
        typeof ( return )
        typeof ( return , Expression )

    typeof ::= ( "typeof" "(" <expression> ")" )
             | ( "typeof" "(" "return" [ "," <expression> ] ")" )

---
    alias-decl ::= "alias" <alias-decl-x>+ ";"

    alias-decl-x ::= <expression> "as" <identifier>

    typedef-decl ::= "typedef" <typedef-decl-x>+ ";"

    typedef-decl-x ::= <type> "as" <identifier>
---
    initializers ::= <intializer> { "," <initializer> }

    initializer ::= <expression>
                  | <tuple-initialzer>
                  | <array-literal>
                  | <assoc-array-literal>
                  | <struct-initializer>
                  | "void"

    tuple-initializer ::= <expression> { "," <expression> }

    struct-initializer ::= "{" <struct-member-initializer> { "," <struct-member-initializer> } [","] "}"

    struct-member-initializer ::= <expression>
                                | ( "." <identifier> "=" <initializer> )

## 3.5. Expressions
    <expression> ::= <assign-expression>

    assign-expression ::= <conditional-expression> [ <assign-operator> <assign-expression> ]

    assign-operator ::= "="
                      | "+="
                      | "-="
                      | "*="
                      | "/="
                      | "%="
                      | "&="
                      | "|="
                      | "^="
                      | "^^="
                      | "~="
                      | "<<="
                      | ">>="

    conditional-expression ::= <or-expression>
                             | ( <or-expression> ? <expression> : <expression> )
                             | ( <or-expression> ?? <expression> )

    OrExpression:
        AndExpression
        OrExpression || AndExpression

    or-expression ::= [ <or-expression> "||" ] <and-expression>

    and-expression ::= [ <and-expression> "&&" ] <binary-or-expression>

    binary-or-expression ::= [ <binary-or-expression> "|" ] <binary-xor-expression>

    binary-xor-expression ::= [ <binary-xor-expression> "^" ] <binary-and-expression>

    binary-and-expression ::= [ <binary-and-expression> "&" ] <compare-expression>

    compare-expression ::= <shift-expression>
                         | <shift-expression> <compare-operator> <shift-expression>

    compare-operator ::= "=="
                       | "!="
                       | "<"
                       | "<="
                       | ">"
                       | ">="

    shift-expression ::= [ <shift-expression> ( "<<" | ">>" ) ] <add-sub-expression>

    add-sub-expression ::= [ <add-sub-expression> ( "+" | "-" ) ] <mul-div-mod-expression>

    mul-div-expression ::= [ <mul-div-expression> ( <mul-div-operator> ) ] <unary-expression>
    mul-div-operator :: = "*" | "/" | "%" | "~" | "^^" | ".."

    unary-expression ::= ( <unary-operator> <unary-expression> )
                       | <cast-expression>
                       | <transmute-expression>
                       | <postfix-expression>
                       | ( <type> "." ( <identifier> | <template-instance> ) )
    unary-operator ::= "+" | "-" | "*" | "&" | "~" | "!" | "++" | "--"

    cast-expression ::= <expression> "as" <type>
    transmute-expression ::= <expression> "transmute" <type>

    PostfixExpression:
        PrimaryExpression
        PostFixExpression ++
        PostFixExpression --
        IndexExpression
        SliceExpression

    postfix-expression ::= <primary-expression>
                         | ( <postfix-expression> ( "++" | "--" ) )
                         | <index-expression>
                         | <slice-expression>

    index-expression ::= "[" <assign-expression> "]"\

    slice-expression ::= <postfix-expression>  "[" <slice> "]"
    slice ::= <assign-expression> [ ".." <assign-expression> ] { "," <slice> }

    function-call-expression ::= [ <symbol-identifier> ( "." | "?." ) ] <identifier> "(" <argument-list> ")"

    primary-expression ::= ( ["."] ( <identifier> | <template-instance> ) )
                         | "this"
                         | "super"
                         | "null"
                         | "true"
                         | "false"
                         | <interger-literal>
                         | <float-literal>
                         | <string-literal>
                         | <array-literal>
                         | <assoc-array-literal>
                         | ( <basic-type> | <template-instance> ) "." <identifier>
                         | <type> "(" [<argument-list>] ")"
                         | "(" <expression> ")"
                         | <mixin-expression>
                         | <import-expression>
                         | <typeid-expression>
                         | <is-expression>
                         | <new-expression>
                         | <delete-expression>
                         | <special-keyword>
---
    mixin-expression ::= "mixin" "(" <assign-expression> ")"

    typeid-expression ::= "typeid" "(" ( <type> | <expression> ) ")"

    is-expression ::= "is" "(" <type> ( ":" | "==" ) <type-specialization> [ ( "," <template-paramter> )+ ] ")"
    type-specialization ::= <type> | <type-modifier> | "struct" | "class" | "union" | "interface" | "enum"

    new-expression ::= "new" [ "(" <argument-list> ")" ] <type> [ ( "(" <argument-list> ")" ) | ( "[" <assign-expression> "]" ) ]

    delete-expression ::= "delete" <unary-expression>
---
    array-literal ::= "[" <initializer> { "," <initializer> } "]"

    assoc-array-literal ::= "[" <assoc-array-member> { "," <assoc-array-member> } "]"

    assoc-array-member ::= <expression> ":" <initializer>

    argument-list ::= <argument> { "," <argument> }

    argument ::= [ <identifier> ":" ] [ "ref" | "out" ] <assign-expression>

    special-keyword ::= "__FILE__"
                      | "__FILE_FULL_PATH__"
                      | "__PACKAGE__"
                      | "__MODULE__"
                      | "__NAMESPACE__"
                      | "__LINE__"
                      | "__FUNC__"
                      | "__PRETTY_FUNC__"

## 3.6. Statements
    statement ::= ";"
                | <statement-not-empty>
                | <scope-block-statement>

    statement-not-empty ::= <statement-not-empty-no-case-no-default>
                          | <case-statement>
                          | <default-statement>

    statement-no-scope ::= ";"
                         | <statement-not-empty>
                         | <block-statement>

    statement-not-empty-no-scope ::= <statement-not-empty>
                                   | <block-statement>

    statement-not-empty-no-case-no-default ::= <labeled-statement>
                                             | <expression-statement>
                                             | <declaration-statement>
                                             | <assert-statement>
                                             | <if-statement>
                                             | <while-statement>
                                             | <do-while-statement>
                                             | <for-statement>
                                             | <foreach-statement>
                                             | <switch-statement>
                                             | <break-statement>
                                             | <fallthrough-statement>
                                             | <continue-statement>
                                             | <return-statement>
                                             | <goto-statement>
                                             | <defer-statement>
                                             | <try-statement>
                                             | <throw-statement>
                                             | <asm-statement>
                                             | <static-assert>
                                             | <compile-time-statement>

    ScopeStatement:
        StatementNotEmpty
        BlockStatement

    scope-statement ::= <statement-not-empty>
                      | <block-statement>
                      
    scope-block-statement ::= <block-statement>


    block-statement ::= "{" <statement>* "}"
    block-statement-not-empty ::= "{" <statement>+ "}"

    label-statement ":" <identifier> ":" [ <statement-no-scope> | <statement> ]

    expression-statement ::= <expression> ";"
    declaration-statement ::= <declaration>
    assert-statement ::= "assert" "(" <expression> { "," <expression> } ")"
---
    if-statement ::= "if" "(" <expression> ")" <scope-statement> [ <else-statement> | <elif-statement> ]
    else-statement ::= "else" <scope-statement>
    elif-statement ::= "elif" "(" <expression> ")" <scope-statement>
---
    while-statement ::= "while" "(" <expression> ")" <scope-statement>
---
    do-while-statement ::= "do" <scope-statement> "while" "(" <expression> ")" ";"
---
    for-statement ::= "for "(" <for-init> ";" <expression> ";" <for-inc> ")" <scope-statement>

    for-init ::= <empty-statement> | <expression> | <varaible-decl>
    for-inc ::= <expression> | <empty-statement>
---
    foreach-statement ::= "foreach" "(" <foreach-expression> ")" <scope-statement>

    foreach-expression ::= <foreach-variable> { "," <foreach-variable> } "in" <expression>

    foreach-variable ::= [<foreach-variable-attrib>] <identifier> { "," [<foreach-variable-attrib>] <identifier> } [ ":" <type> ]
---
    switch-statement ::= "switch" "(" <expression> ")" <scope-statement>
    case-statement ::= "case" <expression> ":" <scope-statement>
    default-statement ::= "default" ":" <scope-statement>
---
    fallthrough-statement ::= "fallthrough" ";"
    continue-statement ::= "continue" ";"
    break-statement ::= "break" ";"
    return-statement ::= "return" [ <expression> { "," <expression> } ] ";"

---
    defer-statement ::= "defer" <exression> ";"
---
    try-statement ::= "try" <scope-statement> <catch-statement>* [<finally-statement>]
    catch-statement ::= "catch" "(" <identifier> ":" <type> ")" <statement-not-empty>
    finally-statement ::= finally <statement-not-empty>

    throw-statement ::= "throw" <expression> ";"
---
    goto-statement ::= "goto" ( ( ["case"] <identifier> ) | "default" ) ";"

## 3.7. Functions
    FunctionDecl:
        func [FunctionAttributes]opt FunctionDeclarator FunctionBody
        func OperatorOverload

    function-decl ::= [<function-attributes>] <function-declarator> <function-body>

    function-declarator ::= ( <identifier> | <overloadable-operator> ) [<template-parameters>] <parameters> [ "->" <type> ]

    parameters ::= "(" ( <multi-parameter> | <single-parameter> ) { "," ( <multi-parameter> | <single-parameter> ) } [ "," <variadic-parameter> ] ")"

    multi-parameter ::= <parameter-identifier> ( "," <parameter-identifier> )+ ":" <type>

    single-parameter ::= <parameter-identifer> ":" <type> [ "=" <expression> ]

    variadic-parameter ::= <identifier> [ ":" <type> ] "..."

    parameter-identifier ::= [<parameter-attributes>] <identifier>

    function-body ::= ( <function-contract>* ["do"] <block-statement> ) | ";"

    parameter-attribute ::= <type-modifier>
                          | "in"
                          | "out"
                          | "ref"
                          | "lazy"
---
    delegate-decl ::= "delegate" <identifier> <parameters> [ "->" <type> ] ";"

    delegate-type ::= "delegate" <parameters> [ "->" <type> ]

    function-literal ::= ( "delegate" <parameters> [ "->" <type> ] <function-body> ) | "lambda
---
    lambda ::= ( <identifier> | ( "(" <identifier> ( "," <identifier> )+ ")" ) ) "=>" [<lambda-captures>] <lambda-body>

    lamda-body ::= ( <function-contract>* ["do"] <block-statement> ) | <expression>

    lambda-captures ::= "[" <lambda-capture> { "," <lambda-capture> } "]"
    lambda-capture ::= ( ["&"] <identiier> )
                     | "="
                     | "&"
                     | "this"
---
    function-contract ::= <in-out-contract-expression> | <in-out-contract-statement>

    in-out-contract-expression ::= <in-contract-expression> | <out-contract-expression>
    in-contract-expression ::= "in" "(" <expression> ")"
    out-contract-expression ::= "out" "(" [<identifier>] ":" <expression> ")"

    in-out-contract-statement ::= <in-contract-statement> | <out-contract-statement>
    in-contract-statement ::= "in" <block-statement>
    out-contract-statement ::= [ "(" <identifier> ")" ] <block-statement>
---
    overloadable-operator ::= "opUnary"
                            | "opBinary"
                            | "opBinaryR"
                            | "opAssign"
                            | "OpEquals"
                            | "opCmp"
                            | "opCast"
                            | "opImplicitCast"
                            | "opCall"
                            | "opIndex"
                            | "opIndexAssign"
                            | "opIndexOpAssign"
                            | "opSlice"
                            | "opSliceOpAssign"
                            | "opDollar"

## 3.8. Classes
    aggregate-decl ::= <class-decl>
                     | <struct-decl>
                     | <interface-decl>
                     | <union-decl>

    class-decl ::= ( [attributes] "class" <identifier> [<base-class-list>] <aggregate-body> )
                 | <class-template-decl>

    base-class-list ::= ":" <identifier> { "," <identifer> }

    aggregate-body ::= "{" <decldef>* "}"

    constructor := [<function-attributes>] [ "static" ["shared"] ] "this" <parameters> ( ";" | <function-body> )
    destructor := [<function-attributes>] [ "static" ["shared"] ] "~" "this" "(" ")" ( ";" | <function-body> )
    post-blit := [<function-attributes>] "this" "(" "this" ")" ( ";" | <function-body> )

    class-invariant ::= "invariant" "(" ")" <block-statement>

    allocator ::= "new" <parameters> ( ";" | <function-body> )
    allocator ::= "delete" "(" ")" ( ";" | <function-body> )

## 3.9. Structures
    struct-decl ::= ( [<attributes>] "struct" [<identifier>] <aggregate-body> ) | <struct-template-decl>
    struct-invariant ::= "invariant" "(" ")" <block-statement>

## 3.10. Interfaces
    interface-decl ::= ( [<attributes>] "interface" <identifier> [<base-interface-list>] <aggregate-body> ) | <interface-template-decl>
    base-interface-list ::= ":" <identifier> { "," <identifier> }

## 3.11. Unions
    union-decl ::= [<attributes>] "union" [<identifer>] <aggregate-body>

## 3.12. Enums
    enum-decl ::= [<attributes>] "enum" [<identifier>] [ ":" <type> ] <enum-body>
    enum-body ::= "{" <enum-member> { "," [<enum-member>] } "}"
    enum-member ::= <identifier> [ "=" <initializer> ] [","]

## 3.13. Properties
    Property:
        [Attribute]opt Identifier : Type { ProperyGetSet }
        [Attribute]opt Identifier : Type { ProperyGetSet } = Initializer;

    ProperyGetSet:
        ProperyGet
        ProperySet
        ProperyGet ProperySet
        ProperySet ProperyGet

    PropertySet:
        set BlockStatementNotEmpty
        set ;
        VisibilityAttribute set;

    PropertyGet:
        get BlockStatementNotEmpty
        get ;

    property ::= [<attrubutes>] <identifier> ":" <type> "{" <property-get-set> "}" [ "=" <initializer> ";" ]
    property-get-set ::= ( <property-set> | <property-get> )+
    property-set := ( "set" <block-statement-not-empty> ) | ( [<visibility-attribute>] "set" ";" )
    property-get := "get" ( <block-statement-not-empty> | ";" )

## 3.14. Attributes
    attribute-specifier ::= ( <colon-attribute> ":" )
                          | ( <attribute> <block-statement> )

    Attribute:
        VisibilityAttribute
        LinkageAttribute
        AlignAttribute
        AbstractAttribute
        ThreadingAttribute
        static
        extern
        inout
        AtAttribute

    attributes ::= <attribute> { "," <attribute> }
    attribute ::= <visibility-attribute>
                | <linkage-attribute>
                | <align-attribute>
                | <thread-attribute>
                | "abstract"
                | "static"
                | "extern"
                | "inout"
                | <at-attribute>

    colon-attribute ::= <visibilty-attribute>

    visibility-attribute ::= "private"
                           | "protected"
                           | "internal"
                           | "public"
                           | "export"

    function-attribute ::= "const"
                         | "cconst"
                         | "final"
                         | "override"
                         | "abstract"
                         | "throws"

    at-attribute ::= ( "@" <depricated-attribute> )
                   | ( "@" "disable" )
                   | ( "@" <user-defined-attribute> )

    deprecated-attribute ::= "deprecated" [ "(" <string-literal> ")" ]

    linkage-attribute ::= "extern" "(" <linkage-type> ")"
    linkage-type ::= "C"
                   | "Windows"
                   | "System"

    align-attribute ::= "align" "(" <integer-literal> ")"

    threading-attribute ::= "synchronized"
                          | "shared"
                          | "__global"

    user-defined-attribute ::= ( <identifier> | <template-instance> ) [ "(" <arguments> ")" ]

## 3.15. Extensions
    extension ::= "extension" <type> [ ":" <identifier> { "," <identifier> } ] <aggregate-body>

## 3.16. Templates
    template-declaration ::= [<attributes>] "template" <template-identifier> <template-parameters> <aggregate-body>

    template-parameters ::= "<" <template-parameter> { "," <template-parameter> } ">"

    template-parameter ::= <template-type-parameter>
                         | <template-value-parameter>
                         | <template-variadic-parameter>

---
    template-type-parameter ::= <identifier> [ <template-type-parameter-specialization> | <template-type-parameter-default> ]

    template-type-parameter-specialization ::= ":" <type>

    template-type-parameter-default ::= "=" <type>

    template-value-parameter ::= <basic-type> <identifier> [ <template-value-parameter-specialization> | <template-value-parameter-default> ]

    template-value-parameter-specialization ::= ":" <expression>

    template-value-parameter-default ::= "=" <expression>

    template-variadic-parameter ::= <identifer> "..."
---
    template-instance ::= <identifier> <template-arguments>

    template-arguments ::= ! ( ( <template-argument> ) | ( "<" <template-argument> { "," <template-argument> } ">" ) )

    template-argument ::= <type> | <expression>
---
    class-template-decl ::= [<attributes>] "template" "class" <template-parameters> [<base-class-list>] [<template-constraint>] <aggregate-body>

    class-template-decl ::= [<attributes>] "template" "class" <template-parameters> [<base-interface-list>] [<template-constraint>] <aggregate-body>

    struct-template-decl ::= [<attributes>] "template" "struct" <template-parameters> [<template-constraint>] <aggregate-body>
---
    template-constraint ::= "if" "(" <expression> ")"

## 3.17. Mixin template
    mixin-template-decl ::= "mixin" "template" <identifier> [<template-parameters>] [<template-constraint>] "{" decldef* "}"

    template-mixin ::= "mxin" <identifier> [<template-arguments>] [<identifier>] ";"

## 3.18. UnitTesting:
    unittest-decl ::= "unittest" [<identifier>] <scope-statement>

# 3.19. Compile-time expressions
    conditional-decl ::= <condition> ( <decl-block> | <statement-not-empty-no-scope> ) [ "else" ( <decl-block> | <statement-not-empty-no-scope> ) ]dd

    condition ::= <version-condition> | <debug-condition>

    version-condition ::= "version" "(" ( <integer-literal> | <identifier> ) ")"
    debug-condition ::= "debug" [ "(" ( <integer-literal> | <identifier> ) ")" ]

    condition-specification ::= ( "version" | "debug" ) "=" ( <integer-literal> | <identifier> )

    compile-time-statement ::= "cconst" ( <if-statement> | <for-statement> | <foreach-statement> )

    static-assert ::= "static" <assert-statement>

## 3.20. Pragma
    pragma ::= "pragma" "(" <idnentifer> ( "," <argument> )* ")"

## 3.21. Inline assembly (x86)
    AsmStatement:
        asm { AsmInstuctionList }

    AsmInstructionList:
        AsmInstruction ;
        AsmInstruction ; AsmInstructionList
---
    AsmInstruction:
        Identifier : AsmInstruction
        align IntegerExpression
        even
        naked
        db Operands
        ds Operands
        di Operands
        dl Operands
        df Operands
        dd Operands
        de Operands
        AsmOpcode
        AsmOpcode AsmOperands

    AsmOperands:
        AsmOperand
        AsmOperand, AsmOperands
---
    AsmRegister:
        AL AH AX EAX
        BL BH BX EBX
        CL CH CX ECX
        DL DH DX EDX
        BP EBP
        SP ESP
        DI EDI
        SI ESI
        ES CS SS DS GS FS
        CR0 CR2 CR3 CR4
        DR0 DR1 DR2 DR3 DR6 DR7
        TR3 TR4 TR5 TR6 TR7
        ST
        ST(0) ST(1) ST(2) ST(3) ST(4) ST(5) ST(6) ST(7)
        MM0 MM1 MM2 MM3 MM4 MM5 MM6 MM7
        XMM0 XMM1 XMM2 XMM3 XMM4 XMM5 XMM6 XMM7

    AsmRegister64:
        RAX RBX RCX RDX
        BPL RBP
        SPL RSP
        DIL RDI
        SIL RSI
        R8B R8W R8D R8
        R9B R9W R9D R9
        R10B R10W R10D R10
        R11B R11W R11D R11
        R12B R12W R12D R12
        R13B R13W R13D R13
        R14B R14W R14D R14
        R15B R15W R15D R15
        XMM8 XMM9 XMM10 XMM11 XMM12 XMM13 XMM14 XMM15
        YMM0 YMM1 YMM2 YMM3 YMM4 YMM5 YMM6 YMM7
        YMM8 YMM9 YMM10 YMM11 YMM12 YMM13 YMM14 YMM15

## 3.22. Application Binary Interface 
    // used: A B C D E F M P S T U W Y Z
    MangledName:
        _M QualifiedName

    QualifiedName:
        QualifiedIdentifier QualifiedTypeData

    QualifiedIdentifier:
        SymbolName
        SymbolName QualifiedIdentifier

    QualifiedTypeData:
        TypeFunction
        TypeAggregate

    SymbolName:
        LName
        TemplateInstance
        IdentifierBackRef
---
    TypeFunction:
        F TypeFunctionNoReturn [Type]opt // Free function
        M TypeFunctionNoReturn [Type]opt // Member function

    TypeFunctionNoReturn:
        CallConvention [FuncAttribs]opt [MangleParameters]opt ParamClose
---
    TypeAggregate:
        C                               // Class
        S                               // Structure
        I                               // Interface
        U                               // Union
---
    TemplateInstanceName:
        TemplateId LName TemplateArgs Z

    TemplateId:
        __T
        __C              // Template contraint arguments

    TemplateArgs:
        TemplateArg
        TemplateArg, TemplateArgs

    TemplateArg:
        TemplateArgX
        H TemplateArgX

    TemplateArgX:
        T Type
        V Type Value
        S QualifiedName
---
    IdentifierBackRef:
        Q NumberBackRed

    NumberBackRef:
        lower-case-letter
        upper-case-letter NumberBackRef
---
    MangleValue:
        n
        i Number
        N Number
        e HexFloat
        CharWidth Number _ HexDigits

    HexFloat:
        NAN
        INF
        NINF
        HexDigits P Exponent
        N HexDigits P Exponent

    Exponent:
        Number
        N Number

    Number:
        N Number
        Number

    CharWidth:
        c
        w
        q
---
    Name:
        NameStart
        NameStart NameChars

    NameStart:
        _
        Alpha

    NameChars:
        NameChar
        NameChar NameChars

    NameChar:
        NameStart
        digit

    LName:
        Number Name

    Number:
        digit
        digit Number

    digit:
        0
        1
        2
        3
        4
        5
        6
        7
        8
        9
---
    CallingConvention:
        m   // MJay
        c   // C
        w   // Windows
---
    FuncAttrs:
        FuncAttr
        FuncAttr FuncAttrs

    FuncAttr:
        FuncAttibConst
        FuncAttibCConst
        FuncAttibSafe
        FuncAttibTrusted

    FuncAttibConst:
        Nc

    FuncAttibCConst:
        Nd

    FuncAttibSafe:
        Ns

    FuncAttibTrusted:
        Nt
---
    MangleParameters:
        MangleParameter
        MangleParameter, MangleParameters

    MangleParameter:
        MangleType
        J MangleType        // out
        K MangleType        // ref
        L MangleType        // lazy

    MangleParamClose:
        X                   // Variadic template: T t... )
        Y                   // C-style variadic: , ... )
        Z                   // Non-variadic

---
    MangleType:
        MangleTypeX
        MangleTypeModifiers MangleTypeX

    MangleTypeModifiers:
        MangleConstModifier
        MangleCConstModifier
        MangleImmutableModifier
        MangleSharedModifier
        MangleGlobalModifier

    MangleConstModifier:
        No

    MangleCConstModifier:
        Nx

    MangleImmutableModifier:
        Ny

    MangleSharedModifier:
        Nz


    MangleTypeX:
        MangleTypeArray
        MangleTypePointer
        MangleTypeClass
        MangleTypeStruct
        MangleTypeEnum
        MangleTypeTypedef
        MangleTypeDelegate
        MangleTypeVoid
        MangleTypeByte
        MangleTypeUByte
        MangleTypeShort
        MangleTypeUShort
        MangleTypeInt
        MangleTypeUInt
        MangleTypeLong
        MangleTypeULong
        MangleTypeFloat
        MangleTypeDouble
        MangleTypeBool
        MangleTypeNull
        MangleTypeTuple
        MangleTyperune

    MangleTypeArray:
        A Type

    MangleTypePointer:
        P Type

    MangleTypeClass:
        C

    MangleTypeStruct:
        S

    MangleTypeEnum:
        E

    MangleTypeTypedef:
        T

    MangleTypeDelegate:
        D

    MangleTypeVoid:
        v

    MangleTypeByte:
        g

    MangleTypeUByte:
        h

    MangleTypeShort:
        s

    MangleTypeUShort:
        t

    MangleTypeInt:
        i

    MangleTypeUInt:
        j

    MangleTypeLong:
        l

    MangleTypeULong:
        m

    MangleTypeFloat:
        f

    MangleTypeDouble:
        d

    MangleTypeBool:
        b

    MangleTypeChar:
        c

    MangleTypeWChar:
        w

    MangleTypeRune:
        r

    MangleTypeString:
        u

    MangleTypeNull:
        n

    MangleTypeTuple
        B Parameters Z

# 4. Packages, modules and namespaces
## 4.1. Packages
Packages are containers containing multiple sub-packages and modules. They are mainly used to organize and group modules depending on their use cases and user specified differences. Often, packages will refer to  the underlying OS file structure of projects.

Any code inside of a package will automatically be put inside a namespace corresponding to the package name.

Packages are defined uses the following syntax: `package name.other;`

If no package is defined, the module will become part of a global package, without any namespace.

## 4.2. Modules
Modules do NOT correspond to underlying files and can therefore exist out of multiple files.

Modules automatically provide a namespace for its content. This namespace will also be in the respective package namespace.

Modules are mostly independant from each other, with the exception that they ARE dependent on modules they import symbols from. If a module imports or uses a symbol symbol from another module, the module should not be affected by any changes to other symbols in the imported module.

A module can be easily defined using the following syntax: 
```
module modname;
```

When no specific module is defined, the compiler will make a corresponding module for each file, with the module name being the name of the file with its extension stripped.

In case no module is defined and the filename does not correspond to a valid module name, the invalid characters will be replaced by an underscore, i.e. `some-file` will become part of the module `some_file`.

A compiler should also allow you to pass a group of files or folder, together with a module identifier. The compiler will than compile these files into a single module with the identifier that was passed.

The module is also allowed to be annoteded with certain attributes.

An example of this, is that is allows the deprication of certain modules and will cause a warning when this module is imported in any file:
```
@deprecated("Use foo2 instead")
module foo;
```
This will give the following warning: `Deprecation: module foo is deprecated - Use foo2 instead`

## 4.3. Namespaces
Namespaces allow the user to split up code inside of a module, grouping certain parts of the code or hidding classes inside of an `internal` namespace.

Packaged and modules also create a namespace of their own, in which the code will resign.

## 4.4. Imports
Imports allow the user to include code from other modules into their projects. Imports are also in no way required to be global, imports are allowed in different scopes, e.g. class or function specific imports. When importing a module inside of a function, it is however required to have the import statement before the use of any symbols from that specific module.

There are several version of the import statement, each with their own uses.

### 4.4.1. Basic import
This is the simplest way of importing modules.
```D
package app;
module main;

import std.io;  // Global import, allows use of symbols throughout entire file

class Foo : BaseClass {

    import app.foo; // Scoped import, allows use of symbols throughout class;

    func bar()
    {
        import app.bar; // Scoped import, allows use of symbols throughout function;
        writeln("Hello bar()!");
    }
}
```

When it's not specified where a symbol comes from, teh compiler will automatically try to infer the type, in the example above, the compiler will search for the symbol in the following order.
- Any declarations inside of `bar()`
- Any declarations inside of `Foo`
- Any declarations inside of `BaseClass`
- Any declarations inside of the module, including other files in the module

If the function wasn't found, the compiler will go over all imports, starting from imports in the most inner scope, and will work its way to the outermost scope. The search process will end once the function is found. If an import is solely imported in a scope, the imported symbols cannot be used outside of this scope.

When the same function is found more than once in a scope or imported module, the compiler will give an error, since these symbols are disambiguous.

```D
module main;
import A;   // Has foo() function
import B;   // Has foo() function

func main(args : string[])
{
    foo(); // Error, disambiguous symbol: A.foo() or B.foo() ?
    A.foo(); // Ok, calls A.foo()
    B.foo(); // Ok, calls B.foo()
}
```

### 4.4.2. Public imports
By default, the imports are handled as if they are private to the file that is being compiled. When an import is declared public, the resulting module will also export the symbols from the imported module. Any other module that imports this module, will have access to the public import.

```D
module B;
public import A; // Has foo() function
func bar() {}
```
```D
module C;
import B;

func main(args : string[])
{
    foo(); // Ok, calls A.foo()
    bar(); // Ok, calls B.bar()
}
```

### 4.4.3. Static import
A static import requires that the user explicitily use the full name of an object or function.
```D
module app;
static import std.io;

func main(args : string[])
{
    writeln("Hello World!"); // Error, writeln is undefined
    std.io.writeln("Hello World!"); // Ok, calls std.io.writeln()
}
```

### 4.4.4. Aliased import
An imported module can be referenced via an alias. Aliasing a module disables the inferring of any function contained in that module and disallows the use of the full name

```D
import std.io as stdio;

func main(args : string[])
{
    stdio.writeln("Hello World!"); // Ok, calls std.io.writeln()
    std.io.writeln("Hello World!"); // Error, std is undefined
    writeln("Hello World!"); // Error, writeln() is undefined
}
```

### 4.4.4. Selective import
Exclusivly importing symbols from a module can also be accomplished. Separate functions and object are also allowed to be aliased
```D
import std.io : writeln, write as foo;

func main(args : string[])
{
    std.io.writeln("Hello World!"); // Error, std is undefined
    writeln("Hello World!"); // Ok, calls std.io.writeln()
    write("Hello World!"); // Error, write() is undefined
    foo("Hello World!"); // Ok, calls std.io.write()
    writef(fmt, "abc"); // Error, writef() is undefined
}
```
### 4.4.5. Combining aliased and selective imports
It is possible to combine aliased and selective imports

```D
import std.io as io : writeln as foo;

func main(args : string[])x : 
{
    writeln("Hello World!"); // Error, writeln() is undefined
    foo("Hello World!"); // Ok, calls std.io.writeln()
    std.io.writeln("Hello World!"); // Error, std is undefined
    io.writeln("Hello World!"); // Ok, calls std.io.writeln(), since io is an alias to std.io
    io.foo("Hello World!"); // Error, foo() is not a member of io, foo is local
}
```
## 4.5. Module scope
It is possible to refer to 'global' data inside each module, even when it has been hidden by a local variable of the same name. This can done by simply prepending a dot to a variable, when this is done, the compiler will look for a variable in the module score, instead of the local scope.

`.name`

## 4.6. Module constructors and destructors
MJay allows each module to have constructors and destructors, these can be used to manually initialize static and 'global' data inside of the module.

Each module is allowed to have multiple constructors and destructors. This allows each file to have a static constructor and destructor to create and destroy static data used by the module. To do this, simply declare a static constructor or destructor in the module, outside of class or struct scopes.

```D
module app;

static this()
{
    // Is run for every thread that is created
}

shared static this()
{
    // Is run for every thread that is destroyed
}

static ~this()
{
    // Is run at the start of the program
}

shared static ~this()
{
    // Is run at the end of the program
}
```

These static constructors will be called in the order they are compiled in the module. If any code is dependent on any, previously initialized, static variable, make sure they are initialized in the same file, since spreading this over multiple files can cause undefined behaviour.

A good comparison for these constructors, is the initialization of static variables in C++, but in this case, when done correctly, you can have complete control over the order of initialization.

### 4.6.1. Order of construction
Shared module constructors will always be called before the module constructor. The order of initialization between each module is implicitly defined by the module imports, starting from the module that is initially executed. Each module expects that it is dependent on the other module's constructor.

Circular dependencies will cause an issue when both modules have a constructor, if this were to happen, this well cause a runtime exception and will teminate the program.

### 4.6.1. Order of destruction
The calling of destructors will happen in the reverse order of construction, since the module expects it will have to destruct its own data before it can destruct the modules it is dependent on.

# 5. Declarations

## 5.1. Declaration syntax
Each declaration read right to left:
```D
x : int;      // x is an int
x : int*;     // x is a pointer to an int
x : int**;    // x is a pointer to a pointer to an int
x : int[];    // x is an array of ints
x : int*[];   // x is an array of pointers to ints
x : int[]*;   // x is a pointer to an array of ints
```

Arrays are also read form right to left:
```D
x : int[3];       // x is an array of 3 ints
x : int[3][5];    // x is an array of 5 arrays of 3 ints
x : int[5]*[3];   // x is an array of 3 arrays of pointers to arrays with 5 ints
```

Delegates are defined, starting with the `delegate` keyword
```D
x : delegate(char) -> int;      // x is a delegate pointing to 
                                // a function taking charater argument 
                                // and returning an int

x : (delegate(char) -> int)[];  // x is an array of delegates pointing to 
                                // a function taking charater argument 
                                // and returning an int
```

A declaration can also create multiple variables of 1 type, in 1 declaration.
```D
x, y : int;   // x and y are ints
x, y : int*;  // x and y are pointers to ints
```

## 5.2. Type inference
It is also possible to infer the type automatically when assigning a value with or without a type modifier from a literal.
```D
static x := 1l;  // x is a long
static x := 1;   // x is a static int
const x := 2.3f; // x is a constant float
```

types can also be infered for dynamically created objects
```D
func foo() { return 1u; }
class C { ... }

x := foo();    // x is an unsinged int
x := new C;     // x is a pointer to an instance of C
```

An array literal will create a dynamic array, instead of a static array
```D
// x is a string[], not a string[7];
x := ["All", "your", "base", "are", "belong", "to", "us"];
```

## 5.3. Aliased types
Every type can have an alias assigned, which can be used anywhere the original type could have been used.

```D
alias foo.bar.fizzbuzz as mytype;
```

When it comes to compilation, the aliased and originally are semantically identical and cannot be distinguished between, this also makes it so there is no differance when it comes to overloading functions.

```D
alias int as myint;

func foo(int val) { ... };
func foo(myint val) { ... }; // Error: multiple definitions of function 'foo'
```

It is also allowed to alias functions.

```D
import abc;
alias abc.foo as bar;

bar(); // calls abc.foo();
```

Aliases can also be used with templates.
```D
template struct Foo<T> { alias T as t; }
alias Foo<int> as t1;
alias Foo<int>.t as t2;
alias t1.t as t3;
alias t3 as t4;

v1 : t1.t;    // v1 is an int
v2 : t2;      // v2 is an int
v3 : t3;      // v3 is an int
v4 : t4;      // v4 is an int
```

Aliases can also be used to allow compile-time switching of the type, while maintaining valid code.

```D
version(Win32)
{
    alias win32.foo as mytype;
}
version(Linux)
{
    alias linux.bar as mytype;
}
```

Aliases can also be used to refer to a function in a base class.

```D
class A
{
    func foo(int a) -> int { return 1; }
}

class B : A
{
    func foo(int a, uint b) -> int { return 2; }
}

class C : B
{
    func foo(int a) -> int { return 3; }
    alias foo = B.foo;
}

class D : C
{
}

void test()
{
    b : D = new D();
    i : int;

    i = b.foo(1, 2u);   // calls B.foo
    i = b.foo(1);       // calls C.foo
}
```

Note that aliases can make it hard to distinguish between objects and functions. The compiler does make this distinction when the alias is subsituted.

It is also illegal to alias espressions
```D
alias a + b as c; // illegal, 'a + b' is an expression
```

## 5.4. Extern declarations
Variables can be annotated by `extern` meaning that the variable is not part of this module and should be defined in another module and be able to be resolved when linking.

While it is bad practice to use extern without any additional info, the compiler allows this. Extern variables are helpful when the external symbol is defined in a C-style library.

```D
extern extern(C) int bar;
```

## 5.5. Void initializers
By default each value will be assigned a default value, when no explicit value is assigned. A void initializer allows the user to tell the compiler, not to set the value to a default value. Using the value before it is initialized, will result in undefined behavior.

```D
x : int := void; // Void will have a 'garbage' value
```

However, a type cannot be infered from a void initializer
```D
x := void; // Cannot infer type from a void initializer
```

Tips for a void initializer:
- Arrays, when you are certain that an array will be initialized manually or if all of the content isn't used, the void initializer can speed up the code, since there is no need to go through the array and set each value.
- The previous can also be applied to structs, classes and unions.
- Using void initializer for single values is rarely usefull, since the compiler will automatically optimize out dead code.
- For hot paths, it can be helpful to check if a void initializer is useful anywhere to speed up the code.

## 5.6 Bitfields
Since some times, the amount of bytes used for a type is more than required, the use of bitfields are allowed, which allows the efficient packing of variables.

```D
struct packed
{
    a : bool.1; // a takes up 1 bit, normally 8 bits
    b : bool.1; // b takes up 1 bit, normally 8 bits
    c : bool.1; // c takes up 1 bit, normally 8 bits
    d : bool.1; // d takes up 1 bit, normally 8 bits
    i : int.4; // i takes up 4 bits, normally 32 bits
}

assert(sizeof(packed) == 1); // packed uses 1 byte, 
                             // instead of the normal 8 bytes
                             // it would use without bitfields
```

# 6. Types
## 6.1. Basic types
| keyword | Default value |        Description       |
|---------|---------------|--------------------------|
|   void  |       -       | no type                  |
|   bool  |     false     | boolean value (8 bits)   |
|   byte  |       0       | signed 8 bits            |
|  ubyte  |       0u      | unsigned 8 bits          |
|  short  |       0       | signed 16 bits           |
|  ushort |       0u      | unsigned 16 bits         |
|   int   |       0       | signed 32 bits           |
|   uint  |       0u      | unsigned 32 bits         |
|   long  |       0l      | signed 64 bits           |
|  ulong  |      0ul      | unsigned 64 bits         |
|  float  |      0.0f     | floating point (32 bits) |
|  double |      0.0      | floating point (64 bits) |

## 6.2. Derived types
|        type       |         syntax        | Default value |
|-------------------|-----------------------|---------------|
|      pointer      |         type*         |      null     | 
|       array       |         type[]        | default(type) |
| associative array |       type[type]      |       -       |
|      delegate     | delegate type(params) |      null     |

## 6.3. User-definable type
- class
- struct
- union
- enum

## 6.4. Arithmatic convertions
Since it is allowed to mix different types in expression, the compiler needs to convert the variables to the correct types for the expression. The following rules are used for the conversion:
1. If both types are the same, no conversions are done
2. If either oparend is a `double`, both sides get converted to `double`
3. If either oparend is a `float`, both sides get converted to `float`
4. If both types are signed or unsigned, the smaller type gets converted to the larger
5. If the signed type is larger than the unsigned type, the unsigned type gets converted to the signed type
6. Otherwise the signed type gets converted to the unsinged type, so the sign is lost

There is a special case if one of the types is an enum value:
1. If both values are different enum types, this will result in a compilation error
2. If both values are the same enum type, that enum type will be returned
3. If one type is the base type of the other enum type, the enum gets converted to its base type
4. If one type does not match the enum's base type, the most appropriate type will be chosen

## 6.x. Typeof
The `typeof` keyword can be used to retrieve the resulting type of an expression.

```D
void foo(int i)
{
    j : typeof(i);        // j is of type int
    k : typeof(1)*;       // k is of type int*
    l : typeof(1 + 2.0);  // l is of type double
    m : int[typeof(k)];   // m is of type int[int*]
}
```

Expression inside the `typeof` operator are not evaluated, only the type gets generated

```D
i := 0;
typeof (i++) j; // j is of type int, i is still 0
```

There are a number of special cases for the `typeof` operator
- Inside an object, `typeof(this)` will return the type of the object
- Inside an object, `typeof(super)` will return the type of the base class, a compilation error will happen when the object has no base class
- Inside an non-member function, `typeof(this)` will return the type of the function
- Inside an function, `typeof(return)` will return the type of the return type

# 7. Properties
# 8. Attributes
# 9. Pragmas
# 10. Expressions
# 11. Statements
# 12. Arrays

# 13. Classes
# 14. Structures
# 15. Interfaces
# 16. Unions
# 17. Enums

# x. Conditional compilation

## x.x. Predefined version identifiers
| Identifier |               Description              |
|:----------:|:---------------------------------------|
|     MJC    | MJC Compiler                           |
|   Windows  | Microsoft Windows system               |
|    Win32   | Microsoft 32-bit Windows system        |
|    Win64   | Microsoft 64-bit Windows system        |
|    Linux   | All Linux systems                      |
|    Posix   | All Posix systems                      |
|   Android  | Android system                         |
|     X86    | 32-bit architecture                    |
|     X64    | 64-bit architecture                    |
|     ARM    | ARM architecture                       |
|    Core    | Defined when building core             |
|     Std    | Defined when building standard library |
|    None    | Never defined, turns code off          |
|     All    | Always defined, turns code on          |

## x.x Slicing
## x.x Contracts

# x. Type system and traits

# x. Inline assembly (x86)

# x. Application Binary Inteface
This section gives info about the ABI used during and after the compilation process. 

## x.x. Name mangling
For MJay to have typesafe linking, the names of symbols are mangled. This is required to ensure compatibility of otherwise conflicting symbols. Some of this mangled info can also be used to extract traits of the specific type.

Every mangled name starts with `_M` notifying that this is a MJay symbol. This is followed up by the `QualifiedName`, which includes all info to differentiate it from other symbols.

### x.x.1. Qualified name
The `QualifiedName` is made up from 2 sections. The first sections or the identifier exist out of the full name, included the template parameters, specializations and constraints, of the object that is being mangled. 

The second section or the type data depends on the whether the name refers to an object or a function. For an object, the section contains information about the objects type and for a function, the section contains the function parameters and its return type.

### x.x.2. Qualified identifier
For parts of the first section that aren't is made up from templates, the mangling is fairly simple. For each dot-separated section of the full name (including package, module, namespaces and parent objects), the section gets transformed by prepending its length to the section, i.e. `name` becomes `4name`. All dots will be ommited from this string, since enough info is given to identify each section.

### x.x.3. Template identifier
When a templated object or function is encounteres, this part gets transformed in a slightly different way. When a templated part of the full name is encountered, the section is started by a symbol identifying that this part references a template: `__T`.

This is then folowed by the name of the template, prepended with the length, as explained in the previous section.

Following this, a list of template arguments is added. The arguments follow a simple ruleset. First, to differentiate specialized and unspecialized arguments, a `H` is added when the argument is specialized. This is then followed up by a single character, identifying the type of arguments, followed by the specific info for that argument:
1. For an argument without a specified type, a `T` is added, followed by the identifier of the base type the argument is expected to have.
2. For an argument with a specified primitive type and a default value, a `V` is added, followed by the identifier of the type and its defualt (mangled) value.
3. For an argument with a specified type, a `S` is added, followed up by the `QualifiedName` of the type.

At the end of the template, `Z` is added, notifying that the template has ended.

### x.x.4. Default mangled value
The way in which the value is mangled, depends on the type of the value (i.e. wheter the value is an int, a float, etc).

- For a null value, `n` is used
- For a positive numeric value, `i` is used, followed up by the value
- For a negative numeric value, `N` is used, followed up by the absolute value
- For a floating point value, `e` is used followed up by the, hexadecimal encoded, value. This value can either be one of 3 special values: `NAN`, `INF`, `NINF`, or is otherwise stored as the values mantissa as hexadecimal, followed by its exponent in decimal.
- For a string, the value is started by a value representing the byte width of a character, followed by the length of the string and an underscore, this is finally followed by the character representations as hexadecimal digits.

### x.x.5. Back-reference
To try and limit the length of the qualified identifier, a special value is added that references a the original occurance of the name, which was already inserted beforehand.

This is identifier by the character `Q`, followed up by a base-26 value, stored as the upper case letters (A-Z), with the final digit stored as a lower case letter (a-z).

### x.x.6. Qualified type data
The type data is represented using a simple scheme. Whenever the `QualifiedName` refers to an object, the objects type get appended to the end of the `QualifiedName`. 

When the `QualifiedName` referse to a function, a more lengthy but simple system is used. 
- Function data starts with the function identifier `F`, immediatally followed by the calling convention.
- Method data starts with the function identifier `M`, immediatally followed by the calling convention.
- When the any function attribute are present, these are appended, each function attribute is notified by a leading `N` followed by the attribute specific identifier.
- For each paramater the function has, the type of the the parameter is added, prepended by any paramater attributes. Variadic varaibles get handled by the `ParamClose` specifier.
- The end of the parameter list is specified by the `ParamClose` specifier, which also handles variadic parameters.
- The final part of the function info is the type the function returns, this is handled like any other type.

# x. Standard library
The standard library is made up from 2 reserved packages: `core` and `std`. Defining these as the packages of other modules will work, but may cause undefined behaviour.

## x.1. Core package
The `core` package contains the runtime libary, this is the minumum code needed for a basic project to be able to run, since this contains core language features, this package will automatically be added to any project, without an explicit import required. This package is developed together with the core language. Any objects or function in this package, aren't directly accessable by the user and need to be interfaced using the respective `std` package


## x.2. Std package
The `std` package contains standalone code and a way of interfacing with the underlying `core` package and thus giving access to certain `core` objects and functions. This libary is developed, together with the `core` package and language.