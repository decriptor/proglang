# MJay (working-name) programming language specification

Note that this file is heavily WIP, formatting and specification are subject to frequent changes

Note that any explenation following the grammar section has been made to be human readable, so does not contain the full grammar, so for the correct representations, explicitly reference the lexical and grammar sections.

# Index
1. Introduction
2. Lexical
    1. Source code
    2. Lexical definitions
        1. Character set
        2. End Of File
        3. End Of Line
        4. WhiteSpace
        5. Comments
        6. Tokens
        7. Identifiers
        8. String literals
        9. Char literals
        10. Integer literals
        11. Floating point literals
        12. Keywords
        13. Built-in attribute keywords
        14. Special keywords
3. Grammar
    1. File structure
    2. Packages, modules and namespaces
    3. Imports
    4. Declarations
    5. Expressions
    6. Statements
    7. Functions
    8. Classes
    9. Structures
    10. Interfaces
    11. Unions
    12. Enums
    13. Properties
    14. Attributes
    15. Templates
    16. Mixin templates
    17. Unittests
    18. Compile-time expressions
    19. Pragma
    20. Inline assembly
    21. ABI
4. Packages, Modules and Namespaces
28. Application Binary Interface
    1. Name mangling
        1. Qualified name
        2. Qualified identifier
        3. Templates identifier
        4. Default mangles values
        5. Back references
        6. Qualified type data
29. Standard library
31. Appendices

# 1. Introduction

MJay is a general purpose programmming language that has both low and high level language features.
This document lays out the specification for this language

MJay is inspired and heavily based on the following languages: D, C++ and C#.
Some people may notice that MJay is a heavily modified version of D.

# 2. Lexical
Lexical analysis is independent of the syntax parsing and sementic analysis. The Lexer's job is to split up the source code  into tokens, which are then used during the compilation process. The lexical grammer describes the systax of these tokens.

## 2.1. Source code
Source code can be stored using the following formats
- ASCII
- UTF-8
- UTF-16BE
- UTF-16LE
- UTF-32BE
- UTF-32LE

The following UTF BOMs (Byte Order Marks) can be present at the start of the source

|  Format  |     BOM     |
|----------|-------------|
| UTF-8    | EF BB BF    |
| UTF-16BE | FE FF       |
| UTF-16LE | FF FE       |
| UTF-32BE | 00 00 FE FF |
| UTF-32LE | FF FE 00 00 |
| ASCII    | no BOM      |

When no BOM is present, the file is expected to start with a character smaller that U+0000007F


Source code is required to made up by the following character sets:
- WhiteSpace
- EndOfLine
- EndOfFile
- Comments
- Tokens
- Special tokens

## 2.2. Lexical definitions

### 2.2.1. Character set
    Character set: 
        Any string of unicode characters

### 2.2.2. End Of File
    EndOfFile:
        End of bytestream (file)

### 2.2.3. End Of Line
    End of Line:
        \u000A (\n)
        \u000D (\r)
        \u000A \u000D (\n\r)
        \u2028 (line seperator)
        \u2029 (paragraph separator)

### 2.2.4. White Space
    WhiteSpace:
        Space
        Space WhiteSpace

    Space:
        \u0009
        \u000B
        \u000C
        \u0020

### 2.2.5. Comments
    Comment:
        BlockComment
        NestedBlockComment
        LineComment

    NestedBlockComment:
        /* NestedBlockComment */
        BlockComment

    BlockComment:
        /* Characters */

    LineComment:
        // Characters EndOfLine

### 2.2.6. Tokens
    Token:
        Identifier
        StringLiteral
        CharacterLiteral
        IntegerLiteral
        FloatLiteral
        Keywords
        AttributeKeywords
        +
        ++
        +=
        -
        --
        -=
        *
        *=
        /
        /=
        |
        |=
        ||
        &
        &=
        &&
        ^
        ^=
        !
        !=
        =
        ==
        <
        <=
        <<
        <<=
        >
        >=
        >>
        >>=
        (
        )
        {
        }
        [
        ]
        ,
        .
        ..
        ...
        %
        %=
        ~
        ~=
        =>
        :
        ;
    
### 2.2.7. Identifier
    Identifier:
        IdentifierStart
        IdentifierStart IdentifierChars
    
    IdenifierChars:
        IdentifierChar
        IdentifierChar IdentifierChars

    IdentifierStart
        _
        Letter

    IdentifierChar
        IdentifierStart
        Digit

@TODO: Move
Identifiers starting with __ (double underscore) are reserved for the compiler and core language. Check specification for more info about core language identifier

### 2.2.8. String Literals
    StringLiteral:
        String
        WysiwygString
        InterpolatedString

    String:
        " StringCharacters "

    StringCharacters:
        StringCharacter
        StringCharacter , StringCharacters

    StringCharacter:
        Character
        EscapeSequence
        EndOfLine

    EscapeSequence:
        \"
        \\
        \x Hexadecimal

    WysiwygString:
        @" WysiwygCharacters "
        ` WysiwygCharacters `

    WysiwygCharacters:
        WysiwygCharacter
        WysiwygCharacter , WysiwygCharacters

    WysiwygCharacter:
        Character

    InterpolatedString:
        $" InterpolatedCharacters "

    InterpolatedCharacters:
        InterpolatedCharacter
        InterpolatedCharacter, InterpolatedCharacters

    InterpolatedCharacter:
        Character
        EscapeSequence
        EndOfLine
        InterpolatedValue
        {{
        }}

    InterpolatedValue:
        { Identifier }

### 2.2.9. Character Literals
    CharacterLiteral
        ' SingleCharacter '

    SingleCharacter:
        Character
        Escape sequence

### 2.2.10. Integer Literals
    IntegerLiteral:
        Integer
        Integer IntegerSuffix

    Integer:
        DecimalInteger
        BinaryInteger
        HexadecimalInteger
        OctalInteger

    IntegerSuffix:
        l
        L
        u
        U
        ul
        Ul
        uL
        UL
        lu
        Lu
        lU
        LU

    DecimalInteger:
        Digit
        NonZeroDigit Decimal

    Decimal:
        Digit Decimal

    BinaryInteger:
        BinaryPrefix BinDigit

    BinaryPrefic:
        0b
        0B

    HexadecimalInteger:
        HexPrefix Hexadecimal
    
    Hexadecimal:
        HexDigit
        HexDigit Hexadecimal

    HexPrefix:
        0x
        0X

    OctalInteger:
        0 Octal

    Octal:
        OctalDigit Octal

Integer literals can be group using a single quote (')

```c++
20'000              // Grouped decimal
0xFFFF'FFFF         // Grouped hexadecimal
0b10101010'11001100 // Grouped binary
```

### 2.2.11. Floating Point Literals
    FloatLiteral:
        Float
        Float Suffix
        Integer Suffix
    
    Float:
        LeadingDecimal .
        LeadingDecimal . Decimal
        LeadingDecimal . Decimal Exponent
        . Decimal
        . Decimal Exponent

    Suffix
        f
        F

    LeadingDecimal:
        0
        NonZeroDigit Decimal

    Exponent
        e Decimal
        E Decimal
        e+ Decimal
        E+ Decimal
        e- Decimal
        E- Decimal


### 2.2.12. Keywords

    Keywords:
        abstract
        alias
        align
        alignof
        as
        asm
        assert
        auto

        bool
        break
        byte

        case
        cast
        catch
        cconst
        char
        class
        const
        continue

        default
        delegate
        delete
        do
        double

        else
        elif
        enum
        export
        extern

        false
        final
        finally
        float
        for
        foreach

        goto

        if
        interface
        immutable
        import
        int
        internal
        is
        in
        inout
        invarient

        lazy
        long

        module
        mutable

        namespace
        new
        null
        nothrow

        out
        override

        package
        pragma
        private
        protected
        public

        ref
        return
        rune

        shared
        short
        sizeof
        static
        string
        struct
        super
        switch
        synchronized

        this
        throw
        true
        try
        typedef
        typeid
        typeof

        ubyte
        uint
        ulong
        union
        ushort

        virtual
        void

        wchar
        while

        __global

### 2.2.13. Built-in attribute keywords
    AttributeKeywords:
        deprecated

### 2.2.14. Special tokens

    __FILE__
    __FILE_FULL_PATH__
    __PACKAGE__
    __MODULE__
    __NAMESPACE__
    __LINE__
    __FUNC__
    __PRETTY_FUNC__

# 3. Grammar
This section contains the grammar specification of the language. A source file should only contain grammar that follows these rules.

## 3.1. File structure
    File:
        Package Module DeclDefs
        Module DeclDefs
        DeclDefs

    DeclDefs
        DeclDef
        DeclDef DeclDefs

    DeclDef
        NamespaceDecl
        ImportDecl
        Declaration
        FuncDecl
        PragmaDecl
        StructInvariant
        ClassInvariant
        Constructor
        StaticConstructor
        SharedStaticConstructor
        Destructor
        StaticDestructor
        SharedStaticDestructor
        PostBlit
        Allocator
        Destructor
        MixinDecl
        TemplateMixinDecl
        TemplateMixin
        StaticAssert
        UnitTestDecl
        AttributeSpecifier

    DeclarationBlock:
        DeclDefs
        {  }
        { DeclDefs }

## 3.2. Package, Module and Namespace
    Package:
        package PackageIdentifier ;

    PackageIdentifier:
        Identifier
        Identifier . PackageIdentifier

    Module:
        module ModuleIdentifier ;

    ModuleIdentifier:
        Identifier

    NamespaceDecl:
        namespace NamespaceIdentifier DeclarationBlock

    NamespaceIdentifier:
        Identifier
        Identifier . NamespaceIdentifier

## 3.3. Imports
    ImportDecl:
        import ImportList ;
        ImportModifiers import ImportList ;
        import package PackageImportList ;
        ImportModifiers import package PackageImportList ;

    ImportModifiers:
        static
        public
        public static
        static public

    ImportList:
        Import
        Import , ImportList

    Import:
        ImportIdentifier
        ImportIdentifier as ModuleAlias

    ImportBindings:
        ImportIdenfier : ImportBindingList

    ImportBindingList:
        ImportIdentifier : ImportBindList

    ImportBindList:
        ImportBind
        ImportBind , ImportBindList

    ImportBind:
        Identifier as Identifier

    ImportIdentifier:
        ModuleIdentifier
        PackageIdentifier . ModuleIdentifier

    ModuleIdentifier:
        Identifier

    ModuleAlias:
        Identifier

    PackageImportList:
        PackageIdentifier
        PackageIdentifier, PackageImportList

## 3.4. Declarations
    Declaration:
        VariableDecl
        EnumDecl
        AliasDecl
        TypedefDecl
        AggregateDecl

    VariableDecl:
        Type Declarators ;
        Attribute Type Declarators ;
        AutoDecl ;
        Attribute AutoDecl ;

    Declarators:
        DeclarationIdentifier
        DeclarationIdentifier, Declarators

    DeclarationIdentifier:
        Identifier
        Identifier : IntegerLiteral
        Identifier = Initializer
        Identifier : IntegerLiteral = Initializer

    Type:
        BasicType
        TypeModifiers BasicType
        BasicType TemplateArguments
        TypeModifier BasicType TemplateArguments
        DelegateType

    TypeModifiers:
        TypeModifier
        TypeModifier TypeModifiers

    TypeModifier:
        cconst
        const
        immutable
        static
        ref
        Attribute

    BasicType:
        BasicType2
        BasicType2 BasicTypeExt

    BasicType2:
        BaseTypes
        TypeIdentifier
        . TypeIdentifier
        Typeof
        Typeof . IdentifierList

    BasicTypeExt:
        BasicTypeExt2
        BasicTypeExt2 BasicTypeExt

    BasicTypeExt2:
        *
        [ ]
        [ Type ]
        [ AssignExpression ]
        [ AssignExpression .. AssignExpression ]

    BaseTypes:
        bool
        byte
        ubyte
        short
        ushort
        int
        uint
        long
        ulong
        float
        double
        char
        wchar
        rune
        string
        auto
        void

    TypeIdentifier:
        Identifier
        Identifier . TypeIdentifier
        TemplateInstance
        TemplateInstance . TypeIdentifier
        TypeIdentifier . TemplateInstance
        TypeIdentifier . TemplateInstance . TypeIdentifier

    IdentifierList:
        Identifier
        Identifier . IdentifierList

    Typeof:
        typeof ( Expression )
        typeof ( return )
        typeof ( return , Expression )

    AutoDecl:
        TypeModifier AutoDeclX

    AutoDeclX:
        AutoDeclY
        AutoDeclY, AutoDeclY

    AutoDeclY:
        Identifier = Initializer
        Identifier TemplateParamaters = Initializer

    AliasDecl:
        alias Type Declarators ;
        alias AliasDeclX ;

    AliasDeclX:
        AliasDeclY
        AliasDeclY, AliasDeclX

    AliasDeclX:
        Identifier TemplateParameters = Type
---
    Initializers:
        VoidInitializer
        NonVoidInitializer

    VoidInitializer:
        void

    NonVoidInitializer:
        ExpInitializer
        ArrayInitializer
        StructInitializer

    ExpInitializer:
        Expression

    ArrayInitializer:
        [ ArrayMemberInitializers ]

    ArrayMemberInitializers:
        ArrayMemberInitializer
        ArrayMemberInitializer, ArrayMemberInitializers

    ArrayMemberInitializer:


    StructInitializer:
        { StructMemberInitializers }

    StructMemberInitializers:
        StructMemberInitializer
        StructMemberInitializer ,
        StructMemberInitializer , StructMemberInitializers

    StructMemberInitializer:
        NonVoidInitializer
        . Identifier = Initializer

    MixinDecl:
        mixin ( AssignExpression ) ;

## 3.5. Expressions
    Expression:
        AssignExpression

    AssignExpression:
        ConditionalExpression
        ConditionalExpression = AssignExpression
        ConditionalExpression += AssignExpression
        ConditionalExpression -= AssignExpression
        ConditionalExpression *= AssignExpression
        ConditionalExpression /= AssignExpression
        ConditionalExpression %= AssignExpression
        ConditionalExpression &= AssignExpression
        ConditionalExpression |= AssignExpression
        ConditionalExpression ^= AssignExpression
        ConditionalExpression <<= AssignExpression
        ConditionalExpression >>= AssignExpression
        ConditionalExpression ~= AssignExpression

    ConditionalExpression:
        OrStatement
        OrStatement ? Expression : Expression
        OrStatement ? ConditionalExpression : Expression
        OrStatement ? Expression : ConditionalExpression
        OrStatement ? ConditionalExpression : ConditionalExpression

    OrExpression:
        AndExpression
        OrExpression || AndExpression

    AndExpression:
        BinaryOrExpression
        AndExpression && BinaryOrExpression

    BinaryOrExpression:
        BinaryXorExpression
        BinaryOrExpression | BinaryXorExpression

    BinaryXorExpression:
        BinaryAndExpression
        BinaryXorExpression ^ BinaryAndExpression

    BinaryAndExpression
        CompareExpression
        BinaryAndExpression & CompareExpression

    CompareExpression:
        ShiftExpression
        EqualityExpression
        RelExpression

    EqualityExpression:
        ShiftExpression == ShiftExpression
        ShiftExpression != ShiftExpression

    RelExpression:
        ShiftExpression < ShiftExpression
        ShiftExpression <= ShiftExpression
        ShiftExpression > ShiftExpression
        ShiftExpression >= ShiftExpression

    ShiftExpression:
        AddMinExpression
        ShiftExpression << AddMinExpression
        ShiftExpression >> AddMinExpression

    AddMinExpression:
        MulDivExpression
        AddMinExpression + MulDivModExpression
        AddMinExpression - MulDivModExpression

    MulDivModExpression
        UnaryExpression
        MulDivModExpression * UnaryExpression
        MulDivModExpression / UnaryExpression
        MulDivModExpression % UnaryExpression
        MulDivModExpression ~ UnaryExpression

    UnaryExpression:
        & UnaryExpression
        + UnaryExpression
        - UnaryExpression
        ! UnaryExpression
        ~ UnaryExpression
        * UnaryExpression
        ++ UnaryExpression
        -- UnaryExpression
        CastExpression
        PostfixExpression
        Type . Identifier
        Type . TemplateInstance

    CastExpression
        cast < Type > ( UnaryExpression )
        UnaryExpression as Type

    PostfixExpression:
        PrimaryExpression
        PostFixExpression ++
        PostFixExpression --
        IndexExpression
        SliceExpression

    SliceExpression:
        PostfixExpression [  ]
        PostfixExpression [ Slice ]

    Slice:
        AssignExpression
        AssignExpression, Slice
        AssignExpression .. AssignExpression
        AssignExpression .. AssignExpression, Slice

    IndexExpression:
        PostFixExpression [ ArgumentList ]

    PrimaryExpression:
        Identifier
        . Identifier
        TemplateInstance
        . TemplateInstance
        this
        null
        true
        false
        IntegerLiteral
        FloatLiteral
        CharacterLiteral
        StringLiteral
        ArrayLiteral
        AssocArrayLiteral
        BasicType . Identifier
        TemplateInstance . Identifier
        Type ( )
        Type ( ArgumentList )
        ( Expression )
        MixinExpression
        ImportExpression
        TypeidExpression
        IsExpression
        NewExpression
        DeleteExpression
        SpecialKeyword
---
    AssertExpression:
        assert ( AssertArguments )

    AssertArguments:
        AssignExpression
        AssignExpression , AssertArguments
---
    MixinExpression:
        mixin ( AssignExpression )
---
    ImportExpression:
        import ( AssignExpression )
---
    TypeidExpression:
        Typeid ( Type )
        Typeid ( Expression )
---
    IsExpression
        is ( Type )
        is ( Type : TypeSpecialization )
        is ( Type == TypeSpecialization )
        is ( Type : TypeSpecialization, TemplateParameterList )
        is ( Type == TypeSpecialization, TemplateParameterList )

    TypeSpecialization:
        Type
        struct
        class
        union
        interface
        enum
        const
        immutable
---
    NewExpression:
        new Type
        new AllocatorArguments Type
        NewArgExpression
    
    NewArgExpression:
        new Type [ AssignExpression ]
        new AllocatorArguments Type [ AssignExpression ]
        new Type ( ArgumentList )
        new AllocatorArguments Type ( ArgumentList )

    AllocatorArguments:
        ( AllocatorArgumentList )

    AllocatorArgumentList:
        AllocatorArgument
        AllocatorArgument , AllocatorArgumentList

    DeleteExpression:
        delete UnaryExpression
---
    ArrayLiteral:
        [ ArgumentList ]

    AssocArrayLiteral:
        [ KeyValuePairs ]

    KeyValuePairs:
        KeyValuePair
        KeyValuePair, KeyValuePairs

    KeyValuePair:
        KeyExpression : ValueExpression

    KeyExpression:
        AssignExpression

    ValueExpression:
        AssingExpression
---
    ArgumentList:
        AssignExpression
        AssignExpression, ArgumentList

    SpecialKeyword:
        __FILE__
        __FILE_FULL_PATH__
        __PACKAGE__
        __MODULE__
        __NAMESPACE__
        __LINE__
        __FUNC__
        __PRETTY_FUNC__

## 3.6. Statements
    Statement:
        ;
        StatementNotEmpty
        ScopeBlockStatement

    StatementNotEmpty:
        StatementNotEmptyNoCaseNoDefault
        CaseStatement
        DefaultStatement
        
    StatementNoScope:
        ;
        StatementNotEmpty
        BlockStatement

    StatementNotEmptyNoScope:
        StatementNotEmpty
        BlockStatement

    StatementNotEmptyNoCaseNoDefault:
        LabeledStatement
        ExpressionStatement
        DeclarationStatement
        IfStatement
        WhileStatement
        DoWhileStatement
        ForStatement
        ForeachStatement
        RangedForeachStatement
        SwitchStatement
        BreakStatement
        ContinueStatement
        ReturnStatement
        GotoStatement
        TryStatement
        ThrowStatement
        AsmStatement
        StaticAssert
        CompileTimeStatement

    ScopeStatement:
        StatementNotEmpty
        BlockStatement

    ScopeBlockStatement:
        BlockStatement

    BlockStatement:
        {  }
        { StatementList }

    BlockStatementNotEmpty:
        { StatementList }

    StatementList:
        Statement
        Statement, StatementList

    LabeledStatement:
        Identifier :
        Identifier : StatementNoScope
        Identifier : Statement

    ExpressionStatement:
        Expression ;

    DeclarationStatement:
        Declaration
---
    IfStatement:
        if ( Expression ) ThenStatement
        if ( Expression ) ThenStatement ElseStatement

    ThenStatement:
        ScopeStatement

    ElseStatement:
        else ScopeStatement

    ElifStatement:
        elif ( Expression ) ThenStatement
        elif ( Expression ) ThenStatement ElseStatement
---
    WhileStatement
        while ( Expression ) ScopeStatement
---
    DoWhileStatement
        do ScopeStatement while ( Expression ) ;
---
    ForStatement:
        for ( ForInitialize ; ForTest ; ForIncrement ) ScopeStatement

    ForInitialize:
        EmptyStatement
        Expression

    ForTest:
        Expression

    Increment:
        Expression
---
    ForeachStatement:
        foreach ( ForeachExpression ) ScopeStatement

    ForeachExpression:
        ForeachTypeList : ForeachAggregate

    ForeachTypeList:
        ForeachType
        ForeachType , ForeachTypeList

    ForeachType:
        Identifier
        ForeachTypeAttributes Identifier
        BasicType Identifier
        ForeachTypeAttributes BasicType Identifier

    ForeachAggregate:
        Expression
---
    RangedForeachStatement:
        foreach ( RangedForeachExpression ) ScopeStatement

    RangedForeachExpression:
        ForeachType : LowerRangedExpression .. UpperRangedExpression

    LowerRangedExpression:
        Expression

    UpperRangedExpression:
        Expression
---
    SwitchStatement:
        switch (Expression) ScopeStatement

    CaseStatement:
        case Argument : ScopeStatement

    DefaultCaseStatement:
        default : ScopeStatement
---
    ContinueStatement:
        continue ;
---
    BreakStatement:
        break ;
---
    ReturnStatement:
        return ;
        return Expression ;
---
    TryStatement:
        try ScopeStatement Catches
        try ScopeStatement Catches FinallyStatement
        try ScopeStatement FinallyStatement

    Catches:
        CatchStatement
        CatchStatement , Catches

    CatchStatement:
        catch ( CatchParameter ) StatementNotEmpty

    CatchParameter:
        BasicType Identifier

    FinallyStatement:
        finally StatementNotEmpty

    ThrowStatement:
        throw Expression ;
---
    GotoStatement:
        goto Identifier ;
        goto default ;
        goto case Identifier;

## 3.7. Functions
    FunctionDecl:
        Type FunctionDeclarator FunctionBody
        FunctionPreAttributes Type FunctionDeclarator FunctionBody
        OperatorOverload

    FunctionDeclatator:
        Identifier Parameters
        Identifier Parameters FunctionAttributes
        Identifier TemplateParameters Parameters
        Identifier TemplateParameters Parameters FunctionAttributes

    Parameters:
        ( ParameterList )

    ParameterList:
        Parameter, ParameterList

    Parameter:
        Type Identifier
        Type Identifier = AssignExpression
        ParameterAttributes Type Identifier
        ParameterAttributes Type Identifier = AssignExpression

    FunctionBody:
        SpecifiedFunctionBody
        MissingFunctionBody

    SpecifiedFunctionBody:
        BlockStatement
        do BlockStatement
        FunctionContract BlockStatement
        FunctionContract do BlockStatement

    MissingFunctionBody:
        ;

    ParameterAttributes:
        TypeModifier
        in
        out
        ref
        lazy
---
    DelegateType:
        delegate Type Identifier ParameterList ;

    FunctionLiteral:
        delegate ParameterList SpecifiedFunctionBody
        Lambda
---
    Lambda:
        ParameterList => LamdaBody
        ParameterList => LambdaCaptureList LamdaBody
        Identifier => LamdaBody
        Identifier => LambdaCaptureList LamdaBody
        LambdaIdentifiers => LamdaBody
        LambdaIdentifiers => LambdaCaptureList LamdaBody

    LamdaBody:
        SpecifiedFunctionBody
        Expression

    LambdaIdentifiers:
        ( LambdaIdentifierList )

    LambdaIdentifierList:
        Identifier
        Identifier , LambdaIdentifierList

    LambdaCaptureList:
        [ LambdaCaptures ]

    LambdaCaptures:
        LambdaCapture
        LambdaCapture , LambdaCaptures

    LambdaCapture:
        Identifier
        & Identifier
        LambdaDefaultCapture

    LambdaDefaultCapture:
        =
        &
        this
---
    FunctionContract:
        InOutContractExpression
        InOutStatement

    InOutContractExpression:
        InContractExpression
        OutContractExpression

    InContractExpression:
        in ( AssertArguments )

    OutContractExpression:
        out ( ; AssertArguments )
        out ( identifier; AssertArguments )

    InOutStatement:
        InStatement
        OutStatement

    InStatement:
        in BlockStatement

    OutStatement:
        out BlockStatement
        out ( identifier ) BlockStatement
---
    OperatorOverload:
        Type OverloadableOperator Parameters FunctionBody
        Type OverloadableOperator Parameters FunctionAttributes FunctionBody
        static opCall TemplateParameters Parameters FunctionBody

    OverloadIdentifier:
        OverloadableOperator
        OverloadableOperator TemplateParameters

    OverloadableOperator:
        opUnary
        opBinary
        opBinaryR
        opAssign
        OpEquals
        opCmp
        opCast
        opImplicitCast
        opCall
        opIndex
        opIndexAssign
        opIndexOpAssign
        opSlice
        opSliceOpAssign
        opDollar

## 3.8. Classes
    AggregateDecl:
        ClassDecl
        StructDecl
        InterfaceDecl
        UnionDecl

    ClassDecl:
        class Identifier AffrefateBody
        Attribute class Identifier AffrefateBody
        class Identifier BaseClassList AffrefateBody
        Attribute class Identifier BaseClassList AffrefateBody
        ClassTemplateDecl

    BaseClassList:
        : SuperClass
        : SuperClass, Interfaces
        : Interfaces

    SuperClass:
        Identifier

    Interfaces:
        Interface
        Interface, Interfaces

    Interface:
        Identifier

    AggregateBody:
        { }
        { DeclDefs }

    Constructor:
        this Parameters ;
        this Parameters FunctionAttributes ;
        this Parameters FunctionBody
        this Parameters FunctionAttributes FunctionBody
    
    Destructor:
        ~ this ( ) ;
        ~ this ( ) FunctionAttributes ;
        ~ this ( ) FunctionBody
        ~ this ( ) FunctionAttributes FunctionBody

    PostBlit:
        this ( this ) ;
        this ( this ) FunctionAttributes ;
        this ( this ) FunctionBody
        this ( this ) FunctionAttributes FunctionBody

    StaticConstructor:
        static this ( ) ;
        static this ( ) FunctionAttributes ;
        static this ( ) FunctionBody
        static this ( ) FunctionAttributes FunctionBody

    StaticDestructor:
        static ~ this ( ) ;
        static ~ this ( ) FunctionAttributes ;
        static ~ this ( ) FunctionBody
        static ~ this ( ) FunctionAttributes FunctionBody

    SharedStaticConstructor:
        shared static this ( ) ;
        shared static this ( ) FunctionAttributes ;
        shared static this ( ) FunctionBody
        shared static this ( ) FunctionAttributes FunctionBody

    SharedStaticDestructor:
        shared static ~ this ( ) ;
        shared static ~ this ( ) FunctionAttributes ;
        shared static ~ this ( ) FunctionBody
        shared static ~ this ( ) FunctionAttributes FunctionBody

    ClassInvariant:
        invariant ( ) BlockStatement
        invariant BlockStatement
        invariant ( AssertArguments )

    Allocator:
        new Parameters ;
        new Parameters FunctionBody

    Deallocator:
        delete ( ) ;
        delete ( ) FunctionBody

## 3.9. Structures
    StructDecl:
        struct Identifier AggregateBody
        Attribute struct Identifier AggregateBody
        AnonStructDecl
        Attribute AnonStructDecl
        StructTemplateDecl

    AnonStructDecl:
        struct AggregateBody

    StructInvariant:
        invariant ( ) BlockStatement
        invariant BlockStatement
        invariant ( AssertArguments )

## 3.10. Interfaces
    InterfaceDecl:
        interface Identifer AggregateBody
        interface Identifer BaseInterfaceList AggregateBody
        InterfaceTemplateDecl

    BaseInterfaceList:
        : Interfaces

## 3.11. Unions
    UnionDecl:
        union Identifier AggregateBody
        AnonUnionDecl

    AnonUnionDecl:
        union AggregateBody

## 3.12. Enums
    EnumDecl:
        enum Identifier EnumBody
        enum Identifier : EnumBaseType EnumBody

    EnumBody:
        { EnumMemberList }

    EnumMemberList:
        EnumMember
        EnumMember, EnumMemberList

    EnumMember:
        Identifier
        Identifier ,
        Identifier = Initializer
        Identifier = Initializer ,

    EnumBaseType:
        Type

    AnonEnumDecl:
        enum { EnumMembers }
        enum : EnumBaseType { EnumMembers }

## 3.13. Properties
    Property:
        Type Identifier { ProperyGetSet }
        Attribute Type Identifier { ProperyGetSet }

    ProperyGetSet:
        ProperyGet
        ProperySet
        ProperyGet ProperySet
        ProperySet ProperyGet

    PropertySet:
        set BlockStatementNotEmpty

    PropertyGet:
        get BlockStatementNotEmpty

## 3.14. Attributes
    AttributeSpecifier:
        ColonAttribute :
        Attribute DeclarationBlock

    Attribute:
        VisibilityAttribute
        LinkageAttribute
        AlignAttribute
        AbstractAttribute
        ThreadingAttribute
        static
        extern
        inout
        AtAttribute

    ColonAttribute:
        VisibilityAttribute

    VisibilityAttribute:
        private
        protected
        internal
        package
        public
        export

    FunctionAttributes:
        const
        cconst
        final
        override
        nothrow

    FunctionPreAttributes:
        abstract
        virtual

    AtAttribute
        @ DeprecationAttribute
        @ disable
        @ safe
        @ trusted
        @ nogc
        UserDefinedAttribute

    DepricationAttribute:
        depricated
        depricated ( Text )

    LinkageAttribute:
        extern ( LinkageType )

    LinkageType:
        C
        C++
        Windows
        System

    AlignAttribute:
        align ( IntegerLiteral )

    AbstractAttribute:
        abstract

    ThreadingAttribute:
        synchronized
        shared
        __global

    UserDefinedAttribute:
        @ Identifier
        @ Identifier ( ArgumentList )
        @ TemplateInstance
        @ TemplateInstance ( ArgumentList )


## 3.15. Templates
    TemplateDeclaration:
        TemplatePrefix TemplateIdentifier TemplateParameters { DeclDefs }

    TemplateParameters:
        < TemplateParameterList >

    TemplateArgumentList:
        TemplateParameter
        TemplateParameter, TemplateParameterList

    TemplateParameter:
        TemplateTypeParamater
        TemplateValueParameter
        TemplateVariadicParameter

    TemplatePrefix:
        template
        Attribute template

---
    TemplateTypeParameter:
        Identifier
        Identifier TemplateTypeParameterSpecialization
        Identifier TemplateTypeParameterDefault

    TemplateTypeParameterSpecialization:
        : Type

    TemplateTypeParameterDefault:
        = Type

    TemplateValueParameter:
        BasicType Identifier
        BasicType Identifier TemplateValueParameterSpecialization
        BasicType Identifier TemplateValueParameterValue

    TemplateValueParameterSpecialization:
        : ConditionalExpression

    TemplateValueParameterValue:
        = AssignExpression
        = SpecialKeyword

    TemplateVariadicParameter:
        Identifier ...

---
    TemplateInstance:
        Identifier TemplateArguments

    TemplateArguments:
        < TemplateArgumentList >

    TemplateArgumentList:
        TemplateArgument
        TemplateArgument , TemplateArgumentList

    TemplateArgument:
        Type
        AssignExpression
---
    ClassTemplateDecl:
        TemplatePrefix class Identifier TemplateParameters AggregateBody
        TemplatePrefix class Identifier TemplateParameters BaseClassList AggregateBody
        TemplatePrefix class Identifier TemplateParameters Constraint AggregateBody
        TemplatePrefix class Identifier TemplateParameters BaseClassList Constraint AggregateBody
        TemplatePrefix class Identifier TemplateParameters Constraint BaseClassList AggregateBody

    InterfaceTemplateDecl:
        TemplatePrefix interface Identifier TemplateParameters AggregateBody
        TemplatePrefix interface Identifier TemplateParameters BaseInterfaceList AggregateBody
        TemplatePrefix interface Identifier TemplateParameters Constraint AggregateBody
        TemplatePrefix interface Identifier TemplateParameters BaseInterfaceList Constraint AggregateBody
        TemplatePrefix interface Identifier TemplateParameters Constraint BaseInterfaceList AggregateBody

    StructTemplateDecl:
        TemplatePrefix struct Identifier TemplateParameters AggregateBody
        TemplatePrefix struct Identifier TemplateParameters Constraint AggregateBody
---
    Constraint:
        if ( Expression )

## 3.16. Mixin template
    MixinTemplateDecl:
        mixin template Identifier TemplateParameter { DeclDefs }
        mixin template Identifier TemplateParameter Constraints { DeclDefs }

    TemplateMixin:
        mixin MixinTemplateName ;
        mixin MixinTemplateName TemplateArguments ;
        mixin MixinTemplateName Identifier ;
        mixin MixinTemplateName TemplateArguments Identifier ;

    MixinTemplateName:
        QualifiedIdentifierList
        Typeof . QualifiedIdentifierList

    QualifiedIdentifierList:
        Identifier
        Identifier . QualifiedIdentifierList
        TemplateInstance . QualifiedIdentifierList

## 3.17. UnitTesting:
    UnitTestDecl:
        unittest ScopeStatement
        unittest Identifier ScopeStatement

# 3.18. Compile-time expressions
    ConditionalDecleration:
        Condition DeclarationBlock
        Condition DeclarationBlock else DeclarationBlock

    ConditionalStatement:
        Condition StatementNotEmptyNoScope
        Condition StatementNotEmptyNoScope else StatementNotEmptyNoScope

    Condition:
        VersionCondition
        DebugCondition

    VersionCondition:
        version ( IntegerLiteral )
        version ( Identifier )

    VersionSpecification:
        version = IntegerLiteral ;
        version = Identifier ;

    DebugCondition:
        debug
        debug ( IntegerLiteral )
        debug ( Identifier )

    DebugSpecification:
        debug = IntegerLiteral ;
        debug = Identifier ;

    CompileTimeStatement:
        cconst IfStatement
        cconst ForStatement
        cconst ForeachStatement
        cconst RangedForeachStatement

    StaticAssert:
        static assert ( AssertArguments )

## 3.19. Pragma
    Pragma:
        pragma (Identifier)
        pragma (Identifier, ArgumentList)

## 3.20. Inline assembly (x86)
    AsmStatement:
        asm { AsmInstuctionList }

    AsmInstructionList:
        AsmInstruction ;
        AsmInstruction ; AsmInstructionList
---
    AsmInstruction:
        Identifier : AsmInstruction
        align IntegerExpression
        even
        naked
        db Operands
        ds Operands
        di Operands
        dl Operands
        df Operands
        dd Operands
        de Operands
        AsmOpcode
        AsmOpcode AsmOperands

    AsmOperands:
        AsmOperand
        AsmOperand, AsmOperands
---
    AsmRegister:
        AL AH AX EAX
        BL BH BX EBX
        CL CH CX ECX
        DL DH DX EDX
        BP EBP
        SP ESP
        DI EDI
        SI ESI
        ES CS SS DS GS FS
        CR0 CR2 CR3 CR4
        DR0 DR1 DR2 DR3 DR6 DR7
        TR3 TR4 TR5 TR6 TR7
        ST
        ST(0) ST(1) ST(2) ST(3) ST(4) ST(5) ST(6) ST(7)
        MM0 MM1 MM2 MM3 MM4 MM5 MM6 MM7
        XMM0 XMM1 XMM2 XMM3 XMM4 XMM5 XMM6 XMM7

    AsmRegister64:
        RAX RBX RCX RDX
        BPL RBP
        SPL RSP
        DIL RDI
        SIL RSI
        R8B R8W R8D R8
        R9B R9W R9D R9
        R10B R10W R10D R10
        R11B R11W R11D R11
        R12B R12W R12D R12
        R13B R13W R13D R13
        R14B R14W R14D R14
        R15B R15W R15D R15
        XMM8 XMM9 XMM10 XMM11 XMM12 XMM13 XMM14 XMM15
        YMM0 YMM1 YMM2 YMM3 YMM4 YMM5 YMM6 YMM7
        YMM8 YMM9 YMM10 YMM11 YMM12 YMM13 YMM14 YMM15

## 3.21. Application Binary Interface 
    // used: A B C D E F M P S T U W Y Z
    MangledName:
        _M QualifiedName

    QualifiedName:
        QualifiedIdentifier QualifiedTypeData

    QualifiedIdentifier:
        SymbolName
        SymbolName QualifiedIdentifier

    QualifiedTypeData:
        TypeFunctionNoReturn
        TypeFunction
        M TypeFunctionNoReturn   // Member function
        M TypeFunction           // Member function

    SymbolName:
        LName
        TemplateInstance
        IdentifierBackRef
---
    TypeFunction:
        F TypeFunctionNoReturn Type

    TypeFunctionNoReturn:
        CallConvention ParamClose
        CallConvention FuncAttribs ParamClose
        CallConvention MangleParameters ParamClose
        CallConvention FuncAttribs MangleParameters ParamClose
---
    TemplateInstanceName:
        TemplateId LName TemplateArgs Z

    TemplateId:
        __T
        __C              // Template contraint arguments

    TemplateArgs:
        TemplateArg
        TemplateArg, TemplateArgs

    TemplateArg:
        TemplateArgX
        H TemplateArgX

    TemplateArgX:
        T Type
        V Type Value
        S QualifiedName
---
    IdentifierBackRef:
        Q NumberBackRed

    NumberBackRef:
        lower-case-letter
        upper-case-letter NumberBackRef
---
    MangleValue:
        n
        i Number
        N Number
        e HexFloat
        CharWidth Number _ HexDigits

    HexFloat:
        NAN
        INF
        NINF
        HexDigits P Exponent
        N HexDigits P Exponent

    Exponent:
        Number
        N Number

    Number:
        N Number
        Number

    CharWidth:
        c
        w
        q
---
    Name:
        NameStart
        NameStart NameChars

    NameStart:
        _
        Alpha

    NameChars:
        NameChar
        NameChar NameChars

    NameChar:
        NameStart
        Digit

    LName:
        Number Name

    Number:
        Digit
        Digit Number

    Digit:
        0
        1
        2
        3
        4
        5
        6
        7
        8
        9
---
    CallingConvention:
        m   // MJay
        c   // C
        w   // Windows
---
    FuncAttrs:
        FuncAttr
        FuncAttr FuncAttrs

    FuncAttr:
        FuncAttibConst
        FuncAttibCConst
        FuncAttibSafe
        FuncAttibTrusted

    FuncAttibConst:
        Nc

    FuncAttibCConst:
        Nd

    FuncAttibSafe:
        Ns

    FuncAttibTrusted:
        Nt
---
    MangleParameters:
        MangleParameter
        MangleParameter, MangleParameters

    MangleParameter:
        MangleType
        J MangleType        // out
        K MangleType        // ref
        L MangleType        // lazy

    MangleParamClose:
        X                   // Variadic template: T t... )
        Y                   // C-style variadic: , ... )
        Z                   // Non-variadic

---
    MangleType:
        MangleTypeX
        MangleTypeModifiers MangleTypeX

    MangleTypeModifiers:
        MangleConstModifier
        MangleCConstModifier
        MangleImmutableModifier
        MangleSharedModifier
        MangleGlobalModifier

    MangleConstModifier:
        o

    MangleCConstModifier:
        x

    MangleImmutableModifier:
        y

    MangleSharedModifier:
        z


    MangleTypeX:
        MangleTypeArray
        MangleTypePointer
        MangleTypeClass
        MangleTypeStruct
        MangleTypeEnum
        MangleTypeTypedef
        MangleTypeDelegate
        MangleTypeVoid
        MangleTypeByte
        MangleTypeUByte
        MangleTypeShort
        MangleTypeUShort
        MangleTypeInt
        MangleTypeUInt
        MangleTypeLong
        MangleTypeULong
        MangleTypeFloat
        MangleTypeDouble
        MangleTypeBool
        MangleTypeNull
        MangleTypeTuple
        MangleTyperune

    MangleTypeArray:
        A Type

    MangleTypePointer:
        P Type

    MangleTypeClass:
        C QualifiedName

    MangleTypeStruct:
        S

    MangleTypeEnum:
        E

    MangleTypeTypedef:
        T

    MangleTypeDelegate:
        D

    MangleTypeVoid:
        v

    MangleTypeByte:
        g

    MangleTypeUByte:
        h

    MangleTypeShort:
        s

    MangleTypeUShort:
        t

    MangleTypeInt:
        i

    MangleTypeUInt:
        j

    MangleTypeLong:
        l

    MangleTypeULong:
        m

    MangleTypeFloat:
        f

    MangleTypeDouble:
        d

    MangleTypeBool:
        b

    MangleTypeChar:
        c

    MangleTypeWChar:
        w

    MangleTyperune:
        q

    MangleTypeNull:
        n

    MangleTypeTuple
        B Parameters Z

# 4. Packages, modules and namespaces
## 4.1. Packages
Packages are containers containing multiple sub-packages and modules. They are mainly used to organize and group modules depending on their use cases and user specified differences. Often, packages will refer to  the underlying OS file structure of projects.

Any code inside of a package will automatically be put inside a namespace corresponding to the package name.

Packages are defined uses the following syntax: `package name.other;`

If no package is defined, the module will become part of a global package, without any namespace.

## 4.2. Modules
Modules do NOT correspond to underlying files and can therefore exist out of multiple files. When no specific module is defined, the compiler will make a corresponding module for each file, with the module name being the name of the file with its extension stripped.

Modules automatically provide a namespace for its content. This namespace will also be in the respective package namespace.

Modules are mostly independant from each other, with the exception that they ARE dependent on modules they import symbols from. If a module imports or uses a symbol symbol from another module, the module should not be affected by any changes to other symbols in the imported module.

A module can be easily defined using the following syntax: 
```
module modname;
```

In case no module is defined and the filename does not correspond to a valid module name, the invalid characters will be replaced by an underscore, i.e. `some-file` will become part of the module `some_file`

The module is also allowed to be annoteded with certain attributes.

An example of this, is that is allows the deprication of certain modules and will cause a warning when this module is imported in any file:
```
@deprecated("Use foo2 instead")
module foo;
```
This will give the following warning: `Deprecation: module foo is deprecated - Use foo2 instead`

## 4.3. Namespaces
Namespaces allow the user to split up code inside of a module, grouping certain parts of the code or hidding classes inside of an `internal` namespace.

Packaged and modules also create a namespace of their own, in which the code will resign.

## 4.4. Imports
Imports allow the user to include code from other modules into their projects. Imports are also in no way required to be global, imports are allowed in different scopes, e.g. class or function specific imports. When importing a module inside of a function, it is however required to have the import statement before the use of any symbols from that specific module.

There are several version of the import statement, each with their own uses.

### 4.4.1. Basic import
This is the simplest way of importing modules.
```D
package app;
module main;

import std.io;  // Global import, allows use of symbols throughout entire file

class Foo : BaseClass {

    import app.foo; // Scoped import, allows use of symbols throughout class;

    void bar()
    {
        import app.bar; // Scoped import, allows use of symbols throughout function;
        writeln("Hello bar()!");
    }
}
```

When it's not specified where a symbol comes from, teh compiler will automatically try to infer the type, in the example above, the compiler will search for the symbol in the following order.
- Any declarations inside of `bar()`
- Any declarations inside of `Foo`
- Any declarations inside of `BaseClass`
- Any declarations inside of the module, including other files in the module

If the function wasn't found, the compiler will go over all imports, starting from imports in the most inner scope, and will work its way to the outermost scope. The search process will end once the function is found. If an import is solely imported in a scope, the imported symbols cannot be used outside of this scope.

When the same function is found more than once in a scope or imported module, the compiler will give an error, since these symbols are disambiguous.

```D
module main;
import A;   // Has foo() function
import B;   // Has foo() function

void main()
{
    foo(); // Error, disambiguous symbol: A.foo() or B.foo() ?
    A.foo(); // Ok, calls A.foo()
    B.foo(); // Ok, calls B.foo()
}
```

### 4.4.2. Public imports
By default, the imports are handled as if they are private to the file that is being compiled. When an import is declared public, the resulting module will also export the symbols from the imported module. Any other module that imports this module, will have access to the public import.

```D
module B;
public import A; // Has foo() function
void bar() {}
```
```D
module C;
import B;

void main()
{
    foo(); // Ok, calls A.foo()
    bar(); // Ok, calls B.bar()
}
```

### 4.4.3. Static import
A static import requires that the user explicitily use the full name of an object or function.
```D
module app;
static import std.io;

void main()
{
    writeln("Hello World!"); // Error, writeln is undefined
    std.io.writeln("Hello World!"); // Ok, calls std.io.writeln()
}
```

### 4.4.4. Aliased import
An imported module can be referenced via an alias. Aliasing a module disables the inferring of any function contained in that module and disallows the use of the full name

```D
import std.io as stdio;

void main()
{
    stdio.writeln("Hello World!"); // Ok, calls std.io.writeln()
    std.io.writeln("Hello World!"); // Error, std is undefined
    writeln("Hello World!"); // Error, writeln() is undefined
}
```

### 4.4.4. Selective import
Exclusivly importing symbols from a module can also be accomplished. Separate functions and object are also allowed to be aliased
```D
import std.io : writeln, write as foo;

void main()
{
    std.io.writeln("Hello World!"); // Error, std is undefined
    writeln("Hello World!"); // Ok, calls std.io.writeln()
    write("Hello World!"); // Error, write() is undefined
    foo("Hello World!"); // Ok, calls std.io.write()
    writef(fmt, "abc"); // Error, writef() is undefined
}
```
### 4.4.5. Combining aliased and selective imports
It is possible to combine aliased and selective imports

```D
import std.io as io : writeln as foo;

void main()
{
    writeln("Hello World!"); // Error, writeln() is undefined
    foo("Hello World!"); // Ok, calls std.io.writeln()
    std.io.writeln("Hello World!"); // Error, std is undefined
    io.writeln("Hello World!"); // Ok, calls std.io.writeln(), since io is an alias to std.io
    io.foo("Hello World!"); // Error, foo() is not a member of io, foo is local
}
```
## 4.5. Module scope
It is possible to refer to 'global' data inside each module, even when it has been hidden by a local variable of the same name. This can done by simply prepending a dot to a variable, when this is done, the compiler will look for a variable in the module score, instead of the local scope.

`.name`

## 4.6. Module constructors and destructors
MJay allows each module to have constructors and destructors, these can be used to manually initialize static and 'global' data inside of the module.

Each module is allowed to have multiple constructors and destructors. This allows each file to have a static constructor and destructor to create and destroy static data used by the module. To do this, simply declare a static constructor or destructor in the module, outside of class or struct scopes.

```D
module app;

static this()
{
    // Is run for every thread that is created
}

shared static this()
{
    // Is run for every thread that is destroyed
}

static ~this()
{
    // Is run at the start of the program
}

shared static ~this()
{
    // Is run at the end of the program
}
```

These static constructors will be called in the order they are compiled in the module. If any code is dependent on any, previously initialized, static variable, make sure they are initialized in the same file, since spreading this over multiple files can cause undefined behaviour.

A good comparison for these constructors, is the initialization of static variables in C++, but in this case, when done correctly, you can have complete control over the order of initialization.

### 4.6.1. Order of construction
Shared module constructors will always be called before the module constructor. The order of initialization between each module is implicitly defined by the module imports, starting from the module that is initially executed. Each module expects that it is dependent on the other module's constructor.

Circular dependencies will cause an issue when both modules have a constructor, if this were to happen, this well cause a runtime exception and will teminate the program.

### 4.6.1. Order of destruction
The calling of destructors will happen in the reverse order of construction, since the module expects it will have to destruct its own data before it can destruct the modules it is dependent on.

# 5. Declarations

## 5.1. Declaration syntax
Each declaration read right to left:
```D
int x;      // x is an int
int* x;     // x is a pointer to an int
int** x;    // x is a pointer to a pointer to an int
int[] x;    // x is an array of ints
int*[] x;   // x is an array of pointers to ints
int[]* x;   // x is a pointer to an array of ints
```

Arrays are also read form right to left:
```D
int[3] x;       // x is an array of 3 ints
int[3][5] x;    // x is an array of 5 arrays of 3 ints
int[5]*[3] x;   // x is an array of 3 arrays of pointers to arrays with 5 ints
```

Delegates are defined, starting with the `delegate` keyword
```D
delegate int(char) x;   // x is a delegate pointing to 
                        // a function taking charater argument 
                        // and returning an int
delegate int(char)[] x; // x is an array of delegates pointing to 
                        // a function taking charater argument 
                        // and returning an int
```

A declaration can also create multiple variables of 1 type, in 1 declaration.
```D
int x, y;   // x and y are ints
int* x, y;  // x and y are pointers to ints
int x, *y;  // invalid, multiple types per declaration
```

## 5.2. Type inference
It is also possible to infer the type automatically when assigning a value with a type modifier from a literal.
```D
static x = 1;   // x is a static int
const x = 2.3f; // x is a constant float
```

The keyword `auto` can also be used for automatic type inference without needing a modifier.
```D
auto x = 1ul;       // x is an unsigned long
auto x = 1.0;       // x is a double

auto x = "Hello";   // x is an immutable char[]
```

`auto` can also be for type inference for dynamically created objects
```D
uint func() { return 1u; }
class C { ... }

auto x = func();    // x is an unsinged int
auto x = new C;     // x is a pointer to an instance of C
```

An array literal will create a dynamic array, instead of a static array
```D
// x is a string[], not a string[7];
auto x = ["All", "your", "base", "are", "belong", "to", "us"];
```

## 5.3. Aliased types
Every type can have an alias assigned, which can be used anywhere the original type could have been used.

```D
alias foo.bar.fizzbuzz as mytype;
```

When it comes to compilation, the aliased and originally are semantically identical and cannot be distinguished between, this also makes it so there is no differance when it comes to overloading functions.

```D
alias int as myint;

void func(int val) { ... };
void func(myint val) { ... }; // Error: multiple definitions of function 'foo'
```

It is also allowed to alias functions.

```D
import abc;
alias abc.foo as bar;

bar(); // calls abc.foo();
```

Aliases can also be used with templates.
```D
template struct Foo<T> { alias T as t; }
alias Foo<int> as t1;
alias Foo<int>.t as t2;
alias t1.t as t3;
alias t3 as t4;

t1.t v1;    // v1 is an int
t2 v2;      // v2 is an int
t3 v3;      // v3 is an int
t4 v4;      // v4 is an int
```

Aliases can also be used to allow compile-time switching of the type, while maintaining valid code.

```D
version(Win32)
{
    alias win32.foo as mytype;
}
version(linux)
{
    alias linux.bar as mytype;
}
```

Aliases can also be used to refer to a function in a base class.

```D
class A
{
    int foo(int a) { return 1; }
}

class B : A
{
    int foo(int a, uint b) { return 2; }
}

class C : B
{
    int foo(int a) { return 3; }
    alias foo = B.foo;
}

class D : C
{
}

void test()
{
    D b = new D();
    int i;

    i = b.foo(1, 2u);   // calls B.foo
    i = b.foo(1);       // calls C.foo
}
```

Note that aliases can make it hard to distinguish between objects and functions. The compiler does make this distinction when the alias is subsituted.

It is also illegal to alias espressions
```D
alias a + b as c; // illegal, 'a + b' is an expression
```

## 5.4. Extern declarations
Variables can be annotated by `extern` meaning that the variable is not part of this module and should be defined in another module and be able to be resolved when linking.

While it is bad practice to use extern without any additional info, the compiler allows this. Extern variables are helpful when the external symbol is defined in a C-style library.

```D
extern extern(C) int bar;
```

## 5.5. Void initializers
By default each value will be assigned a default value, when no explicit value is assigned. A void initializer allows the user to tell the compiler, not to set the value to a default value. Using the value before it is initialized, will result in undefined behavior.

```D
int x = void; // Void will have a 'garbage' value
```

Tips for a void initializer:
- Arrays, when you are certain that an array will be initialized manually or if all of the content isn't used, the void initializer can speed up the code, since there is no need to go through the array and set each value.
- The previous can also be applied to structs, classes and unions.
- Using void initializer for single values is rarely usefull, since the compiler will automatically optimize out dead code.
- For hot paths, it can be helpful to check if a void initializer is useful anywhere to speed up the code.

## 5.6 Bitfields
Since some times, the amount of bytes used for a type is more than required, the use of bitfields are allowed, which allows the efficient packing of variables.

```D
struct packed
{
    bool a : 1; // a takes up 1 bit, normally 8 bits
    bool b : 1; // b takes up 1 bit, normally 8 bits
    bool c : 1; // c takes up 1 bit, normally 8 bits
    bool d : 1; // d takes up 1 bit, normally 8 bits
    int i : 4; // i takes up 4 bits, normally 32 bits
}

assert(sizeof(packed) == 1); // packed uses 1 byte, 
                             // instead of the normal 8 bytes
                             // it would use without bitfields
```

# 6. Types
## 6.1. Basic types
| keyword | Default value |        Description       |
|---------|---------------|--------------------------|
|   void  |       -       | no type                  |
|   bool  |     false     | boolean value (8 bits)   |
|   byte  |       0       | signed 8 bits            |
|  ubyte  |       0u      | unsigned 8 bits          |
|  short  |       0       | signed 16 bits           |
|  ushort |       0u      | unsigned 16 bits         |
|   int   |       0       | signed 32 bits           |
|   uint  |       0u      | unsigned 32 bits         |
|   long  |       0l      | signed 64 bits           |
|  ulong  |      0ul      | unsigned 64 bits         |
|  float  |      0.0f     | floating point (32 bits) |
|  double |      0.0      | floating point (64 bits) |

## 6.2. Derived types
|        type       |         syntax        | Default value |
|-------------------|-----------------------|---------------|
|      pointer      |         type*         |      null     | 
|       array       |         type[]        | default(type) |
| associative array |       type[type]      |       -       |
|      delegate     | delegate type(params) |      null     |

## 6.3. User-definable type
- class
- struct
- union
- enum

## 6.4. Arithmatic convertions
Since it is allowed to mix different types in expression, the compiler needs to convert the variables to the correct types for the expression. The following rules are used for the conversion:
1. If both types are the same, no conversions are done
2. If either oparend is a `double`, both sides get converted to `double`
3. If either oparend is a `float`, both sides get converted to `float`
4. If both types are signed or unsigned, the smaller type gets converted to the larger
5. If the signed type is larger than the unsigned type, the unsigned type gets converted to the signed type
6. Otherwise the signed type gets converted to the unsinged type, so the sign is lost

There is a special case if one of the types is an enum value:
1. If both values are different enum types, this will result in a compilation error
2. If both values are the same enum type, that enum type will be returned
3. If one type is the base type of the other enum type, the enum gets converted to its base type
4. If one type does not match the enum's base type, the most appropriate type will be chosen

## 6.x. Typeof
The `typeof` keyword can be used to retrieve the resulting type of an expression.

```D
void foo(int i)
{
    typeof(i) j;        // j is of type int
    typeof(1)* k;       // k is of type int*
    typeof(1 + 2.0) l;  // l is of type double
    int[typeof(k)] m;   // m is of type int[int*]
}
```

Expression inside the `typeof` operator are not evaluated, only the type gets generated

```D
int i = 0;
typeof (i++) j; // j is of type int, i is still 0
```

There are a number of special cases for the `typeof` operator
- Inside an object, `typeof(this)` will return the type of the object
- Inside an object, `typeof(super)` will return the type of the base class, a compilation error will happen when the object has no base class
- Inside an non-member function, `typeof(this)` will return the type of the function
- Inside an function, `typeof(return)` will return the type of the return type

# 7. Properties
# 8. Attributes
# 9. Pragmas
# 10. Expressions
# 11. Statements
# 12. Arrays

# 13. Classes
# 14. Structures
# 15. Interfaces
# 16. Unions
# 17. Enums

## x.x Slicing
## x.x Contracts

# x. Type system and traits

# x. Inline assembly (x86)

# x. Application Binary Inteface
This section gives info about the ABI used during and after the compilation process. 

## x.x. Name mangling
For MJay to have typesafe linking, the names of symbols are mangled. This is required to ensure compatibility of otherwise conflicting symbols. Some of this mangled info can also be used to extract traits of the specific type.

Every mangled name starts with `_M` notifying that this is a MJay symbol. This is followed up by the `QualifiedName`, which includes all info to differentiate it from other symbols.

### x.x.1. Qualified name
The `QualifiedName` is made up from 2 sections. The first sections or the identifier exist out of the full name, included the template parameters, specializations and constraints, of the object that is being mangled. 

The second section or the type data depends on the whether the name refers to an object or a function. For an object, the section contains information about the objects type and for a function, the section contains the function parameters and its return type.

### x.x.2. Qualified identifier
For parts of the first section that aren't is made up from templates, the mangling is fairly simple. For each dot-separated section of the full name (including package, module, namespaces and parent objects), the section gets transformed by prepending its length to the section, i.e. `name` becomes `4name`. All dots will be ommited from this string, since enough info is given to identify each section.

### x.x.3. Template identifier
When a templated object or function is encounteres, this part gets transformed in a slightly different way. When a templated part of the full name is encountered, the section is started by a symbol identifying that this part references a template: `__T`.

This is then folowed by the name of the template, prepended with the length, as explained in the previous section.

Following this, a list of template arguments is added. The arguments follow a simple ruleset. First, to differentiate specialized and unspecialized arguments, a `H` is added when the argument is specialized. This is then followed up by a single character, identifying the type of arguments, followed by the specific info for that argument:
1. For an argument without a specified type, a `T` is added, followed by the identifier of the base type the argument is expected to have.
2. For an argument with a specified primitive type and a default value, a `V` is added, followed by the identifier of the type and its defualt (mangled) value.
3. For an argument with a specified type, a `S` is added, followed up by the `QualifiedName` of the type.

At the end of the template, `Z` is added, notifying that the template has ended.

### x.x.4. Default mangled value
The way in which the value is mangled, depends on the type of the value (i.e. wheter the value is an int, a float, etc).

- For a null value, `n` is used
- For a positive numeric value, `i` is used, followed up by the value
- For a negative numeric value, `N` is used, followed up by the absolute value
- For a floating point value, `e` is used followed up by the, hexadecimal encoded, value. This value can either be one of 3 special values: `NAN`, `INF`, `NINF`, or is otherwise stored as the values mantissa as hexadecimal, followed by its exponent in decimal.
- For a string, the value is started by a value representing the byte width of a character, followed by the length of the string and an underscore, this is finally followed by the character representations as hexadecimal digits.

### x.x.5. Back-reference
To try and limit the length of the qualified identifier, a special value is added that references a the original occurance of the name, which was already inserted beforehand.

This is identifier by the character `Q`, followed up by a base-26 value, stored as the upper case letters (A-Z), with the final digit stored as a lower case letter (a-z).

### x.x.6. Qualified type data
The type data is represented using a simple scheme. Whenever the `QualifiedName` refers to an object, the objects type get appended to the end of the `QualifiedName`. 

When the `QualifiedName` referse to a function, a more lengthy but simple system is used. 
- Function data starts with the function identifier `F`, immediatally followed by the calling convention.
- When the any function attribute are present, these are appended, each function attribute is notified by a leading `N` followed by the attribute specific identifier.
- For each paramater the function has, the type of the the parameter is added, prepended by any paramater attributes. Variadic varaibles get handled by the `ParamClose` specifier.
- The end of the parameter list is specified by the `ParamClose` specifier, which also handles variadic parameters.
- The final part of the function info is the type the function returns, this is handled like any other type.

# x. Standard library
The standard library is made up from 2 reserved packages: `core` and `std`. Defining these as the packages of other modules will work, but may cause undefined behaviour.

## x.1. Core package
The `core` package contains the runtime libary, this is the minumum code needed for a basic project to be able to run, since this contains core language features, this package will automatically be added to any project, without an explicit import required. This package is developed together with the core language. Any objects or function in this package, aren't directly accessable by the user and need to be interfaced using the respective `std` package


## x.2. Std package
The `std` package contains standalone code and a way of interfacing with the underlying `core` package and thus giving access to certain `core` objects and functions. This libary is developed, together with the `core` package and language.