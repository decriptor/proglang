﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BindingGen
{
	public class DefineProcessor
	{

		internal struct DefinePair
		{
			internal string Identifier;
			internal string Value;
		}

		public void GenerateConstants(string file, CodeGenerator codeGen, List<string> defExlcPrefixes, bool mayContainLowerCase)
		{
			List<string> defines = ExtractDefines(file);

			List<DefinePair> pairs = ExtractPairs(defines);

			bool firstVal = true;

			char[] excludeChars = "abcdefghijklmnopqrstuvwxyz".ToCharArray();

			foreach (DefinePair pair in pairs)
			{
				bool exclude = pair.Identifier.Contains('(');

				if (!exclude)
				{
					foreach (string prefix in defExlcPrefixes)
					{
						if (pair.Identifier.StartsWith(prefix))
						{
							exclude = true;
							break;
						}
					}
				}

				if (!exclude && !mayContainLowerCase && pair.Identifier.IndexOfAny(excludeChars) != -1)
					exclude = true;

				if (!exclude)
				{
					if (firstVal)
					{
						firstVal = false;

						codeGen.GenComment("////////////////////////////////////////");
						codeGen.GenComment("Generated based on defines");
						codeGen.GenComment("");
						codeGen.GenComment("Possibly requires manual modifications");
						codeGen.GenComment("////////////////////////////////////////");
					}

					codeGen.GenCConst(pair.Identifier, pair.Value);
				}
			}
		}

		public List<string> ExtractDefines(string filename)
		{
			List<string> defines = new List<string>();

			using (StreamReader reader = new StreamReader(filename))
			{
				while (!reader.EndOfStream)
				{
					string line = reader.ReadLine();

					if (line.StartsWith("#define"))
					{
						while (line.EndsWith('\\'))
						{
							line = line.Remove(line.Length - 1);
							line += reader.ReadLine().Trim();
						}

						defines.Add(line);
					}
				}
			}


			return defines;
		}

		internal List<DefinePair> ExtractPairs(List<string> defines)
		{
			List<DefinePair> pairs = new List<DefinePair>();

			foreach (string define in defines)
			{
				string tmp = define.Substring("#define ".Length).Trim();

				int space = tmp.IndexOf(' ');

				if (space == -1)
					continue;

				DefinePair pair = new DefinePair
				{
					Identifier = tmp.Substring(0, space),
					Value = tmp.Substring(space + 1).TrimStart()
				};

				pairs.Add(pair);
			}

			return pairs;
		}

	}
}
