﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ClangSharp;
using SealangSharp;

namespace BindingGen
{
	class Program
	{
		static CodeGen _codeGen;
		static int _numNodes = -1;


		private static string _type = "";
		private static string _typedefName = "";
		private static bool _skipNext = false;

		static HashSet<string> _generatedStructs = new HashSet<string>();
		static HashSet<string> _generatedUnions = new HashSet<string>();
		static HashSet<string> _generatedEnums = new HashSet<string>();
		static HashSet<string> _generatedAliases = new HashSet<string>();
		static HashSet<string> _generateFunctions = new HashSet<string>();

		private static StreamWriter _writer;

		private static string _mainHeader = "";
		private static string _outputFile = "output.mj";
		private static string _callingConvention = "C";
		private static List<string> _includeDirs = new List<string>();
		private static string _module = null;

		private static HashSet<string> _keywords = new HashSet<string>
		{
			"alignof",
			"as",
			"asm",
			"assert",
			"break",
			"case",
			"cconst",
			"const",
			"continue",
			"debug",
			"default",
			"defer",
			"delegate",
			"delete",
			"do",
			"else",
			"elif",
			"enum",
			"export",
			"fallthrough",
			"false",
			"for",
			"func",
			"goto",
			"if",
			"interface",
			"immutable",
			"import",
			"internal",
			"is",
			"in",
			"inout",
			"impl",
			"lazy",
			"module",
			"mutable",
			"namespace",
			"new",
			"out",
			"package",
			"private",
			"public",
			"return",
			"self",
			"Self",
			"shared",
			"sizeof",
			"static",
			"struct",
			"switch",
			"synchronized",
			"transmute",
			"typealias",
			"typedef",
			"typeid",
			"typeof",
			"union",
			"unittest",
			"version",
			"while",
			"__global",
		};

		static void Main(string[] args)
		{
			ParseArgs(args);

			CXIndex index = clang.createIndex(0, 0);

			CXUnsavedFile unsavedFile;
			CXTranslationUnit tu = clang.createTranslationUnitFromSourceFile(
				index,
				_mainHeader,
				_includeDirs.Count, _includeDirs.ToArray(),
				0, out unsavedFile);

			FileStream tempFile = new FileStream("temp.c", FileMode.Create);
			_writer = new StreamWriter(tempFile);

			_writer.WriteLine($"#include <{_mainHeader}>");
			_writer.WriteLine("void __MJAY__DEF_GEN_() {");

			CXCursor cursor = clang.getTranslationUnitCursor(tu);
			clang.visitChildren(cursor, VisitCPregen, new CXClientData());

			_writer.WriteLine("}");
			clang.disposeTranslationUnit(tu);

			_writer.Flush();
			_writer.Dispose();
			tempFile.Close();


			// Preprocess and remove line directives

			string processArgs = "-E ";
			processArgs += "temp.c";

			ProcessStartInfo processStartInfo = new ProcessStartInfo();
			processStartInfo.FileName = "clang.exe";
			processStartInfo.Arguments = processArgs;
			processStartInfo.RedirectStandardOutput = true;
			processStartInfo.UseShellExecute = false;

			using (Process process = Process.Start(processStartInfo))
			{
				using (StreamReader reader = process.StandardOutput)
				using (StreamWriter writer = new StreamWriter("prep.c"))
				{
					while (!reader.EndOfStream)
					{
						string line = reader.ReadLine();
						if (!string.IsNullOrWhiteSpace(line) && !line.StartsWith("#"))
							writer.WriteLine(line);
					}
				}
			}

			tu = clang.createTranslationUnitFromSourceFile(
				index,
				"prep.c",
				0, null,
				0, out unsavedFile);

			_codeGen = new CodeGen(_outputFile);
			_codeGen.CallingConvention = _callingConvention;

			if (!string.IsNullOrEmpty(_module))
				_codeGen.GenPackageAndModule(_module);

			cursor = clang.getTranslationUnitCursor(tu);
			clang.visitChildren(cursor, VisitChild, new CXClientData());

			clang.disposeTranslationUnit(tu);
			clang.disposeIndex(index);

			_codeGen.Shutdown();
			//File.Delete("temp.c");
			//File.Delete("prep.c");

			Console.WriteLine($"DONE ({_numNodes + 1} nodes)");
			Thread.Sleep(500);
			//Console.Read();
		}

		static void ParseArgs(string[] args)
		{
			foreach (string s in args)
			{
				if (s.StartsWith("-I"))
				{
					_includeDirs.Add(s);
				}
				else if (s.StartsWith("-o"))
				{
					_outputFile = s.Substring(2);
					if (!_outputFile.EndsWith(".mj"))
						_outputFile += ".mj";
				}
				else if (s.StartsWith("-cc"))
				{
					_callingConvention = s.Substring(3);
				}
				else if (s.StartsWith("-m"))
				{
					_module = s.Substring(2);
				}
				else
				{
					_mainHeader = s;
				}
			}
		}

		static CXChildVisitResult VisitCPregen(CXCursor cursor, CXCursor parent, IntPtr clientData)
		{
			CXCursorKind kind = clang.getCursorKind(cursor);

			if (kind == CXCursorKind.CXCursor_MacroDefinition)
			{
				string name = cursor.ToString();

				if (!name.StartsWith("_"))
				{
					_writer.WriteLine($"auto __mjay__{name} = {name};");
				}
			}

			return CXChildVisitResult.CXChildVisit_Continue;
		}

		static CXChildVisitResult VisitChild(CXCursor cursor, CXCursor parent, IntPtr clientData)
		{
			CXCursorKind kind = clang.getCursorKind(cursor);
			++_numNodes;

			if (_skipNext)
			{
				_skipNext = false;
				return CXChildVisitResult.CXChildVisit_Continue;
			}

			switch (kind)
			{
			case CXCursorKind.CXCursor_FunctionDecl:					GenFunction(cursor); break;
			case CXCursorKind.CXCursor_TypedefDecl:						GenTypedef(cursor, parent); break;
			case CXCursorKind.CXCursor_EnumDecl:
				if (cursor.ToString() != "")
					GenerateEnum(cursor, cursor.ToString());
				break;
			case CXCursorKind.CXCursor_StructDecl:
			case CXCursorKind.CXCursor_UnionDecl:
				if (cursor.ToString() != "")
					GenerateAggregate(cursor, cursor.ToString()); 
				break;
			}
			
			return CXChildVisitResult.CXChildVisit_Continue;
		}

		static void GenTypedef(CXCursor cursor, CXCursor parent)
		{
			string name = clang.getCursorDisplayName(cursor).ToString();

			CXType cxtype = clang.getTypedefDeclUnderlyingType(cursor);
			CXType pointee = clang.getPointeeType(cxtype);

			_typedefName = name;
			_type = "";

			if (name == "SET_POWER_SETTING_VALUE")
			{
				int br = 0;
			}

			if ((cxtype.kind == CXTypeKind.CXType_Pointer &&
			    pointee.kind == CXTypeKind.CXType_Unexposed) ||
			    cxtype.kind == CXTypeKind.CXType_FunctionProto ||
			    cxtype.kind == CXTypeKind.CXType_Unexposed)
			{
				ProcessDelegate(cursor, parent);
			}
			else
			{
				clang.visitChildren(cursor, VisitTypeDecl, new CXClientData());

				if (_type == "" || _type.Contains("(anonymous"))
					ExtractType(cursor);


				if (_type != name &&
					!_generatedStructs.Contains(name) &&
				    !_generatedUnions.Contains(name) &&
				    !_generatedEnums.Contains(name) && 
				    !_generatedAliases.Contains(name) &&
				    !_type.StartsWith("__builtin"))
				{
					_codeGen.GenTypeAlias(_type, name);
					_generatedAliases.Add(name);
				}
			}
		}

		static void GenFunction(CXCursor cursor)
		{
			string delName = cursor.ToString();

			if (_generateFunctions.Contains(delName))
				return;

			if (delName == "__MJAY__DEF_GEN_")
			{
				GenMacroDefinition(cursor);
				return;
			}

			List<string> paramTypes = new List<string>();
			List<string> paramNames = new List<string>();

			CXType curType = clang.getCursorType(cursor);
			CXType cxType = clang.getResultType(curType);

			bool variadic = clang.isFunctionTypeVariadic(cxType) != 0;

			string retType = "";
			if (cxType.kind != CXTypeKind.CXType_Void)
			{
				ExtractType(cxType);
				retType = _type;
			}
			
			int numArgs = clang.Cursor_getNumArguments(cursor);

			// check if function is implemented in header, if so, we don't create the function
			if (HasBody(cursor))
				return;

			for (int i = 0; i < numArgs; i++)
			{
				CXCursor c = clang.Cursor_getArgument(cursor, (uint)i);

				CXType subType = clang.getCursorType(c);
				string name = c.ToString();

				CXCursorKind ckind = clang.getCursorKind(c);

				if (name == "")
					name = "_";
				else if (_keywords.Contains(name))
					name += '_';

				ExtractType(subType, c);

				if (_type != "" && name != _type)
				{
					paramTypes.Add(_type);
					paramNames.Add(name);
				}
			}

			if (retType != null && !paramTypes.Contains(null))
			{
				_codeGen.GenFunc(delName, paramNames, paramTypes, retType, variadic);
				_generateFunctions.Add(delName);
			}
		}

		static void GenMacroDefinition(CXCursor cursor)
		{
			CXTranslationUnit tu = clang.Cursor_getTranslationUnit(cursor);
			CXCursorVisitor memberFunc =
				(c, p, d) =>
				{
					string name = c.ToString();

					if (name == "")
						return CXChildVisitResult.CXChildVisit_Recurse;


					if (name.StartsWith("__mjay__"))
					{
						name = name.Substring("__mjay__".Length);

						string val = ExtractValue(c);
						if (val != null)
							_codeGen.GenCConst(name, val);
					}

					return CXChildVisitResult.CXChildVisit_Recurse;
				};
			clang.visitChildren(cursor, memberFunc, new CXClientData());
		}

		delegate void TokenVisitor(CXToken token, int index);
		static void VisitTokens(CXCursor cursor, TokenVisitor visitor)
		{
			CXSourceRange range = clang.getCursorExtent(cursor);
			CXTranslationUnit tu = clang.Cursor_getTranslationUnit(cursor);
			IntPtr tokenPtr;
			uint numTokens;
			clang.tokenize(tu, range, out tokenPtr, out numTokens);

			CXToken fistTok;
			unsafe
			{
				fistTok = ((CXToken*)tokenPtr.ToPointer())[0];
			}

			List<CXToken> tokens = new List<CXToken>();
			for (int i = 0; i < numTokens - 1; i++)
			{
				CXToken token;
				unsafe
				{
					token = ((CXToken*)tokenPtr.ToPointer())[i];
				}
				visitor(token, i);
			}

			//clang.disposeTokens(tu, out fistTok, numTokens);
		}

		static string ExtractValue(CXCursor cursor)
		{
			if (cursor.kind == CXCursorKind.CXCursor_IntegerLiteral ||
			    cursor.kind == CXCursorKind.CXCursor_StringLiteral)
			{
				return ExtractCursorData(cursor);
			}
			else
			{
				string retStr = null;
				CXCursorVisitor memberFunc =
					(c, p, d) =>
					{
						retStr = ExtractCursorData(c);

						return CXChildVisitResult.CXChildVisit_Continue;
					};
				clang.visitChildren(cursor, memberFunc, new CXClientData());
				return retStr;
			}
		}

		static string ExtractCursorData(CXCursor cursor)
		{
			CXTranslationUnit tu = clang.Cursor_getTranslationUnit(cursor);
			switch (cursor.kind)
			{
			case CXCursorKind.CXCursor_IntegerLiteral:
			{
				string val = "";
				VisitTokens(cursor, (t, i) => { if (i == 0) val = clang.getTokenSpelling(tu, t).ToString(); });

				// Special cases
				val = val.Replace("UI64", "UL");
				val = val.Replace("ULL", "UL");
				val = val.Replace("ui64", "ul");
				val = val.Replace("ull", "ul");
				
				return val;
			}
			case CXCursorKind.CXCursor_FloatingLiteral:
			{
				string val = "";
				VisitTokens(cursor, (t, i) => { if (i == 0) val = clang.getTokenSpelling(tu, t).ToString(); });
				return val;
			}
			case CXCursorKind.CXCursor_StringLiteral:
			{
				string val = "";
				VisitTokens(cursor, (t, i) => { if (i == 0) val = clang.getTokenSpelling(tu, t).ToString(); });

				if (val.StartsWith("L"))
					val = val.Substring(1);
				
				return val;
			}
			case CXCursorKind.CXCursor_ParenExpr:
			{
				string inner = ExtractValue(cursor);
				if (inner == null)
					return null;
				return '(' + inner + ')';
			}
			case CXCursorKind.CXCursor_CStyleCastExpr:
			{
				string inner = ExtractValue(cursor);
				if (inner == null)
					return null;
				ExtractType(cursor);

				return inner + " __c_cast " + _type;
			}
			case CXCursorKind.CXCursor_BinaryOperator:
			{
				CXCursor? left = null;
				CXCursor? right = null;
				int i = 0;
				clang.visitChildren(cursor, (sc, sp, sd) =>
				{
					if (i == 0)
						left = sc;
					else
						right = sc;

					++i;
					return CXChildVisitResult.CXChildVisit_Continue;
				}, new CXClientData());

				string lval = left != null ? ExtractCursorData(left.Value) : "";
				string rval = right != null ? ExtractCursorData(right.Value) : "";

				if (lval == null || rval == null)
					return null;

				BinaryOperatorKind binOp = sealang.cursor_getBinaryOpcode(cursor);
				switch (binOp)
				{
				case BinaryOperatorKind.Mul:  return '(' + lval + " * "  + rval + ')';
				case BinaryOperatorKind.Div:  return '(' + lval + " / "  + rval + ')';
				case BinaryOperatorKind.Rem:  return '(' + lval + " % "  + rval + ')';
				case BinaryOperatorKind.Add:  return '(' + lval + " + "  + rval + ')';
				case BinaryOperatorKind.Sub:  return '(' + lval + " - "  + rval + ')';
				case BinaryOperatorKind.Shl:  return '(' + lval + " << " + rval + ')';
				case BinaryOperatorKind.Shr:  return '(' + lval + " >> " + rval + ')';
				case BinaryOperatorKind.LT:   return '(' + lval + " < "  + rval + ')';
				case BinaryOperatorKind.GT:   return '(' + lval + " > "  + rval + ')';
				case BinaryOperatorKind.LE:   return '(' + lval + " <= " + rval + ')';
				case BinaryOperatorKind.GE:   return '(' + lval + " >= " + rval + ')';
				case BinaryOperatorKind.EQ:   return '(' + lval + " == " + rval + ')';
				case BinaryOperatorKind.NE:   return '(' + lval + " != " + rval + ')';
				case BinaryOperatorKind.And:  return '(' + lval + " & "  + rval + ')';
				case BinaryOperatorKind.Xor:  return '(' + lval + " ^ "  + rval + ')';
				case BinaryOperatorKind.Or:   return '(' + lval + " | "  + rval + ')';
				case BinaryOperatorKind.LAnd: return '(' + lval + " && " + rval + ')';
				case BinaryOperatorKind.LOr:  return '(' + lval + " || " + rval + ')';
				default:
					return "";
				}
			}
			case CXCursorKind.CXCursor_UnaryOperator:
			{
				CXCursor? inner = null;
				clang.visitChildren(cursor, (sc, sp, sd) =>
				{
					inner = sc;
					return CXChildVisitResult.CXChildVisit_Break;
				}, new CXClientData());
				
				string innerVal = inner != null ? ExtractCursorData(inner.Value) : "";

				if (innerVal == null)
					return null;

				UnaryOperatorKind unOp = sealang.cursor_getUnaryOpcode(cursor);
				switch (unOp)
				{
				case UnaryOperatorKind.PostInc: return "("   + innerVal + "++)";
				case UnaryOperatorKind.PostDec: return "("   + innerVal + "--)";
				case UnaryOperatorKind.PreInc:  return "(++" + innerVal +   ')';
				case UnaryOperatorKind.PreDec:  return "(--" + innerVal +   ')';
				case UnaryOperatorKind.AddrOf:  return "(&"  + innerVal +   ')';
				case UnaryOperatorKind.Deref:   return "(*"  + innerVal +   ')';
				case UnaryOperatorKind.Plus:    return "(+"  + innerVal +   ')';
				case UnaryOperatorKind.Minus:   return "(-"  + innerVal +   ')';
				case UnaryOperatorKind.Not:     return "(~"  + innerVal +   ')';
				case UnaryOperatorKind.LNot:    return "(!"  + innerVal +   ')';
				default:
					return null;
				}
			}
			case CXCursorKind.CXCursor_UnexposedExpr:
			{
				return ExtractValue(cursor);
			}
			case CXCursorKind.CXCursor_UnaryExpr:
			{
				string tokStr = "";
				VisitTokens(cursor, (t, i) => { tokStr += clang.getTokenSpelling(tu, t).ToString(); });

				if (tokStr.StartsWith("sizeof"))
				{
					CXCursor child = GetChild(cursor);

					if (child.kind == CXCursorKind.CXCursor_TypeRef)
					{
						return "sizeof(#{" + child.ToString() + "})";
					}
					else
					{
						string inner = ExtractValue(child);
						return "sizeof(" + inner + ")";
					}
				}

				return ExtractValue(cursor);
			}
			default:
			{
				return null;
			}
			}
		}

		static CXChildVisitResult VisitTypeDecl(CXCursor cursor, CXCursor parent, IntPtr clientData)
		{
			CXCursorKind kind = clang.getCursorKind(cursor);
			string name = cursor.ToString();
			//Console.WriteLine($"\t{kind} | {name}");
			
			CXType cxtype = clang.getTypedefDeclUnderlyingType(parent);

			if ((int) cxtype.kind == 119)
			{
				if (kind == CXCursorKind.CXCursor_StructDecl ||
				    kind == CXCursorKind.CXCursor_UnionDecl)
				{
					GenerateAggregate(cursor, name);
				}
				else if (kind == CXCursorKind.CXCursor_EnumDecl)
				{
					GenerateEnum(cursor, name);
				}
				else
				{
					ExtractType(cursor);
				}
			}
			else if (cxtype.kind == CXTypeKind.CXType_Pointer)
			{
				CXType pointee = clang.getPointeeType(cxtype);

				if (pointee.kind == CXTypeKind.CXType_Unexposed)
				{
					ProcessDelegate(cursor, parent);
				}
				else
				{
					ExtractType(cxtype, parent);
				}
			}
			else
			{
				ExtractType(cxtype);
			}

			return CXChildVisitResult.CXChildVisit_Continue;
		}

		static bool GenerateAggregate(CXCursor cursor, string name, string typePrefix = null, bool addAlignment = true, bool isField = false)
		{
			CXCursorKind kind = cursor.kind;
			if (kind == CXCursorKind.CXCursor_StructDecl)
			{
				if ((isField || !_generatedStructs.Contains(name)) && (name == "" || !IsDefinedLater(name, _numNodes, cursor)))
				{
					CXType type = clang.getCursorType(cursor);
					long align = -1;
					if (addAlignment)
						align = clang.Type_getAlignOf(type);

					if (name == "" && !isField)
						name = _typedefName;

					_codeGen.BeginStruct(name, isField, typePrefix, align);

					ProcessFields(cursor);

					_codeGen.EndAggregate(isField);

					if (name != "")
						_generatedStructs.Add(name);
					_type = name;
					return true;
				}
			}
			else if (kind == CXCursorKind.CXCursor_UnionDecl)
			{
				if ((isField || !_generatedUnions.Contains(name)) && (name == "" || !IsDefinedLater(name, _numNodes, cursor)))
				{
					CXType type = clang.getCursorType(cursor);
					long align = -1;
					if (addAlignment)
						align = clang.Type_getAlignOf(type);

					if (name == "" && !isField)
						name = _typedefName;

					_codeGen.BeginUnion(name, isField, typePrefix, align);

					ProcessFields(cursor);

					_codeGen.EndAggregate(isField);

					if (name != "")
						_generatedUnions.Add(name);
					_type = name;
					return true;
				}
			}
			else if (kind == CXCursorKind.CXCursor_TypeRef)
			{
				_codeGen.BeginStruct(name, false);
				_codeGen.EndAggregate(false);
			}

			return false;
		}

		static void GenerateEnum(CXCursor cursor, string name)
		{
			if (!_generatedEnums.Contains(name))
			{
				if (name == "")
					name = _typedefName;

				_codeGen.BeginEnum(name);

				ProcessEnumMembers(cursor);

				_codeGen.EndEnum();
				_generatedEnums.Add(name);
			}
		}

		static void ProcessFields(CXCursor cursor)
		{
			int fieldIdx = 0;
			bool skipNext = false;
			CXCursorVisitor visitFunc =
				(c, p, data) =>
				{
					if (skipNext)
					{
						skipNext = false;
						++fieldIdx;
						return CXChildVisitResult.CXChildVisit_Continue;
					}

					CXCursorKind kind = clang.getCursorKind(c);

					if (kind == CXCursorKind.CXCursor_FieldDecl)
					{
						string name = clang.getCursorDisplayName(c).ToString();
						if (name == "")
							name = "_";
						else if (_keywords.Contains(name))
							name += '_';

						_type = "";
						ExtractType(c);
						if (_type == "")
							clang.visitChildren(c, VisitType, new CXClientData());

						if (!string.IsNullOrEmpty(_type))
							_codeGen.GenField(name, _type);
					}
					else if (kind == CXCursorKind.CXCursor_StructDecl)
					{
						string name = clang.getCursorDisplayName(c).ToString();

						if (name == "")
						{
							(bool nAnon, CXCursor nCur) = IsNextFieldInstanceOfAnon(cursor, fieldIdx);

							if (nAnon)
							{
								ExtractType(nCur);
								int anonIdx = _type.IndexOf("(anonymous ", StringComparison.Ordinal);
								if (anonIdx != -1)
									_type = _type.Substring(0, anonIdx);
								name = clang.getCursorDisplayName(nCur).ToString();

								GenerateAggregate(c, name, _type, isField:true);
								skipNext = true;
							}
							else
							{
								GenerateAggregate(c, "", isField:true);
							}
						}
						else
						{
							if (!_generatedStructs.Contains(name))
							{
								GenerateAggregate(c, name, addAlignment: false);
								_type = name;
							}
							else
							{
								ExtractType(cursor);

								if (!string.IsNullOrEmpty(_type))
									_codeGen.GenField(name, _type);
							}
						}
					}
					else if (kind == CXCursorKind.CXCursor_UnionDecl)
					{
						string name = clang.getCursorDisplayName(c).ToString();

						if (name == "")
						{
							(bool nAnon, CXCursor nCur) = IsNextFieldInstanceOfAnon(cursor, fieldIdx);

							if (nAnon)
							{
								ExtractType(nCur);
								int anonIdx = _type.IndexOf("(anonymous ", StringComparison.Ordinal);
								if (anonIdx != -1)
									_type = _type.Substring(0, anonIdx);
								name = clang.getCursorDisplayName(nCur).ToString();

								GenerateAggregate(c, name, _type, isField: true);
								skipNext = true;
							}
							else
							{
								GenerateAggregate(c, "", isField: true);
							}
						}
						else
						{
							if (!_generatedUnions.Contains(name))
							{
								GenerateAggregate(c, name, addAlignment: false);
								_type = name;
							}
							else
							{
								ExtractType(cursor);

								if (!string.IsNullOrEmpty(_type))
									_codeGen.GenField(name, _type);
							}
						}
					}

					_type = "";
					++fieldIdx;
					return CXChildVisitResult.CXChildVisit_Continue;
				};
			clang.visitChildren(cursor, visitFunc, new CXClientData());
		}

		static void ProcessEnumMembers(CXCursor cursor)
		{
			CXCursorVisitor memberFunc =
				(c, p, d) =>
				{
					CXCursorKind kind = clang.getCursorKind(c);

					if (kind == CXCursorKind.CXCursor_EnumConstantDecl)
					{
						string name = c.ToString();
						long value = clang.getEnumConstantDeclValue(c);
						_codeGen.GenEnumValue(name, $"{value}");
					}

					return CXChildVisitResult.CXChildVisit_Continue;
				};
			clang.visitChildren(cursor, memberFunc, new CXClientData());
		}

		static void ProcessDelegate(CXCursor cursor, CXCursor parent)
		{
			CXType type = clang.getTypedefDeclUnderlyingType(cursor);
			string delName = cursor.ToString();

			if (type.kind == CXTypeKind.CXType_FunctionProto ||
			    type.kind == CXTypeKind.CXType_Unexposed)
			{
				_skipNext = true;
				CXCursor next = GetNextCursor(parent, _numNodes);

				CXType ntype = clang.getTypedefDeclUnderlyingType(next);
				CXType pointee = clang.getPointeeType(ntype);

				if (ntype.kind == CXTypeKind.CXType_Pointer &&
				    (pointee.kind == CXTypeKind.CXType_Unexposed ||
				     pointee.kind == CXTypeKind.CXType_FunctionProto) &&
				    pointee.ToString().Contains(delName))
					delName = next.ToString();
				else
					_skipNext = false;
			}

			List<string> paramTypes = new List<string>();
			List<string> paramNames = new List<string>();
			string retType = "";

			CXCursorVisitor memberFunc =
				(c, p, d) =>
				{
					CXType subType = clang.getCursorType(c);
					string name = c.ToString();

					if (name == "")
						name = "_";

					ExtractType(subType);

					if (retType == "")
					{
						retType = _type;
					}
					else
					{
						paramTypes.Add(_type);
						paramNames.Add(name);
					}

					return CXChildVisitResult.CXChildVisit_Continue;
				};
			clang.visitChildren(cursor, memberFunc, new CXClientData());

			if (retType == null || paramTypes.Contains(null))
				_type = null;
			else
				_codeGen.GenDelegate(delName, paramNames, paramTypes, retType, false);
		}

		static CXChildVisitResult VisitType(CXCursor cursor, CXCursor parent, IntPtr data)
		{
			ExtractType(cursor);

			/*string name = clang.getCursorDisplayName(cursor).ToString();
			CXCursorKind kind = clang.getCursorKind(cursor);
			CXType cxType = clang.getCursorType(cursor);
			Console.WriteLine($"{kind} | {name} | {cxType.kind}");*/

			return CXChildVisitResult.CXChildVisit_Recurse;
		}

		static void ExtractType(CXCursor cursor)
		{
			CXCursorKind cursorKind = clang.getCursorKind(cursor);
			CXType cxType = clang.getCursorType(cursor);

			if (cursorKind == CXCursorKind.CXCursor_TypedefDecl ||
			    cursorKind == CXCursorKind.CXCursor_TypeRef)
			{
				CXType underlying = clang.getTypedefDeclUnderlyingType(cursor);
				if (underlying.kind != CXTypeKind.CXType_Invalid)
					ExtractType(underlying, cursor);
				else
				{
					_type = cursor.ToString();
					RemoveRedundantTypeData(underlying);

					if (_type == "" || 
					    (!IsDefinedLater(_type, _numNodes, cursor) &&
					     !_generatedStructs.Contains(_type) &&
					     !_generatedUnions.Contains(_type) &&
					     !_generatedEnums.Contains(_type) &&
					     !_generatedAliases.Contains(_type)))
					{
						GenerateAggregate(cursor, _type);
					}
				}
				return;
			}

			if (cxType.kind == CXTypeKind.CXType_Record)
			{
				_type = clang.getCursorDisplayName(cursor).ToString();
				RemoveRedundantTypeData(cxType);
			}
			else
			{
				ExtractType(cxType, cursor);
			}
		}

		static void ExtractType(CXType type, CXCursor cursor = new CXCursor())
		{
			if (type.kind == CXTypeKind.CXType_Typedef)
			{
				_type = type.ToString();
			}
			else if (type.kind == CXTypeKind.CXType_Pointer)
			{
				CXType underlying = clang.getPointeeType(type);
				CXCursor child = GetChild(cursor);
				ExtractType(underlying, child);

				if (_type == "")
				{
					_type = type.ToString();
					_type = _type.Substring(0, _type.Length - 1);
					_type.Trim();
					RemoveRedundantTypeData(type);
				}

				if (_type != null &&
					underlying.kind != CXTypeKind.CXType_Unexposed &&
				    underlying.kind != CXTypeKind.CXType_FunctionProto)
					_type = '*' + _type;
			}
			else if (type.kind == CXTypeKind.CXType_Invalid)
			{
				_type = "";
			}
			else if (type.kind == CXTypeKind.CXType_ConstantArray)
			{
				CXType elemType = clang.getArrayElementType(type);
				long arrSize = clang.getArraySize(type);

				ExtractType(elemType);

				if (_type != null)
					_type = $"[{arrSize}]{_type}";
			}
			else if (type.kind == CXTypeKind.CXType_IncompleteArray)
			{
				CXType elemType = clang.getArrayElementType(type);

				ExtractType(elemType);

				if (_type != null)
					_type = $"@:arr_ptr []{_type}";
			}
			else if (type.kind == CXTypeKind.CXType_Enum)
			{
				_type = type.ToString();
			}
			else if (type.kind == CXTypeKind.CXType_Unexposed)
			{
				_type = ConvertUnexposedType(type, cursor);
			}
			else if (type.kind == CXTypeKind.CXType_FunctionProto)
			{
				_type = cursor.ToString();

				CXCursorVisitor memberFunc =
					(c, p, d) =>
					{
						_type = c.ToString();

						return CXChildVisitResult.CXChildVisit_Break;
					};
				clang.visitChildren(cursor, memberFunc, new CXClientData());
			}
			else if (type.kind == CXTypeKind.CXType_Record)
			{
				_type = type.ToString();
			}
			else if (type.kind <= CXTypeKind.CXType_Auto)
			{
				_type = ConvertBaseType(type.kind);

				if (_type == null)
					return;

				if (_type == "")
					Console.WriteLine("not a builtin type !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			}
			else if (type.ToString().Contains("(anonymous"))
			{
				string name = "_anon_" + _typedefName + '_';
				bool ret = GenerateAggregate(cursor, name);

				if (ret)
					_type = name;
				else
					_type = type.ToString();
			}
			else
			{
				_type = type.ToString();
			}

			uint isBitField = clang.Cursor_isBitField(cursor);
			if (isBitField != 0)
			{
				int bits = clang.getFieldDeclBitWidth(cursor);
				_type += $".{bits}";
			}

			RemoveRedundantTypeData(type);
		}

		static void RemoveRedundantTypeData(CXType type)
		{
			// Temporarily trim const
			if (_type.StartsWith("const "))
				_type = _type.Substring(6);

			// vc++ specific
			if (_type.StartsWith("__unaligned "))
				_type = _type.Substring(12);

			// Cleanup unwanted stuff
			if (_type.StartsWith("volatile "))
				_type = _type.Substring(9);
			if (_type.StartsWith("struct "))
				_type = _type.Substring(7);
			if (_type.StartsWith("union "))
				_type = _type.Substring(6);
			if (_type.StartsWith("enum "))
				_type = _type.Substring(5);

			if (clang.isConstQualifiedType(type) != 0)
				_type = "const " + _type;
		}

		static string ConvertUnexposedType(CXType type, CXCursor cursor)
		{
			List<string> paramTypes = new List<string>();
			List<string> paramNames = new List<string>();

			CXType cxRet = clang.getResultType(type);
			ExtractType(cxRet);
			string retType = _type;

			int numArgs = clang.getNumArgTypes(type);
			for (int i = 0; i < numArgs; i++)
			{
				CXType argType = clang.getArgType(type, (uint)i);
				ExtractType(argType);

				paramTypes.Add(_type);
				paramNames.Add("_");
			}

			return _codeGen.GetDelegateType(paramNames, paramTypes, retType, false);
		}

		static string ConvertBaseType(CXTypeKind kind)
		{
			switch (kind)
			{
			case CXTypeKind.CXType_Void:		return "void";
			case CXTypeKind.CXType_Bool:		return "bool";
			case CXTypeKind.CXType_Char_U:		return "u8";
			case CXTypeKind.CXType_UChar:		return "u8";
			case CXTypeKind.CXType_Char16:		return "wchar";
			case CXTypeKind.CXType_Char32:		return "rune";
			case CXTypeKind.CXType_UShort:		return "u16";
			case CXTypeKind.CXType_UInt:		return "u32";
			case CXTypeKind.CXType_ULong:		return "u32";
			case CXTypeKind.CXType_ULongLong:	return "u64";
			case CXTypeKind.CXType_UInt128:		return null; //"u128";
			case CXTypeKind.CXType_Char_S:		return "char";
			case CXTypeKind.CXType_SChar:		return "char";
			case CXTypeKind.CXType_WChar:		return "wchar";
			case CXTypeKind.CXType_Short:		return "i16";
			case CXTypeKind.CXType_Int:			return "i32";
			case CXTypeKind.CXType_Long:		return "i32";
			case CXTypeKind.CXType_LongLong:	return "i64";
			case CXTypeKind.CXType_Int128:		return null; //"i128";
			case CXTypeKind.CXType_Float:		return "f32";
			case CXTypeKind.CXType_Double:		return "f64";
			case CXTypeKind.CXType_LongDouble:	return null; //"f128";
			default: return "";
			}
		}

		static (bool, CXCursor) IsNextFieldInstanceOfAnon(CXCursor cursor, int structIdx)
		{
			int prevIndex = -1;
			bool found = false;
			CXCursor field = cursor;
			CXCursorVisitor searchFunc =
				(c, p, d) =>
				{
					if (prevIndex == structIdx)
					{
						ExtractType(c);

						if (_type.Contains("(anonymous "))
						{
							found = true;
							field = c;
						}

						_type = "";
						return CXChildVisitResult.CXChildVisit_Break;
					}

					++prevIndex;
					return CXChildVisitResult.CXChildVisit_Continue;
				};
			clang.visitChildren(cursor, searchFunc, new CXClientData());
			return (found, field);
		}

		static CXCursor GetNextCursor(CXCursor cursor, int index)
		{
			int prevIndex = -1;
			CXCursor field = cursor;
			CXCursorVisitor searchFunc =
				(c, p, d) =>
				{
					if (prevIndex == index)
					{
						field = c;
						return CXChildVisitResult.CXChildVisit_Break;
					}

					++prevIndex;
					return CXChildVisitResult.CXChildVisit_Continue;
				};
			clang.visitChildren(cursor, searchFunc, new CXClientData());
			return field;
		}

		static CXCursor GetChild(CXCursor cursor)
		{
			CXCursor child = cursor;
			CXCursorVisitor searchFunc =
				(c, p, d) =>
				{
					child = c;
					return CXChildVisitResult.CXChildVisit_Break;
				};
			clang.visitChildren(cursor, searchFunc, new CXClientData());
			return child;
		}

		static bool HasBody(CXCursor cursor)
		{
			bool hasBody = false;
			CXCursorVisitor searchFunc =
				(c, p, d) =>
				{
					if (c.kind == CXCursorKind.CXCursor_CompoundStmt)
						hasBody = true;
					return CXChildVisitResult.CXChildVisit_Continue;
				};
			clang.visitChildren(cursor, searchFunc, new CXClientData());
			return hasBody;
		}

		static bool IsDefinedLater(string name, int idx, CXCursor cursor)
		{
			CXTranslationUnit tu = clang.Cursor_getTranslationUnit(cursor);
			CXCursor baseCursor = clang.getTranslationUnitCursor(tu);
			bool found = false;
			int curIdx = 0;
			CXCursorVisitor memberFunc =
				(c, p, d) =>
				{
					if (curIdx > idx && c.ToString() == name)
					{
						found = true;
						return CXChildVisitResult.CXChildVisit_Break;
					}

					++curIdx;
					return CXChildVisitResult.CXChildVisit_Continue;
				};
			clang.visitChildren(baseCursor, memberFunc, new CXClientData());
			return found;
		}

	}
}
